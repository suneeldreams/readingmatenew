package com.dreams.readingmate

import android.app.Application
import com.dreams.readingmate.data.network.MyApi
import com.dreams.readingmate.data.preferences.PreferenceProvider
import com.dreams.readingmate.data.repositories.*
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.home.books.BooksViewModel
import com.dreams.readingmate.ui.home.dashboard.HomeViewModel
import com.dreams.readingmate.ui.home.progress.ProgressViewModel
import com.dreams.readingmate.ui.home.addchild.AddChildViewModel
import com.dreams.readingmate.ui.home.favouritecharater.FavouriteCharacterViewModel
import com.dreams.readingmate.ui.home.favouritegenres.FavouriteGenresViewModel
import com.dreams.readingmate.ui.home.favouritesbooks.FavouriteBooksViewModel
import com.dreams.readingmate.ui.home.favouritesubject.FavouriteSubjectViewModel
import com.dreams.readingmate.ui.home.news.NewsViewModel
import com.dreams.readingmate.ui.home.profile.ProfileViewModel
import com.dreams.readingmate.ui.home.shoping.ShopingViewModel
import com.dreams.readingmate.ui.home.subscription.SubscriptionViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ApplicationClass : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@ApplicationClass))
        bind() from singleton { MyApi() }
        bind() from singleton { PreferenceProvider(instance()) }

        // Initialize Repositories Here
        bind() from singleton { UserRepository(instance()) }
        bind() from singleton { DashboardRepository(instance()) }


        // Initialize ViewModel Here
        bind() from provider { AuthViewModel(instance()) }
        bind() from provider { HomeViewModel(instance()) }
        bind() from provider { ProgressViewModel(instance()) }
        bind() from provider { BooksViewModel(instance()) }
        bind() from provider { ShopingViewModel(instance()) }
        bind() from provider { AddChildViewModel(instance()) }
        bind() from provider { ProfileViewModel(instance()) }
        bind() from provider { SubscriptionViewModel(instance()) }
        bind() from provider { NewsViewModel(instance()) }
        bind() from provider { FavouriteBooksViewModel(instance()) }
        bind() from provider { FavouriteGenresViewModel(instance()) }
        bind() from provider { FavouriteSubjectViewModel(instance()) }
        bind() from provider { FavouriteCharacterViewModel(instance()) }

    }

}