package com.dreams.readingmate.util

import android.content.Context
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.dreams.readingmate.R


/**
 * Created by Shivank on 4/6/2018.
 */
object AlertUtils {

    fun showToast(context: Context, message: Any) {
        when (message) {
            is String -> {
                val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                toastView.setBackgroundResource(R.drawable.toast_message_style)
                toastView.setPadding(15, 10, 15, 10)
                val v: TextView = toastView.findViewById(android.R.id.message)
                v.setTextColor(ContextCompat.getColor(context, R.color.white))
                toast.setGravity(Gravity.TOP, 0, 0)
                toast.show()
            }
//            is String -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
//            is Int -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}