package com.dreams.readingmate.util

import android.graphics.*
import com.squareup.picasso.Transformation

/**
 * Created by Shivank on 6/5/2018.
 */
class CircleTransform : Transformation {

    override fun transform(source: Bitmap): Bitmap? {
        try {
            val size = Math.min(source.width, source.height)

            val x = (source.width - size) / 2
            val y = (source.height - size) / 2

            val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
            if (squaredBitmap != source) {
                source.recycle()
            }

            val config = if (source.config != null) source.config else Bitmap.Config.ARGB_8888
            val bitmap = Bitmap.createBitmap(size, size, config)

            val canvas = Canvas(bitmap)
            val paint = Paint()
            val shader = BitmapShader(squaredBitmap,
                   Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            paint.shader = shader
            paint.isAntiAlias = true

            val r = size / 2f
            canvas.drawCircle(r, r, r, paint)

            squaredBitmap.recycle()
            return bitmap
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    override fun key(): String {
        return "circle"
    }
}