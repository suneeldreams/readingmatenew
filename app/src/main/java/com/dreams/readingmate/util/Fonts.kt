package com.dreams.readingmate.util

import android.content.Context
import android.graphics.Typeface
import java.util.*

/**
 * Created by Shivank on 6/5/2018.
 */
object Fonts {
    // use Weak so fonts are freed from memory when you stop using them
    private val fonts = WeakHashMap<String, Typeface>(5)

    /***
     * Returns a font at the given path within the assets directory.
     *
     * Caches fonts to save resources.
     *
     * @param context
     * @param fontPath Path to a font file relative to the assets directory, e.g. "fonts/Arial.ttf"
     * @return
     */
    @Synchronized
    fun getFont(context: Context, fontPath: String): Typeface? {
        var font: Typeface? = fonts[fontPath]

        if (font == null) {
            font = Typeface.createFromAsset(context.assets, fontPath)
            fonts.put(fontPath, font)
        }

        return font
    }
}