package com.dreams.readingmate.util

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.dreams.readingmate.R

/**
 * Created by Shivank on 6/5/2018.
 */
class TextViewWithFont : AppCompatTextView {
    private val defaultDimension = 0
    private val TYPE_BOLD = 1
    private val TYPE_ITALIC = 2
    private var fontType: Int = 0

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(attrs, R.styleable.TextViewWithFont, defStyle, 0)
        val fontPath = a.getString(R.styleable.TextViewWithFont_fontFilePath)
//        fontType = a.getInt(R.styleable.TextViewWithFont, defaultDimension)
        a.recycle()

        if (fontPath != null) {
            setFontType(Fonts.getFont(context, fontPath))
        }
    }

    private fun setFontType(font: Typeface?) {
        if (fontType == TYPE_BOLD) {
            setTypeface(font, Typeface.BOLD)
        } else if (fontType == TYPE_ITALIC) {
            setTypeface(font, Typeface.ITALIC)
        } else {
            typeface = font
        }
    }
}