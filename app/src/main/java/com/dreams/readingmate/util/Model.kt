package com.dreams.readingmate.util

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Shivank on 4/17/2018.
 */
object Model {
    data class Comments(val comment: String, val id: String, val name: String, val image: String)
    data class favBookData(val id: String, val img: String, val title: String)
    data class cartItem(val bookId: String, val quantity: String)
    data class childList(val image: String, val name: String,val id: String,var isSelected: Boolean)
    data class cardItem(
        val deliveryCharge: String,
        val serviceCharge: String,
        val grandTotal: String,
        val ititlename: String,
        val iname: String,
        val iaddr1: String,
        val iaddr2: String,
        val iaddr3: String,
        val iaddr4: String,
        val ipcode: String,
        val icountry: String,
        val dtitlename: String,
        val dname: String,
        val daddr1: String,
        val daddr2: String,
        val daddr3: String,
        val daddr4: String,
        val dpcode: String,
        val dcountry: String,
        val trackingsafeplace: String,
        val comm1: String,
        val comm2: String,
        val comm3: String,
        val comm4: String,
        val clientSecret: String,
        val cardNo: String,
        val expMonth: String,
        val expYear: String,
        val cvv: String,
        val cardData: String,
        val couponCode: String
    ) : Serializable

    data class membershipItem(
        val name: String,
        val email: String,
        val nameOnCard: String,
        val cardNumber: String,
        val cvc: String,
        val cardExpMonth: String,
        val cardExpYear: String,
        val postalCode: String,
        val subscriptionPlanId: String,
        val clientSecret: String,
        val from: String,
        val isUpfront: Boolean
    ) : Serializable

    data class Payment(val cardImage: Int, val selectedCart: Int, val cardNumber: String)
    data class LanguageList(val id: Int, val value: String)
    data class User(
        @SerializedName("id") val mId: Int,
        @SerializedName("full_name") val fullName: String,
        @SerializedName("phone_number") val phoneNumber: String,
        @SerializedName("email") val email: String,
        @SerializedName("dob") val dob: String,
        @SerializedName("gender") val gender: String,
        @SerializedName("device_id") val deviceId: String,
        @SerializedName("token") val token: String,
        @SerializedName("login_token") val loginToken: String,
        @SerializedName("user_image") val userImage: String,
        @SerializedName("address") val address: String,
        @SerializedName("registration_type") val registrationType: String
    )


}