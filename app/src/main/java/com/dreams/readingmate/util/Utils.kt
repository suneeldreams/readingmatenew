package com.dreams.readingmate.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.RelativeLayout
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.UserData.set
import com.dreams.readingmate.ui.auth.LoginActivity
import com.squareup.picasso.Picasso
import java.io.File
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * Created by Shivank on 4/6/2018.
 */
object Utils {
    private val no_placeholder = 1001


    fun displayCircularImage(context: Context, imageView: ImageView?, url: Any) {
        when (url) {
            is String -> Picasso.get().load(url).placeholder(R.drawable.ic_avtar).transform(
                CircleTransform()
            ).into(imageView)
            is File -> Picasso.get().load(url).placeholder(R.drawable.ic_avtar).transform(
                CircleTransform()
            ).into(imageView)
        }
    }

    fun displayImage(context: Context, url: String, imageView: ImageView, placeHolder: Int) {
        try {
            if (!TextUtils.isEmpty(url)) {
                if (placeHolder == no_placeholder) {
                    Picasso.get().load(url).into(imageView)
                } else {
                    Picasso.get().load(url).placeholder(placeHolder).error(placeHolder)
                        .into(imageView)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun displayRoundedImage(context: Context, url: String, imageView: ImageView) {
        if (!TextUtils.isEmpty(url)) {
            // add default placeholder image here
            Picasso.get().load(url).placeholder(R.drawable.ic_avtar)
                .transform(RoundedCornersTransform()).into(imageView)
        }
    }

    fun simpleImage(context: Context, imageView: ImageView?, url: Any) {
        when (url) {
            is String -> Picasso.get().load(url).placeholder(R.drawable.ic_avtar).into(
                imageView
            )
            is File -> Picasso.get().load(url).placeholder(R.drawable.ic_avtar).into(
                imageView
            )
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

    fun hideKeyBoard(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun openBrowser(context: Context, url: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = (Uri.parse(url))
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun roundTwoDecimals(data: String?): String {
        if (data == null)
            return "0.00"
        var dataString = "0.00"
        try {
            val d = data.toDouble()
            val twoDForm = DecimalFormat("#,##0.00")
            dataString = twoDForm.format(d)
        } catch (e: Exception) {
        }
        return dataString
    }

    fun getSelectedAppLanguage(context: Context): String {
        var selectedLanguageSymbol = "english"
        val prefs: SharedPreferences? = UserData.customPrefsGlobal(context)
        val selectedLanguage =
            prefs?.getString(UserData.CURRENT_LANGUAGE, UserData.LANGUAGE_ENGLISH)!!

        if (selectedLanguage == UserData.LANGUAGE_ARABIC) {
            selectedLanguageSymbol = "arabic"
        }
        return selectedLanguageSymbol
    }

    fun roundTwoDecimals(value: Double?): String {
        if (value == null)
            return "0.00"
        var valueString = "0.00"
        try {
            val twoDForm = DecimalFormat("#,##0.00")
            valueString = twoDForm.format(value)
        } catch (e: Exception) {
        }
        return valueString
    }

    fun getAppVersion(context: Activity): String {
        var versionCode = ""
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            versionCode = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return versionCode
    }

    fun rateTheApp(context: Context) {
        val appPackageName = context.packageName // getPackageName() from Context or Activity object
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (e: android.content.ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    /*fun rateTheAppInKotlin(context: Context) {
//        val appPackageName = context.packageName
        val mngr = FakeReviewManager(context)
        val manager = ReviewManagerFactory.create(context)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { request ->
            val appPackageName = context.packageName
            if (request.isSuccessful) {
                // We got the ReviewInfo object
                val reviewInfo = request.result
                val flow = manager.launchReviewFlow(activity, reviewInfo)
                flow.addOnCompleteListener { _ ->
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                    try {
                        context.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=$appPackageName")
                            )
                        )
                    } catch (e: android.content.ActivityNotFoundException) {
                        context.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                            )
                        )
                    }
                }
            } else {
                // There was some problem, continue regardless of the result.
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }
    }*/


    fun saveCustomPref(context: Context, key: String, value: String) {
        val prefs: SharedPreferences = UserData.customPrefs(context)
        prefs[key] = value
    }

    fun saveRemPref(context: Context, key: String, value: String) {
        val prefs: SharedPreferences = UserData.rememberPrefs(context)
        prefs[key] = value
    }

    fun getCustomPref(context: Context, key: String): String {
        val prefs: SharedPreferences?
        prefs = UserData.customPrefs(context)
        val data = prefs.getString(key, "")!!
        return data
    }

    fun getRemPref(context: Context, key: String): String {
        val prefs: SharedPreferences?
        prefs = UserData.rememberPrefs(context)
        val data = prefs.getString(key, "")!!
        return data
    }

    fun getDate(dateString: String): String {

        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("dd.MM.yy", Locale.US)
            return outFormat.format(date)
        }
    }

    fun getDateForm(dateString: String): String {
        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("dd MMM, yyyy", Locale.US)
            return outFormat.format(date)
        }
    }

    fun getTime(dateString: String): String {

        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("HH:mm a", Locale.US)
            return outFormat.format(date)
        }
    }

    fun getDateTime(dateString: String): String {

        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("MMM dd, yyy @ HH:mm a", Locale.US)
            return outFormat.format(date)
        }
    }

    fun getDateFormat(dateString: String): String {

        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)
            return outFormat.format(date)
        }
    }

    fun getCalendarDateFormat(dateString: String): String {

        if (dateString.isEmpty()) {
            return ""
        } else {
            val inFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            val date = inFormat.parse(dateString)
            val outFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            return outFormat.format(date)
        }
    }

    fun handleUnAuthorisedRequest(context: Context) {
        UserData.clearCustomPref(context)
        UserData.clearGlobalPref(context)
        context.startActivity(
            Intent(
                context,
                LoginActivity::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }

    @Throws(Exception::class)
    fun maskString(strText: String?, start: Int, end: Int, maskChar: Char): String {
        var start = start
        var end = end

        if (strText == null || strText == "")
            return ""

        if (start < 0)
            start = 0

        if (end > strText.length)
            end = strText.length

        if (start > end)
            throw Exception("End index cannot be greater than start index")

        val maskLength = end - start

        if (maskLength == 0)
            return strText

        val sbMaskString = StringBuilder(maskLength)

        for (i in 0 until maskLength) {
            sbMaskString.append(maskChar)
        }

        return (strText.substring(0, start)
                + sbMaskString.toString()
                + strText.substring(start + maskLength))
    }

    fun checkEmailValid(email_address_get: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email_address_get)
        return matcher.matches()
    }

    fun checkConnectivity(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun isAge18(dateOfBirth: String): Boolean {

        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        val array = dateOfBirth.split("-")

        val selectedDay = array[0].toInt()
        val selectedMonth = array[1].toInt()
        val selectedYear = array[2].toInt()

        var age = currentYear - selectedYear;

        if ((selectedDay - currentDay > 3) || (selectedMonth > currentMonth)) {
            age--
        } else if ((selectedMonth == currentMonth) && (selectedDay > currentDay)) {
            age--
        }

        return age < 18
    }

    fun createCalender(day: String, month: String, year: String): Calendar {
        val calendar = Calendar.getInstance()
        calendar.clear()
        calendar.set(Calendar.MONTH, Integer.parseInt(month))
        calendar.set(Calendar.YEAR, Integer.parseInt(year))
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day))
        return calendar
    }

    fun showProgressbar(ctx: Context, activity: Activity) {
        val progressBar = activity.findViewById<RelativeLayout>(R.id.progress_bar_layout)
        val imageLogo = activity.findViewById<ImageView>(R.id.blink_image)
        imageLogo.animation = Utils.blinkAnimation(ctx)
        progressBar.setOnClickListener(View.OnClickListener { })
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressbar(ctx: Context, activity: Activity) {
        val progressBar = activity.findViewById<RelativeLayout>(R.id.progress_bar_layout)
        val imageLogo = activity.findViewById<ImageView>(R.id.blink_image)
        progressBar.visibility = View.GONE
        imageLogo.animation = Utils.blinkAnimation(ctx)
    }

    private fun rotateAnimation(ctx: Context): Animation {
        return AnimationUtils.loadAnimation(ctx, R.anim.rotate)
    }

    fun blinkAnimation(ctx: Context): Animation {
        val blink = AnimationUtils.loadAnimation(
            ctx,
            R.anim.blink
        )
        return blink
    }

    fun getColorCode(colorName: String): String {
        var cCode: String = ""

        when (colorName) {
            "Black" -> cCode = "#000000"
            "Blue" -> cCode = "#0000ff"
            "Green" -> cCode = "#2e8b57"
            "Grey" -> cCode = "#808080"
            "Pink" -> cCode = "#ffc0cb"
            "Purple" -> cCode = "#a020f0"
            "Red" -> cCode = "#ea1e63"
            "White" -> cCode = "#FFFFFF"
            "Yellow" -> cCode = "#ffe36e"
        }
        return cCode
    }

    /* val drawable = GradientDrawable()
     drawable.setShape(GradientDrawable.RECTANGLE)
     drawable.setStroke(3, Color.BLACK)
     drawable.setCornerRadius(8)
     drawable.setColor(Color.BLUE)
     val ss = drawable
     linearToAdd.setBackgroundDrawable(drawable)*/
    fun getAge(year: Int, month: Int, day: Int): String? {
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob[year, month] = day
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
        if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
            age--
        }
        val ageInt = age
        return ageInt.toString()
    }

}