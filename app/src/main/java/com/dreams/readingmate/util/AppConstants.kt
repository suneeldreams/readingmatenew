package com.dreams.readingmate.util

/**
 * Created by Suneel on 4/6/2018.
 */
object AppConstants {
    const val STRIPE_TEST_KEY =
        "pk_test_51HhyFPGhE8ybL8gDlf8MCdWlceC0fxF9xqIWuD0NzBH1ljg5AnqBaIx9t05hf6IByKQFdo2Nm5FlaWPZjPgPVYOy00wEebDB2R"
    const val STRIPE_NEW_TEST_KEY =
        "pk_test_51IfI9WCTE0iyOZqS1Nucn6usUMdbm021sAivkzkLzd4SlciO67E8m1AY5tSaIocsZIXc67y7V9T34nPeLLwSf4J500pNXY7Viz"
    const val STRIPE_LIVE_KEY =
        "pk_live_51HhyFPGhE8ybL8gD2Lb6ukRqgxnYdVafsSW9pF1wr1I8EOmUoHkac4I63ak48IHDTjSYEHir5OJoufWGALoGVvPG009es4cfVe"
    const val CONST = "value"
    const val EMPTY = ""
    const val PROGRESS = "PROGRESS"
    const val DASHBOARD = "DASHBOARD"
    const val REFRESH = "REFRESH"
    const val REFRESH_BOOKSHELF_LIST = "REFRESH_BOOKSHELF_LIST"
    const val ADD_LOCATION = "ADD_LOCATION"
    const val ADD_CARD = "ADD_CARD"
    const val ADD_CHILD_ID = "ADD_CHILD_ID"
    const val CHILD_ID = "CHILD_ID"
    const val REFERESH_BOOKS = "REFERESH_BOOKS"
    const val CHANGE_COLOR = "CHANGE_COLOR"
    const val UPDATE_PROFILE = "UPDATE_PROFILE"
    const val REFERESH_NEWS = "REFERESH_NEWS"
    const val REFERESH_CURRENTLY_READING_LIST = "REFERESH_CURRENTLY_READING_LIST"
    const val UPDATE_CART_ITEM = "UPDATE_CART_ITEM"
    const val DE_ACTIVE_EVENT = "DE_ACTIVE_EVENT"
}