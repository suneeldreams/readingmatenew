package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChildBooksReviewedData(

    @field:SerializedName("img")
    val image: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null,

    @field:SerializedName("childId")
    val childId: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("review")
    val review: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(bookId)
        parcel.writeString(childId)
        parcel.writeString(status)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(id)
        parcel.writeValue(rating)
        parcel.writeString(review)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChildBooksReviewedData> {
        override fun createFromParcel(parcel: Parcel): ChildBooksReviewedData {
            return ChildBooksReviewedData(parcel)
        }

        override fun newArray(size: Int): Array<ChildBooksReviewedData?> {
            return arrayOfNulls(size)
        }
    }
}