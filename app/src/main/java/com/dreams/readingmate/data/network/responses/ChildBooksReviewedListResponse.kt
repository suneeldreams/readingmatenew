package com.dreams.readingmate.data.network.responses

data class ChildBooksReviewedListResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<ChildBooksReviewedData>?
)