package com.dreams.readingmate.data.network.responses

data class AuthLoginResponse(
    val success: Boolean?,
    val message: String?,
    val data: UserInfoData?

)