package com.dreams.readingmate.data.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.ui.home.books.BooksFragment
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.book_tab_single_list_items.view.*
import kotlinx.android.synthetic.main.book_tab_single_list_items.view.bookStatus
import kotlinx.android.synthetic.main.book_tab_single_list_items.view.txtChange
import kotlinx.android.synthetic.main.bookshelf_single_list_items.view.*
import kotlinx.android.synthetic.main.progress_single_list_items.view.txtChildName
import kotlinx.android.synthetic.main.progress_single_list_items.view.txtChildNickName

class BooksListAdapter(
    private val itemList: List<ChildListItem>,
    private val mListener: BooksFragment.BookFragmentInterface?,
    private val from: String,
    private val booksFragment: BooksFragment,
    private val bookId: String
) :
    RecyclerView.Adapter<BooksListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_tab_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, from, booksFragment, bookId)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ChildListItem,
            mListener: BooksFragment.BookFragmentInterface?,
            from: String,
            booksFragment: BooksFragment,
            bookId: String
        ) {
            val cardViewGenres = itemView.cardViewGenres
            val imgChild = itemView.imgChildBook
            val txtChange = itemView.txtChange
            val lnrStatus = itemView.lnrStatusChange
            val txtChildName = itemView.txtChildName
            val txtChildNickName = itemView.txtChildNickName
            val btnViewBook = itemView.btnViewBook
            btnViewBook.visibility = View.VISIBLE
            txtChildName.text = items.childName
            txtChildNickName.text = items.nickName
            items.image?.let { Utils.displayCircularImage(itemView.context, imgChild, it) }
            if (from == "OrderBook") {
                btnViewBook.visibility = View.GONE
                val htmlString = "<u>Change</u>"
                txtChange.text = Html.fromHtml(htmlString)
                cardViewGenres.setOnClickListener {
                    changeBookStatusDialog(itemView.context, bookId, items.id!!, booksFragment)
                }
            } else if (from == "bookDetail") {
                btnViewBook.visibility = View.GONE
                cardViewGenres.setOnClickListener {
                    changeBookStatusDialog(itemView.context, bookId, items.id!!, booksFragment)
                }
                /*cardViewGenres.setOnClickListener {
                    val intent = Intent()
                    intent.action = AppConstants.ADD_CHILD_ID
//                    intent.putExtra(AppConstants.CHILD_ID, items.id)
                    LocalBroadcastManager.getInstance(itemView.context).sendBroadcast(intent)
                    mListener?.openBackFragment()
                }*/
            } else {
                lnrStatus.visibility = View.GONE
                btnViewBook.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("childId", items.id)
                    bundle.putString("nickName", items.childName)
                    mListener?.openAllBookFragment(bundle)
                }
            }
        }

        private fun changeBookStatusDialog(
            context: Context,
            bookId: String,
            childId: String,
            bookFragment: BooksFragment
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog_book_status)

            val displayRectangle = Rect()
            val window = (context as Activity).window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            window.setGravity(Gravity.CENTER_VERTICAL)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = (displayRectangle.width() * 0.95f).toInt()
            lp.height = (displayRectangle.height() * 0.95f).toInt()
            dialog.window!!.attributes = lp
            dialog.show()

            val imgDismiss = dialog.findViewById(R.id.imgDismiss) as ImageView
            val currentlyReading = dialog.findViewById(R.id.currentlyReading) as TextView
            val txtRead = dialog.findViewById(R.id.txtRead) as TextView
            val txtNonRead = dialog.findViewById(R.id.txtNonRead) as TextView
            val btnSaveStatus = dialog.findViewById(R.id.btnSaveStatus) as Button
            var bookStatus = ""
            currentlyReading.setOnClickListener {
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "1"
            }
            txtRead.setOnClickListener {
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtNonRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "2"
            }
            txtNonRead.setOnClickListener {
                txtNonRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "0"
            }
            btnSaveStatus.setOnClickListener {
                dialog.dismiss()
                val intent = Intent()
                intent.action = AppConstants.REFERESH_BOOKS
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                if (bookStatus == "0") {
                    bookFragment.callFavoriteApi(bookId, childId)
                } else {
                    bookFragment.callChangeBookStatusApi(bookStatus, bookId, childId)
                }
            }
            imgDismiss.setOnClickListener {
                dialog.dismiss()
            }

        }

        /*private fun openDialog(
            context: Context,
            booksFragment: BooksFragment,
            childId: String?
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(context.resources.getString(R.string.app_name))
            alertDialog.setMessage("")
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.ok)
            ) { dialog, _ ->
                dialog.dismiss()
//                booksFragment.assignBookToChild(childId)
            }
            alertDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.cancel)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }*/
    }
}