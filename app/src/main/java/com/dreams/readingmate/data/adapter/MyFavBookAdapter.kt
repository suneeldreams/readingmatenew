package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.ui.home.favouritesbooks.NewFavoriteBookFragment
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*
import kotlin.collections.ArrayList

class MyFavBookAdapter(
    private val itemList: ArrayList<Model.favBookData>,
    private val mListener: NewFavoriteBookFragment.NewFavoriteFragmentInterface,
    private val favouriteBookFragment: NewFavoriteBookFragment
) :
    RecyclerView.Adapter<MyFavBookAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList!!.get(position), mListener, favouriteBookFragment, position)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: Model.favBookData,
            mListener: NewFavoriteBookFragment.NewFavoriteFragmentInterface,
            favouriteBookFragment: NewFavoriteBookFragment,
            position: Int
        ) {
            val imgFavouriteBook = itemView.imgBook
            val rltCross = itemView.rltCross
            val imgClose = itemView.imgClose
            rltCross.visibility = View.VISIBLE
            items.img.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
            imgClose.setOnClickListener {
                favouriteBookFragment.refreshAdapter(position, items.id)
            }
        }
    }
}