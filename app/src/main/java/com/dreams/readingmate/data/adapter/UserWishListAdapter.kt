package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.UserWishListData
import com.dreams.readingmate.ui.home.progress.UserWishListFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.order_book_single_list_items.view.*
import kotlinx.android.synthetic.main.order_book_single_list_items.view.primaryContentCardView1
import kotlinx.android.synthetic.main.order_book_single_list_items.view.txtAuthor

class UserWishListAdapter(
    private val mListener: UserWishListFragment.UserWishListFragmentInterface,
    private val itemList: List<UserWishListData>
) :
    RecyclerView.Adapter<UserWishListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_book_single_list_items, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList?.get(position), mListener
        )
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: UserWishListData,
            mListener: UserWishListFragment.UserWishListFragmentInterface
        ) {
            val primaryContentCardView1 = itemView.primaryContentCardView1
            val imgBook = itemView.imgBook
            val txtTitle = itemView.txtTitle
            txtTitle.text = items.title
            if (!items.authors.isNullOrEmpty()) {
                itemView.txtAuthor.text = items.authors!![0].toString()
            }
            Utils.displayImage(
                itemView.context,
                items.img!!,
                imgBook,
                R.drawable.ic_place_holder
            )
            primaryContentCardView1.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("bookId", items.bookId)
//                bundle.putString("childId", childId)
                mListener?.openBookDetailFragment(bundle)
            }
        }
    }
}