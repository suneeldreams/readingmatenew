package com.dreams.readingmate.data.network.responses


data class AuthResponseSignUp(
    val success: Boolean?,
    val message: String?,
    val data: UserDataSignUp?

)