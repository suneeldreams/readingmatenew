package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PreferenceChildItems(

    /*@field:SerializedName("subjectData")
    val subjectData: List<SubjectDataItem?>? = null,*/
    @field:SerializedName("subjectData")
    val subjectData: List<FavSubjectData?>? = null,

    @field:SerializedName("schoolTypeData")
    val schoolTypeData: List<SchoolTypeDataItem?>? = null,

    @field:SerializedName("colourData")
    val colourData: List<ColourDataItem?>? = null,

    @field:SerializedName("countryData")
    val countryData: List<CountryDataItem?>? = null,

    @field:SerializedName("bookData")
    val bookData: List<BookDataItem?>? = null,

    /*@field:SerializedName("genreData")
    val genreData: List<GenreDataItem?>? = null,*/
    @field:SerializedName("genreData")
    val genreData: List<FavGeneresData?>? = null,

    @field:SerializedName("characterData")
    val characterData: List<FavCharacterData?>? = null,

    @field:SerializedName("favBookData")
    var favBookData: List<FavBookNewData?>? = null,

    @field:SerializedName("currentlyReadingBooks")
    var currentlyReadingBooks: List<CurrentlyReadingBook?>? = null,

    @field:SerializedName("readBooks")
    var readBooks: List<ReadBooks?>? = null,

    @field:SerializedName("favGenreData")
    var favGenreData: List<FavGeneresData?>? = null,

    @field:SerializedName("favCharacterData")
    var favCharacterData: List<FavCharacterData?>? = null,

    @field:SerializedName("favSubjectData")
    var favSubjectData: List<FavSubjectData?>? = null,

    @field:SerializedName("schoolId")
    var schoolId: String? = null,

    @field:SerializedName("schoolName")
    var schoolName: String? = null,

    @field:SerializedName("charity")
    var charity: String? = null,

    @field:SerializedName("image")
    var image: String? = null,

    @field:SerializedName("countryId")
    var countryId: String? = null,

    @field:SerializedName("from")
    var from: String? = null,

    @field:SerializedName("update_child_id")
    var updateChildId: String? = null,

    @field:SerializedName("colorId")
    var colorId: String? = null,

    @field:SerializedName("colorName")
    var colorName: String? = null,

    @field:SerializedName("childName")
    var childName: String? = null,

    @field:SerializedName("nickName")
    var nickName: String? = null,

    @field:SerializedName("age")
    var age: String? = null,

    @field:SerializedName("bookId")
    var bookId: ArrayList<String>? = null,

    @field:SerializedName("currentlyReadingbook")
    var currentlyReadingbook: ArrayList<String>? = null,

    @field:SerializedName("favbookIds")
    var favbookIds: ArrayList<String>? = null,

    @field:SerializedName("readBookss")
    var readBookss: ArrayList<String>? = null,

    @field:SerializedName("generesId")
    var generesId: ArrayList<String>? = null,

    @field:SerializedName("subjectId")
    var subjectId: ArrayList<String>? = null,

    @field:SerializedName("charId")
    var charId: ArrayList<String>? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("subjectData"),
        TODO("schoolTypeData"),
        TODO("colourData"),
        TODO("countryData"),
        TODO("bookData"),
        TODO("currentlyReadingBooks"),
        TODO("readBooks"),
        TODO("genreData"),
        TODO("characterData"),
        TODO("favBookData"),
        TODO("favGenreData"),
        TODO("favCharacterData"),
        TODO("favSubjectData"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(schoolId)
        parcel.writeString(image)
        parcel.writeString(countryId)
        parcel.writeString(colorId)
        parcel.writeString(childName)
        parcel.writeString(nickName)
        parcel.writeString(age)
        parcel.writeString(bookId.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PreferenceChildItems> {
        override fun createFromParcel(parcel: Parcel): PreferenceChildItems {
            return PreferenceChildItems(parcel)
        }

        override fun newArray(size: Int): Array<PreferenceChildItems?> {
            return arrayOfNulls(size)
        }
    }
}

data class ColourDataItem(

    @field:SerializedName("colourName")
    val colourName: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("slug")
    val slug: Any? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class FavBookDataItems(

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class SubjectDataItem(

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("subjectName")
    val subjectName: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null,
    var isSelected: Boolean = false
)

data class BookDataItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bookName")
    val bookName: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    var isSelected: Boolean = false
)

data class CountryDataItem(

    @field:SerializedName("capital")
    val capital: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("countryName")
    val countryName: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("phonecode")
    val phonecode: String? = null,

    @field:SerializedName("currency")
    val currency: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("iso2")
    val iso2: String? = null,

    @field:SerializedName("iso3")
    val iso3: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class GenreDataItem(

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("genreName")
    val genreName: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null,
    var isSelected: Boolean = false
)

data class CharacterDataItem(

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("characterName")
    val characterName: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null,
    var isSelected: Boolean = false
)

data class SchoolTypeDataItem(

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("schoolTypeName")
    val schoolTypeName: String? = null,

    @field:SerializedName("school_selected_id")
    val schoolSelectedId: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
