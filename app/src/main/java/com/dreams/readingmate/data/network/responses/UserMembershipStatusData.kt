package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class UserMembershipStatusData(

	@field:SerializedName("planIntervalCount")
	val planIntervalCount: Int? = null,

	@field:SerializedName("planInterval")
	val planInterval: String? = null,

	@field:SerializedName("planStatus")
	val planStatus: Int? = null,

	@field:SerializedName("planName")
	val planName: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("upfrontAmount")
	val upfrontAmount: Double? = null,

	@field:SerializedName("planCurrency")
	val planCurrency: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("userMembership")
	val userMembership: UserMembership? = null,

	@field:SerializedName("planDescription")
	val planDescription: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("stripeProduct")
	val stripeProduct: String? = null,

	@field:SerializedName("planAmount")
	val planAmount: Double? = null,

	@field:SerializedName("stripePlan")
	val stripePlan: String? = null
)

data class UserMembership(

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("cardExpMonth")
	val cardExpMonth: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("subscriptionPlanId")
	val subscriptionPlanId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("totalRemainingBooks")
	val totalRemainingBooks: Int? = null,

	@field:SerializedName("transactionId")
	val transactionId: String? = null,

	@field:SerializedName("cardExpYear")
	val cardExpYear: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("stripeSourceId")
	val stripeSourceId: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("user")
	val user: String? = null,

	@field:SerializedName("cardLast4")
	val cardLast4: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("isUpfront")
	val isUpfront: Boolean? = null,

	@field:SerializedName("status")
	val status: String? = null
)
