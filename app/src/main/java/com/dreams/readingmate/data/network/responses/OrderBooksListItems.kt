package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class OrderListData(

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("rows")
    val rows: List<RowItems?>? = null
)

data class RowItems(

    @field:SerializedName("orderId")
    val orderId: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null,

    @field:SerializedName("quantity")
    val quantity: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("price")
    val price: String? = null,

    @field:SerializedName("orderStatusKey")
    val orderStatusKey: String? = null,

    @field:SerializedName("orderStatus")
    val orderStatus: String? = null,

    @field:SerializedName("author")
    var authors: List<String?>? = null

)
