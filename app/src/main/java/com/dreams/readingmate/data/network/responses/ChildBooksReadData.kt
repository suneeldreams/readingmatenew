package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChildBooksReadData(

    @field:SerializedName("img")
    var image: String? = null,

    /*@field:SerializedName("author")
    val author: String? = null,*/

    @field:SerializedName("bookId")
    var bookId: String? = null,

    @field:SerializedName("numberOfPages")
    var numberOfPages: String? = null,

    @field:SerializedName("title")
    var bookName: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("child_id")
    val childId: String? = null,

    @field:SerializedName("days_running")
    val daysRunning: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("readingStatus")
    val readingStatus: String? = null,

    @field:SerializedName("fav_subject_ids")
    val favSubjectIds: String? = null,

    @field:SerializedName("isReading")
    val isReading: String? = null,

    @field:SerializedName("page_count")
    val pageCount: String? = null,

    @field:SerializedName("readingHabit")
    val readingHabit: String? = null,

    @field:SerializedName("totalReadingDays")
    val totalReadingDays: String? = null,

    @field:SerializedName("totalDaysToRead")
    val totalDaysToRead: String? = null,

    @field:SerializedName("totalPageRead")
    var totalPageRead: String? = null,

    @field:SerializedName("achievementIds")
    val achievementIds: List<AchievementData?>? = null,

    @field:SerializedName("readingHistory")
    val readingHistory: List<HistoryDataItem?>? = null,

    @field:SerializedName("author")
    var authors: List<String?>? = null,

    @field:SerializedName("status")
    val status: String? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("achievementIds"),
        TODO("readingHistory"),
        parcel.createStringArrayList(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(bookId)
        parcel.writeString(numberOfPages)
        parcel.writeString(bookName)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(childId)
        parcel.writeString(daysRunning)
        parcel.writeString(id)
        parcel.writeString(readingStatus)
        parcel.writeString(favSubjectIds)
        parcel.writeString(isReading)
        parcel.writeString(pageCount)
        parcel.writeString(readingHabit)
        parcel.writeString(totalReadingDays)
        parcel.writeString(totalDaysToRead)
        parcel.writeString(totalPageRead)
        parcel.writeStringList(authors)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChildBooksReadData> {
        override fun createFromParcel(parcel: Parcel): ChildBooksReadData {
            return ChildBooksReadData(parcel)
        }

        override fun newArray(size: Int): Array<ChildBooksReadData?> {
            return arrayOfNulls(size)
        }
    }
}