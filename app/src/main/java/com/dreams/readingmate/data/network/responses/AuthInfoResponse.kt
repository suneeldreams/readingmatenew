package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class AuthInfoResponse(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("img")
    val profileImage: String? = null,

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("pincode")
    val pincode: Any? = null,

    @field:SerializedName("gender")
    val gender: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("mobile")
    val mobile: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("address_line_1")
    val addressLine1: Any? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("address_line_2")
    val addressLine2: Any? = null,

    @field:SerializedName("state")
    val state: Any? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("charity")
    val charity: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
