package com.dreams.readingmate.data.network.responses

data class AddCartDataResponse(
    val status: Boolean?,
    val message: String?,
    val data: AddCartDataItam?
)