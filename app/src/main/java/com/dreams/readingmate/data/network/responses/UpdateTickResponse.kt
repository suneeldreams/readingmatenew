package com.dreams.readingmate.data.network.responses

data class UpdateTickResponse(
    val status: Boolean?,
    val message: String?,
    val data: UpdateTickData2?
)