package com.dreams.readingmate.data.network.responses

data class CartItem(

    var img: String? = null,

    var bookId: String? = null,

    var bookName: String? = null,

    var description: String? = null,

    var price: String? = null,

    var weight: Int? = null,

    var quantity: Int = 1,

    var perItemCharge: Double = 0.05,

    var ukPriceExcVAT: String? = null,

    var ukPriceIncVAT: String? = null

)