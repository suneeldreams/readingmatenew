package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName


data class HistoryDataItem(
    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("child_id")
    val childId: String? = null,

    @field:SerializedName("book_id")
    val bookId: String? = null,

    @field:SerializedName("bookshelf_id")
    val bookshelfId: String? = null,

    @field:SerializedName("pageRead")
    val pageRead: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null
)