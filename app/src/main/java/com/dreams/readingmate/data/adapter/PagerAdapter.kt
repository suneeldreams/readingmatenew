package com.dreams.readingmate.data.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.SubscriptionModel
import com.dreams.readingmate.ui.home.subscription.SubscriptionFragment


class CustomPagerAdapter(
    context: Context,
    pager: ArrayList<SubscriptionModel>?,
    mListener: SubscriptionFragment.SubscriptionFragmentInterface?
) : PagerAdapter() {
    var context: Context
    var pager: ArrayList<SubscriptionModel>
    var listener: SubscriptionFragment.SubscriptionFragmentInterface?
    override fun getCount(): Int {
        return pager.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view === o
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.pager_items, container, false)
//        val imageView: ImageView = view.findViewById(R.id.viewPageImage) as ImageView
        val rltMemberHeader: RelativeLayout =
            view.findViewById(R.id.rltMemberHeader) as RelativeLayout
        val txtPlanName: TextView = view.findViewById(R.id.txtPlanName) as TextView
        val txtPlanAmount: TextView = view.findViewById(R.id.txtPlanAmount) as TextView
        val txtPlanInterval: TextView = view.findViewById(R.id.txtPlanInterval) as TextView
        val txtPlanDescription: TextView = view.findViewById(R.id.txtPlanDescription) as TextView
        val txtUpFrontAmount: TextView = view.findViewById(R.id.txtUpFrontAmount) as TextView
        val btnMembership: Button = view.findViewById(R.id.btnMembership) as Button
//        imageView.setBackgroundResource(pager[position])
//        imageView.setImageResource(pager[position].image!!)
        txtPlanName.text = pager[position].planName
        txtPlanInterval.text = pager[position].planInterval
        txtPlanDescription.text = pager[position].desc
        txtPlanAmount.text =
            context.getString(R.string.currency_pound) + pager[position].planAmount.toString()
        txtUpFrontAmount.text = "Or " +
                context.getString(R.string.currency_pound) + pager[position].upfrontAmount.toString() + " " + "Upfront"


        container.addView(view)
        if (position == 0) {
            rltMemberHeader.background = ContextCompat.getDrawable(
                context,
                R.drawable.membership_top_left_right_corner_readingmate
            )
        } else if (position == 2) {
            rltMemberHeader.background = ContextCompat.getDrawable(
                context,
                R.drawable.membership_top_left_right_corner_readingmate
            )
        }
        btnMembership.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("from", "membership")
            bundle.putDouble("planAmount", pager[position].planAmount!!)
            bundle.putString("planId", pager[position].id)
            listener?.openCheckoutFragments(bundle)
        }
        return view
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        `object`: Any
    ) {
        container.removeView(`object` as View)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

    init {
        this.context = context
        this.pager = pager!!
        this.listener = mListener
    }
}