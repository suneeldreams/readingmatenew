package com.dreams.readingmate.data.network

import com.dreams.readingmate.data.network.model.AddWeekReadDataModel
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.model.UpdateProfileModel
import com.dreams.readingmate.data.network.responses.*
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface MyApi {

    companion object {
        //live url currently use
//        private val BASE_URL: String = "http://35.177.115.101/api/"

        //test url
        private val BASE_URL: String = "http://3.8.215.22/api/"
//        private val BASE_URL: String = "http://3.9.176.182/api/"
//        private val BASE_URL: String = "http://18.130.235.112/api/"

        //        private val BASE_URL: String = "http://18.134.248.208/api/"
        operator fun invoke(): MyApi {

            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)
        }
    }

    @FormUrlEncoded
    @POST("auth")
    suspend fun createUser(
        @Header("Accept") accessToken: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<AuthLoginResponse>

    /*@FormUrlEncoded
    @POST("auth/login")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<AuthResponseLogin>*/

    /*@FormUrlEncoded
    @POST("auth/signup")
    suspend fun userSignUp(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("countryCode") countryCode: String,
        @Field("mobile") mobile: String,
        @Field("role") role: String,
        @Field("confirmpassword") deviceId: String,
        @Field("socialId") socialId: String,
        @Field("socialType") socialTypee: String,
        @Field("isEmailMannual") isEmailMannual: String
    ): Response<AuthResponse>*/
    @FormUrlEncoded
    @POST("users")
    suspend fun registerUser(
        @Header("Accept") accessToken: String,
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("charity") charityName: String
    ): Response<AuthLoginResponse>

    @FormUrlEncoded
    @POST("users/forgot_password")
    suspend fun forgotPassword(
        @Header("Accept") accessToken: String,
        @Field("email") email: String
    ): Response<SuccessResponse>

    @FormUrlEncoded
    @POST("users/change-password")
    suspend fun resetPass(
        @Header("x-auth-token") accessToken: String,
        @Field("current_password") currentPassword: String,
        @Field("new_password") newPassword: String,
        @Field("confirm_new_password") cnfmPassword: String
    ): Response<SuccessResponse>

    @GET("users")
    suspend fun getUserDetail(
        @Header("Authorization") accessToken: String
    ): Response<UserProfileResponse>

    @PUT("users/change-password")
    suspend fun resetPassword(
        @Header("x-auth-token") accessToken: String,
        @Body user: ResetPassword
    ): Response<AuthResponse>

    @GET("child/preference")
    suspend fun getChildPreference(
        @Header("x-auth-token") accessToken: String
    ): Response<PreferenceChildResponse>

    /*@GET("book/recommendedbooks")
    suspend fun getNextBook(
        @Header("x-auth-token") accessToken: String
    ): Response<FavBookListResponse>*/
    @GET("childRecommendation/yournextread/{childId}")
    suspend fun getNextBook(
        @Header("x-auth-token") accessToken: String,
        @Path("childId") childId: String
    ): Response<FavBookListResponse>

    @DELETE("child/{id}")
    suspend fun removeChild(
        @Header("x-auth-token") accessToken: String,
        @Path("id") portfolio_id: String
    ): Response<SuccessResponse>

    @GET("child")
    suspend fun getChildList(
        @Header("x-auth-token") accessToken: String
    ): Response<ChildListResponse>

    @GET("users/profile")
    suspend fun getUserProfile(
        @Header("x-auth-token") accessToken: String
    ): Response<CardListResponse>

    @GET("book")
    suspend fun getBookList(
        @Header("x-auth-token") accessToken: String
    ): Response<BooksRespons>

    @GET("book/{id}")
    suspend fun getBookDetailData(
        @Header("x-auth-token") accessToken: String,
        @Path("id") bookId: String,
        @Query("childId") childId: String
    ): Response<BooksDetailsRespons>

    /*@FormUrlEncoded
    @POST("child/wishlist")
    suspend fun favoriteBook(
        @Header("x-auth-token") accessToken: String,
        @Field("bookId") bookId: String,
        @Path("id") childId: String
    ): Response<BooksDetailsRespons>*/
    @FormUrlEncoded
    @POST("users/wishlist")
    suspend fun favoriteBook(
        @Header("x-auth-token") accessToken: String,
        @Field("bookId") bookId: String
    ): Response<BooksDetailsRespons>


    /*@DELETE("child/wishlist/{childId}/{book_id}")
    suspend fun removefavoriteBook(
        @Header("x-auth-token") accessToken: String,
        @Path("book_id") book_id: String,
        @Path("childId") childId: String
    ): Response<BooksDetailsRespons>*/

    @DELETE("users/wishlist/{book_id}")
    suspend fun removefavoriteBook(
        @Header("x-auth-token") accessToken: String,
        @Path("book_id") book_id: String
    ): Response<BooksDetailsRespons>

    @FormUrlEncoded
    @POST("review")
    suspend fun addReview(
        @Header("x-auth-token") accessToken: String,
        @Field("bookId") bookId: String,
        @Field("childId") childId: String,
        @Field("rating") rating: String,
        @Field("review") review: String
    ): Response<ReviewResponse>

    @FormUrlEncoded
    @POST("order/client-secret")
    suspend fun clientSecretKey(
        @Header("x-auth-token") accessToken: String,
        @Field("amount") grandTotal: String
    ): Response<ClientSecretResponse>

    @GET("child/books/{id}")
    suspend fun getAllBookData(
        @Header("x-auth-token") accessToken: String,
        @Path("id") childId: String
    ): Response<AllBooksDataRespons>

    @GET("child/details/{id}")
    suspend fun getChildDetailList(
        @Header("x-auth-token") accessToken: String,
        @Path("id") childId: String
    ): Response<ChildDetailResponse>

    @GET("book/search?")
    suspend fun getFavList(
        @Header("x-auth-token") accessToken: String,
        @Query("search") search: String,
        @Query("pageSize") pageSize: String,
        @Query("cartitems") cartitems: String,
        @Query("filter") filter: String
    ): Response<FavBookListResponse>

    @GET("blog/search")
    suspend fun getNewsList(
        @Header("x-auth-token") accessToken: String,
        @Query("search") search: String,
        @Query("pageSize") pageSize: String,
        @Query("orderBy") orderBy: String,
        @Query("orderDir") orderDir: String,
        @Query("pageNumber") pageNumber: String
    ): Response<NewsResponse>

    @GET("order/history")
    suspend fun getOrderBookList(
        @Header("x-auth-token") accessToken: String
    ): Response<OrderBookResponse>

    @GET("book/recommendedbooks")
    suspend fun getRecommendBooksList(
        @Header("x-auth-token") accessToken: String
    ): Response<FavBookListResponse>

    @GET("childRecommendation/myRecommendation/{childId}")
    suspend fun getChildRecommendBooksList(
        @Header("x-auth-token") accessToken: String,
        @Path("childId") childId: String
    ): Response<FavBookListResponse>

    @GET("users/wishlist")
    suspend fun getUserWishList(
        @Header("x-auth-token") accessToken: String
    ): Response<UserWishListResponse>

    @GET("subscriptionplan/{type}")
    suspend fun getSubscriptionPlanList(
        @Header("x-auth-token") accessToken: String,
        @Path("type") planType: String
    ): Response<SubscriptionResponse>

    @GET("userMemberships/my-subscriptions")
    suspend fun getMembershipStatus(
        @Header("x-auth-token") accessToken: String
    ): Response<MembershipStatusResponse>

    @GET("bundle")
    suspend fun getBundleList(
        @Header("x-auth-token") accessToken: String
    ): Response<BundleResponse>

    @GET("coupon/couponDetails/ABC123")
    suspend fun validateCouponCode(
        @Header("x-auth-token") accessToken: String
    ): Response<UserWishListResponse>

    @GET("coupon/couponDetails/{couponcode}")
    suspend fun validateCouponCode(
        @Header("x-auth-token") accessToken: String,
        @Path("couponcode") couponCode: String
    ): Response<CouponCodeResponse>

    @GET("blog/{id}")
    suspend fun getNewsDetails(
        @Header("x-auth-token") accessToken: String,
        @Path("id") blogId: String
    ): Response<NewsDetailsResponse>

    @GET("bundle/{slug}")
    suspend fun getBundleDetails(
        @Header("x-auth-token") accessToken: String,
        @Path("slug") slug: String
    ): Response<BundleDetailsResponse>

    @GET("child/progress/{child_id}")
    suspend fun getChildProgress(
        @Header("x-auth-token") accessToken: String,
        @Path("child_id") childId: String
    ): Response<ChildProgressResponse>

    @GET("child/bookshelf/{child_id}")
    suspend fun getChildBooksReadList(
        @Header("x-auth-token") accessToken: String,
        @Path("child_id") childId: String
    ): Response<ChildBooksReadListResponse>

    @FormUrlEncoded
    @POST("child/bookshelf/{id}")
    suspend fun addBookToBookShelf(
        @Header("x-auth-token") token: String,
        @Field("bookId") bookId: String,
        @Path("id") id: String,
        @Field("readingStatus") readingStatus: String
    ): Response<AddBookShelfResponse>

    @DELETE("child/bookshelf/{childId}/{book_id}")
    suspend fun removeBookFromShelf(
        @Header("x-auth-token") token: String,
        @Path("childId") childId: String,
        @Path("book_id") book_id: String
    ): Response<AddBookShelfResponse>

    @GET("child/book-review/{id}")
    suspend fun getChildBooksReviewedList(
        @Header("x-auth-token") token: String,
        @Path("id") childId: String
    ): Response<ChildBooksReviewedListResponse>

    @POST("child/{id}")
    suspend fun createChild(
        @Header("x-auth-token") token: String,
        @Path("id") userId: String,
        @Body user: CrateChildModel
    ): Response<CreateChildResponse>

    @POST("users/address")
    suspend fun createAddress(
        @Header("x-auth-token") token: String,
        @Body user: CrateAddressModel
    ): Response<CreateAddressResponse>

    @POST("child/reminder/{id}")
    suspend fun setAlarm(
        @Header("x-auth-token") token: String,
        @Path("id") childId: String,
        @Body user: AlarmModel
    ): Response<ChildDetailResponse>

    @DELETE("users/card/{id}")
    suspend fun deleteCard(
        @Header("x-auth-token") token: String,
        @Path("id") cardId: String
    ): Response<CreateAddressResponse>

    @POST("users/card")
    suspend fun addCard(
        @Header("x-auth-token") token: String,
        @Body user: CrateCardModel
    ): Response<CreateAddressResponse>

    @POST("order")
    suspend fun createOrder(
        @Header("x-auth-token") token: String,
        @Body user: CrateOrderModel
    ): Response<OrderResponse>

    @POST("checkout")
    suspend fun createMemberShip(
        @Header("x-auth-token") token: String,
        @Body user: CrateMemberShipModel
    ): Response<OrderResponse>

    @POST("order/cart")
    suspend fun addCartData(
        @Header("x-auth-token") token: String,
        @Body user: CartDataModel
    ): Response<AddCartDataResponse>

    @PUT("child/{id}")
    suspend fun updateChild(
        @Header("x-auth-token") token: String,
        @Path("id") userId: String,
        @Body user: UpdateChildModel
    ): Response<CreateChildResponse>

    @PUT("childRecommendation/bookhardrating/{childId}/{bookId}")
    suspend fun hardBookRating(
        @Header("x-auth-token") token: String,
        @Path("childId") childId: String,
        @Path("bookId") bookId: String,
        @Body user: HardRatingRequestModel
    ): Response<SuccessResponse>

    @PUT("childRecommendation/bookenjoyrating/{childId}/{bookId}")
    suspend fun enjoyBookRating(
        @Header("x-auth-token") token: String,
        @Path("childId") childId: String,
        @Path("bookId") bookId: String,
        @Body user: EnjoyRatingRequestModel
    ): Response<SuccessResponse>

    @Multipart
    @POST("child/update/{id}")
    suspend fun updateChild(
        @Header("Accept") accept: String,
        @Header("Authorization") accessToken: String,
        @Part("child_name") childName: RequestBody,
        @Part("nick_name") nickName: RequestBody,
        @Part("age") age: RequestBody,
        @Part("school_type_id") schoolId: RequestBody,
        @Part("country_id") countryId: RequestBody,
        @Part("colour") colorName: RequestBody,
        @Part("fav_book_ids") bookId: RequestBody,
        @Part("fav_genre_ids") generesId: RequestBody,
        @Part("fav_subject_ids") subjectId: RequestBody,
        @Part("fav_character_ids") charId: RequestBody,
        @Part("id") updateChildId: RequestBody,
        @Part userInput: MultipartBody.Part
    ): Response<CreateChildResponse>


    /*@Multipart
    @PUT("users")
    suspend fun updateProfile(
        @Header("x-auth-token") accessToken: String,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("mobile") number: RequestBody,
        @Part userInput: MultipartBody.Part
    ): Response<AuthResponse>*/

    @PUT("users")
    suspend fun updateProfile(
        @Header("x-auth-token") token: String,
        @Body user: UpdateProfileModel
    ): Response<AuthResponse>

    @PUT("child/update-reading/{id}")
    suspend fun addWeekReadData(
        @Header("x-auth-token") token: String,
        @Path("id") userId: String,
        @Body user: AddWeekReadDataModel
    ): Response<AddBookShelfResponse>

    @PUT("child/update-reading/{id}")
    suspend fun bookStatus(
        @Header("x-auth-token") token: String,
        @Path("id") childId: String,
        @Body user: BookStatusDataModel
    ): Response<AddBookShelfResponse>

    @PUT("userMemberships/cancel/{id}")
    suspend fun cancelSubsCription(
        @Header("x-auth-token") token: String,
        @Path("id") membershipID: String
    ): Response<CancelSubscriptionResponse>

    @PUT("blog/favorite/{id}")
    suspend fun addBookMark(
        @Header("x-auth-token") token: String,
        @Path("id") childId: String
    ): Response<AddRemoveFavResponse>

    @PUT("child/tick-reading/{id}")
    suspend fun addWeekTickData(
        @Header("x-auth-token") token: String,
        @Path("id") userId: String,
        @Body user: AddWeekReadDataModel
    ): Response<UpdateTickResponse>

    @DELETE("child/tick-remove/{childId}/{id}")
    suspend fun removeTick(
        @Header("x-auth-token") token: String,
        @Path("childId") cardId: String,
        @Path("id") historyId: String
    ): Response<UpdateTickResponse>
}

