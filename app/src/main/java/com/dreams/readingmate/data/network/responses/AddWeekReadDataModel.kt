package com.dreams.readingmate.data.network.model


data class AddWeekReadDataModel(
    var bookshelfId: String?,
    var pageRead: String?,
    var createdAt: String?,
    var readingStatus: String?
)