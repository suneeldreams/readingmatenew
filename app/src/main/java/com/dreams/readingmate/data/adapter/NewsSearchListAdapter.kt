package com.dreams.readingmate.data.adapter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.RowsItems
import com.dreams.readingmate.ui.home.news.NewsFragment
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.blog_search_single_list_items.view.*

class NewsSearchListAdapter(
    private val itemList: List<RowsItems?>?,
    private val mListener: NewsFragment.NewsFragmentInterface?,
    private val newsFragment: NewsFragment
) :
    RecyclerView.Adapter<NewsSearchListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.blog_search_single_list_items, parent, false)
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, newsFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: RowsItems?,
            mListener: NewsFragment.NewsFragmentInterface?,
            newsFragment: NewsFragment
        ) {
            val rltSearch = itemView.rltSearch
            val txtBlogTitle = itemView.txtBlogTitle
            txtBlogTitle.text = items!!.title
            rltSearch.setOnClickListener {
                val intent = Intent()
                intent.action = AppConstants.REFERESH_NEWS
                LocalBroadcastManager.getInstance(itemView.context).sendBroadcast(intent)
                Utils.hideKeyBoard(itemView.context as Activity)
                val bundle = Bundle()
                bundle.putString("blogId", items.id)
                bundle.putString("slug", items.slug)
                mListener?.openNewsDetailFragment(bundle)
            }
        }
    }
}