package com.dreams.readingmate.data.adapter

import android.view.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.ui.home.shoping.ShopTabFragment
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.child_membership_single_list_items.view.*
import kotlinx.android.synthetic.main.child_single_list_items.view.cardViewChild
import kotlinx.android.synthetic.main.progress_single_list_items.view.txtChildName

class ChildListForMembershipAdapter(

    private val itemList: ArrayList<Model.childList>,
    private val mListener: ShopTabFragment.ShopTabFragmentInterface?,
    private val shopTabFragment: ShopTabFragment
) :
    RecyclerView.Adapter<ChildListForMembershipAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.child_membership_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList[position], mListener, shopTabFragment, position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: Model.childList,
            mListener: ShopTabFragment.ShopTabFragmentInterface?,
            shopTabFragment: ShopTabFragment,
            position: Int
        ) {
            val cardViewChild = itemView.cardViewChild
            val roundImageChild = itemView.roundImageChild
            val txtChildName = itemView.txtChildName
            if (position == 0) {
                txtChildName.text = "Readingmate"
                roundImageChild.setImageResource(R.drawable.ic_simple_logo_img)
            } else {
                txtChildName.text = items.name
                items.image.let {
                    Utils.displayCircularImage(
                        itemView.context,
                        roundImageChild,
                        it
                    )
                }
            }
            cardViewChild.setOnClickListener {
                items.isSelected = !items.isSelected
                shopTabFragment.callRecommendAPi(items.id, position)
            }
            if (items.isSelected) {
                cardViewChild.strokeColor =
                    ContextCompat.getColor(itemView.context, R.color.color_active_button)
            } else {
                cardViewChild.strokeColor =
                    ContextCompat.getColor(itemView.context, R.color.gray_hint)
            }
        }
    }
}