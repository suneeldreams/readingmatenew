package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class CancelSubscriptionData(
    @field:SerializedName("message")
    var messae: String? = null
)