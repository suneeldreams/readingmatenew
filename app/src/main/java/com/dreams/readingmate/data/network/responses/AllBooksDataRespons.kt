package com.dreams.readingmate.data.network.responses

data class AllBooksDataRespons(
    val status: Boolean?,
    val message: String?,
    val data: AllBooksData?
)