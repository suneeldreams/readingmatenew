package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.RowsItems
import com.dreams.readingmate.ui.home.news.NewsFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.news_tab_single_list_items.view.*

class NewsListAdapter(
    private val itemList: List<RowsItems?>?,
    private val mListener: NewsFragment.NewsFragmentInterface?,
    private val newsFragment: NewsFragment
) :
    RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.news_tab_single_list_items, parent, false)
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, newsFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: RowsItems?,
            mListener: NewsFragment.NewsFragmentInterface?,
            newsFragment: NewsFragment
        ) {
            val cardView = itemView.cardView
            val imgNews = itemView.imgNews
            val imgFavorite = itemView.imgFavorite
            val txtName = itemView.txtName
            val txtTime = itemView.txtTime
            val txtNewsTitle = itemView.txtNewsTitle
            val txtDesc = itemView.txtDesc
            txtName.text = "ReadingMate"
            txtTime.text = Utils.getDateForm(items?.createdAt!!) + " . " + items.categoryName
            txtNewsTitle.text = items.title
            txtDesc.text = items.shortDesc
            if (items.isFavorite) {
                imgFavorite.setImageResource(R.drawable.bookmark)
            } else {
                imgFavorite.setImageResource(R.drawable.ic_unbookmark)
            }
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it, imgNews, R.drawable.ic_place_holder
                )
            }
            imgFavorite.setOnClickListener {
                newsFragment.removeBookMark(items.id)
            }
            cardView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("blogId", items.id)
                bundle.putString("slug", items.slug)
                mListener?.openNewsDetailFragment(bundle)
            }
        }
    }
}