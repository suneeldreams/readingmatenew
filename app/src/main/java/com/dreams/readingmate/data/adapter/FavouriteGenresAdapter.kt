package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavGeneresData
import com.dreams.readingmate.ui.home.favouritegenres.FavouriteGenresFragment
import kotlinx.android.synthetic.main.favourite_genres_single_list_items.view.*

class FavouriteGenresAdapter(
    private val itemList: List<FavGeneresData?>?,
    private val mListener: FavouriteGenresFragment.FavouriteGenresFragmentInterface,
    private val favouriteGenresFragment: FavouriteGenresFragment
) :
    RecyclerView.Adapter<FavouriteGenresAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_genres_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, favouriteGenresFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavGeneresData?,
            mListener: FavouriteGenresFragment.FavouriteGenresFragmentInterface?,
            favouriteGenresFragment: FavouriteGenresFragment
        ) {
//            val cardViewGenres = itemView.cardViewGenres
            itemView.txtGenres.text = items!!.genreName
            if (items.isFavourite == true) {
                itemView.cardViewGenres.strokeWidth = 2
                itemView.cardViewGenres.radius = 10F
                itemView.cardViewGenres.strokeColor = ContextCompat.getColor(
                    itemView.context,
                    R.color.color_active_button
                )
                itemView.cardViewGenres.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.color_active_button
                    )
                )
//                itemView.cardViewGenres.setBackgroundResource(R.drawable.active_round_bg)
                itemView.txtGenres.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_weak_tick,
                    0,
                    0,
                    0
                )
            } else {
                itemView.cardViewGenres.strokeWidth = 2
                itemView.cardViewGenres.radius = 10F
                itemView.cardViewGenres.strokeColor = ContextCompat.getColor(
                    itemView.context,
                    R.color.dark_gray
                )
                itemView.cardViewGenres.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.white
                    )
                )
//                itemView.cardViewGenres.setBackgroundResource(R.drawable.gray_border_with_whit_round_bg)

            }
            itemView.setOnClickListener {
                if (items.isFavourite == true) {
                    items.isFavourite = false
                } else {
                    items.isFavourite = true
                }
                /*itemView.cardViewGenres.setBackgroundResource(R.drawable.active_round_bg)
                itemView.txtGenres.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_weak_tick,
                    0,
                    0,
                    0
                )*/
                favouriteGenresFragment.refreshAdapter(items.genreName!!)
            }
        }
    }
}