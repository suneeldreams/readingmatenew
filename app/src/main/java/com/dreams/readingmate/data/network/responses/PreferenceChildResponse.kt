package com.dreams.readingmate.data.network.responses

data class PreferenceChildResponse(
    val status: Boolean?,
    val message: String?,
    val data: PreferenceChildItems?
)