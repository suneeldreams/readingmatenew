package com.dreams.readingmate.data.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.ui.home.dashboard.HomeFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.home_single_list_items.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ChildListAdapter(
    private val itemList: List<ChildListItem?>?,
    private val mListener: HomeFragment.HomeFragmentInterface,
    private val homeFragment: HomeFragment,
    private val preferenceChild: PreferenceChildItems?,
    private val firstTime: Boolean,
    private val changeStatus: String
) :
    RecyclerView.Adapter<ChildListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.home_single_list_items, parent, false)
        val adapter = ViewPagerAdapter()
        val viewPager = v.viewPager
        viewPager.adapter = adapter
        return ViewHolder(v)
    }

    enum class SwipedState {
        SHOWING_PRIMARY_CONTENT,
        SHOWING_SECONDARY_CONTENT
    }

    var mItemSwipedStates: MutableList<SwipedState>? = null
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mItemSwipedStates = ArrayList()
        for (i in itemList!!.indices) {
            mItemSwipedStates?.add(i, SwipedState.SHOWING_PRIMARY_CONTENT)
        }
        holder.bindItems(
            itemList[position],
            mListener,
            homeFragment,
            mItemSwipedStates,
            preferenceChild,
            position,
            firstTime,
            changeStatus
        )
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("ResourceType")
        fun bindItems(
            items: ChildListItem?,
            mListener: HomeFragment.HomeFragmentInterface?,
            homeFragment: HomeFragment,
            mItemSwipedStates: MutableList<SwipedState>?,
            preferenceChildItem: PreferenceChildItems?,
            position: Int,
            firstTime: Boolean,
            changeStatus: String
        ) {
            val rlt1 = itemView.rlt1
            val rlt2 = itemView.rlt2
            val rlt3 = itemView.rlt3
            val rlt4 = itemView.rlt4
//            val cardViewHome = itemView.cardViewHome
            val txtAchievementFour = itemView.txtAchievementFour
            val txtAchievementThree = itemView.txtAchievementThree
            val txtAchievementTwo = itemView.txtAchievementTwo
            val txtAchievementOne = itemView.txtAchievementOne
            txtAchievementOne.text = items!!.achievements!!.ach1Rank
            txtAchievementTwo.text = items.achievements!!.ach2Rank
            txtAchievementFour.text = items.achievements.ach4Rank
            txtAchievementThree.text = items.achievements.ach3Rank

            if (position == 0) {
                (itemView.context as HomeActivity).childId = items.id!!
            }
            if (changeStatus != "status") {
                if (!firstTime) {
                    if (items.achievements.ach1NextRank != "0" && items.achievements.ach1NextMilestone != "0") {
                        if (items.achievements.ach1NextRank == items.achievements.ach1NextMilestone) {
                            val title = "Total days milestone"
                            val rank = items.achievements.ach1NextRank
                            val milesTone = items.achievements.ach1NextNextMilestone
                            (itemView.context as HomeActivity).nextMilsToneValue = rank!!
                            val milestone1 =
                                items.childName + " " + "has read for a total of " + rank + " days, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days of reading."
                            achMatch(
                                itemView.context,
                                milestone1,
                                title,
                                items,
                                items.achievements.ach2NextRank,
                                items.achievements.ach2NextNextMilestone,
                                items.childName,
                                items.achievements.ach2Rank
                            )
//                    AcchivMent2(items.achievements.ach2NextRank,items.achievements.ach2NextMilestone,items.childName)
                            /*Handler().postDelayed({
                                if (items.achievements.ach2NextRank != "0" && items.achievements.ach2NextMilestone != "0") {
                                    if (items.achievements.ach2NextRank == items.achievements.ach2NextMilestone) {
                                        val title = "Total days in a row milestone"
                                        val rank = items.achievements.ach2NextRank
                                        val milesTone = items.achievements.ach2NextMilestone
                                        val milestone2 =
                                            items.childName + " " + "has read for a total of " + rank + " days in a row, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days reading in a row."
                                        ach2Match(itemView.context, milestone2, title)
                                    }
                                }
                            }, 500)*/
                        }
                    } else {
                        AcchivMent2(
                            items.achievements.ach2NextRank,
                            items.achievements.ach2NextMilestone,
                            items.childName
                        )
                        /*if (items.achievements.ach2NextRank != "0" && items.achievements.ach2NextMilestone != "0") {
                            if (items.achievements.ach2NextRank == items.achievements.ach2NextMilestone) {
                                val title = "Total days in a row milestone"
                                val rank = items.achievements.ach2NextRank
                                val milesTone = items.achievements.ach2NextMilestone
                                val milestone2 =
                                    items.childName + " " + "has read for a total of " + rank + " days in a row, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days reading in a row."
                                ach2Match(itemView.context, milestone2, title)
                            }
                        }*/
                    }
                }
            }



            rlt1.setOnClickListener {
                val title = "Total days milestone"
                val rank = items.achievements.ach1Rank
                val milesTone = items.achievements.ach1NextMilestone
                val milestone1 =
                    items.childName + " " + "has read for a total of " + rank + " days, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days of reading."
                if (!items.achievements.ach1Rank.equals("0")) {
                    openMilestone1Dialog(
                        itemView.context,
                        milestone1,
                        title
                    )
                } else {
                    openAchivementDialogLevel4(
                        itemView.context,
                        "hasn’t reviewed any books yet",
                        items.achievements.ach1Rank,
                        items.childName
                    )
                }

            }
            rlt2.setOnClickListener {
                val title = "Total days in a row milestone"
                val rank = items.achievements.ach2Rank
                val milesTone = items.achievements.ach2NextMilestone
                val milestone2 =
                    items.childName + " " + "has read for a total of " + rank + " days in a row, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days reading in a row."
                if (!items.achievements.ach2Rank.equals("0")) {
                    openMilestone2Dialog(
                        itemView.context,
                        milestone2,
                        title
                    )
                } else {
                    openAchivementDialogLevel4(
                        itemView.context,
                        "hasn’t reviewed any books yet",
                        items.achievements.ach1Rank,
                        items.childName
                    )
                }
            }
            rlt3.setOnClickListener {
                val title = "Total books read milestone"
                val rank = items.achievements.ach3Rank
                val milesTone = items.achievements.ach3NextMilestone
                val milestone3 =
                    items.childName + " " + "has read a total of " + rank + " books, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " books read."
                if (!items.achievements.ach3Rank.equals("0")) {
                    openMilestone3Dialog(
                        itemView.context,
                        milestone3,
                        title
                    )
                } else {
                    openAchivementDialogLevel4(
                        itemView.context,
                        "hasn’t reviewed any books yet",
                        items.achievements.ach1Rank,
                        items.childName
                    )
                }
            }
            rlt4.setOnClickListener {
                val title = "Total books reviewed milestone"
                val rank = items.achievements.ach4Rank
                val milesTone = items.achievements.ach4NextMilestone
                val milestone4 =
                    items.childName + " " + "has reviewed a total of " + rank + " books, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " books reviewed."
                val disc = "hasn’t reviewed any books yet"
                if (!items.achievements.ach4Rank.equals("0")) {
                    openMilestone4Dialog(
                        itemView.context,
                        milestone4,
                        title
                    )
                } else {
                    openAchivementDialogLevel4(
                        itemView.context,
                        disc,
                        items.achievements.ach1Rank,
                        items.childName
                    )
                }
            }

            val habbitProgress = itemView.habbitProgress
            val runStreakProgress = itemView.runStreakProgress
            val txtProgress = itemView.txtProgress
            val txtProgressCount = itemView.txtProgressCount
            val imgUser = itemView.imgUser
            val imgEditChild = itemView.imgEditChild
            val rltViewProgress = itemView.rltViewProgress
            val txtEditDetails = itemView.txtEditDetails
            val primaryContentCardView1 = itemView.primaryContentCardView1
            val secondaryContentFrameLayout = itemView.secondaryContentFrameLayout
            val imgDeleteChild = itemView.imgDeleteChild
            val rltAddFirstBook = itemView.rltAddFirstBook
            val txtViewDetail = itemView.txtViewDetail
            val txtCurrentlyReading = itemView.txtCurrentlyReading
            val parentLayout = itemView.lnrDays
            val readingBook = items.currentlyReadingBook
            if (readingBook!!.title != null) {
                txtCurrentlyReading.visibility = View.VISIBLE
                txtCurrentlyReading.text = readingBook.title
                txtViewDetail.text = itemView.context.getString(R.string.complete_book)
            } else {
                txtCurrentlyReading.visibility = View.GONE
                txtViewDetail.text = itemView.context.getString(R.string.add_a_book)
            }
            habbitProgress.max = 100
            runStreakProgress.max = 7
            txtProgress.text = items.readingHabit.toString() + "%"
            txtProgressCount.text = items.week_run_streak + "/" + "7"
            habbitProgress.progress = items.readingHabit!!.toInt()
            runStreakProgress.progress = items.runStreak!!.toInt()

            itemView.txtChildName.text = items.childName
            itemView.txtChildNickName.text = items.nickName
            Utils.displayCircularImage(itemView.context, imgUser, items.image!!)
            val layoutInflater =
                itemView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            parentLayout.removeAllViews()
            val data = items.weekData
            for (i in 0 until data!!.size) {
                val view: View =
                    layoutInflater.inflate(R.layout.week_single_item, parentLayout, false)
                val lnrWeekTick = view.findViewById<LinearLayout>(R.id.lnrWeekTick)
                val txtDay = view.findViewById<TextView>(R.id.txtWeekName)
                val imgTick = view.findViewById<ImageView>(R.id.imgCircle)
                val txtWeekDate = view.findViewById<TextView>(R.id.txtWeekDate)
                txtDay.text = data[i]!!.day
                val dates = data[i]!!.date
                val createdAt = items.createdAt
                val sdf: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                val strDate = sdf.parse(createdAt)
                val weekdate = sdf.parse(dates)
                if (strDate > weekdate) {
                    data[i]!!.isSelected = true
                    Log.d("small_date", weekdate.toString())
                } else {
                    data[i]!!.isSelected = false
                    Log.d("Big_date", weekdate.toString())
                }

                if (data[i]!!.isRead == true) {
//                    imgTick.setImageResource(R.drawable.ic_check)
                    val color = items.favColour
                    when (color) {
                        "Black" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.BLACK)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_black)
                        }
                        "White" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.WHITE)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_white)
                        }
                        "Pink" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(
                                4,
                                ContextCompat.getColor(itemView.context, R.color.pink)
                            )
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_pink)
                        }
                        "Blue" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.BLUE)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_blue)
                        }
                        "Green" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.GREEN)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_green)
                        }
                        "Grey" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.GRAY)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_gray)
                        }
                        "Purple" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(
                                4,
                                ContextCompat.getColor(itemView.context, R.color.purple)
                            )
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_purple)
                        }
                        "Red" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(4, Color.RED)
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_red)
                        }
                        "Yellow" -> {
                            val f = 15
                            val drawable = GradientDrawable()
                            drawable.shape = GradientDrawable.RECTANGLE
                            drawable.setStroke(
                                4,
                                Color.YELLOW
                            )
                            drawable.cornerRadius = f.toFloat()
                            drawable.setColor(Color.WHITE)
                            primaryContentCardView1.setBackgroundDrawable(drawable)
                            imgTick.setImageResource(R.drawable.ic_check_yellow)
                        }
                    }
                } else {
                    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val newDate = df.parse(data[i]!!.date)
                    val df2: DateFormat = SimpleDateFormat("dd")
                    val date = df2.format(newDate)
                    imgTick.setImageResource(R.drawable.ic_weak_circle)
                    txtWeekDate.text = date
                }

                lnrWeekTick.setOnClickListener {
                    if (data[i]!!.isRead != true) {
                        homeFragment.updateTick(
                            items.id,
                            items.currentlyReadingBook.bookshelfId,
                            data[i]!!.date,
                            false
                        )

                    } else {
                        homeFragment.removeTick(data[i]!!.id, items.id)
                    }
                }

                parentLayout?.addView(view)
            }
            val f = 15
            val drawable = GradientDrawable()
            drawable.shape = GradientDrawable.RECTANGLE
            drawable.setStroke(4, ContextCompat.getColor(itemView.context, R.color.black_olive))
            drawable.cornerRadius = f.toFloat()
            drawable.setColor(ContextCompat.getColor(itemView.context, R.color.black_olive))
            secondaryContentFrameLayout.setBackgroundDrawable(drawable)

            val color = items.favColour
            when (color) {
                "Black" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(4, ContextCompat.getColor(itemView.context, R.color.red_new))
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "White" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        ContextCompat.getColor(itemView.context, R.color.pink_new)
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Pink" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        ContextCompat.getColor(itemView.context, R.color.yellow_new)
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Blue" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        ContextCompat.getColor(itemView.context, R.color.green_new)
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Green" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        ContextCompat.getColor(itemView.context, R.color.jelly_fish)
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Grey" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        ContextCompat.getColor(itemView.context, R.color.blue_new)
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Purple" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(4, ContextCompat.getColor(itemView.context, R.color.purple))
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Red" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(4, Color.RED)
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
                "Yellow" -> {
                    val f = 15
                    val drawable = GradientDrawable()
                    drawable.shape = GradientDrawable.RECTANGLE
                    drawable.setStroke(
                        4,
                        Color.YELLOW
                    )
                    drawable.cornerRadius = f.toFloat()
                    drawable.setColor(Color.WHITE)
                    primaryContentCardView1.setBackgroundDrawable(drawable)
                }
            }
            /*rltAddFirstBook.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("childId", items.id)
                mListener?.openBookShelfFragment(bundle)
            }*/
            /*Currently reading books click event*/
            /*  txtCurrentlyReading.setOnClickListener {
                  val bundle = Bundle()
                  bundle.putString("childId", items.id)
                  mListener?.openBookShelfFragment(bundle)
              }*/
            rltAddFirstBook.setOnClickListener {
                if (txtViewDetail.text.toString() == "Add a book") {
                    val bundle = Bundle()
                    bundle.putString("childId", items.id)
                    mListener?.openBookShelfFragment(bundle)
                } else {
                    openHardDialog(
                        itemView.context,
                        homeFragment,
                        items.id,
                        items.currentlyReadingBook.bookId
                    )
                }
                //                homeFragment.nextBookRead(items.id)
            }
            val displayMetrics = Resources.getSystem().displayMetrics
            itemView.layoutParams.width = displayMetrics.widthPixels
            itemView.requestLayout()
            itemView.viewPager.currentItem = mItemSwipedStates!![position].ordinal

            itemView.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                var previousPagePosition = 0

                override fun onPageScrolled(
                    pagePosition: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    if (pagePosition == previousPagePosition)
                        return

                    when (pagePosition) {
                        0 -> mItemSwipedStates[position] = SwipedState.SHOWING_PRIMARY_CONTENT
                        1 -> mItemSwipedStates[position] = SwipedState.SHOWING_SECONDARY_CONTENT
                    }
                    previousPagePosition = pagePosition
                    Log.i(
                        "MyAdapter",
                        "PagePosition " + position + " set to " + mItemSwipedStates[position].ordinal
                    )
                }

                override fun onPageSelected(pagePosition: Int) {
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
            imgEditChild.setOnClickListener {
                itemView.viewPager.currentItem = 0
                val editChildItem = items
                val bundle = Bundle()
                bundle.putString("from", "editProfile")
                bundle.putString("childId", items.id)
                bundle.putParcelable("child", preferenceChildItem)
                bundle.putParcelable("editChild", editChildItem)
                mListener?.openAddChildFragment(bundle)
            }
            imgDeleteChild.setOnClickListener {
                itemView.viewPager.currentItem = 0
                homeFragment.removeChildFromList(items.id)
            }
            itemView.rltViewProgress.setOnClickListener {
                (itemView.context as HomeActivity).childId = items.id!!
                val editChildItem = items
                val bundle = Bundle()
                bundle.putString("childId", items.id)
                bundle.putParcelable("history", items)
                bundle.putString("from", "editProfile")
                bundle.putParcelable("child", preferenceChildItem)
                bundle.putParcelable("editChild", editChildItem)
                mListener?.openProgressFragment(bundle)
            }
        }

        private fun openHardDialog(
            context: Context,
            homeFragment: HomeFragment,
            childId: String?,
            bookId: String?
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.hard_popup)
            val imgSmily = dialog.findViewById(R.id.imgSmily) as ImageView
            val btnSubmit = dialog.findViewById(R.id.btnSubmit) as Button
            val txtCancel = dialog.findViewById(R.id.txtCancel) as TextView
            val textOne = dialog.findViewById(R.id.textOne) as TextView
            val textTwo = dialog.findViewById(R.id.textTwo) as TextView
            val textThree = dialog.findViewById(R.id.textThree) as TextView
            val textFour = dialog.findViewById(R.id.textFour) as TextView
            val textFive = dialog.findViewById(R.id.textFive) as TextView
            val lnrOne = dialog.findViewById(R.id.lnrOne) as LinearLayout
            val lnrTwo = dialog.findViewById(R.id.lnrTwo) as LinearLayout
            val lnrThree = dialog.findViewById(R.id.lnrThree) as LinearLayout
            val lnrFour = dialog.findViewById(R.id.lnrFour) as LinearLayout
            val lnrFive = dialog.findViewById(R.id.lnrFive) as LinearLayout
            dialog.show()
            var ratingIs = ""
            imgSmily.setImageResource(R.drawable.ic_straite_smily)
            /*lnrOne.background =
                ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            lnrTwo.background =
                ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            lnrThree.background = ContextCompat.getDrawable(
                context,
                R.drawable.btn_readingmate_round_bg
            )
            imgSmily.setImageResource(R.drawable.ic_straite_smily)
            textOne.setTextColor(ContextCompat.getColor(context, R.color.black))
            textTwo.setTextColor(ContextCompat.getColor(context, R.color.black))
            textThree.setTextColor(ContextCompat.getColor(context, R.color.white))
            textFour.setTextColor(ContextCompat.getColor(context, R.color.black))
            textFive.setTextColor(ContextCompat.getColor(context, R.color.black))
            lnrFour.background =
                ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            lnrFive.background =
                ContextCompat.getDrawable(context, R.drawable.gray_border_bg)*/
            btnSubmit.setOnClickListener {
                if (ratingIs.isNotEmpty()) {
                    homeFragment.hardBookRating(childId, bookId, ratingIs)
                    dialog.dismiss()
                } else {
                    AlertUtils.showToast(context, "Please rate this.")
                }
            }
            txtCancel.setOnClickListener {
                dialog.dismiss()
            }
            lnrOne.setOnClickListener {
                lnrOne.background =
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.btn_readingmate_round_bg
                    )
                ratingIs = "1"
                imgSmily.setImageResource(R.drawable.ic_happy_smily)
                textOne.setTextColor(ContextCompat.getColor(context, R.color.white))
                textTwo.setTextColor(ContextCompat.getColor(context, R.color.black))
                textThree.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFour.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFive.setTextColor(ContextCompat.getColor(context, R.color.black))
                lnrTwo.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrThree.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFour.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFive.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            }
            lnrTwo.setOnClickListener {
                lnrOne.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrTwo.background = ContextCompat.getDrawable(
                    context,
                    R.drawable.btn_readingmate_round_bg
                )
                ratingIs = "2"
                imgSmily.setImageResource(R.drawable.ic_happy_smily)
                textOne.setTextColor(ContextCompat.getColor(context, R.color.black))
                textTwo.setTextColor(ContextCompat.getColor(context, R.color.white))
                textThree.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFour.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFive.setTextColor(ContextCompat.getColor(context, R.color.black))
                lnrThree.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFour.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFive.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            }
            lnrThree.setOnClickListener {
                lnrOne.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrTwo.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrThree.background = ContextCompat.getDrawable(
                    context,
                    R.drawable.btn_readingmate_round_bg
                )
                ratingIs = "3"
                imgSmily.setImageResource(R.drawable.ic_straite_smily)
                textOne.setTextColor(ContextCompat.getColor(context, R.color.black))
                textTwo.setTextColor(ContextCompat.getColor(context, R.color.black))
                textThree.setTextColor(ContextCompat.getColor(context, R.color.white))
                textFour.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFive.setTextColor(ContextCompat.getColor(context, R.color.black))
                lnrFour.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFive.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            }
            lnrFour.setOnClickListener {
                lnrOne.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrTwo.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrThree.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFour.background = ContextCompat.getDrawable(
                    context,
                    R.drawable.btn_readingmate_round_bg
                )
                ratingIs = "4"
                imgSmily.setImageResource(R.drawable.ic_sad_smily)
                textOne.setTextColor(ContextCompat.getColor(context, R.color.black))
                textTwo.setTextColor(ContextCompat.getColor(context, R.color.black))
                textThree.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFour.setTextColor(ContextCompat.getColor(context, R.color.white))
                textFive.setTextColor(ContextCompat.getColor(context, R.color.black))
                lnrFive.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
            }
            lnrFive.setOnClickListener {
                lnrOne.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrTwo.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrThree.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFour.background =
                    ContextCompat.getDrawable(context, R.drawable.gray_border_bg)
                lnrFive.background = ContextCompat.getDrawable(
                    context,
                    R.drawable.btn_readingmate_round_bg
                )
                ratingIs = "5"
                imgSmily.setImageResource(R.drawable.ic_sad_smily)
                textOne.setTextColor(ContextCompat.getColor(context, R.color.black))
                textTwo.setTextColor(ContextCompat.getColor(context, R.color.black))
                textThree.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFour.setTextColor(ContextCompat.getColor(context, R.color.black))
                textFive.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
        }

        private fun AcchivMent2(
            ach2NextRank: String?,
            ach2NextMilestone: String?,
            childName: String?
        ) {
            Handler().postDelayed({
                if (ach2NextRank != "0" && ach2NextMilestone != "0") {
                    if (ach2NextRank == ach2NextMilestone) {
                        val title = "Total days in a row milestone"
                        val rank = ach2NextRank
                        val milesTone = ach2NextMilestone
                        val milestone2 =
                            childName + " " + "has read for a total of " + rank + " days in a row, achieving this milestone. Keep up the good work!\n\n" + "The next milestone is " + milesTone + " total days reading in a row."
                        ach2Match(itemView.context, milestone2, title)
                    }
                }
            }, 500)
        }

        private fun openReadDialog(
            context: Context,
            message: String,
            id: String?,
            bookshelfId: String?,
            date: String?,
            homeFragment: HomeFragment
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(context.resources.getString(R.string.app_name))
            alertDialog.setMessage(message)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes)
            ) { dialog, _ ->
                dialog.dismiss()
                homeFragment.updateTick(
                    id,
                    bookshelfId,
                    date,
                    false
                )
            }
            alertDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.no)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun openMilestone1Dialog(
            context: Context,
            milestone: String,
            title: String
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(milestone)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.ok)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun openMilestone2Dialog(
            context: Context,
            milestone: String,
            title: String
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(milestone)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun openMilestone3Dialog(
            context: Context,
            milestone: String,
            title: String
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(milestone)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun openMilestone4Dialog(
            context: Context,
            milestone: String,
            title: String
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(milestone)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun openAchivementDialogLevel4(
            context: Context,
            disc1: String,
            ach1Rank: String?,
            childName: String?

        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle(context.resources.getString(R.string.app_name))
            alertDialog.setMessage("$childName $disc1")
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }

        private fun achMatch(
            context: Context,
            milestone: String,
            title: String,
            items: ChildListItem,
            ach2NextRank: String?,
            ach2NextMilestone: String?,
            childName: String?,
            ach2Rank: String?
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.achivemt_match_dialog)
            val txtTitle = dialog.findViewById(R.id.txtTitle) as TextView
            val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
            val imgCloseDialog = dialog.findViewById(R.id.imgCloseDialog) as ImageView
            txtTitle.text = title
            txtMessage.text = milestone
            dialog.show()
            imgCloseDialog.setOnClickListener {
                dialog.dismiss()
                val value = (context as HomeActivity).nextMilsToneValue
                if (value == ach2Rank) {
                    AcchivMent2(ach2NextRank, ach2NextMilestone, childName)
                }
            }
            /*val t = Timer()
            t.schedule(object : TimerTask() {
                override fun run() {
                    dialog.dismiss()
                    t.cancel()
                }
            }, 5000)*/
        }

        private fun ach2Match(
            context: Context,
            milestone: String,
            title: String
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.achivemt_match_dialog)
            val txtTitle = dialog.findViewById(R.id.txtTitle) as TextView
            val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
            val imgCloseDialog = dialog.findViewById(R.id.imgCloseDialog) as ImageView
            txtTitle.text = title
            txtMessage.text = milestone
            imgCloseDialog.setOnClickListener {
                dialog.dismiss()
            }
            /*  val t = Timer()
              t.schedule(object : TimerTask() {
                  override fun run() {
                      dialog.dismiss() // when the task active then close the dialog
                      t.cancel() // also just top the timer thread, otherwise, you may receive a crash report
                  }
              }, 5000)*/
            dialog.show()
        }

        /*private fun openDialogue(
            context: Context,
            rank: String?,
            disc1: String,
            disc2: String,
            disc3: String,
            childName: String
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.achievment_dialog)

            val displayRectangle = Rect()
            val window = (context as Activity).window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            window.setGravity(Gravity.CENTER_VERTICAL)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = (displayRectangle.width() * 0.95f).toInt()
            lp.height = (displayRectangle.height() * 0.95f).toInt()
            dialog.window!!.attributes = lp
            dialog.show()

            val txtRank = dialog.findViewById(R.id.txtRank) as TextView
            val txtDiscription = dialog.findViewById(R.id.txtDiscription) as TextView
            val txtOk = dialog.findViewById(R.id.txtOk) as TextView
            txtRank.text = "Your level is: " + rank
            txtDiscription.text = disc
            txtOk.setOnClickListener {
                dialog.dismiss()
            }
        }*/
        /* fun compareDate(){
             val sdf = SimpleDateFormat("dd/MM/yyyy")
             val strDate = sdf.parse(my_date)
             your_date_is_outdated = System.currentTimeMillis() > strDate.time
         }*/
    }

    inner class ViewPagerAdapter : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {

            var resId = 0
            when (position) {
                0 -> resId = R.id.primaryContentCardView1
                1 -> resId = R.id.secondaryContentFrameLayout
            }
            return collection.findViewById(resId)
        }

        override fun getCount(): Int {
            return 2
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as View
        }
    }
}