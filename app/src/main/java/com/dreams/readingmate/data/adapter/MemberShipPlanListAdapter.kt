package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.SubscriptionPlanListData
import com.dreams.readingmate.ui.home.subscription.SubscriptionFragment
import kotlinx.android.synthetic.main.membership_plan_single_items.view.*

class MemberShipPlanListAdapter(
    private val mListener: SubscriptionFragment.SubscriptionFragmentInterface,
    private val itemList: List<SubscriptionPlanListData>
) :
    RecyclerView.Adapter<MemberShipPlanListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.membership_plan_single_items, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList[position], mListener, position
        )
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: SubscriptionPlanListData,
            mListener: SubscriptionFragment.SubscriptionFragmentInterface,
            position: Int
        ) {
            val lnrMemberShipPlan = itemView.lnrMemberShipPlan
            val txtPlanName = itemView.txtPlanName
            val txtPlanAmount = itemView.txtPlanAmount
            val txtUpFrontAmount = itemView.txtUpFrontAmount
            val txtPlanDescription = itemView.txtPlanDescription
            val txtMembership = itemView.txtMembership
            val imgBookPerMonth = itemView.imgBookPerMonth
            val imgBookChoose = itemView.imgBookChoose
            txtPlanName.text = items.planName
            txtPlanDescription.text = items.planDescription
            txtPlanAmount.text =
                itemView.context.getString(R.string.currency_pound) + items.planAmount + "/" + items.planInterval

            txtUpFrontAmount.text = "Or " +
                    itemView.context.getString(R.string.currency_pound) + items.upfrontAmount + " " + "Upfront"

            when (position) {
                0 -> {
                    lnrMemberShipPlan.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.membership_backgroud_with_border
                    )
                    txtPlanAmount.setTextColor(itemView.context.resources.getColor(R.color.color_active_button))
                    txtMembership.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.btn_login_round_bg
                    )
                    imgBookPerMonth.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_red_tick
                        )
                    )
                    imgBookChoose.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_red_tick
                        )
                    )
                    /*imgBookPerMonth.setColorFilter(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.color_active_button
                        ), android.graphics.PorterDuff.Mode.MULTIPLY
                    )
                    imgBookChoose.setColorFilter(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.color_active_button
                        ), android.graphics.PorterDuff.Mode.MULTIPLY
                    )*/
                }
                1 -> {
                    lnrMemberShipPlan.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.membership_blue_box_backgroud_with_border
                    )
                    txtPlanAmount.setTextColor(itemView.context.resources.getColor(R.color.color_slate))
                    txtMembership.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.btn_blue_round_bg
                    )
                    imgBookPerMonth.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_blue_tick
                        )
                    )
                    imgBookChoose.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_blue_tick
                        )
                    )
                    /*imgBookPerMonth.setColorFilter(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.color_slate
                        ), android.graphics.PorterDuff.Mode.MULTIPLY
                    )
                    imgBookChoose.setColorFilter(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.color_slate
                        ), android.graphics.PorterDuff.Mode.MULTIPLY
                    )*/
                }
                2 -> {
                    lnrMemberShipPlan.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.membership_yellow_box_backgroud_with_border
                    )
                    txtPlanAmount.setTextColor(itemView.context.resources.getColor(R.color.color_stroke_new))
                    txtMembership.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.btn_yellow_round_bg
                    )
                    imgBookPerMonth.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_yellow_tick
                        )
                    )
                    imgBookChoose.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_yellow_tick
                        )
                    )
                    /* imgBookPerMonth.setColorFilter(
                         ContextCompat.getColor(
                             itemView.context,
                             R.color.color_stroke
                         ), android.graphics.PorterDuff.Mode.MULTIPLY
                     )
                     imgBookChoose.setColorFilter(
                         ContextCompat.getColor(
                             itemView.context,
                             R.color.color_stroke
                         ), android.graphics.PorterDuff.Mode.MULTIPLY
                     )*/
                }
            }


            txtMembership.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("from", "membership")
                bundle.putDouble("planAmount", items.planAmount!!)
                bundle.putString("planId", items.id)
                bundle.putBoolean("isUpfront", false)
                mListener.openCheckoutFragments(bundle)
            }
        }
    }
}