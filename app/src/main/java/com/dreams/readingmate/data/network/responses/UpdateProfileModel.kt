package com.dreams.readingmate.data.network.model


data class UpdateProfileModel(
    var name: String?,
    var mobile: String?,
    var email: String?,
    var gender: String?,
    var charity: String?,
    var img: String?
)