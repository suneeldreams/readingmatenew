package com.dreams.readingmate.data.network.responses

data class ChildListResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<ChildListItem>?
)