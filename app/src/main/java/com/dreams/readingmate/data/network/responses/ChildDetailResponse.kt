package com.dreams.readingmate.data.network.responses

data class ChildDetailResponse(
    val status: Boolean?,
    val message: String?,
    val data: ChildDetails?
)