package com.dreams.readingmate.data.network.responses


data class HardRatingRequestModel(
    var bookHardRating: String?
)