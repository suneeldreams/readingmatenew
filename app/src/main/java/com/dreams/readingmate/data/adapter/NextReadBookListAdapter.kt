package com.dreams.readingmate.data.adapter

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.dashboard.HomeFragment
import com.dreams.readingmate.ui.home.dashboard.NextBookReadPopupFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.search_grid_single_item.view.*

class NextReadBookListAdapter(
    private val itemList: List<FavBookData>,
    private val mListener: NextBookReadPopupFragment.HomeFragmentInterface?,
    private val childId: String,
    private val dialog: Dialog
) :
    RecyclerView.Adapter<NextReadBookListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.next_read_grid_single_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList.get(position), mListener, childId, dialog)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData,
            mListener: NextBookReadPopupFragment.HomeFragmentInterface?,
            childId: String,
            dialog: Dialog
        ) {
            val imgSearchBooks = itemView.imgSearchBooks
            val txtBookTitle = itemView.txtBookTitle
            txtBookTitle.text = items.title
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgSearchBooks,
                    R.drawable.ic_place_holder
                )
            }
            imgSearchBooks.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("childId", childId)
                bundle.putString("bookId", items.id)
                mListener?.openBookDetailFragment(bundle)
//                dialog.dismiss()
            }
        }
    }
}