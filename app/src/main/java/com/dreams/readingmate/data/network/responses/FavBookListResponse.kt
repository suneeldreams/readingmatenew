package com.dreams.readingmate.data.network.responses

data class FavBookListResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<FavBookData>?
)