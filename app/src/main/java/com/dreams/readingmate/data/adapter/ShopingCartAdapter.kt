package com.dreams.readingmate.data.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.CartItem
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.ui.home.shoping.ShopingFragment
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.cartlist_single_list_items.view.*
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*

class ShopingCartAdapter(
    private val itemList: ArrayList<CartItem>,
    private val mListener: ShopingFragment.ShopingFragmentInterface?,
    private val shopingFragment: ShopingFragment
) :
    RecyclerView.Adapter<ShopingCartAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.cartlist_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList.get(position), mListener, position, shopingFragment)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: CartItem,
            mListener: ShopingFragment.ShopingFragmentInterface?,
            position: Int,
            shopingFragment: ShopingFragment
        ) {
            val productQuantity = itemView.quantity
            val courseImg = itemView.courseImg
            val courseName = itemView.courseName
            val txtBookDescription = itemView.txtBookDescription
            val txtBookPrice = itemView.txtBookPrice
            val imgClose = itemView.imgClose
            val negative = itemView.negative
            val positive = itemView.positive
            courseName.text = items.bookName
            txtBookDescription.text = items.description
            txtBookPrice.text = itemView.resources.getString(R.string.currency_pound) + items.price
            productQuantity.text = items.quantity.toString()
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    courseImg,
                    R.drawable.ic_place_holder
                )
            }
            positive.setOnClickListener {
                (itemView.context as HomeActivity).increaseQuantity(position)
                shopingFragment.refreshAdapter()
            }
            negative.setOnClickListener {
                if (items.quantity == 1) {
                    openRemoveItemDialog(itemView.context, position, mListener, shopingFragment)
                } else {
                    (itemView.context as HomeActivity).decreaseQuantity(position)
                    shopingFragment.refreshAdapter()
                }
            }
        }

        private fun openRemoveItemDialog(
            context: Context?,
            position: Int,
            mListener: ShopingFragment.ShopingFragmentInterface?,
            shopingFragment: ShopingFragment
        ) {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle(context!!.resources.getString(R.string.app_name))
            alertDialog.setMessage(context.resources.getString(R.string.delete_item_msg))
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.yes),
                DialogInterface.OnClickListener { dialog, _ ->
                    (context as HomeActivity).decreaseQuantity(position)
                    shopingFragment.refreshAdapter()
                    val intent = Intent()
                    intent.action = AppConstants.UPDATE_CART_ITEM
                    LocalBroadcastManager.getInstance(itemView.context).sendBroadcast(intent)
                    dialog.dismiss()
                    if ((context).cartItems.isEmpty()) {
                        mListener!!.openBackFragment()
                    }
                })
            alertDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.go_back),
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                })
            alertDialog.show()
        }
    }
}