package com.dreams.readingmate.data.network.responses

data class BooksRespons(
    val status: Boolean?,
    val message: String?,
    val data: BooksDataList?
)