package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class CardListData(

    @field:SerializedName("cardData")
    val cardData: List<CardDataItem?>? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("gender")
    val gender: String? = null,

    @field:SerializedName("mobile")
    val mobile: String? = null,

    @field:SerializedName("childCount")
    val childCount: Int? = null,

    @field:SerializedName("isEmailVerified")
    val isEmailVerified: Boolean? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("statusText")
    val statusText: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("userType")
    val userType: Int? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("addressData")
    val addressData: List<AddressDataItem?>? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null
)

data class AddressDataItem(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("addr2")
    val addr2: String? = null,

    @field:SerializedName("addr1")
    val addr1: String? = null,

    @field:SerializedName("pcode")
    val pcode: String? = null,

    @field:SerializedName("isPrimary")
    val isPrimary: Int? = null,

    @field:SerializedName("addr4")
    val addr4: String? = null,

    @field:SerializedName("addr3")
    val addr3: String? = null,

    @field:SerializedName("mobile")
    val mobile: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("titlename")
    val titlename: String? = null,
    var isSelected: Boolean = false
)
data class CardDataItem(

	@field:SerializedName("nameOnCard")
	val nameOnCard: String? = null,

	@field:SerializedName("cardNumber")
	val cardNumber: String? = null,

	@field:SerializedName("expMonth")
	val expMonth: String? = null,

	@field:SerializedName("expYear")
	val expYear: String? = null,

	@field:SerializedName("pcode")
	val pcode: String? = null,

    @field:SerializedName("CVV")
    var CVV: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,
    var isSelected: Boolean = false
)
