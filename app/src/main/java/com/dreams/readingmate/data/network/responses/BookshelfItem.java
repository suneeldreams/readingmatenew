package com.dreams.readingmate.data.network.responses;

import com.google.gson.annotations.SerializedName;

public class BookshelfItem{

	@SerializedName("readingStatus")
	private int readingStatus;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("totalPageRead")
	private int totalPageRead;

	@SerializedName("_id")
	private String id;

	@SerializedName("updatedAt")
	private String updatedAt;

	@SerializedName("bookId")
	private String bookId;

	public int getReadingStatus(){
		return readingStatus;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getTotalPageRead(){
		return totalPageRead;
	}

	public String getId(){
		return id;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getBookId(){
		return bookId;
	}
}