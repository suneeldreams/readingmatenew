package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.ui.home.favouritesbooks.FavouriteBookFragment
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*
import kotlin.collections.ArrayList

class FavouritBooksAdapter(
    private val itemList: ArrayList<Model.favBookData>,
    private val mListener: FavouriteBookFragment.FavouriteBookFragmentInterface,
    private val favouriteBookFragment: FavouriteBookFragment
) :
    RecyclerView.Adapter<FavouritBooksAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList!!.get(position), mListener, favouriteBookFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: Model.favBookData,
            mListener: FavouriteBookFragment.FavouriteBookFragmentInterface?,
            favouriteBookFragment: FavouriteBookFragment
        ) {
            val imgFavouriteBook = itemView.imgBook
            val imgTick = itemView.imgClose
            items!!.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
            itemView.setOnClickListener {
//                favouriteBookFragment.refreshAdapter(position, mSelectedItems)
            }
        }
    }
}