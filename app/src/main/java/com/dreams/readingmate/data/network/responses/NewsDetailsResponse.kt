package com.dreams.readingmate.data.network.responses

data class NewsDetailsResponse(
    val status: Boolean?,
    val message: String?,
    val data: NewsDetailsData?
)