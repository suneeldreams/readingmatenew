package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class AllBooksData(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("currentlyReadingBook")
    val currentlyReadingBook: CurrentlyReadingBook? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("achievements")
    val achievements: Achievements1? = null,

    @field:SerializedName("gender")
    val gender: String? = null,

    @field:SerializedName("wishlist")
    val wishlist: List<WishlistItem?>? = null,

    @field:SerializedName("reviewedBooks")
    val reviewedBooks: List<ReviewedBooks?>? = null,

    @field:SerializedName("wantToReadBooks")
    val wantToReadBooks: List<WantToReadBooks?>? = null,

    @field:SerializedName("boughtBooks")
    val boughtBooks: List<BoughtBooks?>? = null,

    @field:SerializedName("toReviewBooks")
    val toReviewBooks: List<ToReviewBooks?>? = null,

    @field:SerializedName("readBooks")
    val readBooks: List<ReadBooks?>? = null,

    @field:SerializedName("schoolType")
    val schoolType: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("favCharacters")
    val favCharacters: List<String?>? = null,

    @field:SerializedName("favColour")
    val favColour: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("totalBooksRead")
    val totalBooksRead: Int? = null,

    @field:SerializedName("reminder")
    val reminder: Reminder? = null,

    @field:SerializedName("bookshelf")
    val bookshelf: List<BookshelfItem1?>? = null,

    @field:SerializedName("nickName")
    val nickName: String? = null,

    @field:SerializedName("favGenres")
    val favGenres: List<String?>? = null,

    @field:SerializedName("userId")
    val userId: String? = null,

    @field:SerializedName("run_streak")
    val runStreak: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("favBookIds")
    val favBookIds: List<String?>? = null,

    @field:SerializedName("favSubjects")
    val favSubjects: List<String?>? = null,

    @field:SerializedName("suggestions")
    val suggestions: List<SuggestionsItem?>? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("age")
    val age: Int? = null,

    @field:SerializedName("reading_habit")
    val readingHabit: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ReadBooks(

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bookshelfId")
    var bookshelfId: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)

data class BoughtBooks(

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)
data class ToReviewBooks(

    @field:SerializedName("bookshelfId")
    val bookshelfIdv: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)

data class WishlistItem(

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)

data class ReviewedBooks(
    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class WantToReadBooks(

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bookshelfId")
    val bookshelfId: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)

data class CurrentlyReadingBook1(

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("gcc")
    val gcc: String? = null,

    @field:SerializedName("bookshelfId")
    val bookshelfId: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)

data class Achievements1(

    @field:SerializedName("ach3Rank")
    val ach3Rank: Int? = null,

    @field:SerializedName("ach2Rank")
    val ach2Rank: Int? = null,

    @field:SerializedName("ach1Rank")
    val ach1Rank: Int? = null,

    @field:SerializedName("ach4Rank")
    val ach4Rank: Int? = null
)

data class SuggestionsItem(

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class Reminder(

    @field:SerializedName("endDate")
    val endDate: String? = null,

    @field:SerializedName("kind")
    val kind: String? = null,

    @field:SerializedName("startDate")
    val startDate: String? = null
)

data class BookshelfItem1(

    @field:SerializedName("readingStatus")
    val readingStatus: Int? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("readingHistory")
    val readingHistory: List<Any?>? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("title")
    val title: String? = null
)
