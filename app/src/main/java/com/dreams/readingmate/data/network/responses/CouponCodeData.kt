package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class CouponCodeData(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("couponAmount")
	val couponAmount: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("couponCode")
	val couponCode: String? = null,

	@field:SerializedName("couponExpiryDate")
	val couponExpiryDate: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("couponType")
	val couponType: Int? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
