package com.dreams.readingmate.data.network.responses

data class OrderResponse(
    val status: Boolean?,
    val message: String?,
    val data: OrderData?
)