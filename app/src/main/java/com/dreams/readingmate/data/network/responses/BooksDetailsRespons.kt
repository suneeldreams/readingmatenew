package com.dreams.readingmate.data.network.responses

data class BooksDetailsRespons(
    val status: Boolean?,
    val message: String?,
    val data: BookDetailsDataItems?
)