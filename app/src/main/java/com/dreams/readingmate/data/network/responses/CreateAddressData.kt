package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class CreateAddressData(

    @field:SerializedName("name")
    val name: String? = null
)