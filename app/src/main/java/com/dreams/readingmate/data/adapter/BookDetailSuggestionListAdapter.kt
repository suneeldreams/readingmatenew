package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.SuggestionBooksItem
import com.dreams.readingmate.ui.home.books.BooksDetailsFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.book_single_list_items.view.*
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.imgBook

class BookDetailSuggestionListAdapter(
    private val itemList: List<SuggestionBooksItem?>?,
    private val mListener: BooksDetailsFragment.BooksDetailsFragmentInterface?,
    private val booksDetailsFragment: BooksDetailsFragment
) :
    RecyclerView.Adapter<BookDetailSuggestionListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, booksDetailsFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: SuggestionBooksItem?,
            mListener: BooksDetailsFragment.BooksDetailsFragmentInterface?,
            booksDetailsFragment: BooksDetailsFragment
        ) {
            val imgFavouriteBook = itemView.imgBook
            val txtBookName = itemView.txtBookName
            val rltImagev = itemView.rltImage
            txtBookName.visibility = View.GONE
            rltImagev.setOnClickListener {
                booksDetailsFragment.callBookDatailAPi(items?.id!!)
            }
            items!!.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
        }
    }
}