package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class AchievementData(
    @field:SerializedName("ach1Rank")
    val ach1Rank: String? = null,

    @field:SerializedName("ach2Rank")
    val ach2Rank: String? = null,

    @field:SerializedName("ach3Rank")
    val ach3Rank: String? = null,

    @field:SerializedName("ach4Rank")
    val ach4Rank: String? = null,

    @field:SerializedName("ach1NextRank")
    val ach1NextRank: String? = null,

    @field:SerializedName("ach2NextRank")
    val ach2NextRank: String? = null,

    @field:SerializedName("ach1NextNextMilestone")
    val ach1NextNextMilestone: String? = null,

    @field:SerializedName("ach2NextNextMilestone")
    val ach2NextNextMilestone: String? = null,

    @field:SerializedName("ach1NextMilestone")
    val ach1NextMilestone: String? = null,

    @field:SerializedName("ach2NextMilestone")
    val ach2NextMilestone: String? = null,

    @field:SerializedName("ach3NextMilestone")
    val ach3NextMilestone: String? = null,

    @field:SerializedName("ach4NextMilestone")
    val ach4NextMilestone: String? = null
)