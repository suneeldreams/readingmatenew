package com.dreams.readingmate.data.network.responses

data class BundleResponse(
    val status: Boolean?,
    val message: String?,
    val data: BundleListData?
)