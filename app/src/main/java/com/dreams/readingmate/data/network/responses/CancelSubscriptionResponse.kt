package com.dreams.readingmate.data.network.responses

data class CancelSubscriptionResponse(
    val status: Boolean?,
    val message: String?,
    val data: CancelSubscriptionData?

)