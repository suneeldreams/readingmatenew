package com.dreams.readingmate.data.network.base


/**
 * Base class for all Activities
 * Author: Shivank Trivedi
 * Dated: 20-06-2020
 */

sealed class Entity {

    data class Error(val errorCode: Int, val errorMessage: String) : Entity()
}