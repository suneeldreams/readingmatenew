package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class BooksDataList(

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItem?>? = null
)

data class RowsItem(

	@field:SerializedName("yearOfPublication")
	val yearOfPublication: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("bicClassification")
	val bicClassification: List<Any?>? = null,

	@field:SerializedName("eanNumber")
	val eanNumber: String? = null,

	@field:SerializedName("isbn")
	val isbn: String? = null,

	@field:SerializedName("ukPriceIncVAT")
	val ukPriceIncVAT: Double? = null,

	@field:SerializedName("binding")
	val binding: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("numberOfPages")
	val numberOfPages: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("stock")
	val stock: Int? = null,

	@field:SerializedName("starRating")
	val starRating: Int? = null,

	@field:SerializedName("publicationDate")
	val publicationDate: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("editor")
	val editor: List<Any?>? = null,

	@field:SerializedName("returnsFlag")
	val returnsFlag: String? = null,

	@field:SerializedName("author")
	val author: List<String?>? = null,

	@field:SerializedName("gardnersClassificationCode")
	val gardnersClassificationCode: String? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("shortDescription")
	val shortDescription: String? = null,

	@field:SerializedName("publisherName")
	val publisherName: String? = null,

	@field:SerializedName("isbn13")
	val isbn13: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("countryOfOrigin")
	val countryOfOrigin: String? = null,

	@field:SerializedName("ukPriceExcVAT")
	val ukPriceExcVAT: Double? = null,

	@field:SerializedName("ukPrice")
	val ukPrice: Double? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("ukAvailability")
	val ukAvailability: String? = null
)
