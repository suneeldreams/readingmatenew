package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PreferenceData(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("totalBooksRead")
	val totalBooksRead: Int? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("bookshelf")
	val bookshelf: List<Any?>? = null,

	@field:SerializedName("nickName")
	val nickName: String? = null,

	@field:SerializedName("schoolType")
	val schoolType: String? = null,

	@field:SerializedName("favGenres")
	val favGenres: List<String?>? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("run_streak")
	val runStreak: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("favCharacters")
	val favCharacters: List<String?>? = null,

	@field:SerializedName("favBookIds")
	val favBookIds: List<String?>? = null,

	@field:SerializedName("favSubjects")
	val favSubjects: List<String?>? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("reading_habit")
	val readingHabit: Int? = null,

	@field:SerializedName("favColour")
	val favColour: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
):Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readString(),
		TODO("bookshelf"),
		parcel.readString(),
		parcel.readString(),
		parcel.createStringArrayList(),
		parcel.readString(),
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.createStringArrayList(),
		parcel.createStringArrayList(),
		parcel.createStringArrayList(),
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(country)
		parcel.writeValue(totalBooksRead)
		parcel.writeString(img)
		parcel.writeString(gender)
		parcel.writeString(nickName)
		parcel.writeString(schoolType)
		parcel.writeStringList(favGenres)
		parcel.writeString(userId)
		parcel.writeString(createdAt)
		parcel.writeValue(runStreak)
		parcel.writeValue(V)
		parcel.writeString(name)
		parcel.writeStringList(favCharacters)
		parcel.writeStringList(favBookIds)
		parcel.writeStringList(favSubjects)
		parcel.writeString(id)
		parcel.writeValue(age)
		parcel.writeValue(readingHabit)
		parcel.writeString(favColour)
		parcel.writeValue(status)
		parcel.writeString(updatedAt)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<PreferenceData> {
		override fun createFromParcel(parcel: Parcel): PreferenceData {
			return PreferenceData(parcel)
		}

		override fun newArray(size: Int): Array<PreferenceData?> {
			return arrayOfNulls(size)
		}
	}
}
