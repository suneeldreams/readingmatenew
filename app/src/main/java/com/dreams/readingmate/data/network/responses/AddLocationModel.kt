package com.dreams.readingmate.data.network.responses

data class AddLocationModel(

    var fullName: String? = null,

    var mobile: String? = null,

    var pincode: String? = null,

    var townCity: String? = null,

    var price: String? = null,

    var country: String? = null,

    var company: String? = null,

    var village: String? = null

)