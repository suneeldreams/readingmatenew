package com.dreams.readingmate.data.adapter

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.progress.BookShelfFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*
import java.util.ArrayList

class SearchBookShelfAdapter(
    private val itemList: List<FavBookData?>?,
    private val mListener: BookShelfFragment.BookShelfFragmentInterface,
    private val bookShelfFragment: BookShelfFragment,
    private val childId: String
) :
    RecyclerView.Adapter<SearchBookShelfAdapter.ViewHolder>() {
    lateinit var mSelectedItems: ArrayList<String>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, bookShelfFragment, childId)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData?,
            mListener: BookShelfFragment.BookShelfFragmentInterface,
            bookShelfFragment: BookShelfFragment,
            childId: String
        ) {
            val tv_label = itemView.txtBooksName
            val imgBook = itemView.imgBook
            tv_label.text = items!!.title
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgBook,
                    R.drawable.ic_place_holder
                )
            }
            itemView.setOnClickListener {
                openConfirmDialog(
                    itemView.context,
                    items.title,
                    items.id,
                    bookShelfFragment,
                    childId
                )
//                bookShelfFragment.AddImageToStringArray(items.id)
            }
        }

        private fun openConfirmDialog(
            context: Context,
            title: String?,
            id: String?,
            bookShelfFragment: BookShelfFragment,
            childId: String
        ) {
            val alertDialog =
                androidx.appcompat.app.AlertDialog.Builder(context).create()
            alertDialog.setTitle("Add Book?")
            alertDialog.setMessage(
                context.getString(R.string.add_book) + " " + title + " " + context.getString(
                    R.string.in_book_shelf
                )
            )
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.ok)
            ) { dialog, _ ->
                dialog.dismiss()
//                bookShelfFragment.AddImageToStringArray(id, "1", "", "1")
                bookShelfFragment.callChangeBookStatusApi("1", id!!, childId)
//                bookShelfFragment.AddImageToStringArray(id, "1")
            }
            alertDialog.setButton(
                DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.cancel)
            ) { dialog, _ ->
                dialog.dismiss()
            }
            alertDialog.show()
        }
    }
}