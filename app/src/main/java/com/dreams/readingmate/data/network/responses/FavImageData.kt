package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class FavImageData(
    @field:SerializedName("image")
    val image: String? = null
)