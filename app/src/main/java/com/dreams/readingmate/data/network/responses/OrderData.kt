package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class OrderData(

    @field:SerializedName("comm3")
    val comm3: String? = null,

    @field:SerializedName("comm2")
    val comm2: String? = null,

    @field:SerializedName("comm1")
    val comm1: String? = null,

    @field:SerializedName("orderNumber")
    val orderNumber: Long? = null,

    @field:SerializedName("dpcode")
    val dpcode: String? = null,

    @field:SerializedName("signature")
    val signature: String? = null,

    @field:SerializedName("maxwait")
    val maxwait: Int? = null,

    @field:SerializedName("bookData")
    val bookData: List<BookDataItem1?>? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("trackingsafeplace")
    val trackingsafeplace: String? = null,

    @field:SerializedName("tracking")
    val tracking: String? = null,

    @field:SerializedName("iname")
    val iname: String? = null,

    @field:SerializedName("comm4")
    val comm4: String? = null,

    @field:SerializedName("ipcode")
    val ipcode: String? = null,

    @field:SerializedName("dtitlename")
    val dtitlename: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("daddr3")
    val daddr3: String? = null,

    @field:SerializedName("daddr4")
    val daddr4: String? = null,

    @field:SerializedName("daddr1")
    val daddr1: String? = null,

    @field:SerializedName("price")
    val price: Double? = null,

    @field:SerializedName("dinitials")
    val dinitials: String? = null,

    @field:SerializedName("daddr2")
    val daddr2: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("dname")
    val dname: String? = null,

    @field:SerializedName("batchref")
    val batchref: String? = null,

    @field:SerializedName("titlename")
    val titlename: String? = null,

    @field:SerializedName("delivery")
    val delivery: Int? = null,

    @field:SerializedName("iaddr1")
    val iaddr1: String? = null,

    @field:SerializedName("initials")
    val initials: String? = null,

    @field:SerializedName("iaddr4")
    val iaddr4: String? = null,

    @field:SerializedName("iaddr2")
    val iaddr2: String? = null,

    @field:SerializedName("iaddr3")
    val iaddr3: String? = null,

    @field:SerializedName("userId")
    val userId: String? = null,

    @field:SerializedName("transactionId")
    val transactionId: String? = null,

    @field:SerializedName("trackingemail")
    val trackingemail: String? = null,

    @field:SerializedName("service")
    val service: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("trackingsms")
    val trackingsms: String? = null,

    @field:SerializedName("dcountry")
    val dcountry: String? = null,

    @field:SerializedName("cardLast4")
    val cardLast4: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("icountry")
    val icountry: String? = null
)

data class BookDataItem1(

    @field:SerializedName("uniqueReference")
    val uniqueReference: Int? = null,

    @field:SerializedName("quantity")
    val quantity: Int? = null,

    @field:SerializedName("additionalReference")
    val additionalReference: String? = null,

    @field:SerializedName("isbn13")
    val isbn13: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bookId")
    val bookId: String? = null
)
