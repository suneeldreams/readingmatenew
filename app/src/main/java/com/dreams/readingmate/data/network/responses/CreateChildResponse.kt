package com.dreams.readingmate.data.network.responses

data class CreateChildResponse(
    val status: Boolean?,
    val message: String?,
    val data: CreateChildData?
)