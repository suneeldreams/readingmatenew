package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class ChildProgressD(

    @field:SerializedName("total_book_read")
    val totalBookRead: Int? = null,

    @field:SerializedName("total_book_reviewed")
    val totalBookReviewed: Int? = null,

    @field:SerializedName("total_page_read")
    val totalPageRead: String? = null,

    @field:SerializedName("is_reading")
    val isReading: String? = null,

    @field:SerializedName("total_days_to_read")
    val totalDaysToRead: String? = null,

    @field:SerializedName("author")
    val author: String? = null,

    @field:SerializedName("days_running")
    val daysRunning: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("book_id")
    val bookId: String? = null,

    @field:SerializedName("history")
    val history: List<HistoryDataItem?>? = null,

    @field:SerializedName("book_name")
    val bookName: String? = null,

    @field:SerializedName("child_id")
    val childId: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("book_image")
    val bookImage: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("reading_habit")
    val readingHabit: String? = null,

    @field:SerializedName("page_count")
    val pageCount: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class HistoryItem(

    @field:SerializedName("child_id")
    val childId: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("book_id")
    val bookId: String? = null,

    @field:SerializedName("page_read")
    val pageRead: String? = null,

    @field:SerializedName("bookshelf_id")
    val bookshelfId: String? = null
)
