package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FavBookData(

    @field:SerializedName("_id")
    var id: String? = null,

    @field:SerializedName("img")
    var img: String? = null,

    @field:SerializedName("author")
    var author: List<String?>? = null,

    @field:SerializedName("shortDescription")
    var shortDescription: String? = null,

    @field:SerializedName("ukPrice")
    var ukPrice: Double? = null,

    @field:SerializedName("starRating")
    var starRating: Double? = null,

    @field:SerializedName("title")
    var title: String? = null,

    var isSelected: Boolean = false
)