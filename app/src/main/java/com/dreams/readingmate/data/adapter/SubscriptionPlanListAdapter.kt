package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.BundleListItem
import com.dreams.readingmate.ui.home.subscription.SubscriptionFragment
import kotlinx.android.synthetic.main.order_book_single_list_items.view.primaryContentCardView1
import kotlinx.android.synthetic.main.subscription_plan_single_list_items.view.*

class SubscriptionPlanListAdapter(
    private val mListener: SubscriptionFragment.SubscriptionFragmentInterface,
    private val itemList: List<BundleListItem?>?
) :
    RecyclerView.Adapter<SubscriptionPlanListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.subscription_plan_single_list_items, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList!![position], mListener, position
        )
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: BundleListItem?,
            mListener: SubscriptionFragment.SubscriptionFragmentInterface,
            position: Int
        ) {
            val primaryContentCardView1 = itemView.primaryContentCardView1
            val bundlePrice = itemView.bundlePrice
            val bookTitle = itemView.bookTitle
            val imgBundle = itemView.imgBundle
            val btnChildShopNow = itemView.btnChildShopNow
            bookTitle.text = items!!.bundleName
            bundlePrice.text =
                itemView.context.getString(R.string.currency_pound) + items.bundleAmount.toString()
            /* items.bundleThumbImg?.let {
                 Utils.displayImage(
                     itemView.context,
                     it,
                     imgBundle,
                     R.drawable.ic_place_holder
                 )
             }*/

            btnChildShopNow.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("slug", items.bundleSlug)
                mListener.openBundleDetailFragment(bundle)
            }
        }
    }
}