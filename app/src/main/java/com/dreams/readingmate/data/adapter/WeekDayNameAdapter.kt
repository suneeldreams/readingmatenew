package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildListItem
import kotlinx.android.synthetic.main.week_single_item.view.*

class WeekDayNameAdapter(
    private val itemList: List<ChildListItem>
) :
    RecyclerView.Adapter<WeekDayNameAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.week_single_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position))
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(items: ChildListItem) {
            val txtWeekName = itemView.txtWeekName
            val imgTick = itemView.imgCircle

            txtWeekName.text = items.weekData?.get(position)!!.day
            if (items.weekData[position]!!.isRead == true) {
                imgTick.setImageResource(R.drawable.ic_weak_tick)
            } else {
                imgTick.setImageResource(R.drawable.ic_weak_circle)
            }
        }
    }
}