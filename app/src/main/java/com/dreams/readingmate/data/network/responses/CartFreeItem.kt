package com.dreams.readingmate.data.network.responses

data class CartFreeItem(
    var price: String? = null
)