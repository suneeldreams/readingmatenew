package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CurrentlyReadingBook(

    @field:SerializedName("img")
    var image: String? = null,

    @field:SerializedName("bookId")
    var bookId: String? = null,

    @field:SerializedName("numberOfPages")
    var numberOfPages: String? = null,

    @field:SerializedName("title")
    var title: String? = null,

    @field:SerializedName("bookshelfId")
    var bookshelfId: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("child_id")
    val childId: String? = null,

    @field:SerializedName("days_running")
    val daysRunning: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("readingStatus")
    val readingStatus: String? = null,

    @field:SerializedName("fav_subject_ids")
    val favSubjectIds: String? = null,

    @field:SerializedName("isReading")
    val isReading: String? = null,

    @field:SerializedName("page_count")
    val pageCount: String? = null,

    @field:SerializedName("readingHabit")
    val readingHabit: String? = null,

    @field:SerializedName("totalReadingDays")
    val totalReadingDays: String? = null,

    @field:SerializedName("totalDaysToRead")
    val totalDaysToRead: String? = null,

    @field:SerializedName("totalPageRead")
    var totalPageRead: String? = null,

    @field:SerializedName("achievementIds")
    val achievementIds: List<AchievementData?>? = null,

    @field:SerializedName("readingHistory")
    val readingHistory: List<HistoryDataItem?>? = null,

    @field:SerializedName("author")
    var authors: List<String?>? = null,

    @field:SerializedName("status")
    val status: String? = null
)