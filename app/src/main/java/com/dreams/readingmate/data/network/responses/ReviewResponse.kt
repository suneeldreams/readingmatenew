package com.dreams.readingmate.data.network.responses

data class ReviewResponse(
    val status: Boolean?,
    val message: String?,
    val data: ReviewData?
)