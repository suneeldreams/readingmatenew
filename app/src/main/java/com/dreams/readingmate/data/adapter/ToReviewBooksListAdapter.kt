package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ToReviewBooks
import com.dreams.readingmate.ui.home.books.AllBooksFragment
import com.dreams.readingmate.ui.home.progress.ProgressDetailFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.book_single_list_items.view.*
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.imgBook

class ToReviewBooksListAdapter(
    private val itemList: List<ToReviewBooks?>?,
    private val mListener: ProgressDetailFragment.HomeFragmentInterface?,
    private val childId: String
) :
    RecyclerView.Adapter<ToReviewBooksListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, childId)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ToReviewBooks?,
            mListener: ProgressDetailFragment.HomeFragmentInterface?,
            childId: String
        ) {
            val imgBook = itemView.imgBook
            val txtBookName = itemView.txtBookName
            txtBookName.visibility = View.GONE
            items!!.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgBook,
                    R.drawable.ic_place_holder
                )
            }
            imgBook.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("bookId", items.bookId)
                bundle.putString("childId", childId)
                mListener?.openBookDetailFragment(bundle)
            }
        }
    }
}