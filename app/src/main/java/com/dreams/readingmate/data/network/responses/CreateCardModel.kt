package com.dreams.readingmate.data.network.responses


data class CrateCardModel(
    var nameOnCard: String?,
    var cardNumber: String?,
    var expMonth: String?,
    var expYear: String?,
    var pcode: String?,
    var isPrimary: String?
)