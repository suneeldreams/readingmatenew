package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.favouritesbooks.NewFavoriteBookFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*
import java.util.ArrayList

class NewFavoriteAdapter(
    private val itemList: List<FavBookData?>?,
    private val mListener: NewFavoriteBookFragment.NewFavoriteFragmentInterface,
    private val readingFragment: NewFavoriteBookFragment
) :
    RecyclerView.Adapter<NewFavoriteAdapter.ViewHolder>() {
    lateinit var mSelectedItems: ArrayList<String>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, readingFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData?,
            mListener: NewFavoriteBookFragment.NewFavoriteFragmentInterface,
            readingFragment: NewFavoriteBookFragment
        ) {
            val txtBooksName = itemView.txtBooksName
            val imgFavouriteBook = itemView.imgBook
            val imgSelected = itemView.imgSelect
            txtBooksName.text = items!!.title
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
            itemView.setOnClickListener {
                readingFragment.AddImageToStringArray(items.img, items.id,items.title)
            }
        }
    }
}