package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class BundleListData(

    @field:SerializedName("bundleList")
    val bundleList: List<BundleListItem?>? = null,

    @field:SerializedName("bannerBundle")
    val bannerBundle: BannerBundle? = null
)

data class BundleListItem(

    @field:SerializedName("bundleThumbImg")
    val bundleThumbImg: String? = null,

    @field:SerializedName("bundlePublisher")
    val bundlePublisher: String? = null,

    @field:SerializedName("bundleReadingAge")
    val bundleReadingAge: String? = null,

    @field:SerializedName("bundleDimensions")
    val bundleDimensions: String? = null,

    @field:SerializedName("bundleLanguage")
    val bundleLanguage: String? = null,

    @field:SerializedName("bundleDescription")
    val bundleDescription: String? = null,

    @field:SerializedName("bundleImg")
    val bundleImg: List<String?>? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("bundleColor")
    val bundleColor: String? = null,

    @field:SerializedName("bundleName")
    val bundleName: String? = null,

    @field:SerializedName("bundleAuthor")
    val bundleAuthor: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bundleSlug")
    val bundleSlug: String? = null,

    @field:SerializedName("bundleAmount")
    val bundleAmount: Double? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("bundleSubDescription")
    val bundleSubDescription: String? = null,

    @field:SerializedName("bundlePublicationDate")
    val bundlePublicationDate: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null
)

data class BannerBundle(

    @field:SerializedName("bundleThumbImg")
    val bundleThumbImg: String? = null,

    @field:SerializedName("bundlePublisher")
    val bundlePublisher: String? = null,

    @field:SerializedName("bundleReadingAge")
    val bundleReadingAge: String? = null,

    @field:SerializedName("bundleDimensions")
    val bundleDimensions: String? = null,

    @field:SerializedName("bundleLanguage")
    val bundleLanguage: String? = null,

    @field:SerializedName("bundleDescription")
    val bundleDescription: String? = null,

    @field:SerializedName("bundleImg")
    val bundleImg: List<String?>? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("bundleColor")
    val bundleColor: String? = null,

    @field:SerializedName("bundleName")
    val bundleName: String? = null,

    @field:SerializedName("bundleAuthor")
    val bundleAuthor: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("bundleSlug")
    val bundleSlug: String? = null,

    @field:SerializedName("bundleAmount")
    val bundleAmount: Double? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("bundlePublicationDate")
    val bundlePublicationDate: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("bundleSubDescription")
    val bundleSubDescription: String? = null
)
