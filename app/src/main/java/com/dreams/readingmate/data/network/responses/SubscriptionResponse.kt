package com.dreams.readingmate.data.network.responses

data class SubscriptionResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<SubscriptionPlanListData>?
)