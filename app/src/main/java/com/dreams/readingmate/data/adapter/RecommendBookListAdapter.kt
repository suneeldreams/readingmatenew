package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.shoping.ShopTabFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.recommend_grid_single_item.view.*

class RecommendBookListAdapter(
    private val itemList: List<FavBookData>,
    private val mListener: ShopTabFragment.ShopTabFragmentInterface?
) :
    RecyclerView.Adapter<RecommendBookListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.recommend_grid_single_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList.get(position), mListener)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData,
            mListener: ShopTabFragment.ShopTabFragmentInterface?
        ) {
            val imgRecommBooks = itemView.imgRecommBooks
            val txtBookTitle = itemView.txtBookTitle
            val txtAuthor = itemView.txtAuthor
            val txtPrice = itemView.txtPrice
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgRecommBooks,
                    R.drawable.ic_place_holder
                )
            }
            txtBookTitle.text = items.title
            if (items.author!!.isNotEmpty()) {
                txtAuthor.text = items.author!![0]
            } else {
                txtAuthor.text = "N/A"
            }
            txtPrice.text =
                itemView.context.resources.getString(R.string.currency_pound) + items.ukPrice
            imgRecommBooks.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("childId", "")
                bundle.putString("bookId", items.id)
                mListener?.openBookDetailFragment(bundle)
            }
        }
    }
}