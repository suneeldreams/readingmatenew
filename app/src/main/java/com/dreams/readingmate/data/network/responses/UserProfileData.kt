package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class UserProfileData(

	@field:SerializedName("forgotPasswordToken")
	val forgotPasswordToken: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("mobile")
	val mobile: Long? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("userType")
	val userType: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("isEmailVerified")
	val isEmailVerified: Boolean? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)