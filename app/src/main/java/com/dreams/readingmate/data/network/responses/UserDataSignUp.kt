package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class UserDataSignUp(
    @field:SerializedName("id")
    val id: String?,

    @field:SerializedName("email")
    val email: String?,

    @field:SerializedName("userType")
    val userType: String?
)