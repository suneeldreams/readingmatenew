package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.RowItems
import com.dreams.readingmate.ui.home.progress.BookOrderFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.bookshelf_single_list_items.view.*
import kotlinx.android.synthetic.main.order_book_single_list_items.view.*
import kotlinx.android.synthetic.main.order_book_single_list_items.view.primaryContentCardView1
import kotlinx.android.synthetic.main.order_book_single_list_items.view.txtAuthor

class BookOrderAdapter(
    private val mListener: BookOrderFragment.BookOrderFragmentInterface,
    private val itemList: List<RowItems?>?
) :
    RecyclerView.Adapter<BookOrderAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_book_single_list_items, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList?.get(position), mListener
        )
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: RowItems?,
            mListener: BookOrderFragment.BookOrderFragmentInterface
        ) {
            val primaryContentCardView1 = itemView.primaryContentCardView1
            val imgBook = itemView.imgBook
            val txtTitle = itemView.txtTitle
            txtTitle.text = items?.title
            if (!items!!.authors.isNullOrEmpty()) {
                itemView.txtAuthor.text = items.authors!![0].toString()
            }
            if (!items.price.equals("0")) {
                itemView.txtStatus.visibility = View.VISIBLE
                itemView.txtStatus.text =
                    itemView.resources.getString(R.string.currency_pound) + items.price
            } else {
                itemView.txtStatus.visibility = View.GONE
            }

            /* if (items.orderStatusKey == "orderConfirm" || items.orderStatusKey == "readyToShip" ||
                 items.orderStatusKey == "shipped" || items.orderStatusKey == "dispatch" ||
                 items.orderStatusKey == "advancedShippingNotice" || items.orderStatusKey == "delivered" ||
                 items.orderStatusKey == "paymentDone"
             ) {
                 itemView.txtStatus.text = items.orderStatus
                 itemView.txtStatus.setTextColor(ContextCompat.getColor(itemView.context,R.color.green))
             } else {
                 itemView.txtStatus.text = items.orderStatus
                 itemView.txtStatus.setTextColor(ContextCompat.getColor(itemView.context,R.color.color_active_button))
             }*/
            Utils.displayImage(
                itemView.context,
                items?.img!!,
                imgBook,
                R.drawable.ic_place_holder
            )
            primaryContentCardView1.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("from", "OrderBook")
                bundle.putString("bookId", items.bookId)
                mListener?.openBooksFragment(bundle)
            }
        }
    }
}