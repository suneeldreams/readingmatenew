package com.dreams.readingmate.data.network.responses

data class CardListResponse(
    val status: Boolean?,
    val message: String?,
    val data: CardListData?
)