package com.dreams.readingmate.data.network.responses

data class OrderBookResponse(
    val status: Boolean?,
    val message: String?,
    val data: OrderListData?
)