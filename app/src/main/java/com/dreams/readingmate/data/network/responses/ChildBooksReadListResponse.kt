package com.dreams.readingmate.data.network.responses

data class ChildBooksReadListResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<ChildBooksReadData>?
)