package com.dreams.readingmate.data.repositories

import com.dreams.readingmate.data.network.MyApi
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.*

class UserRepository(private val api: MyApi) : BaseApiRequest() {

    suspend fun createUser(email: String, password: String): ResultState<AuthLoginResponse> {
        return apiRequest {
            api.createUser("application/json", email, password)
        }
    }
    /*suspend fun userLogin(
        email: String,
        password: String
    ): AuthResponse {
        return apiRequest { api.userLogin(email, password) }
    }*/

    suspend fun registerUser(
        name: String,
        email: String,
        password: String,
        charityName: String
    ): ResultState<AuthLoginResponse> {
        return apiRequest {
            api.registerUser("application/json", name, email, password,charityName)
        }
    }

    suspend fun forgotPassword(email: String): ResultState<SuccessResponse> {
        return apiRequest { api.forgotPassword("application/json", email) }
    }

    suspend fun resetPassw(
        accessToken: String,
        oldPassword: String,
        newPassword: String,
        cnfmPassword: String
    ): ResultState<SuccessResponse> {
        return apiRequest { api.resetPass(accessToken, oldPassword, newPassword, cnfmPassword) }
    }

    suspend fun setAlarm(
        user: AlarmModel,
        accessToken: String,
        childId: String
    ): ResultState<ChildDetailResponse> {
        return apiRequest {
            api.setAlarm(accessToken, childId, user)
        }
    }

    suspend fun resetPassword(
        accessToken: String,
        reset: ResetPassword
    ): ResultState<AuthResponse> {
        return apiRequest { api.resetPassword(accessToken, reset) }
    }

    suspend fun getChildDetailList(
        accessToken: String,
        childId: String
    ): ResultState<ChildDetailResponse> {
        return apiRequest {
            api.getChildDetailList(accessToken, childId)
        }
    }

    suspend fun createOrder(
        user: CrateOrderModel,
        accessToken: String
    ): ResultState<OrderResponse> {
        return apiRequest {
            api.createOrder(accessToken, user)
        }
    }

    suspend fun createMemberShip(
        user: CrateMemberShipModel,
        accessToken: String
    ): ResultState<OrderResponse> {
        return apiRequest {
            api.createMemberShip(accessToken, user)
        }
    }

}