package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ReminderData(

    @field:SerializedName("endDate")
    var endDate: String? = null,

    @field:SerializedName("kind")
    var kind: String? = null,

    @field:SerializedName("startDate")
    var startDate: String? = null
)