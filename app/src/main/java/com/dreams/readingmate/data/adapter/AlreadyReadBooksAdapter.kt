package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.favouritesbooks.AlreadyReadBooksFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.favourite_book_single_list_items.view.*
import java.util.ArrayList

class AlreadyReadBooksAdapter(
    private val itemList: List<FavBookData?>?,
    private val mListener: AlreadyReadBooksFragment.AlreadyReadBookFragmentInterface,
    private val readingFragment: AlreadyReadBooksFragment
) :
    RecyclerView.Adapter<AlreadyReadBooksAdapter.ViewHolder>() {
    lateinit var mSelectedItems: ArrayList<String>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.favourite_book_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, readingFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData?,
            mListener: AlreadyReadBooksFragment.AlreadyReadBookFragmentInterface,
            readingFragment: AlreadyReadBooksFragment
        ) {
            val txtBooksName = itemView.txtBooksName
            val imgFavouriteBook = itemView.imgBook
            val imgSelected = itemView.imgSelect
            txtBooksName.text = items!!.title
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
            itemView.setOnClickListener {
                readingFragment.AddImageToStringArray(items.img, items.id, items.title)
            }
        }
    }
}