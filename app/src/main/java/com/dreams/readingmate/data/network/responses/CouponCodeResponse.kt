package com.dreams.readingmate.data.network.responses

data class CouponCodeResponse(
    val status: Boolean?,
    val message: String?,
    val data: CouponCodeData?
)