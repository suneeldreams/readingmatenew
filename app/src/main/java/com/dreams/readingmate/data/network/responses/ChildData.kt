package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChildData(

    @field:SerializedName("schoolId")
    var schoolId: String? = null,

    @field:SerializedName("image")
    var image: String? = null,

    @field:SerializedName("countryId")
    var countryId: String? = null,

    @field:SerializedName("colorId")
    var colorId: String? = null,

    @field:SerializedName("childName")
    var childName: String? = null,

    @field:SerializedName("nickName")
    var nickName: String? = null,

    @field:SerializedName("age")
    var age: String? = null,

    @field:SerializedName("isPriority")
    val isPriority: String? = null,
    var isSelected: Boolean = false

):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(schoolId)
        parcel.writeString(image)
        parcel.writeString(countryId)
        parcel.writeString(colorId)
        parcel.writeString(childName)
        parcel.writeString(nickName)
        parcel.writeString(age)
        parcel.writeString(isPriority)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChildData> {
        override fun createFromParcel(parcel: Parcel): ChildData {
            return ChildData(parcel)
        }

        override fun newArray(size: Int): Array<ChildData?> {
            return arrayOfNulls(size)
        }
    }
}