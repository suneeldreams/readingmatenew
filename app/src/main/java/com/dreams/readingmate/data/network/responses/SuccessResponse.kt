package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class SuccessResponse(

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Boolean? = null
)
