package com.dreams.readingmate.data.network.responses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UpdateTickData{

	@SerializedName("country")
	private String country;

	@SerializedName("img")
	private String img;

	@SerializedName("achievements")
	private Achievements achievements;

	@SerializedName("gender")
	private String gender;

	@SerializedName("wishlist")
	private List<Object> wishlist;

	@SerializedName("schoolType")
	private String schoolType;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("charity")
	private String charity;

	@SerializedName("__v")
	private int V;

	@SerializedName("favCharacters")
	private List<Object> favCharacters;

	@SerializedName("readingHistory")
	private List<ReadingHistoryItem> readingHistory;

	@SerializedName("schoolName")
	private String schoolName;

	@SerializedName("favColour")
	private String favColour;

	@SerializedName("updatedAt")
	private String updatedAt;

	@SerializedName("totalBooksRead")
	private int totalBooksRead;

	@SerializedName("bookshelf")
	private List<BookshelfItem> bookshelf;

	@SerializedName("nickName")
	private String nickName;

	@SerializedName("favGenres")
	private List<Object> favGenres;

	@SerializedName("userId")
	private String userId;

	@SerializedName("run_streak")
	private int runStreak;

	@SerializedName("name")
	private String name;

	@SerializedName("favBookIds")
	private List<String> favBookIds;

	@SerializedName("favSubjects")
	private List<Object> favSubjects;

	@SerializedName("_id")
	private String id;

	@SerializedName("age")
	private int age;

	@SerializedName("reading_habit")
	private int readingHabit;

	@SerializedName("status")
	private int status;

	public String getCountry(){
		return country;
	}

	public String getImg(){
		return img;
	}

	public Achievements getAchievements(){
		return achievements;
	}

	public String getGender(){
		return gender;
	}

	public List<Object> getWishlist(){
		return wishlist;
	}

	public String getSchoolType(){
		return schoolType;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getCharity(){
		return charity;
	}

	public int getV(){
		return V;
	}

	public List<Object> getFavCharacters(){
		return favCharacters;
	}

	public List<ReadingHistoryItem> getReadingHistory(){
		return readingHistory;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public String getFavColour(){
		return favColour;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getTotalBooksRead(){
		return totalBooksRead;
	}

	public List<BookshelfItem> getBookshelf(){
		return bookshelf;
	}

	public String getNickName(){
		return nickName;
	}

	public List<Object> getFavGenres(){
		return favGenres;
	}

	public String getUserId(){
		return userId;
	}

	public int getRunStreak(){
		return runStreak;
	}

	public String getName(){
		return name;
	}

	public List<String> getFavBookIds(){
		return favBookIds;
	}

	public List<Object> getFavSubjects(){
		return favSubjects;
	}

	public String getId(){
		return id;
	}

	public int getAge(){
		return age;
	}

	public int getReadingHabit(){
		return readingHabit;
	}

	public int getStatus(){
		return status;
	}
}