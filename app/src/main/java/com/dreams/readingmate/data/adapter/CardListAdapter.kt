package com.dreams.readingmate.data.adapter

import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.CardDataItem
import com.dreams.readingmate.ui.home.shoping.CheckOutFragment
import com.dreams.readingmate.util.CardType
import kotlinx.android.synthetic.main.card_single_items.view.*
import kotlinx.android.synthetic.main.card_single_items.view.viewPager2
import java.util.ArrayList

class CardListAdapter(
    private val listItems: List<CardDataItem?>?,
    private val mListener: CheckOutFragment.CheckOutFragmentInterface?,
    private val checkOutFragment: CheckOutFragment
) :
    RecyclerView.Adapter<CardListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_single_items, parent, false)
        val adapter = ViewPagerAdapter()
        val viewPager = v.viewPager2
        viewPager.adapter = adapter
        return ViewHolder(v)
    }

    enum class SwipedState {
        SHOWING_PRIMARY_CONTENT,
        SHOWING_SECONDARY_CONTENT
    }

    var mItemSwipedStates: MutableList<SwipedState>? = null
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mItemSwipedStates = ArrayList()
        for (i in listItems!!.indices) {
            mItemSwipedStates?.add(i, SwipedState.SHOWING_PRIMARY_CONTENT)
        }
        holder.bindItems(
            listItems?.get(position),
            mListener,
            checkOutFragment,
            position,
            mItemSwipedStates
        )
    }

    override fun getItemCount(): Int {
        return listItems!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            dataItems: CardDataItem?,
            mListener: CheckOutFragment.CheckOutFragmentInterface?,
            checkOutFragment: CheckOutFragment,
            position: Int,
            mItemSwipedStates: MutableList<SwipedState>?
        ) {

            val cardNumber: TextView = itemView.cardNumber
            val edtCVVCode: EditText = itemView.edtCvv
            val saveCardCheck: ImageView = itemView.saveCardCheck
            val ivTypeCard: ImageView = itemView.ivTypeCard
            val rltPayment: RelativeLayout = itemView.rltPayment
            val imgDeleteCard = itemView.imgDeleteCard

            val type = CardType.detect(dataItems!!.cardNumber!!.replace(" ", ""))
            when (type) {
                CardType.VISA -> ivTypeCard.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_visa
                    )
                )
                CardType.MASTERCARD -> ivTypeCard.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_mastercard
                    )
                )
                CardType.AMERICAN_EXPRESS -> ivTypeCard.setImageResource(android.R.color.transparent)
                CardType.DISCOVER -> ivTypeCard.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_discover_card
                    )
                )
                CardType.JCB -> ivTypeCard.setImageResource(android.R.color.transparent)
                CardType.UNKNOWN -> ivTypeCard.setImageResource(android.R.color.transparent)
            }
            cardNumber.text = dataItems.cardNumber
            /* cardNumber.text = maskNumber(
                 dataItems.cardNumber!!,
                 itemView.context.resources.getString(R.string.card_digit) + "  #####"
             )*/
            imgDeleteCard.setOnClickListener {
                itemView.viewPager2.currentItem = 0
                checkOutFragment.refreshCardList(dataItems.id)
            }

            itemView.primaryContentCard.setOnClickListener {
                dataItems.isSelected = !dataItems.isSelected
                val cvv = edtCVVCode.text.toString().trim()
                checkOutFragment.refreshCardAdapter(position, cvv)
            }
            if (dataItems.isSelected) {
                rltPayment.background =
                    ContextCompat.getDrawable(
                        itemView.context!!,
                        R.drawable.card_selected_border_bg
                    )
//                saveCardCheck.visibility = View.VISIBLE
                edtCVVCode.visibility = View.VISIBLE
            } else {
                rltPayment.background =
                    ContextCompat.getDrawable(
                        itemView.context!!,
                        R.drawable.card_gray_border_bg
                    )
//                saveCardCheck.visibility = View.GONE
                edtCVVCode.visibility = View.GONE
            }
            val displayMetrics = Resources.getSystem().displayMetrics
            itemView.layoutParams.width = displayMetrics.widthPixels
            itemView.requestLayout()
            itemView.viewPager2.currentItem = mItemSwipedStates!![position].ordinal

            itemView.viewPager2.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                var previousPagePosition = 0

                override fun onPageScrolled(
                    pagePosition: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    if (pagePosition == previousPagePosition)
                        return

                    when (pagePosition) {
                        0 -> mItemSwipedStates[position] = SwipedState.SHOWING_PRIMARY_CONTENT
                        1 -> mItemSwipedStates[position] = SwipedState.SHOWING_SECONDARY_CONTENT
                    }
                    previousPagePosition = pagePosition
                    Log.i(
                        "MyAdapter",
                        "PagePosition " + position + " set to " + mItemSwipedStates[position].ordinal
                    )
                }

                override fun onPageSelected(pagePosition: Int) {
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        }

        private fun maskNumber(cardNumber: String, mask: String): String {
            var index = 0
            val maskedNumber = StringBuilder()
            for (i in 0 until mask.length) {
                val c = mask[i]
                if (c == '#') {
                    maskedNumber.append(cardNumber[index])
                    index++
                } else if (c == 'x') {
                    maskedNumber.append(c)
                    index++
                } else {
                    maskedNumber.append(c)
                }
            }
            return maskedNumber.toString()
        }
    }

    inner class ViewPagerAdapter : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {

            var resId = 0
            when (position) {
                0 -> resId = R.id.primaryContentCard
                1 -> resId = R.id.secondaryContentFrameLayout
            }
            return collection.findViewById(resId)
        }

        override fun getCount(): Int {
            return 2
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as View
        }
    }
}