package com.dreams.readingmate.data.network.base


/**
 * Base class for all Activities
 * Author: Shivank Trivedi
 * Dated: 20-06-2020
 */

sealed class ResultState<T> {

    /**
     * A state of [value] which shows that we know there is still an update to come.
     */
    data class Loading<T>(val value: T) : ResultState<T>()

    /**
     * A state that shows the [value] is the last known update.
     */
    data class Success<T>(val value: T) : ResultState<T>()

    /**
     * Only a user message with code is propagated to UI.
     */
    data class Error<T>(val error: Entity.Error) : ResultState<T>()
}