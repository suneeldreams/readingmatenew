package com.dreams.readingmate.data.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildBooksReadData
import com.dreams.readingmate.ui.home.progress.BookShelfFragment
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.bookshelf_single_list_items.view.*
import java.util.*

class BookShelfAdapter(
    private val mListener: BookShelfFragment.BookShelfFragmentInterface?,
    private val itemList: List<ChildBooksReadData?>?,
    private val bookShelfFragment: BookShelfFragment,
    private val createdAt: String,
    private val childId: String
) :
    RecyclerView.Adapter<BookShelfAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.bookshelf_single_list_items, parent, false)
        val adapter = ViewPagerAdapter()
        val viewPager = v.viewPager2
        viewPager.adapter = adapter
        return ViewHolder(v)
    }

    enum class SwipedState {
        SHOWING_PRIMARY_CONTENT,
        SHOWING_SECONDARY_CONTENT
    }

    var mItemSwipedStates: MutableList<SwipedState>? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mItemSwipedStates = ArrayList()
        for (i in itemList!!.indices) {
            mItemSwipedStates?.add(i, SwipedState.SHOWING_PRIMARY_CONTENT)
        }
        holder.bindItems(
            itemList[position],
            mListener,
            position,
            mItemSwipedStates,
            bookShelfFragment,
            createdAt,
            childId
        )
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ChildBooksReadData?,
            mListener: BookShelfFragment.BookShelfFragmentInterface?,
            position: Int,
            mItemSwipedStates: MutableList<SwipedState>?,
            bookShelfFragment: BookShelfFragment,
            createdAt: String,
            childId: String

        ) {
            val imgFavouriteBook = itemView.imgFavouriteBook
            val imgDeleteChild = itemView.imgDeleteBook
            val progressBar = itemView.progressBar
            val rltTopParent = itemView.rltTopParent
            val txtChange = itemView.txtChange
            val bookStatus = itemView.bookStatus
            val htmlString = "<u>Change</u>"
            txtChange.text = Html.fromHtml(htmlString)
            /* if (!items!!.readingStatus.equals("2")) {
                 progressBar.visibility = View.GONE
                 itemView.txtNoOfPage.visibility = View.GONE
                 itemView.txtBookName.text = items.bookName
                 if (!items.authors.isNullOrEmpty()) {
                     itemView.txtAuthor.text = items.authors!![0].toString()
                 }
                 Utils.displayImage(
                     itemView.context,
                     items.image!!,
                     imgFavouriteBook,
                     R.drawable.ic_place_holder
                 )
             }*/
            if (items!!.totalPageRead != "0") {
                progressBar.visibility = View.GONE
                itemView.txtNoOfPage.visibility = View.GONE
                progressBar.max = items.numberOfPages!!.toInt()
                progressBar.progress = items.totalPageRead!!.toInt()
                itemView.txtNoOfPage.text =
                    "Page " + items.totalPageRead + " of " + items.numberOfPages
            } else {
                progressBar.visibility = View.GONE
                itemView.txtNoOfPage.visibility = View.GONE
            }
            if (items.readingStatus.equals("2")) {
                progressBar.visibility = View.GONE
                itemView.viewPager2.visibility = View.GONE
                itemView.viewSeparator.visibility = View.GONE
                itemView.txtNoOfPage.visibility = View.GONE
                itemView.imgFavouriteBook.visibility = View.GONE
                itemView.txtBookName.visibility = View.GONE
                progressBar.max = items.numberOfPages!!.toInt()
                progressBar.progress = items.numberOfPages!!.toInt()
                itemView.txtNoOfPage.text =
                    "Page " + items.numberOfPages + " of " + items.numberOfPages
            }
            itemView.txtBookName.text = items.bookName
            if (!items.authors.isNullOrEmpty()) {
                itemView.txtAuthor.text = items.authors!![0].toString()
            }
            Utils.displayImage(
                itemView.context,
                items.image!!,
                imgFavouriteBook,
                R.drawable.ic_place_holder
            )
            val readingStatus = items.readingStatus
            when (readingStatus) {
                "0" -> bookStatus.text = "I want to read"
                "1" -> bookStatus.text = "Currently reading"
                "2" -> bookStatus.text = "Read"
            }
            itemView.txtChange.setOnClickListener {
                openDialog(itemView.context, items.bookId!!, childId, bookShelfFragment)
            }

            itemView.primaryContentCardView1.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("bookId", items.bookId)
                bundle.putString("childId", childId)
                mListener?.openBookDetailFragment(bundle)
            }


            /*itemView.primaryContentCardView1.setOnClickListener {
                openDialogue(
                    itemView.context,
                    items.bookName,
                    bookShelfFragment,
                    items.id,
                    createdAt,
                    items.image!!
                )
            }*/
            val displayMetrics = Resources.getSystem().displayMetrics
            itemView.layoutParams.width = displayMetrics.widthPixels
            itemView.requestLayout()
            itemView.viewPager2.currentItem = mItemSwipedStates!![position].ordinal

            itemView.viewPager2.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                var previousPagePosition = 0

                override fun onPageScrolled(
                    pagePosition: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    if (pagePosition == previousPagePosition)
                        return

                    when (pagePosition) {
                        0 -> mItemSwipedStates[position] = SwipedState.SHOWING_PRIMARY_CONTENT
                        1 -> mItemSwipedStates[position] = SwipedState.SHOWING_SECONDARY_CONTENT
                    }
                    previousPagePosition = pagePosition
                    Log.i(
                        "MyAdapter",
                        "PagePosition " + position + " set to " + mItemSwipedStates[position].ordinal
                    )
                }

                override fun onPageSelected(pagePosition: Int) {
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })

            imgDeleteChild.setOnClickListener {
                itemView.viewPager2.currentItem = 0
                bookShelfFragment.removeChildFromList(items.id)
            }
        }

        private fun openDialog(
            context: Context,
            bookId: String,
            childId: String,
            bookShelfFragment: BookShelfFragment
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog_book_status)

            val displayRectangle = Rect()
            val window = (context as Activity).window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            window.setGravity(Gravity.CENTER_VERTICAL)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = (displayRectangle.width() * 0.95f).toInt()
            lp.height = (displayRectangle.height() * 0.95f).toInt()
            dialog.window!!.attributes = lp
            dialog.show()

            val imgDismiss = dialog.findViewById(R.id.imgDismiss) as ImageView
            val currentlyReading = dialog.findViewById(R.id.currentlyReading) as TextView
            val txtRead = dialog.findViewById(R.id.txtRead) as TextView
            val txtNonRead = dialog.findViewById(R.id.txtNonRead) as TextView
            val btnSaveStatus = dialog.findViewById(R.id.btnSaveStatus) as Button
            var bookStatus = ""
            currentlyReading.setOnClickListener {
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "1"
            }
            txtRead.setOnClickListener {
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtNonRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "2"
            }
            txtNonRead.setOnClickListener {
                txtNonRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
                currentlyReading.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                txtRead.background =
                    ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
                bookStatus = "0"
            }
            btnSaveStatus.setOnClickListener {
                dialog.dismiss()
                val intent = Intent()
                intent.action = AppConstants.REFERESH_BOOKS
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                if (bookStatus == "0") {
                    bookShelfFragment.callFavoriteApi(bookId, childId)
                } else {
                    bookShelfFragment.callChangeBookStatusApi(bookStatus, bookId, childId)
                }
            }
            imgDismiss.setOnClickListener {
                dialog.dismiss()
            }

        }

        private fun openDialogue(
            context: Context,
            bookName: String?,
            bookShelfFragment: BookShelfFragment,
            bookshelfId: String?,
            createdAt: String,
            image: String
        ) {
            val dialog = Dialog(context)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.weekdays_update_dialog)

            val displayRectangle = Rect()
            val window = (context as Activity).window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            window.setGravity(Gravity.CENTER_VERTICAL)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = (displayRectangle.width() * 0.95f).toInt()
            lp.height = (displayRectangle.height() * 0.95f).toInt()
            dialog.window!!.attributes = lp
            dialog.show()

            val imgDismiss = dialog.findViewById(R.id.imgDismiss) as ImageView
            val imgBook = dialog.findViewById(R.id.dialogBookImage) as ImageView
            val txtBookName = dialog.findViewById(R.id.txtBookName) as TextView
            val edtPageNumber = dialog.findViewById(R.id.edtPageNumber) as EditText
            val btnSubmit = dialog.findViewById(R.id.btnSubmit) as Button
            val btnMarkRead = dialog.findViewById(R.id.btnMarkRead) as Button
            val readAll = dialog.findViewById(R.id.readAll) as CheckBox
            txtBookName.text = bookName
            val pageRead = edtPageNumber.text.toString().trim()
            Utils.displayImage(
                itemView.context,
                image,
                imgBook,
                R.drawable.ic_place_holder
            )
            var readingStatus = ""
            readAll.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    readingStatus = "2"
                }

            }
            imgDismiss.setOnClickListener {
                dialog.dismiss()
            }
            btnSubmit.setOnClickListener {
                dialog.dismiss()
                if (readingStatus.isNotEmpty()) {
                    bookShelfFragment.addPageRead(
                        bookshelfId,
                        edtPageNumber.text.toString().trim(),
                        createdAt,
                        readingStatus
                    )
                } else {
                    bookShelfFragment.addPageRead(
                        bookshelfId,
                        edtPageNumber.text.toString().trim(),
                        createdAt,
                        "1"
                    )
                }
            }
            /*btnMarkRead.setOnClickListener {
                dialog.dismiss()
                bookShelfFragment.addPageRead(bookshelfId, pageRead, createdAt, "2")
            }*/
//            dialog.show()
        }
    }

    inner class ViewPagerAdapter : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {

            var resId = 0
            when (position) {
                0 -> resId = R.id.primaryContentCardView1
                1 -> resId = R.id.secondaryContentFrameLayout
            }
            return collection.findViewById(resId)
        }

        override fun getCount(): Int {
            return 2
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as View
        }
    }
}