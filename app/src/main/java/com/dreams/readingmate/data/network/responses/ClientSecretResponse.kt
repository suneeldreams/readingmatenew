package com.dreams.readingmate.data.network.responses

data class ClientSecretResponse(
    val status: Boolean?,
    val message: String?,
    val data: ClientSecretKeyData?
)