package com.dreams.readingmate.data.network.responses


data class CrateAddressModel(
    var titlename: String?,
    var name: String?,
    var mobile: String?,
    var addr1: String?,
    var addr2: String?,
    var addr3: String?,
    var addr4: String?,
    var pcode: String?,
    var country: String?,
    var isPrimary: String?
)