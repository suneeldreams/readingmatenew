package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class NewsDetailsData(

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("metaDescription")
    val metaDescription: String? = null,

    @field:SerializedName("categoryName")
    val categoryName: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("metaKeywords")
    val metaKeywords: String? = null,

    @field:SerializedName("metaTitle")
    val metaTitle: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("shortDesc")
    val shortDesc: String? = null,

    @field:SerializedName("isFavorite")
    val isFavorite: Boolean = false,

    @field:SerializedName("sharingURL")
    val sharingURL: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("categoryId")
    val categoryId: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null
)
