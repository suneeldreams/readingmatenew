package com.dreams.readingmate.data.network.responses

data class UserResponse(
    val status: Boolean?,
    val message: String?,
    val data: UserInfoResponse?
)
