package com.dreams.readingmate.data.network.responses


data class AlarmModel(
    var startDate: String?,
    var endDate: String?,
    var kind: String?
)