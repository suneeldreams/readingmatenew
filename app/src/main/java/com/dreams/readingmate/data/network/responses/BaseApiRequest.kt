package com.dreams.readingmate.data.network.responses

import com.dreams.readingmate.data.network.base.Entity
import com.dreams.readingmate.data.network.base.ResultState
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

abstract class BaseApiRequest {

    suspend fun<T: Any> apiRequest(call: suspend () -> Response<T>) : ResultState<T> {
        val response = call.invoke()
        return if(response.isSuccessful){
            ResultState.Success(response.body()!!)
        } else {
            val error = response.errorBody()?.string()
            val message = StringBuilder()
            error?.let{
                try{
                    message.append(JSONObject(it).getString("message"))
                } catch(e: JSONException){
                    e.printStackTrace()
                }
            }
            ResultState.Error(Entity.Error(response.code(), message.toString()))
        }
    }
}