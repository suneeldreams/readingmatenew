package com.dreams.readingmate.data.network.responses


data class CrateChildModel(
    var name: String?,
//    var nickName: String?,
    var age: String?,
    var favColour: String?,
    var schoolType: String?,
    var country: String?,
//    var favBookIds: ArrayList<String>?,
    var favGenres: ArrayList<String>?,
    var favSubjects: ArrayList<String>?,
    var favCharacters: ArrayList<String>?,
    var currentlyReadingBooks: ArrayList<String>?,
//    var readBooks: ArrayList<String>?,
    var schoolName: String?,
    var charity: String?,
    var img: String?
)