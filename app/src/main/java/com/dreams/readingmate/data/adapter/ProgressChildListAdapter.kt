package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.ui.home.progress.ProgressFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.home_single_list_items.view.txtChildName
import kotlinx.android.synthetic.main.home_single_list_items.view.txtChildNickName
import kotlinx.android.synthetic.main.progress_single_list_items.view.*


class ProgressChildListAdapter(
    private val itemList: List<ChildListItem>,
    private val mListener: ProgressFragment.ProgressFragmentInterface?,
    private val preferenceChild: PreferenceChildItems?
) :
    RecyclerView.Adapter<ProgressChildListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.progress_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList[position],
            mListener,
            position,
            preferenceChild
        )
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ChildListItem,
            mListener: ProgressFragment.ProgressFragmentInterface?,
            position: Int,
            preferenceChild: PreferenceChildItems?
        ) {
            val imgChild = itemView.imgChild
            itemView.btnViewProgress.visibility = View.VISIBLE
            itemView.txtChildName.text = items.childName
            itemView.txtChildNickName.text = items.nickName
            Utils.displayCircularImage(itemView.context, imgChild, items.image!!)
            itemView.btnViewProgress.setOnClickListener {
                val editChildItem = items
                val bundle = Bundle()
                bundle.putString("childId", items.id)
                bundle.putParcelable("child", preferenceChild)
                bundle.putParcelable("editChild", editChildItem)
                bundle.putString("nickName", items.nickName)
                bundle.putString("from", "progress")
                mListener?.openProgressFragment(bundle)
            }
        }
    }
}