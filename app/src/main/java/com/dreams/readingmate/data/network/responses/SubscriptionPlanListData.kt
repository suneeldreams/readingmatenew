package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class SubscriptionPlanListData(

    @field:SerializedName("createdAt")
    var createdAt: String? = null,

    @field:SerializedName("planCurrency")
    var planCurrency: String? = null,

    @field:SerializedName("planInterval")
    var planInterval: String? = null,

    @field:SerializedName("planIntervalCount")
    var planIntervalCount: Int? = null,

    @field:SerializedName("planAmount")
    var planAmount: Double? = null,

    @field:SerializedName("upfrontAmount")
    var upfrontAmount: Double? = null,

    @field:SerializedName("planStatus")
    var planStatus: String? = null,

    @field:SerializedName("planName")
    var planName: String? = null,

    @field:SerializedName("planDescription")
    var planDescription: String? = null,

    @field:SerializedName("stripeProduct")
    var stripeProduct: String? = null,

    @field:SerializedName("stripePlan")
    var stripePlan: String? = null,

    @field:SerializedName("_id")
    var id: String? = null
)