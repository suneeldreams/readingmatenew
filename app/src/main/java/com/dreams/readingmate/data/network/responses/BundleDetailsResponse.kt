package com.dreams.readingmate.data.network.responses

data class BundleDetailsResponse(
    val status: Boolean?,
    val message: String?,
    val data: BundleDetail?
)