package com.dreams.readingmate.data.adapter

import android.app.Dialog
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.ui.home.books.BooksDetailsFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.child_single_list_items.view.*
import kotlinx.android.synthetic.main.progress_single_list_items.view.txtChildName

class GetChildListAdapter(
    private val itemList: List<ChildListItem>,
    private val mListener: BooksDetailsFragment.BooksDetailsFragmentInterface?,
    private val booksDetailFragment: BooksDetailsFragment,
    private val dialog: Dialog
) :
    RecyclerView.Adapter<GetChildListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.child_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, booksDetailFragment, dialog)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ChildListItem,
            mListener: BooksDetailsFragment.BooksDetailsFragmentInterface?,
            booksDetailFragment: BooksDetailsFragment,
            dialog: Dialog
        ) {
            val cardViewChild = itemView.cardViewChild
            val imgChild = itemView.imgChildImage
            val txtChildName = itemView.txtChildName
            txtChildName.text = items.childName
            items.image?.let { Utils.displayCircularImage(itemView.context, imgChild, it) }
            cardViewChild.setOnClickListener {
                dialog.dismiss()
                booksDetailFragment.callBookStatusAPi(items.id)
            }
        }
    }
}