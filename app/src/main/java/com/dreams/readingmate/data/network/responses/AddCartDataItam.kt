package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class AddCartDataItam(

    @field:SerializedName("grandTotal")
    val grandTotal: Double? = null,

    @field:SerializedName("subTotal")
    val subTotal: Double? = null,

    @field:SerializedName("deliveryCharge")
    val deliveryCharge: Double? = null
)