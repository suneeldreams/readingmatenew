package com.dreams.readingmate.data.db

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

/**
 * Created by Shivank on 4/2/2018.
 */

object mUserData {

    val USER_PIC = "USER_PIC"
    val USER_NAME = "USER_NAME"
    val REMEMBER_ME = "REMEMBER_ME"
    val IS_FIRST_TIME = "IS_FIRST_TIME"
    val REM_EMAIL = "REM_EMAIL"
    val REM_PASSWORD = "REM_PASSWORD"
    val USER_PASSWORD = "USER_PASSWORD"
    val TOKEN = "token"
    val ISREMEMBER = "remember"
    val ISPROACCOUNT = "is_proaccount"
    val REMAININGDAYS = "remainingdays"
    val OTP_VARIFICATION_TOKEN = "otp_varication_token"
    val USER_EMAIL = "email"
    val LANGUAGE = "language"
    val ENGLISH = "English"
    val KOREAN = "Korean"
    val HINDI = "Hindi"
    val CHINESE = "Chinese"
    val SPANISH = "Spanish"
    val JAPANESE = "Japanese"
    val USER_ID = "user_id"
    val PROFILE_IMAGE = "PROFILE_IMAGE"
    val RELOAD = "reload_chat_fragment"
    val USER_FULLNAME = "user_fullname"
    val USER_FIRSTNAME = "user_firstname"
    val USER_IMG = "user_image"
    val CLICKEDID = "click_id"
    val USER_LASTTNAME = "user_lastname"
    val MOBILE = "mobile"
    val PASSWORD = "password"
    val COUNTRY_CODE = "country_code"
    val SURNAME = "surname"
    val USER_PROFILE_PIC = "user_lastname"
    val BASE_CURRENCY = "base_currency"
    val FROM = "from"
    val WATCHLIST_CURRENCY = "watchlist_currency"
    private val PREFERENCE_KEY = "user_pref"
    private val PREFERENCE = "user_remember_pref"

    fun defaultPrefs(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun customPrefs(context: Context): SharedPreferences = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)
    fun rememberPrefs(context: Context): SharedPreferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE)

    fun clearAllPref(context: Context) {
        val sharedPreferences = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().apply()
    }

    inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    /**
     * puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key]
     */
    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit({ it.putString(key, value) })
            is Int -> edit({ it.putInt(key, value) })
            is Boolean -> edit({ it.putBoolean(key, value) })
            is Float -> edit({ it.putFloat(key, value) })
            is Long -> edit({ it.putLong(key, value) })
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    /**
     * finds value on given key.
     * [T] is the type of value
     * @param defaultValue optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
     */
    operator inline fun <reified T : Any> SharedPreferences.get(key: String, defaultValue: T? = null): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as? String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    fun clearAllData() {

    }
}