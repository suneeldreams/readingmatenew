package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class NewsListData(

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("rows")
    val rows: List<RowsItems?>? = null
)

data class RowsItems(

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("img")
    val img: String? = null,

    @field:SerializedName("shortDesc")
    val shortDesc: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("isFavorite")
    val isFavorite: Boolean = false,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("categoryName")
    val categoryName: String? = null,

    @field:SerializedName("categoryId")
    val categoryId: String? = null
)
