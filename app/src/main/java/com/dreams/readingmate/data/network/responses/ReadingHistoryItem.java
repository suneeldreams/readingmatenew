package com.dreams.readingmate.data.network.responses;

import com.google.gson.annotations.SerializedName;

public class ReadingHistoryItem{

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("_id")
	private String id;

	public String getCreatedAt(){
		return createdAt;
	}

	public String getId(){
		return id;
	}
}