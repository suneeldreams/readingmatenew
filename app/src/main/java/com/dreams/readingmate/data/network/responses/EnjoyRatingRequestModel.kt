package com.dreams.readingmate.data.network.responses


data class EnjoyRatingRequestModel(
    var bookRating: String?
)