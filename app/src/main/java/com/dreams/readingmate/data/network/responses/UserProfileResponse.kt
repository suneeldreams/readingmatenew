package com.dreams.readingmate.data.network.responses

data class UserProfileResponse(
    val success: Boolean?,
    val message: String?,
    val data: UserProfileData?

)