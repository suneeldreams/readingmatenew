package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class UserInfoData(
    @field:SerializedName("userInfo")
    val userInfo: AuthInfoResponse?,

    @field:SerializedName("token")
    val token: String? = null
)