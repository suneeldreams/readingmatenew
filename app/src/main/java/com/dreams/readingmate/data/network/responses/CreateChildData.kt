package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class CreateChildData(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("favBookData")
	val favBookData: List<FavBookDataItem?>? = null,

	@field:SerializedName("fav_character_ids")
	val favCharacterIds: String? = null,

	@field:SerializedName("favSubjectData")
	val favSubjectData: List<FavSubjectDataItem?>? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("child_name")
	val childName: String? = null,

	@field:SerializedName("colour")
	val colour: String? = null,

	@field:SerializedName("favGenreData")
	val favGenreData: List<FavGenreDataItem?>? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("fav_book_ids")
	val favBookIds: String? = null,

	@field:SerializedName("fav_subject_ids")
	val favSubjectIds: String? = null,

	@field:SerializedName("nick_name")
	val nickName: String? = null,

	@field:SerializedName("fav_genre_ids")
	val favGenreIds: String? = null,

	@field:SerializedName("country_name")
	val countryName: String? = null,

	@field:SerializedName("school_type_id")
	val schoolTypeId: Int? = null,

	@field:SerializedName("school_type_name")
	val schoolTypeName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("favCharacterData")
	val favCharacterData: List<FavCharacterDataItem?>? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("country_id")
	val countryId: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class FavSubjectDataItem(

	@field:SerializedName("subject_name")
	val subjectName: String? = null
)

data class FavGenreDataItem(

	@field:SerializedName("genre_name")
	val genreName: String? = null
)

data class FavCharacterDataItem(

	@field:SerializedName("character_name")
	val characterName: String? = null
)
data class FavBookDataItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("book_name")
	val bookName: String? = null
)
