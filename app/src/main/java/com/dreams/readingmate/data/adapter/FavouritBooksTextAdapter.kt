package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.favouritesbooks.FavouriteBookFragment
import kotlinx.android.synthetic.main.item_row_fav_text.view.*
import java.util.ArrayList

class FavouritBooksTextAdapter(
    private val itemList: List<FavBookData?>?,
    private val mListener: FavouriteBookFragment.FavouriteBookFragmentInterface,
    private val favouriteBookFragment: FavouriteBookFragment
) :
    RecyclerView.Adapter<FavouritBooksTextAdapter.ViewHolder>() {
    lateinit var mSelectedItems: ArrayList<String>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_fav_text, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList?.get(position), mListener, favouriteBookFragment)
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData?,
            mListener: FavouriteBookFragment.FavouriteBookFragmentInterface,
            favouriteBookFragment: FavouriteBookFragment
        ) {
            val tv_label = itemView.textFav
            tv_label.text = items!!.title
            itemView.setOnClickListener {
//                favouriteBookFragment.AddImageToStringArray(items.img, items.id)
            }
        }
    }
}