package com.dreams.readingmate.data.network.responses;

import com.google.gson.annotations.SerializedName;

public class Achievements {

    @SerializedName("ach3Rank")
    private int ach3Rank;

    @SerializedName("ach2Rank")
    private int ach2Rank;

    @SerializedName("ach1Rank")
    private int ach1Rank;

    @SerializedName("ach4Rank")
    private int ach4Rank;

    public int getAch3Rank() {
        return ach3Rank;
    }

    public int getAch2Rank() {
        return ach2Rank;
    }

    public int getAch1Rank() {
        return ach1Rank;
    }

    public int getAch4Rank() {
        return ach4Rank;
    }
}