package com.dreams.readingmate.data.network.responses

data class CreateAddressResponse(
    val status: Boolean?,
    val message: String?,
    val data: CreateAddressData?
)