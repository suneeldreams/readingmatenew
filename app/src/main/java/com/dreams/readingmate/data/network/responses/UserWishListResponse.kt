package com.dreams.readingmate.data.network.responses

data class UserWishListResponse(
    val status: Boolean?,
    val message: String?,
    val data: List<UserWishListData>?
)