package com.dreams.readingmate.data.network.responses

data class MembershipStatusResponse(
    val status: Boolean?,
    val message: String?,
    val data: UserMembershipStatusData?
)