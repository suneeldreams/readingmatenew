package com.dreams.readingmate.data.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils

/**
 * Created by Suneel on 08/27/2019.
 */
class CurrentlyReadingAdapter(
    private val mContext: Context,
    private val items: ArrayList<Model.Comments>
) :
    RecyclerView.Adapter<CurrentlyReadingAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /* holder.mTitle.text = items[position].name
         holder.mDescription.text = items[position].comment
         if (!TextUtils.isEmpty(items[position].image)) {
             Utils.displayCircularImage(mContext, holder.mUser, items[position].image)
         }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext)
                .inflate(R.layout.currently_books_single_items, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
//        val mTitle: TextView = view.tv_name
//        val mDescription: TextView = view.tv_comment
//        val mUser: ImageView = view.img_user_comments
    }
}