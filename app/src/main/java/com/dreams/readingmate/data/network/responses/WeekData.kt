package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName


data class WeekData(
    @field:SerializedName("day")
    val day: String? = null,

    @field:SerializedName("dayName")
    val dayName: String? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("isRead")
    val isRead: Boolean? = null,
    var isSelected: Boolean = false
)