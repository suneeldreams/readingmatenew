package com.dreams.readingmate.data.network.responses


data class ForgotPassword(
    var email: String?
)