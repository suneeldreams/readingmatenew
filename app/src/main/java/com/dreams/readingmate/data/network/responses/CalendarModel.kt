package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable


data class CalendarModel(
    var Date: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CalendarModel> {
        override fun createFromParcel(parcel: Parcel): CalendarModel {
            return CalendarModel(parcel)
        }

        override fun newArray(size: Int): Array<CalendarModel?> {
            return arrayOfNulls(size)
        }
    }
}