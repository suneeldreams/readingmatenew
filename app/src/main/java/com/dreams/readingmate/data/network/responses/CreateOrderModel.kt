package com.dreams.readingmate.data.network.responses


data class CrateOrderModel(
    var cartData: String?,
    var stripeToken: String?,
    var deliveryCharge: String?,
    var grandTotal: String?,
    var titlename: String?,
    var iname: String?,
    var iaddr1: String?,
    var iaddr2: String?,
    var iaddr3: String?,
    var iaddr4: String?,
    var ipcode: String?,
    var icountry: String?,
    var dtitlename: String?,
    var dname: String?,
    var daddr1: String?,
    var daddr2: String?,
    var daddr3: String?,
    var daddr4: String?,
    var dpcode: String?,
    var dcountry: String?,
    var trackingsafeplace: String?,
    var comm1: String?,
    var comm2: String?,
    var comm3: String?,
    var comm4: String?,
    var couponCode: String?
)