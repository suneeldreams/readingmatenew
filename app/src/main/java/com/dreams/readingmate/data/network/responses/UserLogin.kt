package com.dreams.readingmate.data.network.model


data class UserLogin(
    var email: String?,
    var password: String?
)