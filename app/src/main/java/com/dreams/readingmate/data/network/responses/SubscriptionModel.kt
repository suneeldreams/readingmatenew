package com.dreams.readingmate.data.network.responses


data class SubscriptionModel(
    var planInterval: String?,
    var planName: String?,
    var desc: String?,
    var planAmount: Double?,
    var id: String?,
    var upfrontAmount: Double?

)