package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class ClientSecretKeyData(

	@field:SerializedName("client_secret")
	val clientSecret: String? = null
)
