package com.dreams.readingmate.data.network.responses

data class ChildProgressResponse(
    val status: Boolean?,
    val message: String?,
    val data: ChildProgressD?
)