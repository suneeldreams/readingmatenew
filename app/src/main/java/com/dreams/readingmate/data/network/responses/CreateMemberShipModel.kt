package com.dreams.readingmate.data.network.responses


data class CrateMemberShipModel(
    var name: String?,
    var email: String?,
    var nameOnCard: String?,
    var cardNumber: String?,
    var cardExpMonth: String?,
    var cardExpYear: String?,
    var postalCode: String?,
    var subscriptionPlanId: String?,
    var cvc: String?,
    var isUpfront: Boolean
)