package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class ReviewData(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("rating")
	val rating: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("childId")
	val childId: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("bookId")
	val bookId: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
