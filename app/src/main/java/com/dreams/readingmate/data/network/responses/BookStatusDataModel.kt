package com.dreams.readingmate.data.network.model


data class BookStatusDataModel(
    var bookId: String?,
    var pageRead: String?,
    var readingStatus: String?
)