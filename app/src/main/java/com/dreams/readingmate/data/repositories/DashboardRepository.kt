package com.dreams.readingmate.data.repositories

import com.dreams.readingmate.data.network.MyApi
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.AddWeekReadDataModel
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.model.UpdateProfileModel
import com.dreams.readingmate.data.network.responses.*

class DashboardRepository(private val api: MyApi) : BaseApiRequest() {
    suspend fun getChildPreference(accessToken: String): ResultState<PreferenceChildResponse> {
        return apiRequest {
            api.getChildPreference(accessToken)
        }
    }

    suspend fun removeChild(accessToken: String, id: String?): ResultState<SuccessResponse> {
        return apiRequest {
            api.removeChild(accessToken, id!!)
        }
    }

    suspend fun getChildList(accessToken: String, userId: String): ResultState<ChildListResponse> {
        return apiRequest {
            api.getChildList(accessToken)
        }
    }

    suspend fun getUserProfile(accessToken: String): ResultState<CardListResponse> {
        return apiRequest {
            api.getUserProfile(accessToken)
        }
    }

    suspend fun getBookList(
        accessToken: String,
        orderBy: String,
        orderDir: String,
        pageNumber: String,
        pageSize: String
    ): ResultState<BooksRespons> {
        return apiRequest {
            api.getBookList(accessToken)
        }
    }

    suspend fun getBookDetailData(
        accessToken: String,
        bookId: String,
        childId: String
    ): ResultState<BooksDetailsRespons> {
        return apiRequest {
            api.getBookDetailData(accessToken, bookId, childId)
        }
    }

    suspend fun favoriteBook(
        accessToken: String,
        bookId: String,
        childId: String
    ): ResultState<BooksDetailsRespons> {
        return apiRequest {
            api.favoriteBook(accessToken, bookId)
        }
    }

    suspend fun removefavoriteBook(
        accessToken: String,
        bookId: String,
        childId: String
    ): ResultState<BooksDetailsRespons> {
        return apiRequest {
            api.removefavoriteBook(accessToken, bookId)
        }
    }

    suspend fun addReview(
        accessToken: String,
        bookId: String,
        childId: String,
        rating: String,
        review: String
    ): ResultState<ReviewResponse> {
        return apiRequest {
            api.addReview(accessToken, bookId, childId, rating, review)
        }
    }

    suspend fun getClientSecret(
        accessToken: String,
        grandTotal: String
    ): ResultState<ClientSecretResponse> {
        return apiRequest {
            api.clientSecretKey(accessToken, grandTotal)
        }
    }

    suspend fun getRecommendBookList(
        accessToken: String
    ): ResultState<FavBookListResponse> {
        return apiRequest {
            api.getRecommendBooksList(accessToken)
        }
    }

    suspend fun getChildRecommendBookList(
        accessToken: String, childId: String
    ): ResultState<FavBookListResponse> {
        return apiRequest {
            api.getChildRecommendBooksList(accessToken, childId)
        }
    }

    suspend fun getAllBookData(
        accessToken: String,
        childId: String
    ): ResultState<AllBooksDataRespons> {
        return apiRequest {
            api.getAllBookData(accessToken, childId)
        }
    }

    suspend fun getChildDetailList(
        accessToken: String,
        childId: String
    ): ResultState<ChildDetailResponse> {
        return apiRequest {
            api.getChildDetailList(accessToken, childId)
        }
    }

    suspend fun getFavList(
        accessToken: String,
        search: String,
        pageSize: String,
        cartitems: String,
        filter: String
    ): ResultState<FavBookListResponse> {
        return apiRequest {
            api.getFavList(accessToken, search, pageSize, cartitems, filter)
        }
    }

    suspend fun getNextBook(
        accessToken: String,
        childId: String
    ): ResultState<FavBookListResponse> {
        return apiRequest {
            api.getNextBook(accessToken, childId)
        }
    }

    suspend fun getNewsList(
        accessToken: String,
        search: String,
        pageSize: String,
        orderBy: String,
        orderDir: String,
        pageNumber: String
    ): ResultState<NewsResponse> {
        return apiRequest {
            api.getNewsList(accessToken, search, pageSize, orderBy, orderDir, pageNumber)
        }
    }

    suspend fun getOrderBookList(
        accessToken: String
    ): ResultState<OrderBookResponse> {
        return apiRequest {
            api.getOrderBookList(accessToken)
        }
    }

    suspend fun getUserWishListList(
        accessToken: String
    ): ResultState<UserWishListResponse> {
        return apiRequest {
            api.getUserWishList(accessToken)
        }
    }

    suspend fun getSubscriptionPlanList(
        accessToken: String,
        planType: String
    ): ResultState<SubscriptionResponse> {
        return apiRequest {
            api.getSubscriptionPlanList(accessToken, planType)
        }
    }

    suspend fun getMembershipStatus(
        accessToken: String
    ): ResultState<MembershipStatusResponse> {
        return apiRequest {
            api.getMembershipStatus(accessToken)
        }
    }

    suspend fun getBundleList(
        accessToken: String
    ): ResultState<BundleResponse> {
        return apiRequest {
            api.getBundleList(accessToken)
        }
    }

    suspend fun getNewsDetails(
        accessToken: String,
        search: String
    ): ResultState<NewsDetailsResponse> {
        return apiRequest {
            api.getNewsDetails(accessToken, search)
        }
    }

    suspend fun getBundleDetails(
        accessToken: String,
        slug: String
    ): ResultState<BundleDetailsResponse> {
        return apiRequest {
            api.getBundleDetails(accessToken, slug)
        }
    }

    suspend fun getChildProgress(
        accessToken: String,
        childId: String
    ): ResultState<ChildProgressResponse> {
        return apiRequest {
            api.getChildProgress(accessToken, childId)
        }
    }

    suspend fun getChildBooksReadList(
        accessToken: String,
        childId: String
    ): ResultState<ChildBooksReadListResponse> {
        return apiRequest {
            api.getChildBooksReadList(accessToken, childId)
        }
    }

    /*suspend fun addBookToBookShelf(
        accessToken: String,
        bookId: String,
        totalDaysToRead: String
    ): ResultState<ChildBooksReadListResponse> {
        return apiRequest {
            api.addBookToBookShelf(accessToken, bookId,totalDaysToRead)
        }
    }*/

    suspend fun addBookToBookShelf(
        accessToken: String,
        bookId: String,
        childId: String,
        readingStatus: String?
    ): ResultState<AddBookShelfResponse> {
        return apiRequest {
            api.addBookToBookShelf(accessToken, bookId, childId, readingStatus!!)
        }
    }

    suspend fun removeBookFromShelf(
        accessToken: String,
        childId: String,
        book_id: String
    ): ResultState<AddBookShelfResponse> {
        return apiRequest {
            api.removeBookFromShelf(accessToken, childId, book_id)
        }
    }

    suspend fun getChildBooksReviewedList(
        accessToken: String,
        childId: String
    ): ResultState<ChildBooksReviewedListResponse> {
        return apiRequest {
            api.getChildBooksReviewedList(accessToken, childId)
        }
    }


    suspend fun createChild(
        user: CrateChildModel,
        accessToken: String,
        userId: String
    ): ResultState<CreateChildResponse> {
        return apiRequest {
            api.createChild(accessToken, userId, user)
        }
    }

    suspend fun createAddress(
        user: CrateAddressModel,
        accessToken: String
    ): ResultState<CreateAddressResponse> {
        return apiRequest {
            api.createAddress(accessToken, user)
        }
    }

    suspend fun deleteCard(
        accessToken: String,
        cardId: String
    ): ResultState<CreateAddressResponse> {
        return apiRequest {
            api.deleteCard(accessToken, cardId)
        }
    }

    suspend fun AddCard(
        user: CrateCardModel,
        accessToken: String
    ): ResultState<CreateAddressResponse> {
        return apiRequest {
            api.addCard(accessToken, user)
        }
    }

    suspend fun createOrder(
        user: CrateOrderModel,
        accessToken: String
    ): ResultState<OrderResponse> {
        return apiRequest {
            api.createOrder(accessToken, user)
        }
    }

    suspend fun addCartData(
        user: CartDataModel,
        accessToken: String
    ): ResultState<AddCartDataResponse> {
        return apiRequest {
            api.addCartData(accessToken, user)
        }
    }

    suspend fun addWeekReadData(
        user: AddWeekReadDataModel,
        accessToken: String,
        childId: String
    ): ResultState<AddBookShelfResponse> {
        return apiRequest {
            api.addWeekReadData(accessToken, childId, user)
        }
    }

    suspend fun bookStatusData(
        user: BookStatusDataModel,
        accessToken: String,
        childId: String
    ): ResultState<AddBookShelfResponse> {
        return apiRequest {
            api.bookStatus(accessToken, childId, user)
        }
    }

    suspend fun cancelSubscription(
        accessToken: String,
        membershipID: String
    ): ResultState<CancelSubscriptionResponse> {
        return apiRequest {
            api.cancelSubsCription(accessToken, membershipID)
        }
    }


    suspend fun addBookMark(
        accessToken: String,
        bookId: String
    ): ResultState<AddRemoveFavResponse> {
        return apiRequest {
            api.addBookMark(accessToken, bookId)
        }
    }

    suspend fun addWeekTickData(
        user: AddWeekReadDataModel,
        accessToken: String,
        childId: String
    ): ResultState<UpdateTickResponse> {
        return apiRequest {
            api.addWeekTickData(accessToken, childId, user)
        }
    }

    suspend fun removeTick(
        accessToken: String,
        history: String,
        childId: String
    ): ResultState<UpdateTickResponse> {
        return apiRequest {
            api.removeTick(accessToken, childId, history)
        }
    }

    suspend fun updateChild(
        user: UpdateChildModel,
        accessToken: String,
        updateChildId: String
    ): ResultState<CreateChildResponse> {
        return apiRequest {
            api.updateChild(accessToken, updateChildId, user)
        }
    }

    suspend fun hardBookRating(
        user: HardRatingRequestModel,
        accessToken: String,
        childId: String,
        bookId: String
    ): ResultState<SuccessResponse> {
        return apiRequest {
            api.hardBookRating(accessToken, childId, bookId, user)
        }
    }

    suspend fun enjoyBookRating(
        user: EnjoyRatingRequestModel,
        accessToken: String,
        childId: String,
        bookId: String
    ): ResultState<SuccessResponse> {
        return apiRequest {
            api.enjoyBookRating(accessToken, childId, bookId, user)
        }
    }


    suspend fun updateProfile(
        user: UpdateProfileModel,
        accessToken: String
    ): ResultState<AuthResponse> {
        return apiRequest {
            api.updateProfile(accessToken, user)
        }
    }

    suspend fun ValidateCoupon(
        accessToken: String,
        couponCode: String
    ): ResultState<CouponCodeResponse> {
        return apiRequest {
            api.validateCouponCode(accessToken, couponCode)
        }
    }
}