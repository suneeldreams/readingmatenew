package com.dreams.readingmate.data.db

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

/**
 * Created by Shivank on 4/2/2018.
 */

object UserData {

    val USER_ID = "USER_ID"
    val ID = "ID"
    val FIRST_NAME = "FIRST_NAME"
    val FULLNAME = "FULLNAME"
    val NAME = "NAME"
    val CHILD_NAME = "CHILD_NAME"
    val NICK_NAME = "NICK_NAME"
    val CHILD_IMAGE = "CHILD_IMAGE"
    val TOKEN = "TOKEN"
    val LAST_NAME = "LAST_NAME"
    val REFERAL_CODE = "REFERAL_CODE"
    val COUNTRY_CODE = "COUNTRY_CODE"
    val COUNTRY = "COUNTRY"
    val MOBILE = "MOBILE"
    val IS_MEMBERSHIP = "IS_MEMBERSHIP"
    val REMAINING_BOOKS = "REMAINING_BOOKS"
    val USER_TYPE = "USER_TYPE"
    val PER_DAY_DRINK = "PER_DAY_DRINK"
    val GENDER = "GENDER"
    val DEVICE_TOKEN = "DEVICE_TOKEN"
    val DOB = "DOB"
    val ADDRESS = "ADDRESS"
    val POSTAL_CODE = "POSTAL_CODE"
    val CITY = "CITY"
    val MEMBER_SINCE = "MEMBER_SINCE"
    val CURRENTLAT = "CURRENTLAT"
    val CURRENTLANG = "CURRENTLANG"
    val EMAIL = "EMAIL"
    val CHARITY = "CHARITY"
    val REM_EMAIL = "REM_EMAIL"
    val REM_PASSWORD = "REM_PASSWORD"
    val REMEMBER_ME = "REMEMBER_ME"
    val TOUCH_ID_INTERVAL = "TOUCH_ID_INTERVAL"
    val IS_ACTIVE_SECURITY = "IS_ACTIVE_SECURITY"
    val PROFILE_IMAGE = "PROFILE_IMAGE"
    val ROLE = "ROLE"
    val QR_TEXT = "QR_TEXT"
    val MEMBERSHIP = "MEMBERSHIP"
    val FREE_DRINK = "FREE_DRINK"
    val PLAN_NAME = "PLAN_NAME"
    val PLAN_IMG = "PLAN_IMG"
    val RESTAURANT_ID = "RESTAURANT_ID"
    val LAST_REVIEW = "LAST_REVIEW"
    val PENDING_FEEDBACK = "PENDING_FEEDBACK"
    val ISAMBASSDOR = "ISAMBASSDOR"
    val CURRENT_BALANCE = "CURRENT_BALANCE"

    val IS_FIRST_TIME = "IS_FIRST_TIME"
    val CURRENT_LANGUAGE = "CURRENT_LANGUAGE"
    val LANGUAGE_ENGLISH = "LANGUAGE_ENGLISH"
    val LANGUAGE_FRENCH = "LANGUAGE_FRENCH"
    val LANGUAGE_ARABIC = "LANGUAGE_ARABIC"
    val CARD_NUMBER = "CARD_NUMBER"
    val CARD_HOLDER = "CARD_HOLDER"
    val CARD_MONTH = "CARD_MONTH"
    val CARD_YEAR = "CARD_YEAR"
    val CVV = "CVV"
    const val BROADCAST_DEFAULT_EVENT = "BROADCAST_DEFAULT_EVENT"


    private val PREFERENCE_KEY = "user_pref"
    private val PREFERENCE_REM = "user_pref_rem"
    private val PREFERENCE_KEY_GLOBAL = "user_pref_global"

    fun defaultPrefs(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    fun customPrefs(context: Context): SharedPreferences =
        context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)

    fun rememberPrefs(context: Context): SharedPreferences =
        context.getSharedPreferences(PREFERENCE_REM, Context.MODE_PRIVATE)

    fun customPrefsGlobal(context: Context): SharedPreferences =
        context.getSharedPreferences(PREFERENCE_KEY_GLOBAL, Context.MODE_PRIVATE)

    fun clearrememberPref(context: Context) {
        val sharedPreferences = context.getSharedPreferences(PREFERENCE_REM, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().apply()
    }

    fun clearCustomPref(context: Context) {
        val sharedPreferences = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().apply()
    }

    fun clearGlobalPref(context: Context) {
        val sharedPreferences =
            context.getSharedPreferences(PREFERENCE_KEY_GLOBAL, Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().apply()
    }

    inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    /**
     * puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key]
     */
    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit({ it.putString(key, value) })
            is Int -> edit({ it.putInt(key, value) })
            is Boolean -> edit({ it.putBoolean(key, value) })
            is Float -> edit({ it.putFloat(key, value) })
            is Long -> edit({ it.putLong(key, value) })
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    /**
     * finds value on given key.
     * [T] is the type of value
     * @param defaultValue optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
     */
    operator inline fun <reified T : Any> SharedPreferences.get(
        key: String,
        defaultValue: T? = null
    ): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as? String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }
}