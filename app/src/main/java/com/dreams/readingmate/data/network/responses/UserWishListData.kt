package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserWishListData(

    @field:SerializedName("createdAt")
    var createdAt: String? = null,

    @field:SerializedName("img")
    var img: String? = null,

    @field:SerializedName("author")
    var authors: List<String?>? = null,

    @field:SerializedName("_id")
    var id: String? = null,

    @field:SerializedName("bookId")
    var bookId: String? = null,

    @field:SerializedName("title")
    var title: String? = null
)