package com.dreams.readingmate.data.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.ui.home.books.AllTypeBooksFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.search_grid_single_item.view.*

class SearchBookListAdapter(
    private val itemList: List<FavBookData>,
    private val mListener: AllTypeBooksFragment.AllTypeBookFragmentInterface?,
    private val childId: String
) :
    RecyclerView.Adapter<SearchBookListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_grid_single_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList.get(position), mListener, childId)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: FavBookData,
            mListener: AllTypeBooksFragment.AllTypeBookFragmentInterface?,
            childId: String
        ) {
            val imgSearchBooks = itemView.imgSearchBooks
            val txtBookTitle = itemView.txtBookTitle
//            val txtDisciption = itemView.txtDisciption
//            val txtBookRating = itemView.txtBookRating
//            val btnBookPrice = itemView.btnBookPrice
            txtBookTitle.text = items.title
//            txtDisciption.text = items.shortDescription
//            txtBookRating.text = items.starRating.toString()
//            btnBookPrice.text =
//                itemView.context.resources.getString(R.string.currency_pound) + items.ukPrice.toString()
            items.img?.let {
                Utils.displayImage(
                    itemView.context,
                    it,
                    imgSearchBooks,
                    R.drawable.ic_place_holder
                )
            }
            imgSearchBooks.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("childId", childId)
                bundle.putString("bookId", items.id)
                mListener?.openBookDetailFragment(bundle)
            }
        }
    }
}