package com.dreams.readingmate.data.network.responses

data class AddRemoveFavResponse(
    val status: Boolean?,
    val message: String?,
    val data: AddRemoveFavData?
)