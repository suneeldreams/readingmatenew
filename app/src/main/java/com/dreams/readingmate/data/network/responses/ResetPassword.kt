package com.dreams.readingmate.data.network.responses


data class ResetPassword(
    var current_password: String?,
    var new_password: String?,
    var confirm_new_password: String?
)