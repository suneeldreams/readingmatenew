package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName

data class BundleDetail(

	@field:SerializedName("bundleThumbImg")
	val bundleThumbImg: String? = null,

	@field:SerializedName("bundlePublisher")
	val bundlePublisher: String? = null,

	@field:SerializedName("bundleReadingAge")
	val bundleReadingAge: String? = null,

	@field:SerializedName("bundleBooks")
	val bundleBooks: List<BundleBooksItem?>? = null,

	@field:SerializedName("bundleDimensions")
	val bundleDimensions: String? = null,

	@field:SerializedName("bundleLanguage")
	val bundleLanguage: String? = null,

	@field:SerializedName("bundleDescription")
	val bundleDescription: String? = null,

	@field:SerializedName("bundleImg")
	val bundleImg: List<String?>? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("bundleColor")
	val bundleColor: String? = null,

	@field:SerializedName("bundleName")
	val bundleName: String? = null,

	@field:SerializedName("bundleAuthor")
	val bundleAuthor: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("bundleSlug")
	val bundleSlug: String? = null,

	@field:SerializedName("bundleAmount")
	val bundleAmount: Double? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("bundlePublicationDate")
	val bundlePublicationDate: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("bundleSubDescription")
	val bundleSubDescription: String? = null
)

data class BundleBooksItem(

	@field:SerializedName("yearOfPublication")
	val yearOfPublication: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("bicClassification")
	val bicClassification: List<Any?>? = null,

	@field:SerializedName("eanNumber")
	val eanNumber: String? = null,

	@field:SerializedName("isbn")
	val isbn: String? = null,

	@field:SerializedName("ukPriceIncVAT")
	val ukPriceIncVAT: Double? = null,

	@field:SerializedName("binding")
	val binding: String? = null,

	@field:SerializedName("language")
	val language: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("imageUpdated")
	val imageUpdated: Boolean? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("numberOfPages")
	val numberOfPages: Int? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("stock")
	val stock: Int? = null,

	@field:SerializedName("starRating")
	val starRating: Int? = null,

	@field:SerializedName("vatFlag")
	val vatFlag: String? = null,

	@field:SerializedName("publicationDate")
	val publicationDate: String? = null,

	@field:SerializedName("duplicateChecked")
	val duplicateChecked: Boolean? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("editor")
	val editor: List<String?>? = null,

	@field:SerializedName("returnsFlag")
	val returnsFlag: String? = null,

	@field:SerializedName("author")
	val author: List<Any?>? = null,

	@field:SerializedName("gardnersClassificationCode")
	val gardnersClassificationCode: String? = null,

	@field:SerializedName("ukAvailability")
	val ukAvailability: String? = null,

	@field:SerializedName("publisherName")
	val publisherName: String? = null,

	@field:SerializedName("isbn13")
	val isbn13: String? = null,

	@field:SerializedName("isRecommended")
	val isRecommended: Boolean? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("countryOfOrigin")
	val countryOfOrigin: String? = null,

	@field:SerializedName("ukPriceExcVAT")
	val ukPriceExcVAT: Double? = null,

	@field:SerializedName("ukPrice")
	val ukPrice: Double? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("subtitle")
	val subtitle: String? = null,

	@field:SerializedName("shortDescription")
	val shortDescription: String? = null
)
