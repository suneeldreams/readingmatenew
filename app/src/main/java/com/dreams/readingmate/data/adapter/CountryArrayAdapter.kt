package com.dreams.readingmate.data.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.CountryDataItem
import com.dreams.readingmate.data.network.responses.SchoolTypeDataItem


class CountryArrayAdapter(context: Context, private var tasks: List<CountryDataItem?>? = listOf()) :
    ArrayAdapter<CountryDataItem>(context, R.layout.item_row_state, R.id.tv_label, tasks!!) {
    private var inflater = LayoutInflater.from(context)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent)
    }

    private fun createItemView(position: Int, convertView: View?, parent: ViewGroup): View {
        val taskInfo = tasks!![position]
        val view = convertView ?: inflater.inflate(R.layout.item_row_state, parent, false)
        val tvLabel = view.findViewById<TextView>(R.id.tv_label)
        tvLabel.text = taskInfo!!.countryName
        return view
    }


}
