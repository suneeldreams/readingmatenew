package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName


data class FavBookNewData(
    @field:SerializedName("_id")
    var id: String? = null,

    @field:SerializedName("img")
    var img: String? = null,

    @field:SerializedName("is_favourite")
    var isFavourite: Boolean? = null,

    @field:SerializedName("title")
    var title: String? = null
)