package com.dreams.readingmate.data.network.model


data class UserSignUp(
    var email: String?,
    var password: String?,
    var name: String?,
    var mobile: String?
)