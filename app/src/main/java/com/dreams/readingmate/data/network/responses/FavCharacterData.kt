package com.dreams.readingmate.data.network.responses

import com.google.gson.annotations.SerializedName


data class FavCharacterData(
    @field:SerializedName("_id")
    var id: String? = null,

    @field:SerializedName("characterName")
    var characterName: String? = null,

    @field:SerializedName("createdAt")
    var createdAt: String? = null,

    @field:SerializedName("slug")
    var slug: String? = null,

    @field:SerializedName("updatedAt")
    var updatedAt: String? = null,

    @field:SerializedName("is_favourite")
    var isFavourite: Boolean? = null,

    @field:SerializedName("status")
    var status: Int? = null
)