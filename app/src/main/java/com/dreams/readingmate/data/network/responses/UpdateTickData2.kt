package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UpdateTickData2(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("achievements")
	val achievements: Achievements2? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("wishlist")
	val wishlist: List<Any?>? = null,

	@field:SerializedName("schoolType")
	val schoolType: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("charity")
	val charity: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("favCharacters")
	val favCharacters: List<Any?>? = null,

	@field:SerializedName("readingHistory")
	val readingHistory: List<ReadingHistoryItem2?>? = null,

	@field:SerializedName("schoolName")
	val schoolName: String? = null,

	@field:SerializedName("favColour")
	val favColour: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("totalBooksRead")
	val totalBooksRead: Int? = null,

	@field:SerializedName("bookshelf")
	val bookshelf: List<BookshelfItem2?>? = null,

	@field:SerializedName("nickName")
	val nickName: String? = null,

	@field:SerializedName("favGenres")
	val favGenres: List<Any?>? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("run_streak")
	val runStreak: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("favBookIds")
	val favBookIds: List<String?>? = null,

	@field:SerializedName("favSubjects")
	val favSubjects: List<Any?>? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("reading_habit")
	val readingHabit: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        TODO("achievements"),
        parcel.readString(),
        TODO("wishlist"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        TODO("favCharacters"),
        TODO("readingHistory"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        TODO("bookshelf"),
        parcel.readString(),
        TODO("favGenres"),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.createStringArrayList(),
        TODO("favSubjects"),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeString(img)
        parcel.writeString(gender)
        parcel.writeString(schoolType)
        parcel.writeString(createdAt)
        parcel.writeString(charity)
        parcel.writeValue(V)
        parcel.writeString(schoolName)
        parcel.writeString(favColour)
        parcel.writeString(updatedAt)
        parcel.writeValue(totalBooksRead)
        parcel.writeString(nickName)
        parcel.writeString(userId)
        parcel.writeValue(runStreak)
        parcel.writeString(name)
        parcel.writeStringList(favBookIds)
        parcel.writeString(id)
        parcel.writeValue(age)
        parcel.writeValue(readingHabit)
        parcel.writeValue(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpdateTickData2> {
        override fun createFromParcel(parcel: Parcel): UpdateTickData2 {
            return UpdateTickData2(parcel)
        }

        override fun newArray(size: Int): Array<UpdateTickData2?> {
            return arrayOfNulls(size)
        }
    }
}

data class ReadingHistoryItem2(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("_id")
	val id: String? = null
)

data class BookshelfItem2(

	@field:SerializedName("readingStatus")
	val readingStatus: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("totalPageRead")
	val totalPageRead: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("bookId")
	val bookId: String? = null
)

data class Achievements2(

	@field:SerializedName("ach3Rank")
	val ach3Rank: Int? = null,

	@field:SerializedName("ach2Rank")
	val ach2Rank: Int? = null,

	@field:SerializedName("ach1Rank")
	val ach1Rank: Int? = null,

	@field:SerializedName("ach4Rank")
	val ach4Rank: Int? = null
)
