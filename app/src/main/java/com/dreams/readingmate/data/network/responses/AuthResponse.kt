package com.dreams.readingmate.data.network.responses

data class AuthResponse(
    val status: Boolean?,
    val message: String?,
    val token: String?,
    val data: AuthInfoResponse?

)