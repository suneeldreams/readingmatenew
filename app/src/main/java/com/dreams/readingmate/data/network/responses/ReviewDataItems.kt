package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ReviewDataItems(
    @field:SerializedName("bookId")
    val bookId: String? = null,

    @field:SerializedName("childId")
    val childId: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("review")
    val review: String? = null
)