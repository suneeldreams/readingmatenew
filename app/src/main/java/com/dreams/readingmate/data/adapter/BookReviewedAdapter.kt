package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildBooksReviewedData
import com.dreams.readingmate.ui.home.progress.BookReviewedFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.book_reviewed_single_list_items.view.*
import kotlinx.android.synthetic.main.bookshelf_single_list_items.view.*
import kotlinx.android.synthetic.main.bookshelf_single_list_items.view.txtBookName

class BookReviewedAdapter(
    private val mListener: BookReviewedFragment.BookReviewedFragmentInterface?,
    private val itemList: List<ChildBooksReviewedData>
) :
    RecyclerView.Adapter<BookReviewedAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_reviewed_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            itemList?.get(position),
            mListener,
            position
        )
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            items: ChildBooksReviewedData,
            mListener: BookReviewedFragment.BookReviewedFragmentInterface?,
            position: Int
        ) {
            val imgReviewedBook = itemView.imgReviewedBook
            itemView.txtBookName.text = items.title
//            itemView.txtHour.text = items.bookName
            itemView.ratingBar.rating = items.rating!!.toFloat()
//            itemView.textRating.text = items.rating.toString() + " (" + items.bookReviewCount + ")"
            itemView.txtReview.text = items.review
//            itemView.txtLike.text = items.bookLikeCount.toString()
//            itemView.txtComment.text = items.author
            Utils.displayImage(
                itemView.context,
                items.image!!,
                imgReviewedBook,
                R.drawable.ic_place_holder
            )
            itemView.setOnClickListener {
            }
        }
    }
}