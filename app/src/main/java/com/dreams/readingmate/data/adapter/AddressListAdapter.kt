package com.dreams.readingmate.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.AddressDataItem
import com.dreams.readingmate.ui.home.shoping.CheckOutFragment
import kotlinx.android.synthetic.main.addnew_location_single_list_items.view.*

class AddressListAdapter(
    private val listItems: List<AddressDataItem?>?,
    private val mListener: CheckOutFragment.CheckOutFragmentInterface?,
    private val checkOutFragment: CheckOutFragment
) :
    RecyclerView.Adapter<AddressListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.addnew_location_single_list_items, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(listItems?.get(position), mListener, checkOutFragment, position)
    }

    override fun getItemCount(): Int {
        return listItems!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            dataItems: AddressDataItem?,
            mListener: CheckOutFragment.CheckOutFragmentInterface?,
            checkOutFragment: CheckOutFragment,
            position: Int
        ) {

            val txtAddress: TextView = itemView.txtAddress
            val imgCheck: ImageView = itemView.imgCheck
            val rltPayment: RelativeLayout = itemView.rltAddress
            txtAddress.text = dataItems!!.addr3

            itemView.setOnClickListener {
                dataItems.isSelected = !dataItems.isSelected
                checkOutFragment.refreshAddress(position)
            }
            if (dataItems.isSelected) {
                rltPayment.background =
                    ContextCompat.getDrawable(
                        itemView.context!!,
                        R.drawable.card_selected_border_bg
                    )
                imgCheck.visibility = View.VISIBLE
            } else {
                rltPayment.background =
                    ContextCompat.getDrawable(
                        itemView.context!!,
                        R.drawable.card_gray_border_bg
                    )
                imgCheck.visibility = View.GONE
            }
        }
    }
}