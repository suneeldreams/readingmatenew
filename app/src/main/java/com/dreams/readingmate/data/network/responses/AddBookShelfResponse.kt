package com.dreams.readingmate.data.network.responses

data class AddBookShelfResponse(
    val status: Boolean?,
    val message: String?,
    val data: AddBookShelfData?

)