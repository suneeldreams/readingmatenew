package com.dreams.readingmate.data.network.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChildListItem(

    @field:SerializedName("img")
    val image: String? = null,

    @field:SerializedName("favCharacters")
    val favCharacterIds: List<String>? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("name")
    val childName: String? = null,

    @field:SerializedName("colour")
    val colour: String? = null,

    @field:SerializedName("favColour")
    val favColour: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("userId")
    val userId: String? = null,

    @field:SerializedName("favBookIds")
    val favBookIds: List<String>? = null,

    @field:SerializedName("favSubjects")
    val favSubjectIds: List<String>? = null,

    @field:SerializedName("nickName")
    val nickName: String? = null,

    @field:SerializedName("favGenres")
    val favGenreIds: List<String>? = null,

    @field:SerializedName("country")
    val countryName: String? = null,

    @field:SerializedName("school_type_id")
    val schoolTypeId: String? = null,

    @field:SerializedName("schoolName")
    val schoolName: String? = null,

    @field:SerializedName("schoolType")
    val schoolTypeName: String? = null,

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("totalBooksRead")
    val totalBooksRead: String? = null,

    @field:SerializedName("reading_habit")
    val readingHabit: String? = null,

    @field:SerializedName("run_streak")
    val runStreak: String? = null,

    @field:SerializedName("week_run_streak")
    val week_run_streak: String? = null,

    @field:SerializedName("age")
    val age: String? = null,

    @field:SerializedName("country_id")
    val countryId: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("achievements")
    val achievements: AchievementData? = null,

    @field:SerializedName("readingHistory")
    val readingHistory: List<HistoryDataItem?>? = null,

    @field:SerializedName("weekData")
    val weekData: List<WeekData?>? = null,

    @field:SerializedName("currentlyReadingBook")
    val currentlyReadingBook: CurrentlyReadingBook? = null,

    @field:SerializedName("favBookData")
    val favBookData: List<FavBookNewData?>? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("achievements"),
        TODO("readingHistory"),
        TODO("weekData"),
        TODO("favBookData")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeStringList(favCharacterIds)
        parcel.writeString(createdAt)
        parcel.writeString(childName)
        parcel.writeString(colour)
        parcel.writeString(updatedAt)
        parcel.writeString(userId)
        parcel.writeStringList(favBookIds)
        parcel.writeStringList(favSubjectIds)
        parcel.writeString(nickName)
        parcel.writeStringList(favGenreIds)
        parcel.writeString(countryName)
        parcel.writeString(schoolTypeId)
        parcel.writeString(schoolTypeName)
        parcel.writeString(id)
        parcel.writeString(totalBooksRead)
        parcel.writeString(readingHabit)
        parcel.writeString(runStreak)
        parcel.writeString(age)
        parcel.writeString(countryId)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChildListItem> {
        override fun createFromParcel(parcel: Parcel): ChildListItem {
            return ChildListItem(parcel)
        }

        override fun newArray(size: Int): Array<ChildListItem?> {
            return arrayOfNulls(size)
        }
    }
}
