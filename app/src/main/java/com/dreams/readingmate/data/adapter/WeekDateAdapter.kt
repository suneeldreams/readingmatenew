package com.dreams.readingmate.data.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import kotlinx.android.synthetic.main.week_date_single_item.view.*
import java.util.ArrayList

/**
 * Created by Suneel on 08/27/2019.
 */
class WeekDateAdapter(
    private val mContext: Context,
    private val dayList: ArrayList<String>,
    private val dateList: ArrayList<String>,
    private val currentDate: String
) :
    RecyclerView.Adapter<WeekDateAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtWeekDayName.text = dayList[position]
        holder.txtDate.text = dateList[position]
        if (dateList[position] == currentDate) {
            holder.lnrDate.setBackgroundResource(R.drawable.active_round_bg)
            holder.txtWeekDayName.setTextColor(mContext.resources.getColor(R.color.white))
            holder.txtDate.setTextColor(mContext.resources.getColor(R.color.white))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext)
                .inflate(R.layout.week_date_single_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dayList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtWeekDayName: TextView = view.txtWeekDayName
        val txtDate: TextView = view.txtDate
        val lnrDate: LinearLayout = view.lnrDate
    }
}