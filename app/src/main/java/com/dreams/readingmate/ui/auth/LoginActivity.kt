package com.dreams.readingmate.ui.auth

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.databinding.ActivityLoginBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : BaseActivity<ActivityLoginBinding>(R.layout.activity_login),
    NavigationContract, KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private val viewModel: AuthViewModel by instance<AuthViewModel>()
    lateinit var binding: ActivityLoginBinding
    var rememberPrefs: SharedPreferences? = null
    var prefs: SharedPreferences? = null

    override fun initComponents(savedInstanceState: Bundle?, binding: ActivityLoginBinding) {
        this.binding = binding
        binding.viewModel = viewModel
        this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setUpData()
        observeLiveEvents()
        observeNavigationEvents()

    }

    private fun setUpData() {
        rememberPrefs = mUserData.rememberPrefs(this)
        prefs = mUserData.customPrefs(this)
        val remember = rememberPrefs?.getBoolean(mUserData.REMEMBER_ME, false)
        if (remember == true) {
            rememberMe.isChecked = true
            val email = rememberPrefs?.getString(mUserData.REM_EMAIL, "")
            val password = rememberPrefs?.getString(mUserData.REM_PASSWORD, "")
            viewModel.setLoginData(email, password)
        }
        val accessToken = Utils.getCustomPref(this, UserData.TOKEN)
        if (accessToken.isNotEmpty()) {
            prefs!![mUserData.CLICKEDID] = "home"
            Intent(this, HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        }
        txtLogin.setTypeface(null, Typeface.BOLD)
    }
    /**
     * MVVM ObserverLiveEvents results.
     */
    override fun observeLiveEvents() {
        viewModel.userDetailLiveData.observe(this, Observer {
            it.userInfo!!.name?.let { it1 -> Utils.saveCustomPref(this, UserData.NAME, it1) }
            it.userInfo.id?.let { it1 -> Utils.saveCustomPref(this, UserData.ID, it1) }
            it.userInfo.email?.let { it1 -> Utils.saveCustomPref(this, UserData.EMAIL, it1) }
            it.userInfo.charity?.let { it1 -> Utils.saveCustomPref(this, UserData.CHARITY, it1) }
            it.userInfo.city?.let { it1 -> Utils.saveCustomPref(this, UserData.CITY, it1) }
            it.userInfo.gender?.let { it1 -> Utils.saveCustomPref(this, UserData.GENDER, it1) }
            it.userInfo.mobile?.let { it1 -> Utils.saveCustomPref(this, UserData.MOBILE, it1) }

            it.userInfo.image?.let { it1 ->
                Utils.saveCustomPref(
                    this,
                    UserData.PROFILE_IMAGE,
                    it1
                )
            }
            prefs!![mUserData.CLICKEDID] = "home"

            Intent(this, HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let { context ->
                AlertUtils.showToast(context, it)
            }
        })
    }

    /**
     * Listeners
     */
    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        binding.btnLogin.setOnClickListener(this)
        binding.txtSignUp.setOnClickListener(this)
        binding.txtForgotPassword.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnLogin -> {
                val email = binding.edtLoginEmail.text.toString().trim()
                val password = binding.edtLoginPassword.text.toString().trim()
                val validEmail = Utils.checkEmailValid(email)
                addValidation(email, password, validEmail)
            }
            R.id.txtSignUp -> {
                Intent(this, SignUpActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.txtForgotPassword -> {
                Intent(this, ForgotPasswordActivity::class.java).also {
                    startActivity(it)
                }
            }
        }
    }

    /**
     * login validations.
     */
    private fun addValidation(email: String, password: String, validEmail: Boolean) {
        if (email.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_Email)
            )
            return
        } else if (!validEmail) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_valid_Email)
            )
            return
        } else if (password.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_Password)
            )
            return
        } else {
            /*prefs!![mUserData.CLICKEDID] = "home"
            Utils.saveCustomPref(this, UserData.TOKEN, "token")
            Intent(this, HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }*/
            if (rememberMe.isChecked) {
                rememberPass(email, password, true)
            } else {
                rememberPass(email, password, false)
            }
            viewModel.callLoginApi(email, password, this)
        }
    }

    private fun rememberPass(email: String, userPassword: String, isCheck: Boolean) {
        rememberPrefs?.set(mUserData.REMEMBER_ME, isCheck)
        rememberPrefs?.set(mUserData.REM_EMAIL, email)
        rememberPrefs?.set(mUserData.REM_PASSWORD, userPassword)
    }


}
