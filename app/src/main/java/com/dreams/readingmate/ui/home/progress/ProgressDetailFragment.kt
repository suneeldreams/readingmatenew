package com.dreams.readingmate.ui.home.progress

import android.content.*
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.*
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.databinding.ProgressDetailFragmentBinding
import com.dreams.readingmate.ui.auth.AlarmActivity
import com.dreams.readingmate.ui.auth.NewCalendar
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ProgressDetailFragment :
    BaseFragment<ProgressDetailFragmentBinding>(R.layout.progress_detail_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: HomeFragmentInterface? = null
    private lateinit var binding: ProgressDetailFragmentBinding
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()
    private var childId = ""
    private var childItems: ChildListItem? = null
    private var childDetail: ChildDetails? = null
    private var historyArrayList: UpdateTickData2? = null
    private var checkActivity: Boolean? = true
    private val mHandler = Handler()
    var remainingDays = 0
    var rremainingDays = 0
    var from = ""
    var nickName = ""
    var childName = ""
    private var bookId = ""
    private var preferenceChild: PreferenceChildItems? = null
    private var editChildProfileItem: ChildListItem? = null
    private var mProgressStatus: Int = 0
    private var rmProgressStatus: Int = 0

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ProgressDetailFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgAdd.visibility = View.GONE
        binding.imgCurrentlyReading.setOnClickListener(this)
        binding.imgAddSuggestedBook.setOnClickListener(this)
        binding.imgSearchBook.setOnClickListener(this)
//        binding.lnrBooksRead.setOnClickListener(this)
//        binding.lnrBooksReviewed.setOnClickListener(this)
        binding.txtSetReminder.setOnClickListener(this)
        binding.txtEditDetails.setOnClickListener(this)
        binding.lnrCurrentProgress.setOnClickListener(this)
        binding.lnrHistory.setOnClickListener(this)
        setUpData()
        observeLiveEvents()
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(
                broadCastReceiver,
                IntentFilter(AppConstants.REFERESH_CURRENTLY_READING_LIST)
            )
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.REFERESH_BOOKS))
    }

    /**
     * getting data from bundle
     */
    private fun setUpData() {
        binding.recyclerBooksHaveRead.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewWant.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerBooksToReview.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerWantToRead.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerBooksIHaveReviewed.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        val args = arguments
        if (args != null) {
            childId = args.getString("childId").toString()
            childItems = args.getParcelable("history")
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
            editChildProfileItem = args.getParcelable("editChild")
            callChildDetailApi()
            callChildBooksApi()
        }
    }

    private fun callChildDetailApi() {
        viewModel.callChildDetailListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            childId
        )
    }

    private fun callChildBooksApi() {
        viewModel.callAllBooksDataAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            childId
        )
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.childDetailListLiveData.observe(this, Observer {
            Log.d("res:-", it.toString())
            childDetail = it
            binding.txtChildName.text = it.name
            val child_Name = it.name
            binding.includeToolbar.toolbar_title.text = "$child_Name's Journey"
            childName = it.name!!
            binding.txtNickName.text = it.nickName
            setColor(it.favColour!!)
            Utils.displayCircularImage(requireContext(), binding.imgUser, it.img!!)
            binding.txtBooksReads.text = it.readBooks!!.size.toString()
            if (it.reviewData != null) {
                binding.txtBooksReviewed.text = it.reviewData!!.size.toString()
            }
            binding.runningProgress.invalidate()
            binding.runningProgress.max = 7
            remainingDays = it.runStreak!!
            mProgressStatus = 0
            binding.runningProgress.progress = mProgressStatus.toFloat()
            var task = object : AsyncTask<Void, Void, String>() {
                override fun doInBackground(vararg p0: Void?): String {
                    while (mProgressStatus < remainingDays) {
                        try {
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                        mProgressStatus += 1
                        mHandler.post(Runnable() {
                            if (checkActivity!!) {
                                try {
                                    binding.runningProgress!!.progress = mProgressStatus.toFloat()
                                    binding.txtDaysLeft.text =
                                        mProgressStatus.toString()
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        })
                    }
                    return mProgressStatus.toString()
                }
            }.execute()
            if (it.runStreak == 0) {
                binding.txtDaysLeft.text =
                    it.runStreak.toString()
            }
            binding.readingHabbitProgress.invalidate()
            rremainingDays = it.readingHabit!!
            rmProgressStatus = 0
            binding.readingHabbitProgress.progress = rmProgressStatus.toFloat()
            var rtask = object : AsyncTask<Void, Void, String>() {
                //           private var name = ""
                override fun doInBackground(vararg p0: Void?): String {
                    while (rmProgressStatus < rremainingDays) {
                        try {
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                        rmProgressStatus += 1
                        mHandler.post(Runnable() {
                            if (checkActivity!!) {
                                try {
                                    binding.readingHabbitProgress!!.progress =
                                        rmProgressStatus.toFloat()
                                    binding.txtDaysPercent.text =
                                        rmProgressStatus.toString() + "%"
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        })
                    }
                    return rmProgressStatus.toString()
                }
            }.execute()
            if (it.readingHabit == 0) {
                binding.txtDaysPercent.text =
                    it.readingHabit.toString() + "%"
            }
            if (it.currentlyReadingBook != null) {
                binding.txtBookReading.text = it.currentlyReadingBook!!.title
            }
            binding.txtKind.text = it.reminder!!.kind
            setWeekData(
                it.weekData,
                it.currentlyReadingBook!!.bookshelfId,
                it.createdAt,
                it.name,
                it.favColour
            )
        })
        viewModel.updateTickDataLiveData.observe(this, Observer {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.good_work)
            )
            historyArrayList = it
            val intent = Intent()
            intent.action = AppConstants.REFRESH
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callChildDetailApi()
        })
        viewModel.removeTickDataLiveData.observe(this, Observer {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.reading_removed)
            )
            historyArrayList = it
            val intent = Intent()
            intent.action = AppConstants.REFRESH
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callChildDetailApi()
        })
        viewModel.allBookLiveData.observe(this, Observer {
            Log.d("bookList:-", it.toString())
            if (!it.currentlyReadingBook!!.image.isNullOrEmpty()) {
                binding.txtCurrentReading.visibility = View.GONE
                binding.cardView.visibility = View.VISIBLE
                bookId = it.currentlyReadingBook.bookId!!
                Utils.displayImage(
                    requireContext(),
                    it.currentlyReadingBook.image!!,
                    binding.imgCurrentlyReading,
                    R.drawable.ic_place_holder
                )
            } else {
                binding.txtCurrentReading.visibility = View.VISIBLE
                binding.cardView.visibility = View.INVISIBLE
            }
            if (it.readBooks!!.isNotEmpty()) {
                binding.txtReadBookNoData.visibility = View.GONE
                binding.recyclerBooksHaveRead.visibility = View.VISIBLE
                binding.recyclerBooksHaveRead.adapter =
                    ReadBooksListAdapter(it.readBooks, mListener, childId)
            } else {
                binding.txtReadBookNoData.visibility = View.VISIBLE
                binding.recyclerBooksHaveRead.visibility = View.GONE
            }

            if (it.boughtBooks!!.isNotEmpty()) {
                binding.txtBoughtNoData.visibility = View.GONE
                binding.recyclerViewWant.visibility = View.VISIBLE
                binding.recyclerViewWant.adapter =
                    BoughtBooksListAdapter(it.boughtBooks, mListener, childId)
            } else {
                binding.txtBoughtNoData.visibility = View.VISIBLE
                binding.recyclerViewWant.visibility = View.GONE
            }
            if (it.wishlist!!.isNotEmpty()) {
                binding.txtWantToReadNoData.visibility = View.GONE
                binding.recyclerWantToRead.visibility = View.VISIBLE
                binding.recyclerWantToRead.adapter =
                    WishListBooksListAdapter(it.wishlist, mListener, childId)
            } else {
                binding.txtWantToReadNoData.visibility = View.VISIBLE
                binding.recyclerWantToRead.visibility = View.GONE
            }

            if (it.reviewedBooks!!.isNotEmpty()) {
                binding.txtBooksHaveReviewedNoData.visibility = View.GONE
                binding.recyclerBooksIHaveReviewed.visibility = View.VISIBLE
                binding.recyclerBooksIHaveReviewed.adapter =
                    BooksIHaveReviewedListAdapter(it.reviewedBooks, mListener, childId)
            } else {
                binding.txtBooksHaveReviewedNoData.visibility = View.VISIBLE
                binding.recyclerBooksIHaveReviewed.visibility = View.GONE
            }

            if (it.toReviewBooks!!.isNotEmpty()) {
                binding.txtWishlistNoData.visibility = View.GONE
                binding.recyclerBooksToReview.visibility = View.VISIBLE
                binding.recyclerBooksToReview.adapter =
                    ToReviewBooksListAdapter(it.toReviewBooks, mListener, childId)
            } else {
                binding.txtWishlistNoData.visibility = View.VISIBLE
                binding.recyclerBooksToReview.visibility = View.GONE
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * set color related to action
     */
    private fun setColor(color: String) {
        when (color) {
            "Black" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.red_new), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.red_new), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.red_new))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "White" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                drawable.cornerRadius = f.toFloat()
                binding.runningProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.pink_new), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.pink_new), Color.GRAY
                )
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.pink_new))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)

                binding.txtBooksReads.setTextColor(Color.BLACK)
                binding.titleBookSelf.setTextColor(Color.BLACK)
                binding.txtBooksReviewed.setTextColor(Color.BLACK)
                binding.titleRevived.setTextColor(Color.BLACK)
            }
            "Pink" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.yellow_new
                    ), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.yellow_new
                    ), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.yellow_new))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.BLACK)
                binding.titleBookSelf.setTextColor(Color.BLACK)
                binding.txtBooksReviewed.setTextColor(Color.BLACK)
                binding.titleRevived.setTextColor(Color.BLACK)
            }
            "Blue" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.green_new), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.green_new), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.green_new))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "Green" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.jelly_fish), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.jelly_fish), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.jelly_fish))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "Grey" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.blue_new), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(requireContext(), R.color.blue_new), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.blue_new))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "Purple" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.purple
                    ), Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.purple
                    ), Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(ContextCompat.getColor(requireContext(), R.color.purple))
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "Red" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    Color.RED, Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    Color.RED, Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(Color.RED)
                binding.lnrTabsBg.setBackgroundDrawable(drawable)
                binding.txtBooksReads.setTextColor(Color.WHITE)
                binding.titleBookSelf.setTextColor(Color.WHITE)
                binding.txtBooksReviewed.setTextColor(Color.WHITE)
                binding.titleRevived.setTextColor(Color.WHITE)
            }
            "Yellow" -> {
                val f = 70
                val drawable = GradientDrawable()
                drawable.shape = GradientDrawable.RECTANGLE
                binding.runningProgress.setColor(
                    Color.YELLOW, Color.GRAY
                )
                binding.readingHabbitProgress.setColor(
                    Color.YELLOW, Color.GRAY
                )
                drawable.cornerRadius = f.toFloat()
                drawable.setColor(
                    Color.YELLOW
                )
                binding.lnrTabsBg.setBackgroundDrawable(drawable)

                binding.txtBooksReads.setTextColor(Color.BLACK)
                binding.titleBookSelf.setTextColor(Color.BLACK)
                binding.txtBooksReviewed.setTextColor(Color.BLACK)
                binding.titleRevived.setTextColor(Color.BLACK)
            }
        }
    }

    /**
     * set wecckly data here
     */
    private fun setWeekData(
        data: List<WeekData?>?,
        bookshelfId: String?,
        createdAt: String?,
        name: String?,
        favColour: String
    ) {
        val layoutInflater =
            requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding.lnrParentLayout.removeAllViews()
        for (i in 0 until data!!.size) {
            val view: View =
                layoutInflater.inflate(R.layout.week_single_item, binding.lnrParentLayout, false)
            val lnrWeekTick = view.findViewById<LinearLayout>(R.id.lnrWeekTick)
            val txtDay = view.findViewById<TextView>(R.id.txtWeekName)
            val imgTick = view.findViewById<ImageView>(R.id.imgCircle)
            val txtWeekDate = view.findViewById<TextView>(R.id.txtWeekDate)
            txtDay.text = data[i]!!.day
            val dates = data[i]!!.date
            val sdf: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val strDate = sdf.parse(createdAt)
            val weekdate = sdf.parse(dates)
            if (strDate > weekdate) {
                data[i]!!.isSelected = true
                Log.d("small_date", weekdate.toString())
            } else {
                data[i]!!.isSelected = false
                Log.d("Big_date", weekdate.toString())
            }
            if (data[i]!!.isRead == true) {
//                imgTick.setImageResource(R.drawable.ic_check)
                when (favColour) {
                    "Black" -> {
                        imgTick.setImageResource(R.drawable.ic_check_black)
                    }
                    "White" -> {
                        imgTick.setImageResource(R.drawable.ic_check_white)
                    }
                    "Pink" -> {
                        imgTick.setImageResource(R.drawable.ic_check_pink)
                    }
                    "Blue" -> {
                        imgTick.setImageResource(R.drawable.ic_check_blue)
                    }
                    "Green" -> {
                        imgTick.setImageResource(R.drawable.ic_check_green)
                    }
                    "Grey" -> {
                        imgTick.setImageResource(R.drawable.ic_check_gray)
                    }
                }
            } else {
                val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                val newDate = df.parse(data[i]!!.date)
                val df2: DateFormat = SimpleDateFormat("dd")
                val date = df2.format(newDate)
                imgTick.setImageResource(R.drawable.ic_weak_circle)
                txtWeekDate.text = date
            }
            lnrWeekTick.setOnClickListener {
                val token = Utils.getCustomPref(
                    requireContext(),
                    com.dreams.readingmate.data.db.UserData.TOKEN
                )
                if (data[i]!!.isRead != true) {
                    viewModel.addWeekTickData(
                        requireContext(),
                        token,
                        bookshelfId, "", data[i]!!.date!!, "", childId
                    )

                } else {
                    viewModel.removeTick(
                        requireContext(),
                        Utils.getCustomPref(
                            requireContext(),
                            com.dreams.readingmate.data.db.UserData.TOKEN
                        ),
                        data[i]!!.id, childId
                    )
                }
            }
            binding.lnrParentLayout.addView(view)
        }
    }

    /**
     * Handle click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.lnrBooksRead -> {
                val bundle = Bundle()
                bundle.putString("childId", childId)
                bundle.putString("nickName", childName)
                mListener?.openAllBookFragment(bundle)
            }
            R.id.lnrBooksReviewed -> {
                val bundle = Bundle()
                bundle.putString("childId", childId)
                bundle.putString("nickName", childName)
                mListener?.openAllBookFragment(bundle)
            }
            R.id.txtSetReminder -> {
                val intent = Intent(requireContext(), AlarmActivity::class.java);
                intent.putExtra("childId", childId)
                startActivity(intent)
            }
            R.id.lnrHistory -> {
                val intent = Intent(requireContext(), NewCalendar::class.java)
                intent.putExtra("childId", childId)
                startActivity(intent)
            }
            R.id.txtEditDetails -> {
                val bundle = Bundle()
                bundle.putString("from", "editProfile")
                bundle.putString("childId", childId)
                bundle.putParcelable("child", preferenceChild)
                bundle.putParcelable("editChild", editChildProfileItem)
                mListener?.openAddChildFragment(bundle)
            }
            R.id.imgCurrentlyReading -> {
                val bundle = Bundle()
                bundle.putString("bookId", bookId)
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openBookDetailFragment(bundle)
            }
            R.id.imgSearchBook -> {
                val bundle = Bundle()
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openAllTypeBookFragment(bundle)
            }
            R.id.imgAddSuggestedBook -> {
                val bundle = Bundle()
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openAllTypeBookFragment(bundle)
            }
        }
    }


    interface HomeFragmentInterface {
        fun openCloseDrawer()
        fun openBookShelfFragment(bundle: Bundle)
        fun openAddChildFragment(bundle: Bundle)
        fun openBookReviewedFragment(bundle: Bundle)
        fun openSetReminderFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openHistoryFragment(bundle: Bundle)
        fun openAllBookFragment(bundle: Bundle)
        fun openAllTypeBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HomeFragmentInterface")
        }
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            when (intent?.action) {
                AppConstants.REFERESH_CURRENTLY_READING_LIST -> {
                    callChildDetailApi()
                }
                AppConstants.REFERESH_BOOKS -> {
                    viewModel.callAllBooksDataAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        childId
                    )
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

}