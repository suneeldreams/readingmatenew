package com.dreams.readingmate.ui.auth

import android.R
import android.app.IntentService
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat


/**
 * Created by Suneel on 30/03/2020.
 */
class AlarmService : IntentService("AlarmService") {
    private var alarmNotificationManager: NotificationManager? = null
    override fun onHandleIntent(intent: Intent?) {
        sendNotification("Wake Up! Wake Up!")
    }
    private fun sendNotification(msg:String) {
        Log.d("AlarmService", "Preparing to send notification...: " + msg)
        alarmNotificationManager = this
            .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val contentIntent = PendingIntent.getActivity(this, 0,
            Intent(this, AlarmActivity::class.java), 0)
        val alamNotificationBuilder = NotificationCompat.Builder(
            this).setContentTitle("Alarm").setSmallIcon(R.drawable.ic_lock_idle_alarm)
            .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
            .setContentText(msg)
        alamNotificationBuilder.setContentIntent(contentIntent)
        alarmNotificationManager!!.notify(1, alamNotificationBuilder.build())
        Log.d("AlarmService", "Notification sent.")
    }
}