package com.dreams.readingmate.ui.auth

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.AlarmActivityBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.ui.home.progress.SetReminderFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Suneel on 30/03/2020.
 */
class AlarmActivity : BaseActivity<AlarmActivityBinding>(R.layout.alarm_activity),
    NavigationContract, KodeinAware,
    View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private var mListener: SetReminderFragment.SetReminderFragmentInterface? = null
    private lateinit var binding: AlarmActivityBinding
    val RQS_1 = 1
    var childId = ""
    var newDate = ""
    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0
    override val kodein by kodein()
    private val viewModel: AuthViewModel by instance<AuthViewModel>()
    override fun initComponents(savedInstanceState: Bundle?, binding: AlarmActivityBinding) {
        this.binding = binding
        binding.viewModel = viewModel
        this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setUpData()
        observeLiveEvents()
        observeNavigationEvents()
    }

    private fun setUpData() {
        val bundle: Bundle? = intent.extras
        childId = bundle!!.get("childId").toString()
        val c = Calendar.getInstance()
        val df =
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val formattedDate = df.format(c.time)
        println("Currrent Date Time : $formattedDate")
        Log.d("newTime", formattedDate)
        binding.txtSetAlarm.text = formattedDate
        binding.txtSetEndDate.text = formattedDate
    }

    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        binding.txtSetAlarm.setOnClickListener(this)
        binding.txtSetEndDate.setOnClickListener(this)
        binding.mTitle.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> finish()
            R.id.txtSetAlarm -> {
                val c = Calendar.getInstance()
                val df =
                    SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val formattedDate = df.format(c.time)
                println("Currrent Date Time : $formattedDate")
                Log.d("newTime", formattedDate)
                binding.txtSetAlarm.text = ""
                openTimePickerDialog(false)
            }
            R.id.txtSetEndDate -> {
                val calendar = Calendar.getInstance()
                year = calendar[Calendar.YEAR]
                month = calendar[Calendar.MONTH]
                day = calendar[Calendar.DAY_OF_MONTH]
                val datePickerDialog =
                    DatePickerDialog(this@AlarmActivity, this@AlarmActivity, year, month, day)
                datePickerDialog.show()
            }
            R.id.mTitle -> {
                val startD = binding.txtSetAlarm.text.toString().trim()
                val endD = binding.txtSetEndDate.text.toString().trim()
                if (startD == endD) {
                    AlertUtils.showToast(this, "Please Select End Date.")
                } else {
                    val kind = "daily"
                    callAlarmApi(kind, newDate, endD, childId)
                }
            }
        }
    }


    private fun openTimePickerDialog(is24r: Boolean) {
        val calendar: Calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
            this,
            onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE), is24r
        )
        timePickerDialog.setTitle("Set Alarm Time")
        timePickerDialog.show()
    }

    var onTimeSetListener =
        TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            val calNow = Calendar.getInstance()
            val calSet = calNow.clone() as Calendar
            calSet[Calendar.HOUR_OF_DAY] = hourOfDay
            calSet[Calendar.MINUTE] = minute
            calSet[Calendar.SECOND] = 0
            calSet[Calendar.MILLISECOND] = 0
            if (calSet.compareTo(calNow) <= 0) {
                // Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1)
            }
            setAlarm(calSet)

        }

    private fun setAlarm(targetCal: Calendar) {
//        val calendar = Calendar.getInstance().time
        val df: DateFormat =
            SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH)
        val result: Date = df.parse("${targetCal.time}".trimIndent())
//        val today: Date = df.parse(calendar.time.toString())
        val ss: DateFormat =
            SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
        newDate = ss.format(result)
        Log.d("timeis", newDate)
        binding.txtSetAlarm.text = newDate
        binding.txtSetEndDate.text = newDate

//        binding.txtSetAlarm.text = Utils.getTime(newDate)
//        binding.txtSetEndDate.text = Utils.getTime(newDate)
        val intent = Intent(applicationContext, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            this, RQS_1, intent, 0
        )
        val alarmManager =
            this.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!![AlarmManager.RTC_WAKEUP, targetCal.timeInMillis] = pendingIntent
        // reapeet every 20 mint
        alarmManager?.setRepeating(
            AlarmManager.RTC_WAKEUP,
            targetCal.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )
        val kind = "daily"
//        callAlarmApi(kind, newDate, "2020-11-12 06:00:00", childId)
    }

    private fun callAlarmApi(
        kind: String,
        startDate: String,
        endDate: String,
        childId: String
    ) {
        viewModel.callSetReminderApi(
            Utils.getCustomPref(this, UserData.TOKEN),
            this, startDate, endDate, kind, childId
        )
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        myDay = day
        myYear = year
        myMonth = month + 1
        val calendar: Calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR)
        val minute = calendar.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(
            this@AlarmActivity, this@AlarmActivity, hour, minute,
            false
        )
        timePickerDialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        myHour = hourOfDay
        myMinute = minute
        /*binding.txtSetEndDate.text =
            "Year: $myYear\nMonth: $myMonth\nDay: $myDay\nHour: $myHour\nMinute: $myMinute"*/

        binding.txtSetEndDate.text =
            myYear.toString() + "-" + myMonth + "-" + myDay + " " + myHour + ":" + myMinute
    }
}