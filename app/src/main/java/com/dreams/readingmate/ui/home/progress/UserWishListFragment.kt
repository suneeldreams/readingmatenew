package com.dreams.readingmate.ui.home.progress

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.UserWishListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.network.responses.UserWishListData
import com.dreams.readingmate.databinding.UserWishlistFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class UserWishListFragment :
    BaseFragment<UserWishlistFragmentBinding>(R.layout.user_wishlist_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: UserWishListFragmentInterface? = null
    private lateinit var binding: UserWishlistFragmentBinding
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()
    var prefs: SharedPreferences? = null

    override fun initComponents(savedInstanceState: Bundle?, binding: UserWishlistFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.userWishlistRecycler.layoutManager = LinearLayoutManager(context)
        viewModel.callUserWishListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = "User WishList"
    }
    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.userWishListLiveData.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                binding.userWishlistRecycler.visibility = View.VISIBLE
                binding.txtNoData.visibility = View.GONE
                setBookShelfListData(it)
            } else {
                binding.userWishlistRecycler.visibility = View.GONE
                binding.txtNoData.visibility = View.VISIBLE
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface UserWishListFragmentInterface {
        fun openBackFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openBooksFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is UserWishListFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement UserWishListFragmentInterface")
        }
    }

    private fun setBookShelfListData(wishList: List<UserWishListData>) {
        binding.userWishlistRecycler.adapter =
            UserWishListAdapter(mListener!!, wishList)
    }
}