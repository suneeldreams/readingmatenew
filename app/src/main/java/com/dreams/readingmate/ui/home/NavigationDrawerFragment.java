package com.dreams.readingmate.ui.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dreams.readingmate.util.Utils;
import com.squareup.picasso.Picasso;
import com.dreams.readingmate.R;
import com.dreams.readingmate.data.db.UserData;
import com.dreams.readingmate.util.CircleTransform;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    private RecyclerView drawerRecyclerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    public OnDrawerItemSelectedListener mCallback;
    private Toolbar mToolbar;
    private TextView userName, email;
    private ImageView profileImg;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnDrawerItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnDrawerItemSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userName = view.findViewById(R.id.userName);
        email = view.findViewById(R.id.email);
        profileImg = view.findViewById(R.id.profileImg);

        view.findViewById(R.id.parentLayout).setOnClickListener(this);
        view.findViewById(R.id.imgClose).setOnClickListener(this);
        view.findViewById(R.id.txtHome).setOnClickListener(this);
        view.findViewById(R.id.txtProfile).setOnClickListener(this);
        view.findViewById(R.id.txtTermsCondition).setOnClickListener(this);
        view.findViewById(R.id.txtFAQ).setOnClickListener(this);
        view.findViewById(R.id.logOut).setOnClickListener(this);
        setFragmentChangeListener();
        setData();
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        View containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mToolbar = toolbar;
        setData();
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                setData();
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    private void setData() {
        String name = Utils.INSTANCE.getCustomPref(getActivity(), UserData.INSTANCE.getNAME());
        String emails = Utils.INSTANCE.getCustomPref(getActivity(), UserData.INSTANCE.getEMAIL());
        String image = Utils.INSTANCE.getCustomPref(getActivity(), UserData.INSTANCE.getPROFILE_IMAGE());
        userName.setText(name);
        email.setText(emails);
//
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).placeholder(R.drawable.ic_avtar).transform(new CircleTransform()).into(profileImg);
        }
    }

    private void setFragmentChangeListener() {
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        int backStackEntryCount = getActivity().getSupportFragmentManager().getBackStackEntryCount();
                        lockDrawer2(backStackEntryCount != 0);
                    }
                }
        );
    }

    public void lockDrawer2(boolean isLocked) {
        if (isLocked) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            final Drawable upArrow = ContextCompat.getDrawable(getActivity(), R.drawable.ic_back);
            mToolbar.setNavigationIcon(upArrow);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }
                }
            });
        }
    }

    /**
     * It is used to call from HomeActivity after login or signup to update user data
     */
    public void notifyNavDrawerAdapter() {

    }

    @Override
    public void onClick(View v) {
        mCallback.onDrawerItemSelected(v.getId());
    }

    // HomeActivity Activity must implement this interface
    public interface OnDrawerItemSelectedListener {
        void onDrawerItemSelected(Integer catId);

        void openProfile();
    }
}
