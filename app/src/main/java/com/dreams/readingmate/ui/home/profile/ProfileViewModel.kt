package com.dreams.readingmate.ui.home.profile

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.model.UpdateProfileModel
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException
import okhttp3.MultipartBody

class ProfileViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {
    var fileToUpload: MultipartBody.Part? = null
    var filePath = ""

    private val _updateProfileLiveData = MutableLiveData<AuthInfoResponse>()
    val updateProfileLiveData: LiveData<AuthInfoResponse>
        get() = _updateProfileLiveData

    private val _membershipStatusLiveData = MutableLiveData<UserMembershipStatusData>()
    val membershipStatusLiveData: LiveData<UserMembershipStatusData>
        get() = _membershipStatusLiveData

    private val _cancelSubscriptionLiveData = MutableLiveData<CancelSubscriptionData>()
    val cancelSubscriptionLiveData: LiveData<CancelSubscriptionData>
        get() = _cancelSubscriptionLiveData


    fun callUploadImageApi(
        mContext: Context,
        accessToken: String,
        name: String,
        number: String,
        email: String,
        gendar: String,
        charity: String,
        filePath: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.updateProfile(
                    UpdateProfileModel(
                        name,
                        "", email, gendar, charity, filePath
                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _updateProfileLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callMembershipStatusAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getMembershipStatus(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _membershipStatusLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
//                    AlertUtils.showToast(context, result.error.errorMessage)
                    _errorCode.postValue(result.error.errorCode)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun cancelSubscription(
        mContext: Context,
        accessToken: String,
        membershipID: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.cancelSubscription(
                    accessToken,
                    membershipID
                )
                if (result is ResultState.Success) {
                    _cancelSubscriptionLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                    _errorCode.postValue(result.error.errorCode)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

}
