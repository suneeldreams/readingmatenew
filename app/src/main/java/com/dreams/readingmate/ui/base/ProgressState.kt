package com.dreams.readingmate.ui.base

/**
 * Base class for all Activities
 * Author: Shivank Trivedi
 * Dated: 20-06-2020
 */

enum class ProgressState {
    LOADING, ERROR, SUCCESS
}
