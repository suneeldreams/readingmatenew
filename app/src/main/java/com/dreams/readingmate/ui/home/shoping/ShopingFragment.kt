package com.dreams.readingmate.ui.home.shoping

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ShopingCartAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.ShopingFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.shoping_fragment.*
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*


class ShopingFragment : BaseFragment<ShopingFragmentBinding>(R.layout.shoping_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: ShopingFragmentInterface? = null
    private lateinit var binding: ShopingFragmentBinding
    private var mainAmount = 0.0
    private var grandTotal = ""
    private var subTotal = ""
    private var shopping = ""
    private var deliveryCharge = ""
    private val viewModel: ShopingViewModel by instance<ShopingViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: ShopingFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.shoping_cart)
        binding.checkOutBtn.setOnClickListener(this)
        binding.includeToolbar.imgSearch.setOnClickListener(this)
        binding.rltAddBook.setOnClickListener(this)
        binding.txtValidate.setOnClickListener(this)
//        binding.imgSearchBook.setOnClickListener(this)
        setUpData()
        observeLiveEvents()
    }

    /**
     * set initial data
     */
    private fun setUpData() {
        val args = arguments
        if (args != null) {
            grandTotal = args.getString("grandTotal").toString()
            subTotal = args.getString("subTotal").toString()
            shopping = args.getString("shopping").toString()
            deliveryCharge = args.getString("deliveryCharge").toString()
        }
        binding.recyclerCartItems.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        setCartList()
        if (grandTotal != "0.0" && grandTotal != "null") {
            mainAmount = grandTotal.toDouble()
            binding.txtTotal.text = resources.getString(R.string.currency_pound) + grandTotal
            binding.txtSubTotal.text = resources.getString(R.string.currency_pound) + subTotal
            binding.txtDelivery.text = resources.getString(R.string.currency_pound) + deliveryCharge
            binding.txtTokenUsed.visibility = View.GONE
            binding.rltCoupon.visibility = View.VISIBLE
        } else {
            binding.txtTokenUsed.visibility = View.VISIBLE
            binding.rltCoupon.visibility = View.GONE
            binding.txtTotal.text = resources.getString(R.string.currency_pound) + grandTotal
            binding.txtSubTotal.text = resources.getString(R.string.currency_pound) + subTotal
            binding.txtDelivery.text = resources.getString(R.string.currency_pound) + deliveryCharge
        }
    }

    /**
     * set cart items list
     */
    private fun setCartList() {
        val cartData = (activity as HomeActivity).cartItems
        if (cartData.isNotEmpty()) {
            binding.includeToolbar.imgSearch.visibility = View.VISIBLE
            binding.cardViewAddBook.visibility = View.GONE
            binding.recyclerCartItems.visibility = View.VISIBLE
            binding.mainLayout.visibility = View.VISIBLE
            binding.checkOutBtn.visibility = View.VISIBLE
            binding.recyclerCartItems.adapter =
                ShopingCartAdapter(cartData, mListener, this)
        } else {
            binding.includeToolbar.imgSearch.visibility = View.GONE
            binding.recyclerCartItems.visibility = View.INVISIBLE
            binding.mainLayout.visibility = View.GONE
            binding.checkOutBtn.visibility = View.GONE
            binding.cardViewAddBook.visibility = View.VISIBLE
        }
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    @SuppressLint("SetTextI18n")
    private fun observeLiveEvents() {
        viewModel.addCartLiveData.observe(this, Observer {
            setCartList()
            mainAmount = it.grandTotal!!.toDouble()
            binding.txtTotal.text = resources.getString(R.string.currency_pound) + it.grandTotal
            binding.txtSubTotal.text = resources.getString(R.string.currency_pound) + it.subTotal
            binding.txtDelivery.text =
                resources.getString(R.string.currency_pound) + it.deliveryCharge
            callValidateCouponCode()
        })

        viewModel.validateCouponCodeLiveData.observe(this, Observer {
            imgValidate.visibility = View.VISIBLE
            imgValidate.setImageResource(R.drawable.check)
            txtCouponAmount.visibility = View.VISIBLE
            txtTotal.text = resources.getString(R.string.currency_pound) + mainAmount.toString()
            (activity as HomeActivity).couponCodeUsed = it.couponCode!!
            if (it.couponType == 1) {
                val amountInPercentage = mainAmount * it.couponAmount!!.toDouble() / 100
                val percAmount = mainAmount - amountInPercentage
                txtCouponAmount.text =
                    resources.getString(R.string.currency_pound) + Utils.roundTwoDecimals(
                        percAmount.toString()
                    )
                txtTotal.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray_hint))
                txtTotal.paintFlags = txtCouponAmount.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                val couponDiscountAmount = mainAmount - it.couponAmount!!.toDouble()
                txtCouponAmount.text =
                    resources.getString(R.string.currency_pound) + Utils.roundTwoDecimals(
                        couponDiscountAmount.toString()
                    )
                txtTotal.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray_hint))
                txtTotal.paintFlags = txtCouponAmount.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            imgValidate.visibility = View.VISIBLE
            imgValidate.setImageResource(R.drawable.ic_cross)
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
        viewModel.errorCode.observe(this, Observer {
            if (it == 404) {
                imgValidate.visibility = View.VISIBLE
                imgValidate.setImageResource(R.drawable.ic_cross)
                this.let {
                    AlertUtils.showToast(requireContext(), "Invalid coupon code.")
                }
            }
        })
    }

    /**
     * Handle click event
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.checkOutBtn -> mListener?.openCheckoutFragment()
            R.id.rltAddBook -> {
                val bundle = Bundle()
                bundle.putString("from", "shopping")
                mListener?.openBooksFragment(bundle)
            }
            R.id.imgSearch -> {
                val bundle = Bundle()
                mListener?.openAllTypeBookFragment(bundle)
            }
            R.id.txtValidate -> {
                val coupon = edtCouponCode.text.toString().trim()
                if (coupon.isNotEmpty()) {
                    callValidateCouponCode()
                } else {
                    AlertUtils.showToast(requireContext(), "Please enter coupon code.")
                }
            }
            /* R.id.imgSearchBook -> {
                 val bundle = Bundle()
                 bundle.putString("childId", (requireContext() as HomeActivity).childId)
                 mListener?.openAllTypeBookFragment(bundle)
             }*/
        }
    }

    private fun callValidateCouponCode() {
        viewModel.callValidateCouponAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            edtCouponCode.text.toString().trim()
        )
    }


    interface ShopingFragmentInterface {
        fun openCheckoutFragment()
        fun openBackFragment()
        fun openBooksFragment(bundle: Bundle)
        fun openAllTypeBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ShopingFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ShopingFragmentInterface")
        }
    }

    /**
     * Refreshing data
     */
    fun refreshAdapter() {
        viewModel.addCartData(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            getJsonOrderList()
        )
    }

    /**
     * set cart item in JsonArray
     */
    private fun getJsonOrderList(): String {
        val jsonArray = JSONArray()
        for (i in 0 until (activity as HomeActivity).cartItems.size) {
            val cartItem = (activity as HomeActivity).cartItems[i]
            val jsonObject = JSONObject()
            jsonObject.put("bookId", cartItem.bookId)
            jsonObject.put("quantity", cartItem.quantity)
            jsonArray.put(jsonObject)
        }
        return jsonArray.toString()
    }

}