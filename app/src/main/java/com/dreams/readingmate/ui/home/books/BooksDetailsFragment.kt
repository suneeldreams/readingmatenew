package com.dreams.readingmate.ui.home.books

import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.BookDetailSuggestionListAdapter
import com.dreams.readingmate.data.adapter.GetChildListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.BookDetailsDataItems
import com.dreams.readingmate.data.network.responses.CartFreeItem
import com.dreams.readingmate.data.network.responses.CartItem
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.databinding.BookDetailFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.text.DecimalFormat


class BooksDetailsFragment : BaseFragment<BookDetailFragmentBinding>(R.layout.book_detail_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: BooksDetailsFragmentInterface? = null
    private lateinit var binding: BookDetailFragmentBinding
    private val viewModel: BooksViewModel by instance<BooksViewModel>()
    private var bookId = ""
    private var bookListData: BookDetailsDataItems? = null
    private var childId = ""
    private var popUpTitle = ""
    private var bookStatus = ""

    override fun initComponents(savedInstanceState: Bundle?, binding: BookDetailFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.bookDetailToolbar.toolbar_title.text = getString(R.string.book_details)
        binding.bookDetailToolbar.imgBack.visibility = View.VISIBLE
//        binding.bookDetailToolbar.imgCart.visibility = View.VISIBLE
        binding.bookDetailToolbar.imgRecommCart.visibility = View.VISIBLE
//        binding.bookDetailToolbar.imgCart.setOnClickListener(this)
        binding.bookDetailToolbar.imgRecommCart.setOnClickListener(this)
        binding.bookDetailToolbar.imgBack.setOnClickListener(this)
        binding.imgFavorite.setOnClickListener(this)
        binding.txtChange.setOnClickListener(this)
        binding.lnrRateThisBook.setOnClickListener(this)
        binding.btnAddToCart.setOnClickListener(this)
        binding.txtReadThisBook.setOnClickListener(this)
        binding.txtFinishedBook.setOnClickListener(this)
        binding.txtWantThisBook.setOnClickListener(this)
        setData()
        observeLiveEvents()
        cartCountAndVisibility()
        binding.rltMain.setOnClickListener { }
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.UPDATE_CART_ITEM))
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.ADD_CHILD_ID))
    }

    /**
     * to check cart count, and set visibilty according to data.
     *
     */
    private fun cartCountAndVisibility() {
        val cartCount = (activity as HomeActivity).getCartCount()
        if (cartCount > 0) {
            binding.bookDetailToolbar.txtCount.visibility = View.VISIBLE
            binding.bookDetailToolbar.txtCount.text = cartCount.toString()
        } else {
            binding.bookDetailToolbar.txtCount.visibility = View.GONE
        }
    }

    private fun setData() {
        val args = arguments
        if (args != null) {
            bookId = args.getString("bookId").toString()
            childId = args.getString("childId").toString()
            callBookDetailApi(bookId, childId)
        }
        binding.recyclerSuggestion.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun callBookDetailApi(bookId: String, childId: String) {
        viewModel.callBookDetailAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId
        )
    }

    /**
     * MVVM ObserverLiveEvents results
     */
    private fun observeLiveEvents() {
        viewModel.bookDetailLiveData.observe(this, Observer {
            bookListData = it
            binding.txtTitle.text = it.title
            bookId = it.id!!
            binding.txtNumberOfPages.text = it.numberOfPages.toString()
            if (!it.language.isNullOrEmpty()) {
                binding.txtLanguage.text = it.language
            } else {
                binding.txtLanguage.text = "N/A"
            }
            binding.ratingBar.rating = it.starRating!!.toFloat()
            binding.txtRating.text = it.starRating.toString()
            if (it.isWishlisted == true) {
                binding.imgFavorite.setImageResource(R.drawable.fill)
            } else {
                binding.imgFavorite.setImageResource(R.drawable.ic_unfill)
            }
            val readingStatus = it.readingStatus
            when (readingStatus) {
                0 -> binding.bookStatus.text = "I want to read"
                1 -> binding.bookStatus.text = "Currently reading"
                2 -> binding.bookStatus.text = "Read"
            }
            val dc = DecimalFormat("0.00")
            val price = dc.format(it.ukPrice)
            binding.txtPrice.text =
                resources.getString(R.string.currency_pound) + price
            val htmlString = "<u>Change</u>"
            binding.txtChange.text = Html.fromHtml(htmlString)
            for (i in 0 until it.author!!.size) {
                binding.txtAuthorName.text = it.author[i]
            }
            binding.txtDesc.text = it.shortDescription
            it.img?.let { it1 ->
                Utils.displayImage(
                    requireContext(),
                    it1,
                    binding.imgFavouriteBook,
                    R.drawable.ic_place_holder
                )
            }
            binding.recyclerSuggestion.adapter =
                BookDetailSuggestionListAdapter(it.suggestionBooks, mListener, this)


        })
        viewModel.bookReviewLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callBookDetailApi(bookId, childId)
        })
        viewModel.bookFavoriteLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callBookDetailApi(bookId, childId)
        })
        viewModel.removebookFavoriteLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callBookDetailApi(bookId, childId)
        })
        viewModel.addCartLiveData.observe(this, Observer {
            if (it.grandTotal == 0.0) {
                (activity as HomeActivity).freeBookGrandTotal = it.grandTotal.toString()
            }
            val bundle = Bundle()
            bundle.putString("grandTotal", it.grandTotal.toString())
            bundle.putString("subTotal", it.subTotal.toString())
            bundle.putString("deliveryCharge", it.deliveryCharge.toString())
            mListener!!.openCartFragment(bundle)
        })

        viewModel.bookStatusLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), getString(R.string.this_book_has_been_added))
            val intent = Intent()
            intent.action = AppConstants.REFRESH_BOOKSHELF_LIST
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            val intent2 = Intent()
            intent2.action = AppConstants.REFRESH
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent2)

            val intent3 = Intent()
            intent3.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent3)
            callBookDetailApi(bookId, childId)
        })

        viewModel.childListLiveData.observe(this, Observer {
            Log.d("childList:-", it.toString())
            if (it.size == 1) {
                for (element in it!!) {
                    callBookStatusAPi(element.id)
                }
            } else {
                childListingDialog(it, requireContext(), popUpTitle)
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    private fun childListingDialog(
        childData: List<ChildListItem>?,
        context: Context,
        popUpTitle: String
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.child_listing_popup)

        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        dialog.show()

        val imgDismiss = dialog.findViewById(R.id.imgCross) as ImageView
        val txtTitle = dialog.findViewById(R.id.txtTitle) as TextView
        val recyclerChildList = dialog.findViewById(R.id.recyclerChildList) as RecyclerView
        recyclerChildList.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerChildList.adapter = GetChildListAdapter(childData!!, mListener, this, dialog)
//        val btnSaveStatus = dialog.findViewById(R.id.btnSaveStatus) as Button
        /*btnSaveStatus.setOnClickListener {
            dialog.dismiss()
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            if (bookStatus == "0") {
                bookFragment.callFavoriteApi(bookId, childId)
            } else {
                bookFragment.callChangeBookStatusApi(bookStatus, bookId, childId)
            }
        }*/
        txtTitle.text = popUpTitle
        imgDismiss.setOnClickListener {
            dialog.dismiss()
        }
    }

    /**
     * Click event action manage
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.imgRecommCart -> {
                if ((activity as HomeActivity).cartItems.isEmpty()) {
                    AlertUtils.showToast(
                        requireContext(),
                        resources.getString(R.string.no_item_in_cart)
                    )
                } else {
                    callCartApi()
                }
            }
            R.id.imgFavorite -> {
                val intent = Intent()
                intent.action = AppConstants.REFERESH_BOOKS
                LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
                if (bookListData!!.isWishlisted == true) {
                    callUnFavoriteApi(bookId)
                } else {
                    callFavoriteApi(bookId)
                }
            }
            R.id.lnrRateThisBook -> OpenReviewDialog(bookId, requireContext())
            R.id.txtChange -> {
                if (childId.isNotEmpty() && childId != "null") {
                    openDialog(requireContext(), bookId, childId)
                } else {
                    val bundle = Bundle()
                    bundle.putString("from", "bookDetail")
                    bundle.putString("bookId", bookId)
                    mListener?.openBooksFragment(bundle)
                }
            }
            R.id.btnAddToCart -> addToCart()
            R.id.txtReadThisBook -> {
                (context as HomeActivity).refreshBookStatus = "1"
//                bookStatus = "1"
                popUpTitle = "Who's reading this book?"
                callGetChildListApi()
            }
            R.id.txtFinishedBook -> {
                (context as HomeActivity).refreshBookStatus = "1"
//                bookStatus = "2"
                popUpTitle = "Who's finished this book?"
                callGetChildListApi()
            }
            R.id.txtWantThisBook -> {
                (context as HomeActivity).refreshBookStatus = "1"
//                bookStatus = "0"
                popUpTitle = "Who want's this book?"
                callGetChildListApi()
            }
        }
    }

    private fun callGetChildListApi() {
        viewModel.callChildListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            Utils.getCustomPref(requireContext(), UserData.ID)
        )
    }

    private fun callCartApi() {
        viewModel.addCartData(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            getJsonOrderList()
        )
    }

    private fun openDialog(
        context: Context,
        bookId: String,
        childId: String
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_book_status)

        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        dialog.show()

        val imgDismiss = dialog.findViewById(R.id.imgDismiss) as ImageView
        val currentlyReading = dialog.findViewById(R.id.currentlyReading) as TextView
        val txtRead = dialog.findViewById(R.id.txtRead) as TextView
        val txtNonRead = dialog.findViewById(R.id.txtNonRead) as TextView
        val btnSaveStatus = dialog.findViewById(R.id.btnSaveStatus) as Button
        var bookStatus = ""
        currentlyReading.setOnClickListener {
            currentlyReading.background =
                ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
            txtRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            txtRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            bookStatus = "1"
        }
        txtRead.setOnClickListener {
            txtRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
            currentlyReading.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            txtNonRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            bookStatus = "2"
        }
        txtNonRead.setOnClickListener {
            txtNonRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_selected_border_bg)
            currentlyReading.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            txtRead.background =
                ContextCompat.getDrawable(context, R.drawable.card_gray_border_bg)
            bookStatus = "0"
        }
        btnSaveStatus.setOnClickListener {
            dialog.dismiss()
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            if (bookStatus == "0") {
                callFavoriteApi(bookId)
            } else {
                callChangeBookStatusApi(bookStatus, bookId, childId)
            }
        }
        imgDismiss.setOnClickListener {
            dialog.dismiss()
        }

    }

    private fun callChangeBookStatusApi(
        bookStatus: String,
        bookId: String,
        childId: String
    ) {
        viewModel.bookStatus(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookId, "", bookStatus, childId
        )
    }

    /**
     * add items to Cart
     */
    private fun addToCart() {
        val cartItem = CartItem()
        val cartfreeItem = CartFreeItem()
        cartItem.bookId = bookListData!!.id
        cartItem.weight = bookListData!!.weight
        cartItem.bookName = bookListData!!.title
        cartItem.perItemCharge = 0.05
        cartItem.description = bookListData!!.shortDescription
        cartItem.img = bookListData!!.img
        cartItem.price = bookListData!!.ukPrice.toString()
        cartfreeItem.price = bookListData!!.ukPrice.toString()
        (activity as HomeActivity).addDataToCartList(cartItem)
        (activity as HomeActivity).addFreeDataToCartList(cartfreeItem)
        (context as HomeActivity).calculateShippingCharge()
        cartCountAndVisibility()
    }

    /**
     * call review api
     */
    private fun callReviewApi(bookId: String, rating: String, review: String) {
        viewModel.addReview(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId, rating, review
        )
    }

    /**
     * call Favorite api
     */
    private fun callFavoriteApi(bookId: String) {
        viewModel.addFavoriteBook(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId
        )
    }

    /**
     * call UnFavorite api
     */
    private fun callUnFavoriteApi(bookId: String) {
        viewModel.removeFavoriteBook(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId
        )
    }


    interface BooksDetailsFragmentInterface {
        fun openBackFragment()
        fun openBooksFragment(bundle: Bundle)
        fun openCartFragment(bundle: Bundle)
        fun openBookDetailFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BooksDetailsFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BooksDetailsFragmentInterface")
        }
    }

    /**
     * Dialog for to give review on books.
     */
    private fun OpenReviewDialog(
        bookId: String,
        context: Context
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_rate_this_book)

        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        dialog.show()

        val ratingBar1 = dialog.findViewById(R.id.reviewRatingBar) as RatingBar
        val txtSkipNow = dialog.findViewById(R.id.txtSkipNow) as TextView
        val edtReview = dialog.findViewById(R.id.edtReview) as EditText
        val btnSubmitReview = dialog.findViewById(R.id.btnSubmitReview) as Button
        txtSkipNow.setOnClickListener {
            dialog.dismiss()
        }
        btnSubmitReview.setOnClickListener {
            dialog.dismiss()
            callReviewApi(
                bookId,
                ratingBar1.rating.toInt().toString(),
                edtReview.text.toString().trim()
            )
        }
    }

    /**
     * BroadCastReceiver used for refreshing data
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.UPDATE_CART_ITEM -> {
                    cartCountAndVisibility()
                }
                AppConstants.ADD_CHILD_ID -> {
                    val id = intent.getStringExtra(AppConstants.CHILD_ID)
                    childId = id!!
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

    /**
     * to add cart item in JsonArray form.
     */
    private fun getJsonOrderList(): String {
        val jsonArray = JSONArray()
        for (i in 0 until (activity as HomeActivity).cartItems.size) {
            val cartItem = (activity as HomeActivity).cartItems[i]
            val jsonObject = JSONObject()
            jsonObject.put("bookId", cartItem.bookId)
            jsonObject.put("quantity", cartItem.quantity)
            jsonArray.put(jsonObject)
        }
        return jsonArray.toString()
    }

    /**
     * call BookDetail Api.
     */
    fun callBookDatailAPi(bookId: String) {
        val bundle = Bundle()
        bundle.putString("bookId", bookId)
        bundle.putString("childId", childId)
        mListener?.openBookDetailFragment(bundle)
    }

    fun callBookStatusAPi(childId: String?) {
        if (bookStatus == "0") {
            callFavoriteApi(bookId)
        } else {
            callChangeBookStatusApi(bookStatus, bookId, childId!!)
        }
    }
}