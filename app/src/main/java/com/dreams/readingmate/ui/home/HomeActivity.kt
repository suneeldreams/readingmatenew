package com.dreams.readingmate.ui.home

import android.content.*
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.CartFreeItem
import com.dreams.readingmate.data.network.responses.CartItem
import com.dreams.readingmate.databinding.ActivityHomeBinding
import com.dreams.readingmate.ui.ContactUsFragment
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.ui.base.Navigator
import com.dreams.readingmate.ui.home.addchild.AddChildFragment
import com.dreams.readingmate.ui.home.books.*
import com.dreams.readingmate.ui.home.dashboard.HomeFragment
import com.dreams.readingmate.ui.home.dashboard.NextBookReadPopupFragment
import com.dreams.readingmate.ui.home.favouritecharater.FavouriteCharacterFragment
import com.dreams.readingmate.ui.home.favouritegenres.FavouriteGenresFragment
import com.dreams.readingmate.ui.home.favouritesbooks.AlreadyReadBooksFragment
import com.dreams.readingmate.ui.home.favouritesbooks.FavouriteBookFragment
import com.dreams.readingmate.ui.home.favouritesbooks.NewFavoriteBookFragment
import com.dreams.readingmate.ui.home.favouritesbooks.ReadingFragment
import com.dreams.readingmate.ui.home.favouritesubject.FavouriteSubjectFragment
import com.dreams.readingmate.ui.home.news.NewsDetailsFragment
import com.dreams.readingmate.ui.home.news.NewsFragment
import com.dreams.readingmate.ui.home.profile.AccountSettingFragment
import com.dreams.readingmate.ui.home.profile.ProfileFragment
import com.dreams.readingmate.ui.home.profile.UpdateProfileFragment
import com.dreams.readingmate.ui.home.progress.*
import com.dreams.readingmate.ui.home.shoping.*
import com.dreams.readingmate.ui.home.staticpage.FaqFragment
import com.dreams.readingmate.ui.home.subscription.BundleDetailFragment
import com.dreams.readingmate.ui.home.subscription.SubscriptionFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity<ActivityHomeBinding>(R.layout.activity_home), NavigationContract,
    NavigationDrawerFragment.OnDrawerItemSelectedListener,
    FavouriteBookFragment.FavouriteBookFragmentInterface,
    FavouriteCharacterFragment.FavouriteCharacterFragmentInterface,
    FavouriteSubjectFragment.FavouriteSubjectFragmentInterface,
    BookReviewedFragment.BookReviewedFragmentInterface,
    CheckOutFragment.CheckOutFragmentInterface,
    ReadingFragment.ReadingFragmentInterface,
    AddCardFragment.AddCardFragmentInterface,
    HistoryFragment.HistoryFragmentInterface,
    BookOrderFragment.BookOrderFragmentInterface,
    NewsFragment.NewsFragmentInterface,
    NextBookReadPopupFragment.HomeFragmentInterface,
    BundleDetailFragment.BundleDetailFragmentInterface,
    NewsDetailsFragment.NewsDetailFragmentInterface,
    AddNewLocationFragment.AddNewLocationFragmentInterface,
    AllBooksFragment.ALLBookFragmentInterface,
    ProfileFragment.ProfileFragmentInterface,
    SubscriptionFragment.SubscriptionFragmentInterface,
    AlreadyReadBooksFragment.AlreadyReadBookFragmentInterface,
    ProgressFragment.ProgressFragmentInterface,
    AllTypeBooksFragment.AllTypeBookFragmentInterface,
    UpdateProfileFragment.UpdateProfileFragmentInterface,
    AccountSettingFragment.AccountSettingInterface,
    UserWishListFragment.UserWishListFragmentInterface,
    NewFavoriteBookFragment.NewFavoriteFragmentInterface,
    BooksDetailsFragment.BooksDetailsFragmentInterface,
    BuyBookFragment.BuyBookFragmentInterface,
    SetReminderFragment.SetReminderFragmentInterface, ShopTabFragment.ShopTabFragmentInterface,
    BookShelfFragment.BookShelfFragmentInterface, ShopingFragment.ShopingFragmentInterface,
    FavouriteGenresFragment.FavouriteGenresFragmentInterface,
    HomeFragment.HomeFragmentInterface, ProgressDetailFragment.HomeFragmentInterface,
    BooksFragment.BookFragmentInterface, AddChildFragment.AddChildFragmentInterface,
    ContactUsFragment.ContactUsFragmentInterface, View.OnClickListener {

    private var mDrawer: DrawerLayout? = null
    private var toolbar: Toolbar? = null
    var prefs: SharedPreferences? = null
    var cartItems: ArrayList<CartItem> = arrayListOf()
    var cartFreeItems: ArrayList<CartFreeItem> = arrayListOf()
    var tabClickedId: String? = null
    var childId = ""
    var childEmpty = ""
    var nextMilsToneValue = ""
    var couponCodeUsed = ""
    var refreshBookStatus = ""
    var freeBookGrandTotal = ""
    var homeFragment = HomeFragment()
    private var mDrawerFragment: NavigationDrawerFragment? = null
    lateinit var binding: ActivityHomeBinding

    /**
     * set hete initial data and manage tabs
     */
    override fun initComponents(savedInstanceState: Bundle?, binding: ActivityHomeBinding) {
        this.binding = binding
        toolbar = findViewById(R.id.toolbar)
        mDrawer = findViewById(R.id.drawer_layout)
        setupToolbar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setDisplayShowCustomEnabled(true)
        setUpData()
        setTabImage("home")
        observeNavigationEvents()
        mDrawerFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_navigation_drawer) as NavigationDrawerFragment
        mDrawerFragment?.setUp(R.id.fragment_navigation_drawer, mDrawer, toolbar)
        Navigator.addFragment(homeFragment, this, R.id.fl_fragment_container, false)
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.DE_ACTIVE_EVENT))
    }

    /**
     * setUp initial data
     */
    private fun setUpData() {
        prefs = mUserData.customPrefs(this)
        tabClickedId = prefs?.getString(mUserData.CLICKEDID, "")
        when (tabClickedId) {
            "home" -> {
                lnrHomeTab.callOnClick()
            }
            "progress" -> {
                lnrProgressTab.callOnClick()
            }
            "books" -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add at least 1 child to use this feature.")
                } else {
                    lnrBooksTab.callOnClick()
                }
            }
            "shop" -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add at least 1 child to use this feature.")
                } else {
                    lnrShopTab.callOnClick()
                }
            }
        }
    }

    /**
     * Click Listeners
     */
    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        lnrHomeTab.setOnClickListener(this)
        lnrProgressTab.setOnClickListener(this)
        lnrBooksTab.setOnClickListener(this)
        lnrNewsTab.setOnClickListener(this)
        lnrShopTab.setOnClickListener(this)
        lnrProfileTab.setOnClickListener(this)
    }


    override fun onDrawerItemSelected(viewId: Int?) {
        when (viewId) {
            R.id.logOut -> {
                openLogoutDialog(this)
            }
            R.id.imgClose, R.id.txtHome -> {
                Navigator.replaceFragment(homeFragment, this, R.id.fl_fragment_container, false)
            }
            R.id.txtProfile -> {
                Navigator.addFragment(ProfileFragment(), this, R.id.fl_fragment_container, true)
            }

            R.id.txtTermsCondition -> {
                Navigator.addFragment(FaqFragment(), this, R.id.fl_fragment_container, true)
            }

            R.id.txtFAQ -> {
                Navigator.addFragment(FaqFragment(), this, R.id.fl_fragment_container, true)
            }

        }
//        openCloseDrawer()
    }

    /**
     * handle OnBackpressed.
     */
    override fun openBackFragment() {
        val count = supportFragmentManager.backStackEntryCount
        super.onBackPressed()
    }

    override fun openProfile() {
    }

    override fun openCloseDrawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            drawer_layout.openDrawer(GravityCompat.START)
        }
    }

    private fun openLogoutDialog(context: Context) {
        val alertDialog =
            androidx.appcompat.app.AlertDialog.Builder(context).create()
        alertDialog.setTitle(context.resources.getString(R.string.app_name))
        alertDialog.setMessage(getString(R.string.signout_msg))
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setButton(
            DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.signout)
        ) { dialog, _ ->
            dialog.dismiss()
            homeFragment.logout()
        }
        alertDialog.setButton(
            DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.cancel)
        ) { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        mDrawerFragment?.notifyNavDrawerAdapter()
    }

    internal fun setupToolbar(toolbar: Toolbar?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    /**
     * Handle Tab click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.lnrHomeTab -> {
                prefs!![mUserData.CLICKEDID] = "home"
                setTabImage("home")
                Navigator.replaceFragment(homeFragment, this, R.id.fl_fragment_container, false)
            }
            R.id.lnrProgressTab -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add a reader first.")
                } else {
                    prefs!![mUserData.CLICKEDID] = "progress"
                    setTabImage("progress")
                    Navigator.addFragment(
                        ProgressFragment(),
                        this,
                        R.id.fl_fragment_container,
                        false
                    )
                }

            }
            R.id.lnrBooksTab -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add a reader first.")
                } else {
                    prefs!![mUserData.CLICKEDID] = "books"
                    setTabImage("books")
                    Navigator.addFragment(BooksFragment(), this, R.id.fl_fragment_container, false)
                }
            }
            R.id.lnrNewsTab -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add a reader first.")
                } else {
                    prefs!![mUserData.CLICKEDID] = "news"
                    setTabImage("news")
                    Navigator.addFragment(NewsFragment(), this, R.id.fl_fragment_container, false)
                }
            }
            R.id.lnrShopTab -> {
                if (childEmpty == "0") {
                    AlertUtils.showToast(this, "Please add a reader first.")
                } else {
                    prefs!![mUserData.CLICKEDID] = "shop"
                    setTabImage("shop")
                    Navigator.addFragment(
                        ShopTabFragment(),
                        this,
                        R.id.fl_fragment_container,
                        false
                    )
                }
            }
            R.id.lnrProfileTab -> {
                prefs!![mUserData.CLICKEDID] = "profile"
                setTabImage("profile")
                Navigator.addFragment(
                    AccountSettingFragment(),
                    this,
                    R.id.fl_fragment_container,
                    false
                )
            }
        }
    }

    override fun openAddChildFragment(bundle: Bundle) {
        Navigator.addFragment(AddChildFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openAddCardFragment() {
        Navigator.addFragment(AddCardFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openAddLocationFragment() {
        Navigator.addFragment(AddNewLocationFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openBooksFragment(bundle: Bundle) {
        Navigator.addFragment(BooksFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openAlreadyReadFragment(bundle: Bundle) {
        Navigator.addFragment(
            AlreadyReadBooksFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openReadingFragment(bundle: Bundle) {
        Navigator.addFragment(
            ReadingFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openCartFragment(bundle: Bundle) {
        Navigator.addFragment(
            ShopingFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    /*override fun openFavouriteBookFragment(bundle: Bundle) {
        Navigator.addFragment(
            FavouriteBookFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }*/
    override fun openNewFragmentFragment(bundle: Bundle) {
        Navigator.addFragment(
            NewFavoriteBookFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openGenresFragment(bundle: Bundle) {
        Navigator.addFragment(
            FavouriteGenresFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openSubjectFragment(bundle: Bundle) {
        Navigator.addFragment(
            FavouriteSubjectFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openCharacterFragment(bundle: Bundle) {
        Navigator.addFragment(
            FavouriteCharacterFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openBookShelfFragment(bundle: Bundle) {
        Navigator.addFragment(BookShelfFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openAccountSettingFragment() {
        Navigator.addFragment(
            AccountSettingFragment(),
            this,
            R.id.fl_fragment_container,
            true
        )
    }

    override fun openSubsciptionFragment() {
        Navigator.addFragment(
            SubscriptionFragment(),
            this,
            R.id.fl_fragment_container,
            true
        )
    }

    override fun openBundleDetailFragment(bundle: Bundle) {
        Navigator.addFragment(
            BundleDetailFragment(),
            this,
            R.id.fl_fragment_container,
            true, bundle
        )
    }

    override fun openBookReviewedFragment(bundle: Bundle) {
        Navigator.addFragment(
            BookReviewedFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openSetReminderFragment() {
        Navigator.addFragment(SetReminderFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openBookDetailFragment(bundle: Bundle) {
        Navigator.addFragment(
            BooksDetailsFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openNextPopUpFragment(bundle: Bundle) {
        Navigator.addFragment(
            NextBookReadPopupFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openAllTypeBookFragment(bundle: Bundle) {
        Navigator.addFragment(
            AllTypeBooksFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
    }

    override fun openHistoryFragment(bundle: Bundle) {
        Navigator.addFragment(HistoryFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openAllBookFragment(bundle: Bundle) {
        Navigator.addFragment(AllBooksFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openBuyBookFragment() {
        Navigator.addFragment(BuyBookFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openCheckoutFragment() {
        Navigator.addFragment(CheckOutFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openCheckoutFragments(bundle: Bundle) {
        Navigator.addFragment(CheckOutFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openProfileFragment() {
        Navigator.addFragment(ProfileFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openUpdateProfileFragment() {
        Navigator.addFragment(UpdateProfileFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openNewsDetailFragment(bundle: Bundle) {
        Navigator.addFragment(NewsDetailsFragment(), this, R.id.fl_fragment_container, true, bundle)
    }

    override fun openOrderFragment() {
        Navigator.addFragment(BookOrderFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openUserWishListFragment() {
        Navigator.addFragment(UserWishListFragment(), this, R.id.fl_fragment_container, true)
    }

    override fun openHomeFragment() {
//        prefs!![mUserData.CLICKEDID] = "home"
//        setTabImage("home")
//        Navigator.replaceFragment(homeFragment, this, R.id.fl_fragment_container, false)
        lnrHomeTab.callOnClick()
    }

    override fun openProgressFragment(bundle: Bundle) {
        Navigator.addFragment(
            ProgressDetailFragment(),
            this,
            R.id.fl_fragment_container,
            true,
            bundle
        )
//        lnrProgressTab.performClick()
    }


    private fun setTabImage(tab: String) {
        when (tab) {
            "home" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_active_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_book
                    )
                )
                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_shop
                    )
                )
//                imgProfileInactive.visibility = View.VISIBLE
                imgProfileActive.visibility = View.VISIBLE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
            }
            "progress" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_active_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_book
                    )
                )
                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_shop
                    )
                )
//                imgProfileInactive.visibility = View.VISIBLE
                imgProfileActive.visibility = View.VISIBLE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_black))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
            }
            "books" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_active_book
                    )
                )

                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_shop
                    )
                )
//                imgProfileInactive.visibility = View.VISIBLE
                imgProfileActive.visibility = View.VISIBLE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_black))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
            }

            "news" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_book
                    )
                )
                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_shop
                    )
                )
//                imgProfileInactive.visibility = View.VISIBLE
                imgProfileActive.visibility = View.VISIBLE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
            }

            "shop" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_book
                    )
                )

                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_active_shop
                    )
                )
//                imgProfileInactive.visibility = View.VISIBLE
                imgProfileActive.visibility = View.VISIBLE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))

            }
            "profile" -> {
                imgHome.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_home
                    )
                )
                imgProgress.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_progress
                    )
                )
                imgBooks.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_book
                    )
                )
                imgNews.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_newspaper_active
                    )
                )
                imgNews.setColorFilter(
                    imgNews.context.resources.getColor(R.color.color_active_button),
                    PorterDuff.Mode.SRC_ATOP
                )
                imgShop.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_inactive_shop
                    )
                )
                /* imgProfileActive.setImageDrawable(
                     ContextCompat.getDrawable(
                         applicationContext,
                         R.drawable.ic_user_inactive
                     )
                 )*/
                imgProfileActive.visibility = View.VISIBLE
//                imgProfileInactive.visibility = View.GONE

                txtTabHome.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabProgress.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabBooks.setTextColor(ContextCompat.getColor(this, R.color.color_gray))
                txtTabNews.setTextColor(ContextCompat.getColor(this, R.color.color_active_button))
                txtTabShop.setTextColor(ContextCompat.getColor(this, R.color.color_slate))
                txtTabProfile.setTextColor(ContextCompat.getColor(this, R.color.color_slate))

                viewHome.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewBooks.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewNews.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewShop.setBackgroundColor(ContextCompat.getColor(this, R.color.color_white))
                viewProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.color_stroke))

            }
        }
    }

    fun addDataToCartList(cartItem: CartItem) {
        cartItems.add(cartItem)
        /*if (cartItems.isEmpty()) {
            cartItems.add(cartItem)
        } else {
            for (i in 0 until cartItems.size) {
                if (cartItems[i].bookId != cartItem.bookId) {
                    cartItems.add(cartItem)
                } else {
                    AlertUtils.showToast(
                        this,
                        resources.getString(R.string.already_added_cart_item)
                    )
                }
            }
        }*/

    }

    fun addFreeDataToCartList(cartItem: CartFreeItem) {
        cartFreeItems.add(cartItem)
    }

    fun increaseQuantity(position: Int) {
        try {
            cartItems[position].quantity++
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun decreaseQuantity(position: Int) {
        try {
            if (cartItems[position].quantity == 1) {
                cartItems.removeAt(position)
            } else {
                cartItems[position].quantity--
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getCartCount(): Int {
        var cartCount = 0
        for (i in 0 until cartItems.size) {
            cartCount += cartItems[i].quantity
        }
        return cartCount
    }

    fun getGrandTotalPrice(calWeight: Double): Double {
        var grandTotal = 0.0
        grandTotal = getTotalPrice() + calWeight + calculateShippingCharge()
        return grandTotal
    }

    fun getRemainingBookGrandTotal(calWeight: Double, grandFreeTotal: Double): Double {
        var grandTotal = 0.0
        grandTotal = grandFreeTotal + calWeight + calculateShippingCharge()
        return grandTotal
    }

    fun getFreeCartTotal(token: Int): Double {
        var cartFreeTotal = 0.0
        var cartGrandTotal = 0.0
        try {
            cartItems.sortBy { it.price }
//            cartFreeItems.forEach { println(it) }
            for (i in 0 until cartItems.size) {
                cartGrandTotal += cartItems[i].price!!.toDouble()
            }
            cartItems.sortBy { it.price }
            for (j in 0 until token) {
//                Log.d("FreeData", cartItems[j].price!!)
                cartFreeTotal += cartItems[j].price!!.toDouble()
//                cartItems.removeAt(j)
            }
            for (j in 0 until token) {
                cartItems.removeAt(j)
            }
        } catch (e: Exception) {
        }
        /*try {
            cartFreeItems.sortBy { it.price }
//            cartFreeItems.forEach { println(it) }
            for (i in 0 until cartFreeItems.size) {
                cartGrandTotal += cartItems[i].price!!.toDouble()
            }
            for (j in 0 until token) {
                Log.d("FreeData", cartFreeItems[j].price!!)
                cartFreeTotal += cartFreeItems[j].price!!.toDouble()
            }
        } catch (e: Exception) {
        }*/
        return cartGrandTotal - cartFreeTotal
    }

    fun calculateShippingCharge(): Double {
        var shippingCharge = 0.0
        val serviceCharge = 0.65
        try {
            for (i in 0 until cartItems.size) {
                shippingCharge += (cartItems[i].perItemCharge * cartItems[i].quantity.toDouble())
            }
        } catch (e: Exception) {
        }
        return shippingCharge + serviceCharge
    }

    fun calculateWeight(): Int {
        var weight = 0
        try {
            for (i in 0 until cartItems.size) {
                weight += (cartItems[i].weight!!.toInt() * cartItems[i].quantity)
            }
        } catch (e: Exception) {
        }
        return weight
    }

    fun calculatePaidBookWeight(): Int {
        var weight = 0
        try {
            for (i in 0 until cartItems.size) {
                weight += (cartItems[i].weight!!.toInt() * cartItems[i].quantity)
            }
        } catch (e: Exception) {
        }
        return weight
    }

    fun getTotalPrice(): Double {
        return getCartTotal()
    }

    private fun getCartTotal(): Double {
        var cartTotal = 0.0
        try {
            for (i in 0 until cartItems.size) {
                cartTotal += (cartItems[i].price!!.toDouble() * cartItems[i].quantity.toDouble())
            }
        } catch (e: Exception) {
        }
        return cartTotal
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.DE_ACTIVE_EVENT -> {
                    childEmpty = "0"
//                    lnrProgressTab.isClickable = false
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver)
    }
}
