package com.dreams.readingmate.ui.home.profile

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.AccountSettingBinding
import com.dreams.readingmate.ui.auth.LoginActivity
import com.dreams.readingmate.ui.auth.ResetPasswordActivity
import com.dreams.readingmate.ui.base.BaseFragment
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AccountSettingFragment : BaseFragment<AccountSettingBinding>(R.layout.account_setting),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: AccountSettingInterface? = null
    private lateinit var binding: AccountSettingBinding
    private val viewModel: ProfileViewModel by instance<ProfileViewModel>()

    /**
     * click event listener.
     */
    override fun initComponents(savedInstanceState: Bundle?, binding: AccountSettingBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.acountToolbar.imgBack.visibility = View.GONE
        binding.acountToolbar.imgBack.setOnClickListener(this)
        binding.acountToolbar.toolbar_title.text = getString(R.string.account_setting)
        binding.txtProfile.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener { }
        binding.txtOrder.setOnClickListener(this)
        binding.txtWishList.setOnClickListener(this)
//        binding.txtMemberShip.setOnClickListener(this)
        binding.txtResetPassword.setOnClickListener(this)
        binding.txtLogout.setOnClickListener(this)
    }

    /**
     * Handle click event.
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtProfile -> mListener?.openProfileFragment()
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.txtOrder -> mListener?.openOrderFragment()
            R.id.txtWishList -> mListener?.openUserWishListFragment()
//            R.id.txtMemberShip -> mListener?.openSubsciptionFragment()
            R.id.txtResetPassword -> {
                Intent(requireContext(), ResetPasswordActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.txtLogout -> {
                openLogoutDialog(requireContext())
            }
        }
    }

    /**
     * Logout form app.
     */
    private fun openLogoutDialog(context: Context) {
        val alertDialog =
            androidx.appcompat.app.AlertDialog.Builder(context).create()
        alertDialog.setTitle(context.resources.getString(R.string.app_name))
        alertDialog.setMessage(getString(R.string.signout_msg))
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setButton(
            DialogInterface.BUTTON_POSITIVE, context.resources.getString(R.string.signout)
        ) { dialog, _ ->
            dialog.dismiss()
            UserData.clearCustomPref(requireContext())
            requireContext().startActivity(
                Intent(
                    context,
                    LoginActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
        alertDialog.setButton(
            DialogInterface.BUTTON_NEGATIVE, context.resources.getString(R.string.cancel)
        ) { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }


    interface AccountSettingInterface {
        fun openProfileFragment()
        fun openBackFragment()
        fun openOrderFragment()
        fun openSubsciptionFragment()
        fun openUserWishListFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AccountSettingInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AccountSettingInterface")
        }
    }

}