package com.dreams.readingmate.ui.home.shoping

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.*
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.ShopTabFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.shop_tab_fragment.*
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ShopTabFragment : BaseFragment<ShopTabFragmentBinding>(R.layout.shop_tab_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: ShopTabFragmentInterface? = null
    private lateinit var binding: ShopTabFragmentBinding
    private var isMembership = false
    var itemChildList: ArrayList<Model.childList> = ArrayList()
    private var childListAdapter: ChildListForMembershipAdapter? = null
    private val viewModel: ShopingViewModel by instance<ShopingViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: ShopTabFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgUserProfile.visibility = View.GONE
        binding.includeToolbar.imgRecommCart.visibility = View.VISIBLE
        binding.includeToolbar.imgRecommBook.visibility = View.VISIBLE
        binding.includeToolbar.imgRecommCart.setOnClickListener(this)
        binding.includeToolbar.imgRecommBook.setOnClickListener(this)
        binding.txtMembership.setOnClickListener(this)
        setUpData()
        observeLiveEvents()
        cartCountAndVisibility()
        getRecommendDataList()
        callMembershipApi()
        callGetChildListApi()
    }

    /**
     * call api for Recommend books.
     */
    private fun getRecommendDataList() {
        viewModel.callRecommendBooksAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    private fun getChildRecommendData(childId: String?) {
        viewModel.callChildRecommendBooksAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            childId!!
        )
    }

    private fun callMembershipApi() {
        viewModel.callMembershipStatusAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    private fun callGetChildListApi() {
        viewModel.callChildListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            Utils.getCustomPref(requireContext(), UserData.ID)
        )
    }

    /**
     * set Cart items count.
     */
    private fun cartCountAndVisibility() {
        val cartCount = (activity as HomeActivity).getCartCount()
        if (cartCount > 0) {
            binding.includeToolbar.txtCount.visibility = View.VISIBLE
            binding.includeToolbar.txtCount.text = cartCount.toString()
        } else {
            binding.includeToolbar.txtCount.visibility = View.GONE
        }
    }

    private fun setUpData() {
        binding.recyclerRecommends.layoutManager = GridLayoutManager(activity, 2)
        binding.recyclerChildRecommendsList.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.addCartLiveData.observe(this, Observer {
            val bundle = Bundle()
            bundle.putString("grandTotal", it.grandTotal.toString())
            bundle.putString("subTotal", it.subTotal.toString())
            bundle.putString("deliveryCharge", it.deliveryCharge.toString())
            mListener!!.openCartFragment(bundle)
        })

        viewModel.recommendBookListLiveData.observe(this, Observer {
            if (it.isEmpty()) {
                binding.txtNoDataRecommendation.visibility = View.VISIBLE
                binding.recyclerRecommends.visibility = View.GONE
            } else {
                binding.txtNoDataRecommendation.visibility = View.GONE
                binding.recyclerRecommends.visibility = View.VISIBLE
                binding.recyclerRecommends.adapter = RecommendBookListAdapter(it, mListener)
            }
        })

        viewModel.membershipStatusLiveData.observe(this, Observer {
            binding.txtMembership.visibility = View.GONE
            isMembership = true
        })
        viewModel.childListLiveData.observe(this, Observer {
            Log.d("childList:-", it.toString())
            itemChildList.add(Model.childList("", "Readingmate", "", true))
            for (child in it) {
                itemChildList.add(
                    Model.childList(
                        child.image!!,
                        child.childName!!,
                        child.id!!,
                        false
                    )
                )
            }
            childListAdapter =
                ChildListForMembershipAdapter(itemChildList, mListener, this)
            recyclerChildRecommendsList.adapter = childListAdapter
        })
        viewModel.errorCode.observe(this, Observer {
            if (it == 404) {
                binding.txtMembership.visibility = View.VISIBLE
                isMembership = false
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * Handle Click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.imgRecommCart -> {
                if ((activity as HomeActivity).cartItems.isEmpty()) {
                    val bundle = Bundle()
                    bundle.putString("from", "shopping")
                    mListener?.openCartFragment(bundle)
//                    mListener?.openBooksFragment(bundle)
                } else {
                    callCartApi()
                }
            }
            R.id.imgRecommBook -> {
                val bundle = Bundle()
                bundle.putString("childId", "")
                mListener?.openAllTypeBookFragment(bundle)
            }
            R.id.txtMembership -> {
                if (isMembership) {
                    mListener?.openProfileFragment()
                } else {
                    mListener?.openSubsciptionFragment()
                }
            }
        }

    }

    private fun callCartApi() {
        viewModel.addCartData(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            getJsonOrderList()
        )
    }

    /**
     * set Cart item in JsonArray.
     */
    private fun getJsonOrderList(): String {
        val jsonArray = JSONArray()
        for (i in 0 until (activity as HomeActivity).cartItems.size) {
            val cartItem = (activity as HomeActivity).cartItems[i]
            val jsonObject = JSONObject()
            jsonObject.put("bookId", cartItem.bookId)
            jsonObject.put("quantity", cartItem.quantity)
            jsonArray.put(jsonObject)
        }
        return jsonArray.toString()
    }

    interface ShopTabFragmentInterface {
        fun openBackFragment()
        fun openBooksFragment(bundle: Bundle)
        fun openCartFragment(bundle: Bundle)
        fun openSubsciptionFragment()
        fun openProfileFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openAllTypeBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ShopTabFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ShopTabFragmentInterface")
        }
    }

    fun callRecommendAPi(childId: String?, position: Int) {
        for (i in itemChildList.indices) {
            itemChildList[i].isSelected = position == i
        }
        childListAdapter!!.notifyDataSetChanged()
        if (position == 0) {
            getRecommendDataList()
        } else {
            getChildRecommendData(childId)
        }
    }

}