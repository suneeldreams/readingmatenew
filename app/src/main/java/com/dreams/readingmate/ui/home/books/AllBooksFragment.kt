package com.dreams.readingmate.ui.home.books

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.*
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.AllBooksFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AllBooksFragment : BaseFragment<AllBooksFragmentBinding>(R.layout.all_books_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: ALLBookFragmentInterface? = null
    private lateinit var binding: AllBooksFragmentBinding
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    private val viewModel: BooksViewModel by instance<BooksViewModel>()
    private var bookId = ""
    private var childId = ""

    override fun initComponents(savedInstanceState: Bundle?, binding: AllBooksFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.allBooksToolbar.imgUserProfile.visibility = View.GONE
        binding.imgAddSuggestedBook.setOnClickListener(this)
        setUpData()
        setData()
        observeLiveEvents()
        binding.imgCurrentlyReading.setOnClickListener(this)
        binding.imgSearchBook.setOnClickListener(this)
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.REFERESH_BOOKS))
    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            childId = args.getString("childId").toString()
            val childName = args.getString("nickName").toString()
            (requireContext() as HomeActivity).childId = childId
            val upperString: String =
                childName.substring(0, 1).toUpperCase() + childName.substring(1).toLowerCase()
            binding.allBooksToolbar.toolbar_title.text = "$upperString's Books"
        }
        viewModel.callAllBooksDataAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            childId
        )
    }

    private fun setData() {
        binding.recyclerBooksHaveRead.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewWant.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerBooksToReview.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerWantToRead.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerBooksIHaveReviewed.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    /**
     * MVVM ObserverLiveEvents result.
     */
    private fun observeLiveEvents() {
        viewModel.allBookLiveData.observe(this, Observer {
            Log.d("bookList:-", it.toString())
            if (!it.currentlyReadingBook!!.image.isNullOrEmpty()) {
                binding.txtCurrentReading.visibility = View.GONE
                binding.cardView.visibility = View.VISIBLE
                bookId = it.currentlyReadingBook.bookId!!
                Utils.displayImage(
                    requireContext(),
                    it.currentlyReadingBook.image!!,
                    binding.imgCurrentlyReading,
                    R.drawable.ic_place_holder
                )
            } else {
                binding.txtCurrentReading.visibility = View.VISIBLE
                binding.cardView.visibility = View.INVISIBLE
            }
            if (it.readBooks!!.isNotEmpty()) {
                binding.txtReadBookNoData.visibility = View.GONE
                binding.recyclerBooksHaveRead.visibility = View.VISIBLE
                /* binding.recyclerBooksHaveRead.adapter =
                     ReadBooksListAdapter(it.readBooks, mListener, childId)*/
            } else {
                binding.txtReadBookNoData.visibility = View.VISIBLE
                binding.recyclerBooksHaveRead.visibility = View.GONE
            }

            if (it.boughtBooks!!.isNotEmpty()) {
                binding.txtBoughtNoData.visibility = View.GONE
                binding.recyclerViewWant.visibility = View.VISIBLE
                /*binding.recyclerViewWant.adapter =
                    BoughtBooksListAdapter(it.boughtBooks, mListener, childId)*/
            } else {
                binding.txtBoughtNoData.visibility = View.VISIBLE
                binding.recyclerViewWant.visibility = View.GONE
            }
            if (it.wishlist!!.isNotEmpty()) {
                binding.txtWantToReadNoData.visibility = View.GONE
                binding.recyclerWantToRead.visibility = View.VISIBLE
                /*binding.recyclerWantToRead.adapter =
                    WishListBooksListAdapter(it.wishlist, mListener, childId)*/
            } else {
                binding.txtWantToReadNoData.visibility = View.VISIBLE
                binding.recyclerWantToRead.visibility = View.GONE
            }

            if (it.reviewedBooks!!.isNotEmpty()) {
                binding.txtBooksHaveReviewedNoData.visibility = View.GONE
                binding.recyclerBooksIHaveReviewed.visibility = View.VISIBLE
                /* binding.recyclerBooksIHaveReviewed.adapter =
                     BooksIHaveReviewedListAdapter(it.reviewedBooks, mListener, childId)*/
            } else {
                binding.txtBooksHaveReviewedNoData.visibility = View.VISIBLE
                binding.recyclerBooksIHaveReviewed.visibility = View.GONE
            }

            if (it.toReviewBooks!!.isNotEmpty()) {
                binding.txtWishlistNoData.visibility = View.GONE
                binding.recyclerBooksToReview.visibility = View.VISIBLE
                /* binding.recyclerBooksToReview.adapter =
                     ToReviewBooksListAdapter(it.toReviewBooks, mListener, childId)*/
            } else {
                binding.txtWishlistNoData.visibility = View.VISIBLE
                binding.recyclerBooksToReview.visibility = View.GONE
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * To perform click event actions
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgCurrentlyReading -> {
                val bundle = Bundle()
                bundle.putString("bookId", bookId)
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openBookDetailFragment(bundle)
            }
            R.id.imgSearchBook -> {
                val bundle = Bundle()
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openAllTypeBookFragment(bundle)
            }
            R.id.imgAddSuggestedBook -> {
                val bundle = Bundle()
                bundle.putString("childId", (requireContext() as HomeActivity).childId)
                mListener?.openAllTypeBookFragment(bundle)
            }
        }
    }


    interface ALLBookFragmentInterface {
        fun openBackFragment()
        fun openBuyBookFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openAllTypeBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ALLBookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ALLBookFragmentInterface")
        }
    }

    /**
     * Local BroadCastReceiver for refreshing data
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.REFERESH_BOOKS -> {
                    viewModel.callAllBooksDataAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        childId
                    )
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

}