package com.dreams.readingmate.ui.home.dashboard

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ColorArrayAdapter
import com.dreams.readingmate.data.adapter.CountryArrayAdapter
import com.dreams.readingmate.data.adapter.SchoolArrayAdapter
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.EditChildProfileFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.add_child_fragment.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class EditChildProfileFragment :
    BaseFragment<EditChildProfileFragmentBinding>(R.layout.edit_child_profile_fragment),
    NavigationContract,
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    var from = ""
    var schoolId = ""
    var countryId = ""
    var colorId = ""
    private val TAKE_PHOTO_REQUEST = 101
    private val TAKE_STORAGE_REQUEST = 102
    private val REQUEST_GALLERY_CODE = 103
    private val REQUEST_IMAGE_CAPTURE = 104
    private var filePath = ""
    var uri: Uri? = null
    private var preferenceChild: PreferenceChildItems? = null
    private lateinit var ageList: Array<String>
    private lateinit var schoolAdapter: SchoolArrayAdapter
    private lateinit var countryAdapter: CountryArrayAdapter
    private lateinit var colorAdapter: ColorArrayAdapter
    private var mListener: AddChildFragmentInterface? = null
    private lateinit var binding: EditChildProfileFragmentBinding
    private val viewModel: HomeViewModel by instance<HomeViewModel>()

    /**
     * Init all views and set data when screen load.
     */
    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: EditChildProfileFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpData()
        setUpToolBar()
        observeLiveEvents()
        observeNavigationEvents()
        initAgeList()
        initSchoolList()
        initCountryList()
        initColorList()

    }

    /**
     * load country data
     */
    private fun initCountryList() {
        countryAdapter = CountryArrayAdapter(requireContext(), preferenceChild?.countryData)
        binding.spinnerCountry.adapter = countryAdapter
        binding.spinnerCountry.onItemSelectedListener = countryListListener
    }

    /**
     * load colour data
     */
    private fun initColorList() {
        colorAdapter = ColorArrayAdapter(requireContext(), preferenceChild?.colourData)
        binding.spinnerColor.adapter = colorAdapter
        binding.spinnerColor.onItemSelectedListener = colorListListener
    }

    /**
     * load school data
     */
    private fun initSchoolList() {
        schoolAdapter = SchoolArrayAdapter(requireContext(), preferenceChild?.schoolTypeData)
        binding.spinnerSchool.adapter = schoolAdapter
        binding.spinnerSchool.onItemSelectedListener = schoolListListener
    }


    private fun initAgeList() {
        binding.spinnerYear.onItemSelectedListener = yearTypeListListener
    }

    /**
     * Listener to select year
     */
    private val yearTypeListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val fuel = ageList[position]
            if (fuel.isNotEmpty()) {
                binding.txtChildAge.text = fuel
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Listener to select school
     */
    private val schoolListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            val selectedState = preferenceChild?.schoolTypeData?.get(position)
            if (selectedState != null) {
                binding.txtSchoolName.text = selectedState.schoolTypeName
                schoolId = selectedState.id!!
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Listener to select country
     */
    private val countryListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            val selectedCountry = preferenceChild?.countryData?.get(position)
            if (selectedCountry != null) {
                binding.txtCountry.text = selectedCountry.countryName
                countryId = selectedCountry.id!!
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Listener to select color
     */
    private val colorListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            val selectedColor = preferenceChild?.colourData?.get(position)
            if (selectedColor != null) {
                binding.txtColor.text = selectedColor.colourName
                colorId = selectedColor.id!!
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
        }

        ageList = resources.getStringArray(R.array.ageList)
        binding.spinnerYear.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, ageList)
    }

    /**
     * set toolbar and toolbar title,
     * according to action
     */
    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (from == "dashboard") {
            binding.includeToolbar.toolbar_title.text = getString(R.string.add_child)
        } else {
            binding.includeToolbar.toolbar_title.text = getString(R.string.edit_profile)
        }

        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.text = getString(R.string.save)
    }

    override fun observeLiveEvents() {
        super.observeLiveEvents()
    }

    /**
     * Observer for ClickEvent Listener.
     */
    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.lnrSelectYear.setOnClickListener(this)
        binding.lnrSchool.setOnClickListener(this)
        binding.lnrCountry.setOnClickListener(this)
        binding.lnrColor.setOnClickListener(this)
        binding.txtBrowzeImage.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                val childName = edtChildName.text.toString().trim()
                val nickName = edtNickName.text.toString().trim()
                val age = txtChildAge.text.toString()
                if (filePath != "") {
                    preferenceChild!!.schoolId = schoolId
                    preferenceChild!!.age = age
                    preferenceChild!!.childName = childName
                    preferenceChild!!.nickName = nickName
                    preferenceChild!!.colorId = colorId
                    preferenceChild!!.countryId = countryId
                    preferenceChild!!.image = filePath
                    val bundle = Bundle()
                    bundle.putParcelable("child", preferenceChild)
                    mListener?.openFavouriteBookFragment(bundle)
                } else {
                    AlertUtils.showToast(requireContext(), "Please upload image.")
                }
            }
            R.id.lnrSelectYear -> binding.spinnerYear.performClick()
            R.id.lnrSchool -> binding.spinnerSchool.performClick()
            R.id.lnrCountry -> binding.spinnerCountry.performClick()
            R.id.lnrColor -> binding.spinnerColor.performClick()
            R.id.txtBrowzeImage -> checkAndAskCameraPermission()
        }
    }


    interface AddChildFragmentInterface {
        fun openBackFragment()
        fun openFavouriteBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AddChildFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AddChildFragmentInterface")
        }
    }

    private fun checkAndAskCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), TAKE_PHOTO_REQUEST)
        } else {
            checkAndAskStoragePermission()
        }
    }

    private fun checkAndAskStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                TAKE_STORAGE_REQUEST
            )
        } else {
            openChooser()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            TAKE_PHOTO_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkAndAskStoragePermission()
                }
            }
            TAKE_STORAGE_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    openChooser()
                }
            }
        }
    }

    private fun openChooser() {
        val dialog = Dialog(requireContext())
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_choose_galery)

        val displayRectangle = Rect()
        val window = activity?.window
        window!!.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()

        dialog.window!!.attributes = lp

        val btnCamera = dialog.findViewById<Button>(R.id.btnCamera)
        val btnGallery = dialog.findViewById<Button>(R.id.btnGallery)
        val btnCancel = dialog.findViewById<ImageView>(R.id.btn_CloseDialog)
        dialog.show()
        btnCamera.setOnClickListener {
            takePicture()
            dialog.dismiss()
        }

        btnGallery.setOnClickListener {
            openGallery()
            dialog.dismiss()
        }

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    /**
     * action capture image from camera.
     */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()
        val uri: Uri = FileProvider.getUriForFile(
            requireContext(), "com.readingmate.android.fileprovider", file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return createTempFile(
            "JPEG_${timeStamp}_", ".jpg", storageDir
        ).apply {
            filePath = absolutePath
        }
    }

    /**
     * Choose image from gallery.
     */
    private fun openGallery() {
        val openGalleryIntent = Intent(Intent.ACTION_PICK)
        openGalleryIntent.type = "image/*"
        startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE)
    }

    /**
     * selecting image results.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
//            binding.imgAddChild.setI
            Utils.displayCircularImage(requireContext(), imgAddChild, filePath)
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_GALLERY_CODE) {
            uri = data?.data
            filePath = getRealPathFromURIPath(uri!!, requireActivity())
            Utils.displayCircularImage(requireContext(), imgAddChild, filePath)
        } else if (requestCode == 1) {
            if (data != null) {
                val bundle = data.extras
                val bitmap = bundle!!.getParcelable<Bitmap>("data")
                val urii = getImageUri(requireContext(), bitmap!!)
                filePath = getRealPathFromURIPath(urii, requireActivity())
                binding.imgAddChild.setImageBitmap(bitmap)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getImageUri(context: Context, bitmap: Bitmap): Uri {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
        val path =
            MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path)
    }

    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path!!
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            val path = cursor.getString(idx)
            cursor.close()
            path
        }
    }

}