package com.dreams.readingmate.ui.home.addchild

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class AddChildViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {

    private val _childDetailListLiveData = MutableLiveData<ChildDetails>()
    val childDetailListLiveData: LiveData<ChildDetails>
        get() = _childDetailListLiveData

    private val _createChildLiveData = MutableLiveData<CreateChildData>()
    val createChildLiveData: LiveData<CreateChildData>
        get() = _createChildLiveData

    fun callChildDetailListAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildDetailList(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childDetailListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callCreateChildApi(
        accessToken: String,
        userId: String,
        childName: String?,
        age: String?,
        schoolId: String?,
        countryId: String?,
        colorName: String?,
        bookId: ArrayList<String>?,
        generesId: ArrayList<String>?,
        subjectId: ArrayList<String>?,
        charId: ArrayList<String>?,
        imReadingID: ArrayList<String>?,
        alreadyReadId: ArrayList<String>?,
        filePath: String?,
        from: String?,
        updateChildId: String?,
        schoolName: String?,
        charity: String?,
        mContext: FragmentActivity
    ) {
        if (from == "editProfile") {
            Coroutines.main {
                try {
                    _state.postValue(ProgressState.LOADING)
                    val result = repository.updateChild(
                        UpdateChildModel(
                            childName,
                            age,
                            colorName,
                            schoolId,
                            countryId,
                            generesId,
                            subjectId,
                            charId,
                            imReadingID,
                            schoolName,
                            charity,
                            filePath
                        ), accessToken, updateChildId!!
                    )
                    if (result is ResultState.Success) {
                        _createChildLiveData.postValue(result.value.data)
                        _state.postValue(ProgressState.SUCCESS)
                        println(result.value)
                    } else if (result is ResultState.Error) {
                        _state.postValue(ProgressState.ERROR)
                        AlertUtils.showToast(mContext, result.error.errorMessage)
                    }

                } catch (e: NoInternetException) {
                    _state.postValue(ProgressState.ERROR)
                }
            }
        } else {
            Coroutines.main {
                try {
                    _state.postValue(ProgressState.LOADING)
                    val result = repository.createChild(
                        CrateChildModel(
                            childName, age, colorName, schoolId, countryId,
                            generesId, subjectId, charId, imReadingID,
                            schoolName,
                            charity, filePath
                        ), accessToken, userId
                    )
                    if (result is ResultState.Success) {
                        _createChildLiveData.postValue(result.value.data)
                        _state.postValue(ProgressState.SUCCESS)
                        AlertUtils.showToast(mContext, result.value.message!!)
                        println(result.value)
                    } else if (result is ResultState.Error) {
                        _state.postValue(ProgressState.ERROR)
                        AlertUtils.showToast(mContext, result.error.errorMessage)
                    }

                } catch (e: NoInternetException) {
                    _state.postValue(ProgressState.ERROR)
                }
            }
        }

    }
}
