package com.dreams.readingmate.ui.home.books

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.SearchBookListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.AllTypeBooksFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AllTypeBooksFragment :
    BaseFragment<AllTypeBooksFragmentBinding>(R.layout.all_type_books_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: AllTypeBookFragmentInterface? = null
    private lateinit var binding: AllTypeBooksFragmentBinding
    private var childId = ""
    private var filter = ""
    private lateinit var searchTypeList: Array<String>
    private val viewModel: BooksViewModel by instance<BooksViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: AllTypeBooksFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgUserProfile.visibility = View.GONE
        binding.includeToolbar.toolbar_title.text = getString(R.string.search_books)
        binding.searchRecyclerView.setOnClickListener(this)
        binding.lnrBooksType.setOnClickListener(this)
        binding.rltSearchImage.setOnClickListener(this)
        setUpData()
        initSearchTypeList()
        observeLiveEvents()
        /**
         * TextChanged Listener.
         * to search books here
         */
        /*binding.edtSearch.addTextChangedListener(object : TextWatcher {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    val cartData = (activity as HomeActivity).cartItems
                    var cartitems = ""
                    if (cartData.isNotEmpty()) {
                        for (j in 0 until cartData.size) {
                            val bookIds = cartData[j].bookId
                            cartitems = if (cartitems.isNotEmpty()) {
                                "$cartitems,$bookIds"
                            } else {
                                bookIds!!
                            }
                        }
                    }
                    binding.searchRecyclerView.visibility = View.VISIBLE
                    Log.d("idsss", cartitems)
                    viewModel.callSearchBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(), "50", cartitems, filter
                    )
                } else {
                    binding.searchRecyclerView.visibility = View.GONE
                    binding.searchRecyclerView.adapter = null
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })*/
    }

    private fun initSearchTypeList() {
        binding.spnrBooksType.onItemSelectedListener = searchTypeListListener
    }

    /**
     * Listener for filter type
     */
    private val searchTypeListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val filterType = searchTypeList[position]
            if (filterType.isNotEmpty()) {
                binding.booksType.text = filterType
                if (filterType == "Book") {
                    filter = "book"
                } else {
                    filter = "author"
                }
                Log.d("filterType", filterType)
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * get value from bundle, and set on screen when open.
     */
    private fun setUpData() {
        val args = arguments
        if (args != null) {
            childId = args.getString("childId").toString()
        }
        searchTypeList = resources.getStringArray(R.array.search_type)
        binding.spnrBooksType.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, searchTypeList)
        binding.searchRecyclerView.layoutManager = GridLayoutManager(activity, 2)
    }

    /**
     * MVVM ObserverLiveEvents results
     */
    private fun observeLiveEvents() {
        viewModel.SeachListLiveData.observe(this, Observer {
            Log.d("bookList:-", it.toString())
            binding.searchRecyclerView.adapter = SearchBookListAdapter(it, mListener, childId)
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.lnrBooksType -> binding.spnrBooksType.performClick()
            R.id.rltSearchImage -> {
                val searchValue = binding.edtSearch.text.toString()
                if (searchValue > "1") {
                    val cartData = (activity as HomeActivity).cartItems
                    var cartitems = ""
                    if (cartData.isNotEmpty()) {
                        for (j in 0 until cartData.size) {
                            val bookIds = cartData[j].bookId
                            cartitems = if (cartitems.isNotEmpty()) {
                                "$cartitems,$bookIds"
                            } else {
                                bookIds!!
                            }
                        }
                    }
                    binding.searchRecyclerView.visibility = View.VISIBLE
                    Log.d("idsss", cartitems)
                    viewModel.callSearchBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        searchValue, "50", cartitems, filter
                    )
                } else {
                    binding.searchRecyclerView.visibility = View.GONE
                    binding.searchRecyclerView.adapter = null
                }
            }
        }
    }


    interface AllTypeBookFragmentInterface {
        fun openBackFragment()
        fun openBuyBookFragment()
        fun openBookDetailFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AllTypeBookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AllTypeBookFragmentInterface")
        }
    }
}