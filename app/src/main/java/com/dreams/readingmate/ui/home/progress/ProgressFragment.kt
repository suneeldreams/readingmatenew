package com.dreams.readingmate.ui.home.progress

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ProgressChildListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ProgressFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ProgressFragment : BaseFragment<ProgressFragmentBinding>(R.layout.progress_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: ProgressFragmentInterface? = null
    private lateinit var binding: ProgressFragmentBinding
    private var preferenceChild: PreferenceChildItems? = null
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: ProgressFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgUserProfile.visibility = View.GONE
        binding.includeToolbar.toolbar_title.text = getString(R.string.progress)
        setUpData()
        observeLiveEvents()
    }

    private fun setUpData() {
        binding.progressChildRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        viewModel.callPreferenceChildAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }
    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.userDetailLiveData.observe(this, Observer {
            preferenceChild = it
            viewModel.callChildListAPI(
                requireContext(),
                Utils.getCustomPref(requireContext(), UserData.TOKEN),
                Utils.getCustomPref(requireContext(), UserData.ID)
            )
        })
        viewModel.childListLiveData.observe(this, Observer {
            binding.progressChildRecyclerView.adapter = ProgressChildListAdapter(it, mListener,preferenceChild)
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface ProgressFragmentInterface {
        fun openBackFragment()
        fun openProgressFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ProgressFragmentInterface")
        }
    }

}