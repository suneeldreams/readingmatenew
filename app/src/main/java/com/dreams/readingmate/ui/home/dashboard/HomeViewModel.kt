package com.dreams.readingmate.ui.home.dashboard

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.AddWeekReadDataModel
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.responses.*

import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class HomeViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {

    private val _preferenceChildLiveData = MutableLiveData<PreferenceChildItems>()
    val userDetailLiveData: LiveData<PreferenceChildItems>
        get() = _preferenceChildLiveData

    private val _nextBookReadLiveData = MutableLiveData<List<FavBookData>>()
    val nextBookReadLiveData: LiveData<List<FavBookData>>
        get() = _nextBookReadLiveData

    private val _removeChildLiveData = MutableLiveData<SuccessResponse>()
    val removeChildLiveData: LiveData<SuccessResponse>
        get() = _removeChildLiveData

    private val _hardRatingLiveData = MutableLiveData<SuccessResponse>()
    val hardRatingLiveData: LiveData<SuccessResponse>
        get() = _hardRatingLiveData

    private val _enjoyRatingLiveData = MutableLiveData<SuccessResponse>()
    val enjoyRatingLiveData: LiveData<SuccessResponse>
        get() = _enjoyRatingLiveData

    private val _bookStatusLiveData = MutableLiveData<AddBookShelfData>()
    val bookStatusLiveData: LiveData<AddBookShelfData>
        get() = _bookStatusLiveData

    private val _childListLiveData = MutableLiveData<List<ChildListItem>>()
    val childListLiveData: LiveData<List<ChildListItem>>
        get() = _childListLiveData

    private val _updateTickDataLiveData = MutableLiveData<UpdateTickData2>()
    val updateTickDataLiveData: LiveData<UpdateTickData2>
        get() = _updateTickDataLiveData

    private val _removeTickDataLiveData = MutableLiveData<UpdateTickData2>()
    val removeTickDataLiveData: LiveData<UpdateTickData2>
        get() = _removeTickDataLiveData

    private val _membershipStatusLiveData = MutableLiveData<UserMembershipStatusData>()
    val membershipStatusLiveData: LiveData<UserMembershipStatusData>
        get() = _membershipStatusLiveData

    fun callPreferenceChildAPI(context: Context, accessToken: String) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildPreference(accessToken)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _preferenceChildLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callNextReadBookAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getNextBook(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _nextBookReadLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callChildListAPI(
        context: Context,
        accessToken: String,
        userId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildList(accessToken, userId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callRemoveChildFromListAPI(context: Context, accessToken: String, id: String?) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.removeChild(accessToken, id)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _removeChildLiveData.postValue(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addWeekTickData(
        mContext: Context,
        accessToken: String,
        bookshelfId: String?,
        pageRead: String?,
        createdAt: String,
        readingStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addWeekTickData(
                    AddWeekReadDataModel(bookshelfId, pageRead, createdAt, readingStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _updateTickDataLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun removeTick(
        mContext: Context,
        accessToken: String,
        historyId: String?,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.removeTick(
                    accessToken, historyId!!, childId
                )
                if (result is ResultState.Success) {
                    _removeTickDataLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun hardBookRatingApi(
        mContext: Context?,
        accessToken: String,
        bookId: String?,
        childId: String?,
        hardRating: String?
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.hardBookRating(
                    HardRatingRequestModel(
                        hardRating
                    ), accessToken, childId!!, bookId!!
                )
                if (result is ResultState.Success) {
                    _hardRatingLiveData.postValue(result.value)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext!!, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun enjoyBookRatingApi(
        mContext: Context?,
        accessToken: String,
        bookId: String?,
        childId: String?,
        hardRating: String?
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.enjoyBookRating(
                    EnjoyRatingRequestModel(
                        hardRating
                    ), accessToken, childId!!, bookId!!
                )
                if (result is ResultState.Success) {
                    _enjoyRatingLiveData.postValue(result.value)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext!!, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun bookStatus(
        mContext: Context,
        accessToken: String,
        bookId: String?,
        pageRead: String?,
        bookStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.bookStatusData(
                    BookStatusDataModel(bookId, pageRead, bookStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _bookStatusLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callMembershipStatusAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getMembershipStatus(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _membershipStatusLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
//                    AlertUtils.showToast(context, result.error.errorMessage)
                    _errorCode.postValue(result.error.errorCode)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }
}
