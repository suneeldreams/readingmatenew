package com.dreams.readingmate.ui.auth.viewmodel

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.UserRepository
import com.dreams.readingmate.ui.auth.AlarmActivity
import com.dreams.readingmate.ui.auth.LoginActivity
import com.dreams.readingmate.util.*
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.ui.base.BaseViewModel


class AuthViewModel(private val repository: UserRepository) : BaseViewModel() {

    var name: String? = null
    var email: String? = null
    var password: String? = null
    var oldPassword: String? = null
    var newPassword: String? = null
    var passwordconfirm: String? = null
    var charity: String? = null
    var valid_email: Boolean = false
    var countryCode: String? = "1"
    var countryName: String? = ""
    var mobile: String? = null
    var isSelected: Boolean = false


    private val _userDetailLiveData = MutableLiveData<UserInfoData>()
    val userDetailLiveData: LiveData<UserInfoData>
        get() = _userDetailLiveData

    private val _setAlarmLiveData = MutableLiveData<ChildDetails>()
    val setAlarmLiveData: LiveData<ChildDetails>
        get() = _setAlarmLiveData

    private val _childDetailListLiveData = MutableLiveData<ChildDetails>()
    val childDetailListLiveData: LiveData<ChildDetails>
        get() = _childDetailListLiveData


    private val _userSignUpLiveData = MutableLiveData<UserInfoData>()
    val userSignUpLiveData: LiveData<UserInfoData>
        get() = _userSignUpLiveData

    private val _resetPasswordLiveData = MutableLiveData<AuthInfoResponse>()
    val resetPasswordLiveData: LiveData<AuthInfoResponse>
        get() = _resetPasswordLiveData

    private val _createOrderLiveData = MutableLiveData<OrderData>()
    val createOrderLiveData: LiveData<OrderData>
        get() = _createOrderLiveData

    fun onResetButtonClick(view: View) {
        if (oldPassword.isNullOrEmpty()) {
            AlertUtils.showToast(
                view.context,
                view.resources.getString(R.string.Please_enter_your_oldPassword)
            )
            return
        }
        if (newPassword.isNullOrEmpty()) {
            AlertUtils.showToast(
                view.context,
                view.resources.getString(R.string.Please_enter_your_newPassword)
            )
            return
        }
        if (passwordconfirm.isNullOrEmpty()) {
            AlertUtils.showToast(
                view.context,
                view.resources.getString(R.string.Please_enter_your_confirmPassword)
            )
            return
        }

        if (newPassword != passwordconfirm) {
            AlertUtils.showToast(
                view.context,
                view.resources.getString(R.string.password_not_matched)
            )
            return
        }

        /*Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.resetPassword(
                    Utils.getCustomPref(view.context, UserData.TOKEN),
                    ResetPassword(oldPassword, newPassword)
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    result.value.message?.let {
                        AlertUtils.showToast(view.context, it)
                    }
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(view.context, result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(view.context, e.message ?: "")
            }
        }*/

    }

    fun callLoginApi(
        email: String,
        password: String,
        mContext: Context
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.createUser(email, password)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    result.value.data!!.token?.let {
                        Utils.saveCustomPref(
                            mContext,
                            UserData.TOKEN,
                            it
                        )
                    }
                    _userDetailLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callSignUpApi(
        email: String,
        password: String,
        name: String,
        confirmPassword: String,
        mContext: Context,
        charityName: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.registerUser(name, email, password, charityName)
                if (result is ResultState.Success) {
                    /*_userSignUpLiveData.postValue(result.value.data)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)*/

                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    result.value.data!!.token?.let {
                        Utils.saveCustomPref(
                            mContext, UserData.TOKEN,
                            it
                        )
                    }
                    _userSignUpLiveData.postValue(result.value.data)


                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callForgotPasswordApi(
        email: String,
        context: Context
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.forgotPassword(email)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    result.value.message?.let {
                        AlertUtils.showToast(context, it)
                        Intent(context, LoginActivity::class.java).also {
                            it.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            context.startActivity(it)
                        }
                    }
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(context, result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callResetPasswordApi(
        accessToken: String,
        oldPassword: String,
        newPassword: String,
        cnfmPassword: String,
        context: Context
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.resetPassword(
                        accessToken,
                        ResetPassword(oldPassword, newPassword, cnfmPassword)
                    )
//                    repository.resetPassw(accessToken, oldPassword, newPassword, cnfmPassword)
                if (result is ResultState.Success) {
                    _resetPasswordLiveData.postValue(result.value.data)
                    AlertUtils.showToast(context, result.value.message!!)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(context, result.error.errorMessage)
                }
                /*if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    result.value.message?.let {
                        AlertUtils.showToast(context, it)
                        Intent(context, LoginActivity::class.java).also {
                            it.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            context.startActivity(it)
                        }
                    }
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(context, result.error.errorMessage)
                }*/
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callSetReminderApi(
        accessToken: String,
        mContext: AlarmActivity,
        startDate: String,
        endDate: String,
        kind: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.setAlarm(
                    AlarmModel(startDate, endDate, kind), accessToken, childId
                )
                if (result is ResultState.Success) {
                    _setAlarmLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callChildDetailListAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildDetailList(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childDetailListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun createMemberShipPlan(
        mContext: Context,
        accessToken: String,
        name: String,
        email: String,
        nameOnCard: String,
        cardNumber: String,
        cardExpMonth: String,
        cardExpYear: String,
        postalCode: String,
        subscriptionPlanId: String,
        cvv: String,
        upfront: Boolean
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.createMemberShip(
                    CrateMemberShipModel(
                        name,
                        email,
                        nameOnCard,
                        cardNumber,
                        cardExpMonth,
                        cardExpYear,
                        postalCode,
                        subscriptionPlanId,
                        cvv,
                        upfront
                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _createOrderLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun createOrder(
        mContext: Context,
        accessToken: String,
        cartData: String,
        stripeToken: String,
        deliveryCharge: String,
        grandTotal: String,
        ititlename: String,
        iname: String,
        iaddr1: String,
        iaddr2: String,
        iaddr3: String,
        iaddr4: String,
        ipcode: String,
        icountry: String,
        dtitlename: String,
        dname: String,
        daddr1: String,
        daddr2: String,
        daddr3: String,
        daddr4: String,
        dpcode: String,
        dcountry: String,
        trackingsafeplace: String,
        comm1: String,
        comm2: String,
        comm3: String,
        comm4: String,
        couponCode: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.createOrder(
                    CrateOrderModel(
                        cartData,
                        stripeToken,
                        deliveryCharge,
                        grandTotal,
                        ititlename,
                        iname,
                        iaddr1,
                        iaddr2,
                        iaddr3,
                        iaddr4,
                        ipcode,
                        icountry,
                        dtitlename,
                        dname,
                        daddr1,
                        daddr2,
                        daddr3,
                        daddr4,
                        dpcode,
                        dcountry,
                        trackingsafeplace,
                        comm1,
                        comm2,
                        comm3,
                        comm4,
                        couponCode
                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _createOrderLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun setLoginData(emailLogin: String?, passwordLogin: String?) {
        email = emailLogin
        password = passwordLogin
    }
}