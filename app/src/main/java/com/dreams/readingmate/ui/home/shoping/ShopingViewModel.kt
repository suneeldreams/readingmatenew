package com.dreams.readingmate.ui.home.shoping

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class ShopingViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {

    private val _createAddressLiveData = MutableLiveData<CreateAddressData>()
    val createAddressLiveData: LiveData<CreateAddressData>
        get() = _createAddressLiveData

    private val _createCardLiveData = MutableLiveData<CreateAddressData>()
    val createCardLiveData: LiveData<CreateAddressData>
        get() = _createCardLiveData

    private val _deleteCardLiveData = MutableLiveData<CreateAddressData>()
    val deleteCardLiveData: LiveData<CreateAddressData>
        get() = _deleteCardLiveData

    private val _getAddressAndCardListLiveData = MutableLiveData<CardListData>()
    val getAddressAndCardListLiveData: LiveData<CardListData>
        get() = _getAddressAndCardListLiveData

    private val _createOrderLiveData = MutableLiveData<OrderData>()
    val createOrderLiveData: LiveData<OrderData>
        get() = _createOrderLiveData

    private val _addCartLiveData = MutableLiveData<AddCartDataItam>()
    val addCartLiveData: LiveData<AddCartDataItam>
        get() = _addCartLiveData

    private val _secretLeytLiveData = MutableLiveData<ClientSecretKeyData>()
    val secretLeytLiveData: LiveData<ClientSecretKeyData>
        get() = _secretLeytLiveData

    private val _recommendBookListLiveData = MutableLiveData<List<FavBookData>>()
    val recommendBookListLiveData: LiveData<List<FavBookData>>
        get() = _recommendBookListLiveData

    private val _validateCouponCodeLiveData = MutableLiveData<CouponCodeData>()
    val validateCouponCodeLiveData: LiveData<CouponCodeData>
        get() = _validateCouponCodeLiveData

    private val _membershipStatusLiveData = MutableLiveData<UserMembershipStatusData>()
    val membershipStatusLiveData: LiveData<UserMembershipStatusData>
        get() = _membershipStatusLiveData

    private val _childListLiveData = MutableLiveData<List<ChildListItem>>()
    val childListLiveData: LiveData<List<ChildListItem>>
        get() = _childListLiveData

    fun callCreateAddress(
        accessToken: String,
        mContext: FragmentActivity,
        titleName: String,
        fullName: String,
        mobile: String,
        townCity: String,
        company: String,
        village: String,
        add4: String,
        pinCode: String,
        countryName: String,
        isPrimary: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.createAddress(
                    CrateAddressModel(
                        titleName,
                        fullName,
                        mobile,
                        townCity,
                        company,
                        village,
                        add4,
                        pinCode,
                        countryName,
                        isPrimary
                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _createAddressLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun getUserProfile(mContext: Context, accessToken: String) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getUserProfile(accessToken)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _getAddressAndCardListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(mContext, e.message ?: "")
            }
        }
    }

    fun addCardApi(
        accessToken: String,
        mContext: FragmentActivity,
        cardNumber: String,
        month: String,
        year: String,
        cardHolderName: String,
        cvv: String,
        priority: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.AddCard(
                    CrateCardModel(
                        cardHolderName, cardNumber, month, year, cvv, priority

                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _createCardLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun deleteCard(mContext: Context, accessToken: String, cardId: String?) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.deleteCard(accessToken, cardId!!)
                if (result is ResultState.Success) {
                    _deleteCardLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun addCartData(
        mContext: Context,
        accessToken: String,
        cartData: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addCartData(
                    CartDataModel(cartData), accessToken
                )
                if (result is ResultState.Success) {
                    _addCartLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
//                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
//                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun getClientSecretKey(
        context: Context,
        accessToken: String,
        grandTotal: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.getClientSecret(accessToken, grandTotal)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
//                    AlertUtils.showToast(context, result.value.message!!)
                    _secretLeytLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callRecommendBooksAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getRecommendBookList(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _recommendBookListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callChildRecommendBooksAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildRecommendBookList(
                    accessToken,childId
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _recommendBookListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callMembershipStatusAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getMembershipStatus(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _membershipStatusLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
//                    AlertUtils.showToast(context, result.error.errorMessage)
                    _errorCode.postValue(result.error.errorCode)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callValidateCouponAPI(
        context: Context,
        accessToken: String,
        couponCode: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.ValidateCoupon(accessToken, couponCode)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _validateCouponCodeLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                    _errorCode.postValue(result.error.errorCode)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun createOrder(
        mContext: Context,
        accessToken: String,
        cartData: String,
        stripeToken: String,
        deliveryCharge: String,
        grandTotal: String,
        ititlename: String,
        iname: String,
        iaddr1: String,
        iaddr2: String,
        iaddr3: String,
        iaddr4: String,
        ipcode: String,
        icountry: String,
        dtitlename: String,
        dname: String,
        daddr1: String,
        daddr2: String,
        daddr3: String,
        daddr4: String,
        dpcode: String,
        dcountry: String,
        trackingsafeplace: String,
        comm1: String,
        comm2: String,
        comm3: String,
        comm4: String,
        couponCode: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.createOrder(
                    CrateOrderModel(
                        cartData,
                        stripeToken,
                        deliveryCharge,
                        grandTotal,
                        ititlename,
                        iname,
                        iaddr1,
                        iaddr2,
                        iaddr3,
                        iaddr4,
                        ipcode,
                        icountry,
                        dtitlename,
                        dname,
                        daddr1,
                        daddr2,
                        daddr3,
                        daddr4,
                        dpcode,
                        dcountry,
                        trackingsafeplace,
                        comm1,
                        comm2,
                        comm3,
                        comm4,
                        couponCode
                    ), accessToken
                )
                if (result is ResultState.Success) {
                    _createOrderLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun callChildListAPI(
        context: Context,
        accessToken: String,
        userId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildList(accessToken, userId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

}
