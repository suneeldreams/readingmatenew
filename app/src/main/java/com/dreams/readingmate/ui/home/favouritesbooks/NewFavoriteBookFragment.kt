package com.dreams.readingmate.ui.home.favouritesbooks

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.*
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.NewFavoriteBookFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class NewFavoriteBookFragment :
    BaseFragment<NewFavoriteBookFragmentBinding>(R.layout.new_favorite_book_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var preferenceChild: PreferenceChildItems? = null
    private var favStringList = ArrayList<String>()
    private var favBookDataList = ArrayList<Model.favBookData>()
    private var favStringIdsList = ArrayList<String>()
    var from = ""
    var prefs: SharedPreferences? = null
    private var mListener: NewFavoriteFragmentInterface? = null
    private var myFavBookAdapter: MyFavBookAdapter? = null
    private var favoriteAdapter: NewFavoriteAdapter? = null
    private lateinit var binding: NewFavoriteBookFragmentBinding
    private val viewModel: FavouriteBooksViewModel by instance<FavouriteBooksViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: NewFavoriteBookFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

        binding.edtFavoriteSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    binding.noFavoriteBooks.visibility = View.GONE
                    binding.recyclerFavorite.visibility = View.VISIBLE
                    binding.recyclerMyFavBook.visibility = View.GONE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(), "50",""
                    )
                } else {
//                    binding.noFavoriteBooks.visibility = View.VISIBLE
                    binding.recyclerFavorite.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
            if (preferenceChild!!.from != "dashboard") {
                binding.recyclerMyFavBook.visibility = View.VISIBLE
            } else {
                binding.recyclerFavorite.visibility = View.GONE
            }
        }
        val numberOfColumns = 3
        binding.recyclerFavorite.layoutManager = GridLayoutManager(context, numberOfColumns)
        binding.recyclerMyFavBook.layoutManager = GridLayoutManager(context, numberOfColumns)

        if (preferenceChild!!.from == "editProfile") {
            if (preferenceChild!!.readBooks!!.isNotEmpty()) {
                binding.noFavoriteBooks.visibility = View.GONE
                for (i in 0 until preferenceChild!!.favBookData!!.size) {
                    val id = preferenceChild!!.favBookData!![i]!!.id
                    val img = preferenceChild!!.favBookData!![i]!!.img
                    val title = preferenceChild!!.favBookData!![i]!!.title
                    favStringIdsList.add(id!!)
                    val favBook: Model.favBookData = Model.favBookData(id, img!!, title!!)
                    favBookDataList.add(favBook)
                }
            } else {
                binding.noFavoriteBooks.visibility = View.VISIBLE
            }
        }
        preferenceChild!!.bookId = favStringIdsList
        callFavoriteBookAdapter(favBookDataList)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener {}
        binding.includeToolbar.mTitle.text = getString(R.string.done)
    }

    private fun observeLiveEvents() {
        viewModel.FavBookListLiveData.observe(this, Observer {
            Log.d("readingBooks:-", it.toString())
            binding.recyclerFavorite.visibility = View.VISIBLE
            favoriteAdapter = NewFavoriteAdapter(it, mListener!!, this)
            binding.recyclerFavorite.adapter = favoriteAdapter
        })
        viewModel.createChildLiveData.observe(this, Observer {
            Log.d("childVal:-", it.toString())
            prefs!![mUserData.CLICKEDID] = "home"
            Intent(requireContext(), HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                Utils.hideKeyBoard(requireActivity())
                viewModel.callCreateChildApi(
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    Utils.getCustomPref(requireContext(), UserData.ID),
                    preferenceChild!!.childName,
                    preferenceChild!!.nickName,
                    preferenceChild!!.age,
                    preferenceChild!!.schoolId,
                    preferenceChild!!.countryId,
                    preferenceChild!!.colorName,
                    preferenceChild!!.generesId,
                    preferenceChild!!.subjectId,
                    preferenceChild!!.charId,
                    preferenceChild!!.currentlyReadingbook,
                    preferenceChild!!.image,
                    preferenceChild!!.from,
                    preferenceChild!!.updateChildId,
                    preferenceChild!!.schoolName,
                    preferenceChild!!.charity,
                    requireActivity()
                )
                /*if (favStringIdsList.isNotEmpty()) {
                    val bundle = Bundle()
                    bundle.putParcelable("child", preferenceChild)
                    mListener?.openNewFragmentFragment(bundle)
                } else {
                    AlertUtils.showToast(
                        requireContext(),
                        "Please search books to add as your favourite!"
                    )
                }*/
            }
        }
    }


    interface NewFavoriteFragmentInterface {
        fun openBackFragment()
        fun openNewFragmentFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NewFavoriteFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement NewFavoriteFragmentInterface")
        }
    }

    fun AddImageToStringArray(img: String?, id: String?, title: String?) {
        if (id != null) {
            favStringIdsList.add(id)
        }
        preferenceChild!!.bookId = favStringIdsList
        Log.d("array", favStringIdsList.toString())
        favoriteAdapter!!.notifyDataSetChanged()
        val favBook: Model.favBookData = Model.favBookData(id!!, img!!, title!!)
        favBookDataList.add(favBook)
        if (favBookDataList.isNotEmpty()) {
            binding.recyclerMyFavBook.visibility = View.VISIBLE
            binding.recyclerFavorite.visibility = View.GONE
            binding.noFavoriteBooks.visibility = View.GONE
            callFavoriteBookAdapter(favBookDataList)
        }
        binding.edtFavoriteSearch.setText("")
    }

    private fun callFavoriteBookAdapter(favBookDataList: java.util.ArrayList<Model.favBookData>) {
        myFavBookAdapter = MyFavBookAdapter(favBookDataList, mListener!!, this)
        binding.recyclerMyFavBook.adapter = myFavBookAdapter
    }

    fun refreshAdapter(position: Int, id: String) {
        if (favBookDataList[position].id == id) {
            favBookDataList.removeAt(position)

        }
        if (favStringIdsList[position] == id) {
            favStringIdsList.removeAt(position)
        }
        preferenceChild!!.bookId = favStringIdsList
        callFavoriteBookAdapter(favBookDataList)
        myFavBookAdapter!!.notifyDataSetChanged()
    }
}