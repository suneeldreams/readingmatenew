package com.dreams.readingmate.ui.auth

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.dreams.readingmate.R
import com.dreams.readingmate.util.Coroutines
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.terms_condition_fragment.*

/**
 * Created by Suneel on 30/03/2020.
 */
class TermsConditionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.terms_condition_fragment)
        toolbar_title.text = getString(R.string.terms_amp_serv)
        imgBack.visibility = View.VISIBLE
        imgBack.setOnClickListener {
            finish()
        }
        callWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun callWebView() = Coroutines.main {
        progressBarTerms.visibility = View.VISIBLE
        terms_conditions.webViewClient = WebViewController()
        terms_conditions.settings.javaScriptEnabled = true
        terms_conditions.loadUrl("https://readingmate.co.uk/")
    }

    private inner class WebViewController : WebViewClient() {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            progressBarTerms.visibility = View.VISIBLE
            view.loadUrl(request.url.toString())
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBarTerms.visibility = View.GONE
        }
    }
}