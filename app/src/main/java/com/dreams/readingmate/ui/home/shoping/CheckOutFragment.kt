package com.dreams.readingmate.ui.home.shoping

import android.app.Dialog
import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.AddressListAdapter
import com.dreams.readingmate.data.adapter.CardListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.AddressDataItem
import com.dreams.readingmate.data.network.responses.CardDataItem
import com.dreams.readingmate.databinding.CheckoutFragmentBinding
import com.dreams.readingmate.ui.auth.CheckoutActivity
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class CheckOutFragment : BaseFragment<CheckoutFragmentBinding>(R.layout.checkout_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: CheckOutFragmentInterface? = null
    private lateinit var binding: CheckoutFragmentBinding
    private var cardListAdapter: CardListAdapter? = null
    private var addressList: List<AddressDataItem?>? = null
    private var cardList: List<CardDataItem?>? = null
    private var addressListAdapter: AddressListAdapter? = null
    var prefs: SharedPreferences? = null
    private var from = ""
    private var planId = ""
    private var isUpfront = false
    private var isCardSelected = false
    private var isAddressSelected = false
    private var planAmount = 0.0
    private var cardPaymentData: Model.cardItem? = null
    private lateinit var memberShipPaymentData: Model.membershipItem

    private val viewModel: ShopingViewModel by instance<ShopingViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: CheckoutFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.checkout)
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.txtAddLocation.setOnClickListener(this)
        binding.txtAddCard.setOnClickListener(this)
        binding.btnPayment.setOnClickListener(this)
        setUpData()
        observeLiveEvents()
        callGetAddressAndCardList()
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.ADD_LOCATION))
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.ADD_CARD))
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        binding.recyclerAddNewLocation.layoutManager = LinearLayoutManager(context)
        binding.recyclerCardList.layoutManager = LinearLayoutManager(context)
        val args = arguments
        if (args != null) {
            from = args.getString("from").toString()
            planId = args.getString("planId").toString()
            planAmount = args.getDouble("planAmount")
            isUpfront = args.getBoolean("isUpfront")
            if (from == "membership") {
                binding.rltAddress.visibility = View.GONE
                binding.txtDelivery.visibility = View.GONE
                binding.txtAddLocation.visibility = View.GONE
            } else {
                binding.rltAddress.visibility = View.VISIBLE
                binding.txtDelivery.visibility = View.VISIBLE
                binding.txtAddLocation.visibility = View.VISIBLE
            }
        }
        val freeBookTotal = (activity as HomeActivity).freeBookGrandTotal
        if (freeBookTotal == "0.0") {
            binding.txtAddCard.visibility = View.GONE
            binding.txtPaymentMethod.visibility = View.GONE
            binding.rltCardList.visibility = View.GONE
        } else {
            binding.txtAddCard.visibility = View.VISIBLE
            binding.txtPaymentMethod.visibility = View.VISIBLE
            binding.rltCardList.visibility = View.VISIBLE
        }
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.getAddressAndCardListLiveData.observe(this, Observer {
            this.addressList = it.addressData
            this.cardList = it.cardData
            if (addressList!!.isNotEmpty()) {
                binding.txtNoAddress.visibility = View.GONE
                binding.recyclerAddNewLocation.visibility = View.VISIBLE
                addressListAdapter = AddressListAdapter(addressList, mListener, this)
                binding.recyclerAddNewLocation.adapter = addressListAdapter
            } else {
                binding.recyclerAddNewLocation.visibility = View.INVISIBLE
                binding.txtNoAddress.visibility = View.VISIBLE
            }
            if (cardList!!.isNotEmpty()) {
                binding.recyclerCardList.visibility = View.VISIBLE
                binding.txtNoCard.visibility = View.GONE
                cardListAdapter = CardListAdapter(cardList, mListener, this)
                binding.recyclerCardList.adapter = cardListAdapter
            } else {
                binding.recyclerCardList.visibility = View.INVISIBLE
                binding.txtNoCard.visibility = View.VISIBLE
            }
        })
        viewModel.createOrderLiveData.observe(this, Observer {
            openOrderDialog(it.orderNumber)
        })
        viewModel.deleteCardLiveData.observe(this, Observer {
            callGetAddressAndCardList()
        })

        viewModel.secretLeytLiveData.observe(this, Observer {
            val secret = it.clientSecret
            Log.d("ClientSecret", secret)
            if (from == "membership") {
                callMembershipApi(secret)
                val intent = Intent(requireContext(), CheckoutActivity::class.java)
                intent.putExtra("memberShipModel", memberShipPaymentData)
                intent.putExtra("from", "membership")
                startActivity(intent)
            } else {
                callPaymentApi(secret)
                val intent = Intent(requireContext(), CheckoutActivity::class.java)
                intent.putExtra("cardModel", cardPaymentData)
                startActivity(intent)
            }

        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    private fun callMembershipApi(clientSecret: String?) {
        if (cardList != null) {
            var name = "Suneel"
            var email = "dsmail.suneel@gmail.com"
            var postalCode = "123456"
            var cardNo = ""
            var expMonth = ""
            var expYear = ""
            var nameOnCard = ""
            var cvv = ""
            var from = "membership"
            var flag = false
            for (i in cardList!!.indices) {
                if (cardList!![i]!!.isSelected) {
                    cardNo = cardList!![i]!!.cardNumber!!
                    expMonth = cardList!![i]!!.expMonth!!
                    expYear = cardList!![i]!!.expYear!!
                    nameOnCard = cardList!![i]!!.nameOnCard!!
                    cvv = "123"
                    flag = true
                    break
                }
            }
            if (flag) {
                memberShipPaymentData = Model.membershipItem(
                    name,
                    email,
                    nameOnCard,
                    cardNo,
                    cvv,
                    expMonth,
                    expYear,
                    postalCode,
                    planId,
                    clientSecret!!,
                    from,
                    isUpfront
                )
            }
        }
    }

    /**
     * call Payment Api.
     */
    private fun callPaymentApi(clientSecret: String?) {
        if (cardList != null) {
            var cardNo = ""
            var expMonth = ""
            var expYear = ""
            var cvv = ""
            var flag = false
            for (i in 0 until cardList!!.size) {
                if (cardList!![i]!!.isSelected) {
                    cardNo = cardList!![i]!!.cardNumber!!
                    expMonth = cardList!![i]!!.expMonth!!
                    expYear = cardList!![i]!!.expYear!!
                    cvv = "123"
                    flag = true
                    break
                }
            }
            var ititlename = ""
            var iname = ""
            var iaddr1 = ""
            var iaddr2 = ""
            var iaddr3 = ""
            var iaddr4 = ""
            var ipcode = ""
            var icountry = ""

            var dtitlename = ""
            var dname = ""
            var daddr1 = ""
            var daddr2 = ""
            var daddr3 = ""
            var daddr4 = ""
            var dpcode = ""
            var dcountry = ""
            var trackingsafeplace = ""
            var comm1 = ""
            var comm2 = ""
            var comm3 = ""
            var comm4 = ""
            for (i in 0 until addressList!!.size) {
                if (addressList!![i]!!.isSelected) {
                    ititlename = "Mr"
                    iname = addressList!![i]!!.name!!
                    iaddr1 = addressList!![i]!!.addr1!!
                    iaddr2 = addressList!![i]!!.addr2!!
                    iaddr3 = addressList!![i]!!.addr3!!
                    iaddr4 = addressList!![i]!!.addr4!!
                    ipcode = "E15 1EH"
                    icountry = addressList!![i]!!.country!!

                    dtitlename = "Mr"
                    dname = addressList!![i]!!.name!!
                    daddr1 = addressList!![i]!!.addr1!!
                    daddr2 = addressList!![i]!!.addr2!!
                    daddr3 = addressList!![i]!!.addr3!!
                    daddr4 = addressList!![i]!!.addr4!!
                    dpcode = "E15 1EH"
                    dcountry = addressList!![i]!!.country!!

                    trackingsafeplace = "With neighbour"
                    comm1 = ""
                    comm2 = ""
                    comm3 = ""
                    comm4 = ""
                }
            }
            if (flag) {
                val couponCode = (activity as HomeActivity).couponCodeUsed

                val weight = (activity as HomeActivity).calculateWeight()
                val deliveryCharge = getWeightPrice(weight)
                val grandTotal =
                    (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)

                val serviceCharge =
                    (activity as HomeActivity).calculateShippingCharge().toString()
                val cardData = getJsonOrderList()
                cardPaymentData = Model.cardItem(
                    deliveryCharge.toString(),
                    serviceCharge,
                    grandTotal.toString(),
                    ititlename,
                    iname,
                    iaddr1,
                    iaddr2,
                    iaddr3,
                    iaddr4,
                    ipcode,
                    icountry,
                    dtitlename,
                    dname,
                    daddr1,
                    daddr2,
                    daddr3,
                    daddr4,
                    dpcode,
                    dcountry,
                    trackingsafeplace,
                    comm1,
                    comm2,
                    comm3,
                    comm4,
                    clientSecret!!,
                    cardNo,
                    expMonth,
                    expYear,
                    cvv,
                    cardData,
                    couponCode
                )
                /*getStripeToken(
                    deliveryCharge.toString(),
                    serviceCharge,
                    grandTotal.toString(),
                    ititlename,
                    iname,
                    iaddr1,
                    iaddr2,
                    iaddr3,
                    iaddr4,
                    ipcode,
                    icountry,
                    dtitlename,
                    dname,
                    daddr1,
                    daddr2,
                    daddr3,
                    daddr4,
                    dpcode,
                    dcountry,
                    trackingsafeplace,
                    comm1,
                    comm2,
                    comm3,
                    comm4,
                    clientSecret!!
                )*/
            }
        } else {
            /*var cardNo = ""
            var expMonth = ""
            var expYear = ""
            var cvv = ""
            var flag = false
            var ititlename = ""
            var iname = ""
            var iaddr1 = ""
            var iaddr2 = ""
            var iaddr3 = ""
            var iaddr4 = ""
            var ipcode = ""
            var icountry = ""

            var dtitlename = ""
            var dname = ""
            var daddr1 = ""
            var daddr2 = ""
            var daddr3 = ""
            var daddr4 = ""
            var dpcode = ""
            var dcountry = ""
            var trackingsafeplace = ""
            var comm1 = ""
            var comm2 = ""
            var comm3 = ""
            var comm4 = ""
            for (i in 0 until addressList!!.size) {
                if (addressList!![i]!!.isSelected) {
                    ititlename = "Mr"
                    iname = addressList!![i]!!.name!!
                    iaddr1 = addressList!![i]!!.addr1!!
                    iaddr2 = addressList!![i]!!.addr2!!
                    iaddr3 = addressList!![i]!!.addr3!!
                    iaddr4 = addressList!![i]!!.addr4!!
                    ipcode = "E15 1EH"
                    icountry = addressList!![i]!!.country!!

                    dtitlename = "Mr"
                    dname = addressList!![i]!!.name!!
                    daddr1 = addressList!![i]!!.addr1!!
                    daddr2 = addressList!![i]!!.addr2!!
                    daddr3 = addressList!![i]!!.addr3!!
                    daddr4 = addressList!![i]!!.addr4!!
                    dpcode = "E15 1EH"
                    dcountry = addressList!![i]!!.country!!

                    trackingsafeplace = "With neighbour"
                    comm1 = ""
                    comm2 = ""
                    comm3 = ""
                    comm4 = ""
                }
            }
            *//*for sending data on api*//*
            val couponCode = (activity as HomeActivity).couponCodeUsed
            val weight = (activity as HomeActivity).calculateWeight()
            val deliveryCharge = getWeightPrice(weight)
            val grandTotal =
                (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)

            val serviceCharge =
                (activity as HomeActivity).calculateShippingCharge().toString()
            val cardData = getJsonOrderList()
            cardPaymentData = Model.cardItem(
                "",
                "",
                "",
                ititlename,
                iname,
                iaddr1,
                iaddr2,
                iaddr3,
                iaddr4,
                ipcode,
                icountry,
                dtitlename,
                dname,
                daddr1,
                daddr2,
                daddr3,
                daddr4,
                dpcode,
                dcountry,
                trackingsafeplace,
                comm1,
                comm2,
                comm3,
                comm4,
                "",
                cardNo,
                expMonth,
                expYear,
                cvv,
                cardData,
                ""
            )*/
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.txtAddLocation -> mListener?.openAddLocationFragment()
            R.id.txtAddCard -> {
                mListener?.openAddCardFragment()
            }
            R.id.btnPayment -> {
                val freeBookTotal = (activity as HomeActivity).freeBookGrandTotal
                if (freeBookTotal == "0.0") {
                    if (!isAddressSelected) {
                        AlertUtils.showToast(requireContext(), "Please select your address.")
                    } else {
                        if (from == "membership") {
                            getClientSecretKey(planAmount)
                        } else {
                            val memberShip =
                                Utils.getCustomPref(requireContext(), UserData.IS_MEMBERSHIP)
                            if (memberShip == "1") {
                                val remainingBooks =
                                    Utils.getCustomPref(requireContext(), UserData.REMAINING_BOOKS)
                                if (remainingBooks != "0") {
                                    val grandFreeTotal =
                                        (activity as HomeActivity).getFreeCartTotal(remainingBooks.toInt())
                                    if (freeBookTotal == "0.0") {
                                        callFreeBookPaymentAPI()
                                    } else {
                                        val weightRemainingBooks =
                                            (activity as HomeActivity).calculateWeight()
                                        val remainingBookDeliveryCharge =
                                            getWeightPrice(weightRemainingBooks)
                                        val finalGrandTotal =
                                            (activity as HomeActivity).getRemainingBookGrandTotal(
                                                remainingBookDeliveryCharge,
                                                grandFreeTotal
                                            )
                                        getClientSecretKey(finalGrandTotal)
                                        Log.d("singleBookTotal", grandFreeTotal.toString())
                                        Log.d("Wait", weightRemainingBooks.toString())
                                        Log.d("deliverC", remainingBookDeliveryCharge.toString())
                                        Log.d(
                                            "ListSize",
                                            (activity as HomeActivity).cartItems.size.toString()
                                        )
                                        Log.d("GrandTotal", finalGrandTotal.toString())
                                    }


                                } else {
                                    val weight = (activity as HomeActivity).calculateWeight()
                                    val deliveryCharge = getWeightPrice(weight)
                                    val grandTotal =
                                        (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)
                                    getClientSecretKey(grandTotal)
                                }
                            } else {
                                val weight = (activity as HomeActivity).calculateWeight()
                                val deliveryCharge = getWeightPrice(weight)
                                val grandTotal =
                                    (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)
                                getClientSecretKey(grandTotal)
                            }

                        }
                    }
                } else {
                    if (!isAddressSelected) {
                        AlertUtils.showToast(requireContext(), "Please select your address.")
                    } else if (!isCardSelected) {
                        AlertUtils.showToast(requireContext(), "Please select your card.")
                    } else {
                        if (from == "membership") {
                            getClientSecretKey(planAmount)
                        } else {
                            val memberShip =
                                Utils.getCustomPref(requireContext(), UserData.IS_MEMBERSHIP)
                            if (memberShip == "1") {
                                val remainingBooks =
                                    Utils.getCustomPref(requireContext(), UserData.REMAINING_BOOKS)
                                if (remainingBooks != "0") {
                                    val grandFreeTotal =
                                        (activity as HomeActivity).getFreeCartTotal(remainingBooks.toInt())
                                    val freeBookTotals =
                                        (activity as HomeActivity).freeBookGrandTotal
                                    if (freeBookTotals == "0.0") {
                                        callFreeBookPaymentAPI()
                                    } else {
                                        val weightRemainingBooks =
                                            (activity as HomeActivity).calculateWeight()
                                        val remainingBookDeliveryCharge =
                                            getWeightPrice(weightRemainingBooks)
                                        val finalGrandTotal =
                                            (activity as HomeActivity).getRemainingBookGrandTotal(
                                                remainingBookDeliveryCharge,
                                                grandFreeTotal
                                            )
                                        getClientSecretKey(finalGrandTotal)
                                        Log.d("singleBookTotal", grandFreeTotal.toString())
                                        Log.d("Wait", weightRemainingBooks.toString())
                                        Log.d("deliverC", remainingBookDeliveryCharge.toString())
                                        Log.d(
                                            "ListSize",
                                            (activity as HomeActivity).cartItems.size.toString()
                                        )
                                        Log.d("GrandTotal", finalGrandTotal.toString())
                                    }


                                } else {
                                    val weight = (activity as HomeActivity).calculateWeight()
                                    val deliveryCharge = getWeightPrice(weight)
                                    val grandTotal =
                                        (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)
                                    getClientSecretKey(grandTotal)
                                }
                            } else {
                                val weight = (activity as HomeActivity).calculateWeight()
                                val deliveryCharge = getWeightPrice(weight)
                                val grandTotal =
                                    (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)
                                getClientSecretKey(grandTotal)
                            }

                        }
                    }
                }
            }
        }
    }

    private fun callFreeBookPaymentAPI() {
        var cardNo = ""
        var expMonth = ""
        var expYear = ""
        var cvv = ""
        var flag = false
        var ititlename = ""
        var iname = ""
        var iaddr1 = ""
        var iaddr2 = ""
        var iaddr3 = ""
        var iaddr4 = ""
        var ipcode = ""
        var icountry = ""

        var dtitlename = ""
        var dname = ""
        var daddr1 = ""
        var daddr2 = ""
        var daddr3 = ""
        var daddr4 = ""
        var dpcode = ""
        var dcountry = ""
        var trackingsafeplace = ""
        var comm1 = ""
        var comm2 = ""
        var comm3 = ""
        var comm4 = ""
        for (i in 0 until addressList!!.size) {
            if (addressList!![i]!!.isSelected) {
                ititlename = "Mr"
                iname = addressList!![i]!!.name!!
                iaddr1 = addressList!![i]!!.addr1!!
                iaddr2 = addressList!![i]!!.addr2!!
                iaddr3 = addressList!![i]!!.addr3!!
                iaddr4 = addressList!![i]!!.addr4!!
                ipcode = "E15 1EH"
                icountry = addressList!![i]!!.country!!

                dtitlename = "Mr"
                dname = addressList!![i]!!.name!!
                daddr1 = addressList!![i]!!.addr1!!
                daddr2 = addressList!![i]!!.addr2!!
                daddr3 = addressList!![i]!!.addr3!!
                daddr4 = addressList!![i]!!.addr4!!
                dpcode = "E15 1EH"
                dcountry = addressList!![i]!!.country!!

                trackingsafeplace = "With neighbour"
                comm1 = ""
                comm2 = ""
                comm3 = ""
                comm4 = ""
            }
        }
        /*for sending data on api*/
        val couponCode = (activity as HomeActivity).couponCodeUsed
        val weight = (activity as HomeActivity).calculateWeight()
        val deliveryCharge = getWeightPrice(weight)
        val grandTotal =
            (activity as HomeActivity).getGrandTotalPrice(deliveryCharge)

        val serviceCharge =
            (activity as HomeActivity).calculateShippingCharge().toString()
        val cardData = getJsonOrderList()
        cardPaymentData = Model.cardItem(
            "",
            "",
            "",
            ititlename,
            iname,
            iaddr1,
            iaddr2,
            iaddr3,
            iaddr4,
            ipcode,
            icountry,
            dtitlename,
            dname,
            daddr1,
            daddr2,
            daddr3,
            daddr4,
            dpcode,
            dcountry,
            trackingsafeplace,
            comm1,
            comm2,
            comm3,
            comm4,
            "",
            cardNo,
            expMonth,
            expYear,
            cvv,
            cardData,
            ""
        )
        callOrderApi(
            "0",
            "0",
            ititlename,
            iname,
            iaddr1,
            iaddr2,
            iaddr3,
            iaddr4,
            ipcode,
            icountry,
            dtitlename,
            dname,
            daddr1,
            daddr2,
            daddr3,
            daddr4,
            dpcode,
            dcountry,
            trackingsafeplace,
            comm1,
            comm2,
            comm3,
            comm4,
            "",
            "",
            cardData,
            ""
        )
    }

    private fun callOrderApi(
        deliveryCharge: String,
        grandTotal: String,
        ititlename: String,
        iname: String,
        iaddr1: String,
        iaddr2: String,
        iaddr3: String,
        iaddr4: String,
        ipcode: String,
        icountry: String,
        dtitlename: String,
        dname: String,
        daddr1: String,
        daddr2: String,
        daddr3: String,
        daddr4: String,
        dpcode: String,
        dcountry: String,
        trackingsafeplace: String,
        comm1: String,
        comm2: String,
        comm3: String,
        comm4: String,
        clientSecretKey: String,
        tokenId: String?,
        cardData: String,
        couponCode: String
    ) {
        viewModel.createOrder(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            cardData,
            tokenId!!,
            deliveryCharge,
            grandTotal,
            ititlename,
            iname,
            iaddr1,
            iaddr2,
            iaddr3,
            iaddr4,
            ipcode,
            icountry,
            dtitlename,
            dname,
            daddr1,
            daddr2,
            daddr3,
            daddr4,
            dpcode,
            dcountry,
            trackingsafeplace,
            comm1,
            comm2,
            comm3,
            comm4,
            couponCode
        )
    }

    /**
     * api call for to get ClientSecretKey.
     */
    private fun getClientSecretKey(grandTotal: Double) {
        viewModel.getClientSecretKey(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            grandTotal.toString()
        )
    }

    interface CheckOutFragmentInterface {
        fun openBackFragment()
        fun openAddCardFragment()
        fun openAddLocationFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CheckOutFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckOutFragmentInterface")
        }
    }

    /**
     * BroadcastReceiver for refreshing data
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.ADD_LOCATION -> {
                    callGetAddressAndCardList()
                }
                AppConstants.ADD_CARD -> {
                    callGetAddressAndCardList()
                }
            }
        }
    }

    private fun callGetAddressAndCardList() {
        viewModel.getUserProfile(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

    fun refreshAddress(position: Int) {
        for (i in addressList!!.indices) {
            addressList!![i]!!.isSelected = position == i
            if (addressList!![i]!!.isSelected) {
                isAddressSelected = true
            } else {
                isAddressSelected = false
            }
        }
        addressListAdapter!!.notifyDataSetChanged()
    }

    fun refreshCardAdapter(position: Int, cvv: String) {
        for (i in cardList!!.indices) {
            cardList!![i]!!.isSelected = position == i
            cardList!![i]!!.CVV = cvv
            if (cardList!![i]!!.isSelected) {
                isCardSelected = true
            } else {
                isCardSelected = false
            }
        }
        cardListAdapter!!.notifyDataSetChanged()
    }

    /**
     * set cart data in JsonArray.
     */
    private fun getJsonOrderList(): String {
        val jsonArray = JSONArray()
        for (i in 0 until (activity as HomeActivity).cartItems.size) {
            val cartItem = (activity as HomeActivity).cartItems[i]
            val jsonObject = JSONObject()
            jsonObject.put("bookId", cartItem.bookId)
            jsonObject.put("quantity", cartItem.quantity)

            jsonArray.put(jsonObject)
        }
        return jsonArray.toString()
    }

    fun getWeightPrice(weight: Int): Double {
        var weightPrice = 0.0
        if (weight in 1..99) {
            weightPrice = 0.78
        } else if (weight in 102..249) {
            weightPrice = 1.09
        } else if (weight in 252..499) {
            weightPrice = 1.23
        } else if (weight in 502..749) {
            weightPrice = 1.64
        }
        return weightPrice
    }

    private fun openOrderDialog(orderNumber: Long?) {
        val dialog = context?.let { Dialog(it) }
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCancelable(true)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.order_confirmation_dialog)
        val btnContinue = dialog.findViewById(R.id.btnContinueShopping) as Button
        val txtGoTOOrder = dialog.findViewById(R.id.txtGoTOOrder) as TextView
        val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
        txtMessage.text =
            resources.getString(R.string.Your_order_confirmation_number_is) + " " + orderNumber + "."
        btnContinue.setOnClickListener {
            dialog.dismiss()
            prefs!![mUserData.CLICKEDID] = "home"
            (activity as HomeActivity).freeBookGrandTotal = "0.0"
            requireContext().startActivity(
                Intent(
                    context,
                    HomeActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
        txtGoTOOrder.setOnClickListener {
            dialog.dismiss()
            prefs!![mUserData.CLICKEDID] = "home"
            (activity as HomeActivity).freeBookGrandTotal = "0.0"
            requireContext().startActivity(
                Intent(
                    context,
                    HomeActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
        dialog.show()
    }

    /**
     * refresh card list.
     */
    fun refreshCardList(cardId: String?) {
        viewModel.deleteCard(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            cardId
        )
    }

}