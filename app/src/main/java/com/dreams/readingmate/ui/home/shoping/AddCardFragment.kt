package com.dreams.readingmate.ui.home.shoping

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.AddCardsFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.*
import com.dreams.readingmate.util.CreditCardUtils.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AddCardFragment : BaseFragment<AddCardsFragmentBinding>(R.layout.add_cards_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var cardType: Int = 0
    private var mListener: AddCardFragmentInterface? = null
    private lateinit var binding: AddCardsFragmentBinding
    private val viewModel: ShopingViewModel by instance<ShopingViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: AddCardsFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.cardToolBar.toolbar_title.text = getString(R.string.add_card)
        binding.cardToolBar.imgBack.visibility = View.VISIBLE
        binding.cardToolBar.imgBack.setOnClickListener(this)
        binding.btnAddCard.setOnClickListener(this)
        observeLiveEvents()
        binding.editTextCardHolder.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                if (TextUtils.isEmpty(editable.toString().trim { it <= ' ' }))
                    binding.cardHolderNameOnCard.setText(R.string.enter_valid_name)
                else
                    binding.cardHolderNameOnCard.text = editable.toString()
            }
        })
        binding.editTextCardNumber.addTextChangedListener(
            CreditCardFormattingTextWatcher(
                binding.editTextCardNumber,
                binding.numberOnCard,
                getCardType(),
                CreditCardFormattingTextWatcher.CreditCardType { type ->
                    cardType = type
                    cardTypeSet(type)
                })
        )
        binding.editTextExpireDate.addTextChangedListener(
            CreditCardExpiryTextWatcher(
                binding.editTextExpireDate,
                binding.validityOnCard
            )
        )
        binding.editTextExpireDate.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                binding.validityOnCard.text = editable.toString()
            }
        })
    }

    private fun getCardType(): ImageView {
        return binding.ivType
    }

    /**
     * set Card type
     */
    private fun cardTypeSet(type: Int) {
        when (type) {
            VISA -> binding.ivType.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_visa
                )
            )
            MASTERCARD -> binding.ivType.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_mastercard
                )
            )
            AMEX -> binding.ivType.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_discover_card
                )
            )
            DISCOVER -> binding.ivType.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_discover_card
                )
            )
            NONE -> binding.ivType.setImageResource(android.R.color.transparent)
        }
    }
    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.createAddressLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.ADD_LOCATION
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            mListener?.openBackFragment()
        })
        viewModel.createCardLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.ADD_CARD
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            mListener?.openBackFragment()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.btnAddCard -> addCardValidation(requireContext())
        }
    }

    /**
     * Payment Card Validation.
     */
    private fun addCardValidation(context: Context) {
        val immm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        immm.hideSoftInputFromWindow(requireView().windowToken, 0)
        if (TextUtils.isEmpty(binding.editTextCardHolder.text.toString().trim { it <= ' ' })) run {
            AlertUtils.showToast(context, resources.getString(R.string.card_holder_name))
        } else if (TextUtils.isEmpty(
                binding.editTextCardNumber.text.toString()
                    .trim { it <= ' ' }) || !CreditCardUtils.isValid(
                binding.editTextCardNumber.text.toString().trim { it <= ' ' }.replace(" ", "")
            )
        ) run {
            AlertUtils.showToast(context, resources.getString(R.string.card_holder_name))
        } else if (TextUtils.isEmpty(
                binding.editTextExpireDate.text.toString()
                    .trim { it <= ' ' }) || !CreditCardUtils.isValidDate(
                binding.editTextExpireDate.text.toString().trim { it <= ' ' })
        ) run {
            AlertUtils.showToast(context, resources.getString(R.string.exp_date_month))
        } else if (TextUtils.isEmpty(binding.editTextCVV.text.toString().trim { it <= ' ' })) run {
            AlertUtils.showToast(context, resources.getString(R.string.valid_cvv))
        } else run {
            val cardHolderName = binding.editTextCardHolder.text.toString().trim { it <= ' ' }
//            val cardNumber = binding.editTextCardNumber.text.toString().trim { it <= ' ' }
            val cardNumber =
                binding.editTextCardNumber.text.toString().trim { it <= ' ' }.replace(" ", "")
            val expireDate = binding.editTextExpireDate.text.toString().trim { it <= ' ' }
            val cvv = binding.editTextCVV.text.toString().trim { it <= ' ' }
            val monthYear =
                expireDate.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val month = monthYear[0]
            val year = monthYear[1]
//            val numberOnCard = cardNumber.trim()
            addCard(
                cardNumber,
                month,
                year,
                cardHolderName,
                "E15 1EH",
                "1"
            )

        }
    }

    private fun addCard(
        cardNumber: String,
        month: String,
        year: String,
        cardHolderName: String,
        cvv: String,
        isPriority: String
    ) {
        viewModel.addCardApi(
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            requireActivity(),
            cardNumber,
            month,
            year,
            cardHolderName,
            cvv,
            isPriority
        )
    }

    interface AddCardFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AddCardFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AddCardFragmentInterface")
        }
    }

}