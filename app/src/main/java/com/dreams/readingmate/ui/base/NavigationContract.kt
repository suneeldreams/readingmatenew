package com.jsw.mobility.presentation.navigation

/**
 * This class represents the contract for navigation pattern
 * Author: Shivank Trivedi
 * Dated: 20-05-2020
 */

interface NavigationContract {
    fun observeLiveEvents() {
        //Nothing goes here
    }

    fun observeNavigationEvents() {
        //Nothing goes here
    }
}
