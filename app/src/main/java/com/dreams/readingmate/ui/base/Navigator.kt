package com.dreams.readingmate.ui.base

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * This class keep all the navigation methods here
 * Author: Shivank Trivedi
 * Dated: 20-05-2020
 */

object Navigator {


    fun replaceFragment(fragment: Fragment, context: Context, id: Int, addToBackStack: Boolean = false, bundle: Bundle? = null) {
        navigate(context, frag = fragment, args = bundle, id = id, addToBackStack = addToBackStack)
    }


    /**
     * The method for adding a new fragment
     * @param context : Activity's context
     * @param frag: Fragment to be replaced
     * @param args: Bundle to be passed
     * @param id: Fragment container ID
     * @param addToBackStack: Flag indicating whether to add to backstack or not
     */
    private fun navigate(
        context: Context, args: Bundle? = null,
        frag: Fragment, id: Int, addToBackStack: Boolean = false
    ) {
        args?.let {
            frag.arguments = args
        }
        TransitionManager.replaceFragment(
            context as AppCompatActivity, frag,
            id, addToBackStack, TransitionManager.TransitionAnimation.TRANSITION_NONE
        )
    }

    fun <T> navigateToActivity(context: Context,activityClass: Class<T>, bundle: Bundle? = null) {
        context.startActivity(Intent(context, activityClass).apply {
            bundle?.let { putExtras(it) }
        })
    }

    fun <T> navigateToActivityWithResult(context: AppCompatActivity, activityClass: Class<T>, request: Int, bundle: Bundle? = null) {
        context.startActivityForResult(Intent(context, activityClass).apply {
            bundle?.let { putExtras(it) }
        }, request)
    }

    /**
     * The method for adding a new fragment
     * @param context : Activity's context
     * @param frag: Fragment to be added
     * @param args: Bundle to be passed
     * @param id: Fragment container ID
     * @param addToBackStack: Flag indicating whether to add to backstack or not
     */
    fun addFragment(frag: Fragment, context: Context, id: Int, addToBackStack: Boolean = false, args: Bundle? = null) {
        args?.let {
            frag.arguments = it
        }
        TransitionManager.addFragment(
            context as AppCompatActivity, frag,
            id, addToBackStack, false
        )
    }
    fun openDialogFragment(
            activity: AppCompatActivity,
            fragment: androidx.fragment.app.DialogFragment,
            cancelable: Boolean = true,
            bundle: Bundle?=null
    ) {
        fragment.isCancelable = cancelable
        fragment.arguments=bundle
        fragment.show(activity.supportFragmentManager, ContentValues.TAG)
    }
}