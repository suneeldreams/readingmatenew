package com.dreams.readingmate.ui.auth

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.ChildDetails
import com.dreams.readingmate.databinding.NewCalendarBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import sun.bob.mcalendarview.MarkStyle
import sun.bob.mcalendarview.listeners.OnMonthChangeListener
import sun.bob.mcalendarview.vo.DateData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class NewCalendar :
    BaseActivity<NewCalendarBinding>(R.layout.new_calendar),
    KodeinAware {

    override val kodein by kodein()
    lateinit var binding: NewCalendarBinding
    private var childDetail: ChildDetails? = null
    private val viewModel: AuthViewModel by instance<AuthViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: NewCalendarBinding
    ) {
        this.binding = binding
        binding.viewModel = viewModel
        binding.imgBackPass.setOnClickListener {
            finish()
        }
        setUpData()
        observeLiveEvents()
        /*binding.calendar.setOnMonthChangeListener(object : OnMonthChangeListener() {
            override fun onMonthChange(year: Int, month: Int) {
                val format = SimpleDateFormat("yyyy-MMM")
                val monthF = SimpleDateFormat("MMM", Locale.getDefault())
                val dayf: DateFormat = SimpleDateFormat("MMMM")
                val yearF = SimpleDateFormat("yyyy", Locale.getDefault())
                val cMonth = monthF.format(month)
                binding.txtDateTitle.text = String.format("%d-%d", year, month).format("MMM ,yyyy")
                *//*val ddMMMMyyyyFormat =
                    SimpleDateFormat("MMMM, yyyy", Locale.US)*//*

//                Log.d("gggg", ddMMMMyyyyFormat.format(month))
                Toast.makeText(
                    this@NewCalendar,
                    String.format("%d-%d", year, month),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })*/
    }

    private fun observeLiveEvents() {
        viewModel.childDetailListLiveData.observe(this, Observer {
            childDetail = it
            setCalendar(it)
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(this, it)
            }
        })
    }

    private fun setCalendar(childItems: ChildDetails?) {
        val calendar = Calendar.getInstance().time
        binding.calendar.hasTitle(true)
        val datet = SimpleDateFormat("dd", Locale.US)
        val montht = SimpleDateFormat("MM", Locale.US)
        val yeart = SimpleDateFormat("yyyy", Locale.US)
        val tdate = datet.format(calendar)
        val tmonth = montht.format(calendar)
        val tyear = yeart.format(calendar)
        binding.calendar.markDate(
            DateData(
                tyear.toInt(),
                tmonth.toInt(),
                tdate.toInt()
            ).setMarkStyle(
                MarkStyle(
                    MarkStyle.DEFAULT,
                    resources.getColor(R.color.color_active_button)
                )
            )
        )

        for (i in childItems!!.readingHistory!!.indices) {
            val dates = binding.calendar.markedDates
            val markedData = dates.all
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childItems?.readingHistory?.get(i)?.createdAt)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            binding.calendar.markDate(
                DateData(
                    year.toInt(),
                    month.toInt(),
                    date.toInt()
                ).setMarkStyle(
                    MarkStyle(
                        MarkStyle.DEFAULT,
                        resources.getColor(R.color.color_slate)
                    )
                )
            )
            /*binding.calendar.markDate(
                year.toInt(),
                month.toInt(),
                date.toInt()
            )*/
        }
    }

    private fun setUpData() {
        val intent = intent
        val childId = intent.getStringExtra("childId")
        viewModel.callChildDetailListAPI(
            this,
            Utils.getCustomPref(this, UserData.TOKEN),
            childId
        )
    }

    override fun onPause() {
        super.onPause()
        for (i in childDetail!!.readingHistory!!.indices) {
            val dates = binding.calendar.markedDates
            val markedData = dates.all
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childDetail?.readingHistory?.get(i)?.createdAt)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            binding.calendar.unMarkDate(
                year.toInt(),
                month.toInt(),
                date.toInt()
            )
        }
    }

    override fun onStop() {
        super.onStop()
        for (i in childDetail!!.readingHistory!!.indices) {
            val dates = binding.calendar.markedDates
            val markedData = dates.all
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childDetail?.readingHistory?.get(i)?.createdAt)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            binding.calendar.unMarkDate(
                year.toInt(),
                month.toInt(),
                date.toInt()
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        for (i in childDetail!!.readingHistory!!.indices) {
            val dates = binding.calendar.markedDates
            val markedData = dates.all
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childDetail?.readingHistory?.get(i)?.createdAt)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            binding.calendar.unMarkDate(
                year.toInt(),
                month.toInt(),
                date.toInt()
            )
        }
    }

    override fun onStart() {
        super.onStart()
        setUpData()
    }

    override fun onResume() {
        super.onResume()
        setUpData()
    }

}
