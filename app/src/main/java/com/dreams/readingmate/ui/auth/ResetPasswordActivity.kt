package com.dreams.readingmate.ui.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.ActivityResetPasswordBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ResetPasswordActivity :
    BaseActivity<ActivityResetPasswordBinding>(R.layout.activity_reset_password),
    NavigationContract, KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    lateinit var binding: ActivityResetPasswordBinding
    private val viewModel: AuthViewModel by instance<AuthViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ActivityResetPasswordBinding
    ) {
        this.binding = binding
        binding.viewModel = viewModel

        imgBack.visibility = View.VISIBLE
        toolbar_title.text = getString(R.string.change_password)
        imgBack.setOnClickListener(this)
        binding.resetPassword.setOnClickListener(this)
        observeLiveEvents()
    }

    override fun observeLiveEvents() {
        viewModel.resetPasswordLiveData.observe(this, Observer {
            finish()
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let { context ->
                AlertUtils.showToast(context, it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> finish()
            R.id.resetPassword -> {
                val oldPassword = edtOldPassword.text.toString().trim()
                val newPassword = edtNewPassword.text.toString().trim()
                val cnfmPassword = edtConfirmPassword.text.toString().trim()
                if (oldPassword.isEmpty()) run {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.Please_enter_your_old_password),
                        Toast.LENGTH_LONG
                    ).show()
                } else if (newPassword.isEmpty()) {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.error_new_password),
                        Toast.LENGTH_LONG
                    ).show()
                } else if (cnfmPassword.isEmpty()) {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.Please_confirm_your_password),
                        Toast.LENGTH_LONG
                    ).show()
                } else if (newPassword != cnfmPassword) {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.password_does_not_match),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    viewModel.callResetPasswordApi(
                        Utils.getCustomPref(this, UserData.TOKEN),
                        oldPassword,
                        newPassword,
                        cnfmPassword,
                        this
                    )
                }
            }
        }
    }

}
