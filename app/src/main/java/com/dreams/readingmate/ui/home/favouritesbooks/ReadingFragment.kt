package com.dreams.readingmate.ui.home.favouritesbooks

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ReadingAdapter
import com.dreams.readingmate.data.adapter.ReadingFavAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ReadingBookFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ReadingFragment :
    BaseFragment<ReadingBookFragmentBinding>(R.layout.reading_book_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var preferenceChild: PreferenceChildItems? = null
    private var readingList = ArrayList<String>()
    private var readingBookList = ArrayList<Model.favBookData>()
    private var favStringIdsList = ArrayList<String>()
    var from = ""
    private var filter = ""
    private lateinit var searchTypeList: Array<String>
    var prefs: SharedPreferences? = null
    var data: List<FavBookData?>? = null
    private var mListener: ReadingFragmentInterface? = null
    private var readingFavAdapter: ReadingFavAdapter? = null
    private var readingAdapter: ReadingAdapter? = null
    private lateinit var binding: ReadingBookFragmentBinding
    private val viewModel: FavouriteBooksViewModel by instance<FavouriteBooksViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ReadingBookFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        initSearchTypeList()
        observeLiveEvents()

        /*binding.edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    binding.noReadingBooks.visibility = View.GONE
                    binding.recyclerReadingBooks.visibility = View.GONE
                    binding.recyclerReading.visibility = View.VISIBLE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(), "50", filter
                    )
                } else {
//                    binding.noReadingBooks.visibility = View.VISIBLE
                    binding.recyclerReading.visibility = View.GONE
                    binding.recyclerReading.adapter = null
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })*/

    }

    private fun initSearchTypeList() {
        binding.spnrBooksType.onItemSelectedListener = searchTypeListListener
    }

    private val searchTypeListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val filterType = searchTypeList[position]
            if (filterType.isNotEmpty()) {
                binding.booksType.text = filterType
                if (filterType == "Book") {
                    filter = "book"
                } else {
                    filter = "author"
                }
                Log.d("filterType", filterType)
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
            if (preferenceChild!!.from != "dashboard") {
                binding.recyclerReadingBooks.visibility = View.VISIBLE
            } else {
                binding.recyclerReading.visibility = View.GONE
            }
        }
        searchTypeList = resources.getStringArray(R.array.search_type)
        binding.spnrBooksType.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, searchTypeList)
        val numberOfColumns = 3
        binding.recyclerReading.layoutManager = GridLayoutManager(context, numberOfColumns)
        binding.recyclerReadingBooks.layoutManager = GridLayoutManager(context, numberOfColumns)
        if (preferenceChild!!.from == "editProfile") {
            if (preferenceChild!!.currentlyReadingBooks!!.isNotEmpty()) {
                binding.noReadingBooks.visibility = View.GONE
                for (i in 0 until preferenceChild!!.currentlyReadingBooks!!.size) {
                    val id = preferenceChild!!.currentlyReadingBooks!![i]!!.bookId
                    val img = preferenceChild!!.currentlyReadingBooks!![i]!!.image
                    val title = preferenceChild!!.currentlyReadingBooks!![i]!!.title
                    favStringIdsList.add(id!!)
                    val favBook: Model.favBookData = Model.favBookData(id, img!!, title!!)
                    readingBookList.add(favBook)
                }
            } else {
                binding.noReadingBooks.visibility = View.VISIBLE
            }
        }
        preferenceChild!!.currentlyReadingbook = favStringIdsList
        callFavoriteBookAdapter(readingBookList)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.lnrBooksType.setOnClickListener(this)
        binding.rltSearchs.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener {}
        binding.includeToolbar.mTitle.text = getString(R.string.save)
    }

    private fun observeLiveEvents() {
        viewModel.FavBookListLiveData.observe(this, Observer {
            this.data = it
            binding.recyclerReading.visibility = View.VISIBLE
            readingAdapter = ReadingAdapter(data, mListener!!, this)
            binding.recyclerReading.adapter = readingAdapter
        })
        viewModel.createChildLiveData.observe(this, Observer {
            Log.d("childVal:-", it.toString())
            prefs!![mUserData.CLICKEDID] = "home"
            Intent(requireContext(), HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
        viewModel.errorCode.observe(this, Observer {
            if (it == 400) {
                openDialog(requireContext())
            }
        })
    }

    private fun openDialog(
        context: Context
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.already_child_popup)

        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        dialog.show()
        val btnOk = dialog.findViewById(R.id.btnOk) as Button
        btnOk.setOnClickListener {
            dialog.dismiss()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.lnrBooksType -> binding.spnrBooksType.performClick()
            R.id.rltSearchs -> {
                val searchValue = binding.edtSearch.text.toString()
                if (searchValue > "1") {
                    binding.noReadingBooks.visibility = View.GONE
                    binding.recyclerReadingBooks.visibility = View.GONE
                    binding.recyclerReading.visibility = View.VISIBLE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        searchValue, "50", filter
                    )
                } else {
                    binding.recyclerReading.visibility = View.GONE
                    binding.recyclerReading.adapter = null
                }
            }
            R.id.mTitle -> {
                Utils.hideKeyBoard(requireActivity())
                viewModel.callCreateChildApi(
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    Utils.getCustomPref(requireContext(), UserData.ID),
                    preferenceChild!!.childName,
                    preferenceChild!!.nickName,
                    preferenceChild!!.age,
                    preferenceChild!!.schoolId,
                    preferenceChild!!.countryId,
                    preferenceChild!!.colorName,
                    preferenceChild!!.generesId,
                    preferenceChild!!.subjectId,
                    preferenceChild!!.charId,
                    preferenceChild!!.currentlyReadingbook,
                    preferenceChild!!.image,
                    preferenceChild!!.from,
                    preferenceChild!!.updateChildId,
                    preferenceChild!!.schoolName,
                    preferenceChild!!.charity,
                    requireActivity()
                )
                /*if (favStringIdsList.isNotEmpty()) {
                    val bundle = Bundle()
                    bundle.putParcelable("child", preferenceChild)
                    mListener?.openAlreadyReadFragment(bundle)
                } else {
                    AlertUtils.showToast(
                        requireContext(),
                        "Please search books to add as your favourite!"
                    )
                }*/
            }
        }
    }


    interface ReadingFragmentInterface {
        fun openBackFragment()

        //        fun openGenresFragment(bundle: Bundle)
        fun openAlreadyReadFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ReadingFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ReadingFragmentInterface")
        }
    }

    fun AddImageToStringArray(
        img: String?,
        id: String?,
        title: String?
    ) {
        if (id != null) {
            favStringIdsList.add(id)
        }
        preferenceChild!!.currentlyReadingbook = favStringIdsList
        Log.d("array", favStringIdsList.toString())
        readingAdapter!!.notifyDataSetChanged()
        val favBook: Model.favBookData = Model.favBookData(id!!, img!!, title!!)
        readingBookList.add(favBook)
        if (readingBookList.isNotEmpty()) {
            binding.recyclerReadingBooks.visibility = View.VISIBLE
            binding.recyclerReading.visibility = View.GONE
            binding.noReadingBooks.visibility = View.GONE
            callFavoriteBookAdapter(readingBookList)
        }
        binding.edtSearch.setText("")
    }

    private fun callFavoriteBookAdapter(readingBookList: ArrayList<Model.favBookData>) {
        readingFavAdapter = ReadingFavAdapter(readingBookList, mListener!!, this)
        binding.recyclerReadingBooks.adapter = readingFavAdapter
    }

    fun refreshAdapter(position: Int, id: String) {
        if (readingBookList[position].id == id) {
            readingBookList.removeAt(position)

        }
        if (favStringIdsList[position] == id) {
            favStringIdsList.removeAt(position)
        }
        preferenceChild!!.currentlyReadingbook = favStringIdsList
        callFavoriteBookAdapter(readingBookList)
        /*for (i in readingBookList.indices) {
            if (readingBookList[i].id == id) {
                readingBookList.removeAt(i)
            }
            callFavoriteBookAdapter(readingBookList)
            Log.d("mainList", readingBookList.toString())
        }*/
        readingFavAdapter!!.notifyDataSetChanged()
    }
}