package com.dreams.readingmate.ui.home.subscription

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.BundleDetailFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BundleDetailFragment :
    BaseFragment<BundleDetailFragmentBinding>(R.layout.bundle_detail__fragment),
    KodeinAware, NavigationContract, View.OnClickListener {

    override val kodein by kodein()
    private var mListener: BundleDetailFragmentInterface? = null
    private lateinit var binding: BundleDetailFragmentBinding
    private val viewModel: SubscriptionViewModel by instance<SubscriptionViewModel>()
    override fun initComponents(savedInstanceState: Bundle?, binding: BundleDetailFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolbar()
        setUpData()
        observeLiveEvents()
        cartCountAndVisibility()
        binding.homeRootLayout.setOnClickListener { }
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.UPDATE_CART_ITEM))
    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            val slug = args.getString("slug").toString()
            callBundleDetailApi(slug)
        }
    }

    /**
     * setUp toolBar
     */
    private fun setUpToolbar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.subscriptionToolbar.imgBack.visibility = View.VISIBLE
        binding.subscriptionToolbar.imgBack.setOnClickListener(this)
        binding.subscriptionToolbar.imgRecommCart.visibility = View.VISIBLE
        binding.subscriptionToolbar.imgRecommCart.setOnClickListener(this)
        binding.btnAddToCart.setOnClickListener(this)
        binding.subscriptionToolbar.toolbar_title.text = resources.getString(R.string.bundle_detail)
    }

    override fun observeLiveEvents() {
        viewModel.bundleDetailLiveData.observe(this, Observer {
            binding.txtBundleName.text = it.bundleName
            binding.txtBundleAuthorName.text = it.bundleAuthor
            binding.txtBundlePrice.text =
                getString(R.string.currency_pound) + it.bundleAmount.toString()
            binding.txtBundleDescription.text = it.bundleDescription
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    private fun callBundleDetailApi(slug: String) {
        viewModel.callBundleDetailListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            slug
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.btnAddToCart -> addToCart()
        }
    }

    private fun addToCart() {
        /*  val cartItem = CartItem()
          cartItem.bookId = bookListData!!.id
          cartItem.weight = bookListData!!.weight
          cartItem.bookName = bookListData!!.title
          cartItem.perItemCharge = 0.05
          cartItem.description = bookListData!!.shortDescription
          cartItem.img = bookListData!!.img
          cartItem.price = bookListData!!.ukPrice.toString()
          (activity as HomeActivity).addDataToCartList(cartItem)
          (context as HomeActivity).calculateShippingCharge()
          cartCountAndVisibility()*/
    }

    private fun cartCountAndVisibility() {
        val cartCount = (activity as HomeActivity).getCartCount()
        if (cartCount > 0) {
            binding.subscriptionToolbar.txtCount.visibility = View.VISIBLE
            binding.subscriptionToolbar.txtCount.text = cartCount.toString()
        } else {
            binding.subscriptionToolbar.txtCount.visibility = View.GONE
        }
    }

    interface BundleDetailFragmentInterface {
        fun openBackFragment()
        fun openCheckoutFragments(bundle: Bundle)
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            when (intent?.action) {
                AppConstants.UPDATE_CART_ITEM -> {
                    cartCountAndVisibility()
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BundleDetailFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BundleDetailFragmentInterface")
        }
    }
}