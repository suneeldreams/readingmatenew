package com.dreams.readingmate.ui.home.favouritegenres

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.FavouriteGenresAdapter
import com.dreams.readingmate.data.network.responses.FavGeneresData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ChooseFavoriteGenresFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.Model
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class FavouriteGenresFragment :
    BaseFragment<ChooseFavoriteGenresFragmentBinding>(R.layout.choose_favorite_genres_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var data: List<FavGeneresData?>? = null
    private var generesAdapter: FavouriteGenresAdapter? = null
    private var preferenceChild: PreferenceChildItems? = null
    private var favGeneresIdsList = ArrayList<String>()
    private var mListener: FavouriteGenresFragmentInterface? = null
    private lateinit var binding: ChooseFavoriteGenresFragmentBinding
    private var commentList: ArrayList<Model.LanguageList> = ArrayList()
    private val viewModel: FavouriteGenresViewModel by instance<FavouriteGenresViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ChooseFavoriteGenresFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            preferenceChild = args.getParcelable("child")
            this.data = preferenceChild?.favGenreData
            Log.d("GenreData", preferenceChild?.favGenreData.toString())
        }
        binding.txtChildName.text = "Hi" + " " + preferenceChild!!.childName + ","
        val numberOfColumns = 2
        binding.recyclerFavouriteGenres.layoutManager = GridLayoutManager(context, numberOfColumns)
        if (preferenceChild!!.from == "dashboard") {
            generesAdapter = FavouriteGenresAdapter(preferenceChild?.genreData, mListener!!, this)
        } else {
            generesAdapter =
                FavouriteGenresAdapter(preferenceChild?.favGenreData, mListener!!, this)
        }

        binding.recyclerFavouriteGenres.adapter = generesAdapter
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener { }
        binding.includeToolbar.mTitle.text = getString(R.string.next)
    }

    private fun observeLiveEvents() {
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                val bundle = Bundle()
                bundle.putParcelable("child", preferenceChild)
                mListener?.openSubjectFragment(bundle)
            }
        }
    }


    interface FavouriteGenresFragmentInterface {
        fun openBackFragment()
        fun openSubjectFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavouriteGenresFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement FavouriteGenresFragmentInterface")
        }
    }

    fun refreshAdapter(name: String) {
        /*for (i in data!!.indices) {
//            data!![i]!!.isSelected = position == i
//            preferenceChild!!.generesId = data!![i]!!.id
            favGeneresIdsList.add(name)
        }*/
        favGeneresIdsList.add(name)
        preferenceChild!!.generesId = favGeneresIdsList
        generesAdapter!!.notifyDataSetChanged()
    }
}