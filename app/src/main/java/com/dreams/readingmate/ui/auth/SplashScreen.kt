package com.dreams.readingmate.ui.auth

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Created by Suneel on 30/03/2020.
 */
class SplashScreen : AppCompatActivity() {
    private val splashInterval: Long = 3000
    var prefs: SharedPreferences? = null
    var isFirstOpen: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
//        getKeyHash()
        prefs = UserData.customPrefsGlobal(this)
        isFirstOpen = prefs?.getBoolean(UserData.IS_FIRST_TIME, true)
        Handler().postDelayed({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, splashInterval)
    }

    private fun getKeyHash() {
        try {
            val info = packageManager.getPackageInfo(
                "com.dreams.readingmate",
                PackageManager.GET_SIGNING_CERTIFICATES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }
    }
}