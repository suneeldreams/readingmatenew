package com.dreams.readingmate.ui.home.favouritecharater

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ChooseFavoriteCharacterFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class FavouriteCharacterFragment :
    BaseFragment<ChooseFavoriteCharacterFragmentBinding>(R.layout.choose_favorite_character_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var preferenceChild: PreferenceChildItems? = null
    private var favCharIdsList = ArrayList<String>()
    private var mListener: FavouriteCharacterFragmentInterface? = null
    private lateinit var binding: ChooseFavoriteCharacterFragmentBinding
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    var prefs: SharedPreferences? = null
    private val viewModel: FavouriteCharacterViewModel by instance<FavouriteCharacterViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ChooseFavoriteCharacterFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()
        binding.radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            Log.d("radioId:-", checkedId.toString())
            favCharIdsList.add(checkedId.toString())
            preferenceChild!!.charId = favCharIdsList
        })
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        val args = arguments
        if (args != null) {
            preferenceChild = args.getParcelable("child")
        }
        binding.txtChildName.text = "Hi" + " " + preferenceChild!!.childName + ","
        if (preferenceChild!!.from == "dashboard") {
            for (element in preferenceChild?.characterData!!) {
                val params = RadioGroup.LayoutParams(context, null)
                params.setMargins(0, 5, 0, 0)
                val rbn = RadioButton(requireContext())
                rbn.id = View.generateViewId()
                rbn.text = element!!.characterName
                rbn.layoutParams = params
                binding.radioGroup.addView(rbn)
            }
        } else {
            for (element in preferenceChild?.favCharacterData!!) {
                val params = RadioGroup.LayoutParams(context, null)
                params.setMargins(0, 5, 0, 0)
                val rbn = RadioButton(requireContext())
                rbn.id = View.generateViewId()
                rbn.text = element!!.characterName
                rbn.layoutParams = params
                binding.radioGroup.addView(rbn)
            }
        }

    }

    /**
     * apply click event listener.
     */
    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener { }
        binding.includeToolbar.mTitle.text = getString(R.string.done)

    }
    /**
     * MVVM ObserverLiveEvents results
     */
    private fun observeLiveEvents() {
        viewModel.createChildLiveData.observe(this, Observer {
            Log.d("childVal:-", it.toString())
            prefs!![mUserData.CLICKEDID] = "home"
            Intent(requireContext(), HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let { context ->
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * handle click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                viewModel.callCreateChildApi(
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    Utils.getCustomPref(requireContext(), UserData.ID),
                    preferenceChild!!.childName,
                    preferenceChild!!.nickName,
                    preferenceChild!!.age,
                    preferenceChild!!.schoolId,
                    preferenceChild!!.countryId,
                    preferenceChild!!.colorName,
                    preferenceChild!!.bookId,
                    preferenceChild!!.generesId,
                    preferenceChild!!.subjectId,
                    preferenceChild!!.charId,
                    preferenceChild!!.image,
                    preferenceChild!!.from,
                    preferenceChild!!.updateChildId,
                    requireActivity()
                )
            }
        }
    }


    interface FavouriteCharacterFragmentInterface {
        fun openBackFragment()
        fun openHomeFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavouriteCharacterFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement FavouriteCharacterFragmentInterface")
        }
    }

}