package com.dreams.readingmate.ui.home.favouritesbooks

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.data.adapter.FavoriteBookArrayAdapter
import com.dreams.readingmate.data.adapter.FavouritBooksAdapter
import com.dreams.readingmate.data.adapter.FavouritBooksTextAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.BookDataItem
import com.dreams.readingmate.data.network.responses.FavImageData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ChooseFavoriteBookFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class FavouriteBookFragment :
    BaseFragment<ChooseFavoriteBookFragmentBinding>(R.layout.choose_favorite_book_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var preferenceChild: PreferenceChildItems? = null
    private var data: List<BookDataItem?>? = null
    private var FavImageList: List<FavImageData?>? = null
    private var favStringList = ArrayList<String>()
    private var favBookDataList = ArrayList<Model.favBookData>()
    private var favStringIdsList = ArrayList<String>()
    var myCryptoArray: Array<CharSequence?>? = null
    var selectedItems = ""
    var from = ""
    private lateinit var favBookAdapter: FavoriteBookArrayAdapter
    private var mListener: FavouriteBookFragmentInterface? = null
    private var bookAdapter: FavouritBooksAdapter? = null
    private var bookTextAdapter: FavouritBooksTextAdapter? = null
    private lateinit var binding: ChooseFavoriteBookFragmentBinding
    private val viewModel: FavouriteBooksViewModel by instance<FavouriteBooksViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ChooseFavoriteBookFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

        binding.edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(),"50",""
                    )
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
            if (from != "dashboard") {
                binding.recyclerFavouriteBook.visibility = View.VISIBLE
            } else {
                binding.recyclerFavouriteBook.visibility = View.GONE
            }
        }
        binding.txtChildName.text = "Hi" + " " + preferenceChild!!.childName + ","
        val numberOfColumns = 3
        binding.recyclerFavouriteBook.layoutManager = GridLayoutManager(context, numberOfColumns)
        if (from == "editProfile") {
            for (i in 0 until preferenceChild!!.favBookData!!.size) {
                val id = preferenceChild!!.favBookData!![i]!!.id
                val img = preferenceChild!!.favBookData!![i]!!.img
                val favorite = preferenceChild!!.favBookData!![i]!!.isFavourite
                favStringIdsList.add(id!!)
//                val favBook: Model.favBookData = Model.favBookData(id!!, img!!, favorite!!)
//                favBookDataList.add(favBook)
            }
        }
        callFavoriteBookAdapter(favBookDataList)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener {}
        binding.includeToolbar.mTitle.text = getString(R.string.next)
    }

    private fun observeLiveEvents() {
        viewModel.FavBookListLiveData.observe(this, Observer {
            Log.d("FavBook:-", it.toString())
            binding.textList.visibility = View.VISIBLE
            binding.textList.layoutManager =
                LinearLayoutManager(context)
            bookTextAdapter = FavouritBooksTextAdapter(it, mListener!!, this)
            binding.textList.adapter = bookTextAdapter
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                if (favStringIdsList.isNotEmpty()) {
                    val bundle = Bundle()
                    bundle.putParcelable("child", preferenceChild)
                    mListener?.openGenresFragment(bundle)
                } else {
                    AlertUtils.showToast(
                        requireContext(),
                        "Please search books to add as your favourite!"
                    )
                }
            }
        }
    }


    interface FavouriteBookFragmentInterface {
        fun openBackFragment()
        fun openGenresFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavouriteBookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement FavouriteBookFragmentInterface")
        }
    }

    /*fun AddImageToStringArray(img: String?, id: String?) {
        binding.textList.visibility = View.GONE
        if (id != null) {
            favStringIdsList.add(id)
        }
        preferenceChild!!.bookId = favStringIdsList
        Log.d("array", favStringList.toString())
//        val favBook: Model.favBookData = Model.favBookData("", img!!, false)
        favBookDataList.add(favBook)
        if (img.isNotEmpty()) {
            binding.rltRecyclerView.visibility = View.VISIBLE
            callFavoriteBookAdapter(favBookDataList)
        }
        binding.edtSearch.setText("")

    }*/

    private fun callFavoriteBookAdapter(favBookDataList: ArrayList<Model.favBookData>) {
        bookAdapter = FavouritBooksAdapter(favBookDataList, mListener!!, this)
        binding.recyclerFavouriteBook.adapter = bookAdapter
    }

}