package com.dreams.readingmate.ui.home.progress

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.AddWeekReadDataModel
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class ProgressViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {
    private val _childBookReadListLiveData = MutableLiveData<List<ChildBooksReadData>>()
    val childBookReadListLiveData: LiveData<List<ChildBooksReadData>>
        get() = _childBookReadListLiveData

    private val _removeBookFromShelfLiveData = MutableLiveData<AddBookShelfData>()
    val removeBookFromShelfLiveData: LiveData<AddBookShelfData>
        get() = _removeBookFromShelfLiveData

    private val _childBookReviewedListLiveData = MutableLiveData<List<ChildBooksReviewedData>>()
    val childBookReviewedListLiveData: LiveData<List<ChildBooksReviewedData>>
        get() = _childBookReviewedListLiveData

    private val _childProgressLiveData = MutableLiveData<ChildProgressD>()
    val childProgressLiveData: LiveData<ChildProgressD>
        get() = _childProgressLiveData

    private val _FavBookListLiveData = MutableLiveData<List<FavBookData>>()
    val FavBookListLiveData: LiveData<List<FavBookData>>
        get() = _FavBookListLiveData

    private val _addBookLiveData = MutableLiveData<AddBookShelfData>()
    val addBookLiveData: LiveData<AddBookShelfData>
        get() = _addBookLiveData

    private val _childDetailListLiveData = MutableLiveData<ChildDetails>()
    val childDetailListLiveData: LiveData<ChildDetails>
        get() = _childDetailListLiveData

    private val _childListLiveData = MutableLiveData<List<ChildListItem>>()
    val childListLiveData: LiveData<List<ChildListItem>>
        get() = _childListLiveData

    private val _preferenceChildLiveData = MutableLiveData<PreferenceChildItems>()
    val userDetailLiveData: LiveData<PreferenceChildItems>
        get() = _preferenceChildLiveData

    private val _updateWeekDataLiveData = MutableLiveData<AddBookShelfData>()
    val updateWeekDataLiveData: LiveData<AddBookShelfData>
        get() = _updateWeekDataLiveData

    private val _updateTickDataLiveData = MutableLiveData<UpdateTickData2>()
    val updateTickDataLiveData: LiveData<UpdateTickData2>
        get() = _updateTickDataLiveData

    private val _removeTickDataLiveData = MutableLiveData<UpdateTickData2>()
    val removeTickDataLiveData: LiveData<UpdateTickData2>
        get() = _removeTickDataLiveData

    private val _bookFavoriteLiveData = MutableLiveData<BookDetailsDataItems>()
    val bookFavoriteLiveData: LiveData<BookDetailsDataItems>
        get() = _bookFavoriteLiveData

    private val _bookStatusLiveData = MutableLiveData<AddBookShelfData>()
    val bookStatusLiveData: LiveData<AddBookShelfData>
        get() = _bookStatusLiveData

    private val _orderBookListLiveData = MutableLiveData<OrderListData>()
    val orderBookListLiveData: LiveData<OrderListData>
        get() = _orderBookListLiveData

    private val _userWishListLiveData = MutableLiveData<List<UserWishListData>>()
    val userWishListLiveData: LiveData<List<UserWishListData>>
        get() = _userWishListLiveData

    private val _allBookLiveData = MutableLiveData<AllBooksData>()
    val allBookLiveData: LiveData<AllBooksData>
        get() = _allBookLiveData


    fun getChildProgrss(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildProgress(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childProgressLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun getChildBooksReadList(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildBooksReadList(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childBookReadListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addBookToBookShelf(
        mContext: Context,
        accessToken: String,
        bookId: String?,
        childId: String?,
        readingStatus: String?
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addBookToBookShelf(
                    accessToken, bookId!!, childId!!, readingStatus
                )
                if (result is ResultState.Success) {
                    _addBookLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun addWeekReadData(
        mContext: Context,
        accessToken: String,
        bookshelfId: String?,
        pageRead: String?,
        createdAt: String,
        readingStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addWeekReadData(
                    AddWeekReadDataModel(bookshelfId, pageRead, createdAt, readingStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _updateWeekDataLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun removeBookFromShelf(
        context: Context,
        accessToken: String,
        childId: String,
        book_id: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.removeBookFromShelf(accessToken, childId, book_id)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    result.value.message?.let { AlertUtils.showToast(context, it) }
                    println(result.value)
                    _removeBookFromShelfLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun getChildBooksReviewedList(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildBooksReviewedList(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childBookReviewedListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callFavoriteBookListAPI(
        context: Context,
        accessToken: String,
        search: String,
        pageSize: String,
        cartitems: String,
        filter: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getFavList(accessToken, search, pageSize, cartitems, filter)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _FavBookListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callChildDetailListAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildDetailList(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childDetailListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    println("result.value")
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                println("errorrror")
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callChildListAPI(
        context: Context,
        accessToken: String,
        userId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildList(accessToken, userId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callPreferenceChildAPI(context: Context, accessToken: String) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildPreference(accessToken)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _preferenceChildLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addWeekTickData(
        mContext: Context,
        accessToken: String,
        bookshelfId: String?,
        pageRead: String?,
        createdAt: String,
        readingStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addWeekTickData(
                    AddWeekReadDataModel(bookshelfId, pageRead, createdAt, readingStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _updateTickDataLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun removeTick(
        mContext: Context,
        accessToken: String,
        historyId: String?,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.removeTick(
                    accessToken, historyId!!, childId
                )
                if (result is ResultState.Success) {
                    _removeTickDataLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun bookStatus(
        mContext: Context,
        accessToken: String,
        bookId: String?,
        pageRead: String?,
        bookStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.bookStatusData(
                    BookStatusDataModel(bookId, pageRead, bookStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _bookStatusLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun addFavoriteBook(
        context: Context,
        accessToken: String,
        bookId: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.favoriteBook(accessToken, bookId, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    AlertUtils.showToast(context, result.value.message!!)
                    _bookFavoriteLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callOrderBookAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getOrderBookList(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _orderBookListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callUserWishListAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getUserWishListList(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _userWishListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }
    fun callAllBooksDataAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.getAllBookData(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _allBookLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }
}
