package com.dreams.readingmate.ui.home.addchild

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ColorArrayAdapter
import com.dreams.readingmate.data.adapter.SchoolArrayAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.ChildDetails
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.AddChildFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.add_child_fragment.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class AddChildFragment : BaseFragment<AddChildFragmentBinding>(R.layout.add_child_fragment),
    NavigationContract,
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    var from = ""
    var schoolId = ""
    var countryId = ""
    var colorName = ""
    var charityName = ""
    private val TAKE_PHOTO_REQUEST = 101
    private val TAKE_STORAGE_REQUEST = 102
    private val REQUEST_GALLERY_CODE = 103
    private val REQUEST_IMAGE_CAPTURE = 104
    private var filePath = ""
    private var selectedYear = ""
    var uri: Uri? = null
    private lateinit var charityList: Array<String>
    private var preferenceChild: PreferenceChildItems? = null
    private var editChildProfileItem: ChildListItem? = null
    private var childDetaildata: ChildDetails? = null
    private lateinit var ageList: Array<String>
    private var currReadingBooks = ArrayList<String>()
    private var favBooksIds = ArrayList<String>()
    private lateinit var schoolAdapter: SchoolArrayAdapter
    var prefs: SharedPreferences? = null
    private lateinit var colorAdapter: ColorArrayAdapter

    private var mListener: AddChildFragmentInterface? = null
    private lateinit var binding: AddChildFragmentBinding
    private val viewModel: AddChildViewModel by instance<AddChildViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: AddChildFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpData()
        setUpToolBar()
        observeLiveEvents()
        observeNavigationEvents()
        initSchoolList()
//        initColorList()
        setSchoolTypeList()
//        initCharityList()
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.CHANGE_COLOR))
    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            val childId = args.getString("childId", "")
            preferenceChild = args.getParcelable("child")
            editChildProfileItem = args.getParcelable("editChild")
            Log.d("schoolType", preferenceChild?.schoolTypeData.toString())
            if (from != "dashboard") {
                viewModel.callChildDetailListAPI(
                    requireContext(),
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    childId
                )
            }
        }
        prefs = mUserData.customPrefs(requireContext())

        ageList = resources.getStringArray(R.array.ageList)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (from == "dashboard") {
            binding.includeToolbar.toolbar_title.text = getString(R.string.add_reader)
        } else {
            binding.includeToolbar.toolbar_title.text = getString(R.string.edit_profile)
            Utils.displayCircularImage(
                requireContext(),
                binding.imgAddChild,
                editChildProfileItem!!.image!!
            )
            binding.edtChildName.setText(editChildProfileItem!!.childName)
//            binding.edtNickName.setText(editChildProfileItem!!.nickName)
            binding.edtSchoolName.setText(editChildProfileItem!!.schoolName)
        }
        charityList = resources.getStringArray(R.array.charity_list)
        binding.spinnerCharity.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, charityList)
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.text = getString(R.string.save)
    }

    /**
     * MVVM ObserverLiveEvents
     */
    override fun observeLiveEvents() {
        viewModel.childDetailListLiveData.observe(this, Observer {
            childDetaildata = it
            binding.edtChildAge.setText(childDetaildata!!.age!!.toString() + " Years old")
            selectedYear = childDetaildata!!.age.toString()

            /*if (from != "dashboard") {
                Utils.displayCircularImage(
                    requireContext(),
                    binding.imgAddChild,
                    childDetaildata!!.img!!
                )
                binding.edtChildName.setText(childDetaildata!!.name)
                binding.edtNickName.setText(childDetaildata!!.nickName)
                binding.edtSchoolName.setText(childDetaildata!!.schoolType)
            }*/
//            setColorList()
//            setCharityList()
        })
        viewModel.createChildLiveData.observe(this, Observer {
            prefs!![mUserData.CLICKEDID] = "home"
            Intent(requireContext(), HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * Populate colors in list
     */
    private fun initColorList() {
        colorAdapter = ColorArrayAdapter(requireContext(), preferenceChild?.colourData)
        binding.spinnerColor.adapter = colorAdapter
        binding.spinnerColor.onItemSelectedListener = colorListListener
    }

    /**
     * Populate School in list
     */
    private fun initSchoolList() {
        schoolAdapter = SchoolArrayAdapter(requireContext(), preferenceChild?.schoolTypeData)
        binding.spinnerSchool.adapter = schoolAdapter
        binding.spinnerSchool.onItemSelectedListener = schoolListListener
    }

    /**
     * Listener for school
     */
    private val schoolListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            if (from == "dashboard") {
                val selectedState = preferenceChild?.schoolTypeData?.get(position)
                if (selectedState != null) {
                    binding.txtSchoolName.text = selectedState.schoolTypeName
                    schoolId = selectedState.schoolTypeName!!
                }
            } else {
                val selectedState = preferenceChild?.schoolTypeData?.get(position)
                if (selectedState != null) {
                    binding.txtSchoolName.text = selectedState.schoolTypeName
                    schoolId = selectedState.schoolTypeName!!
                }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Listener for Country
     */
    private val countryListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            if (from == "dashboard") {
                val selectedCountry = preferenceChild?.countryData?.get(position)
                if (selectedCountry != null) {
                    binding.txtCountry.text = selectedCountry.countryName
                    countryId = selectedCountry.countryName!!
                }
            } else {
                val selectedCountry = preferenceChild?.countryData?.get(position)
                if (selectedCountry != null) {
                    binding.txtCountry.text = selectedCountry.countryName
                    countryId = selectedCountry.countryName!!
                }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Listener for coulor
     */
    private val colorListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>, view: View, position: Int, id: Long
        ) {
            if (from == "dashboard") {
                val selectedColor = preferenceChild?.colourData?.get(position)
                if (selectedColor != null) {
                    binding.txtColor.text = selectedColor.colourName
                    colorName = selectedColor.colourName!!
                }
            } else {
                val selectedColor = preferenceChild?.colourData?.get(position)
                for (i in preferenceChild?.colourData!!.indices) {
                    preferenceChild?.colourData!![i]!!.colourName.equals(editChildProfileItem!!.colour)
                    binding.txtColor.text = selectedColor!!.colourName
                    colorName = selectedColor.colourName!!
                    break
                }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * Observer click listeners
     */
    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.visibility = View.GONE
        binding.saveAddReader.setOnClickListener(this)
        binding.edtChildAge.setOnClickListener(this)
        binding.lnrSchool.setOnClickListener(this)
//        binding.lnrCharity.setOnClickListener(this)
        binding.lnrColor.setOnClickListener(this)
        binding.txtBrowzeImage.setOnClickListener(this)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.saveAddReader -> {
                if (from == "dashboard") {
                    val childName = edtChildName.text.toString().trim()
//                    val nickName = edtNickName.text.toString().trim()
                    val age = edtChildAge.text.toString().trim()
                    val shoolName = edtSchoolName.text.toString().trim()
                    val country = binding.txtCountry.text.toString().trim()
                    if (childName.isEmpty()) {
                        AlertUtils.showToast(requireContext(), "Please enter child name.")
                        return
                    } /*else if (nickName.isEmpty()) {
                        AlertUtils.showToast(requireContext(), "Please enter child nickname.")
                        return
                    }*/ else if (age.isEmpty()) {
                        AlertUtils.showToast(requireContext(), "Please select your Age.")
                        return
                    } else if (shoolName.isEmpty()) {
                        AlertUtils.showToast(requireContext(), "Please enter your school name.")
                        return
                    } else if (colorName.isEmpty()) {
                        AlertUtils.showToast(requireContext(), "Please choose your colour.")
                        return
                    } else {
                        var base64 = ""
                        if (filePath.isEmpty()) {
                            base64 = ""
                        } else {
                            val bm: Bitmap = BitmapFactory.decodeFile(filePath)
                            val baos = ByteArrayOutputStream()
                            bm.compress(Bitmap.CompressFormat.JPEG, 0, baos)
                            val byteArrayImage: ByteArray = baos.toByteArray()
                            base64 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
                        }
                        preferenceChild!!.schoolId = schoolId
                        preferenceChild!!.age = selectedYear
                        preferenceChild!!.from = from
                        preferenceChild!!.childName = childName
//                        preferenceChild!!.nickName = nickName
                        preferenceChild!!.colorName = colorName
                        preferenceChild!!.schoolName = shoolName
                        preferenceChild!!.charity = charityName
                        preferenceChild!!.countryId = country
                        preferenceChild!!.image = base64
                        val bundle = Bundle()
                        bundle.putParcelable("child", preferenceChild)
                        mListener?.openReadingFragment(bundle)
                    }
                } else {
                    val childName = edtChildName.text.toString().trim()
//                    val nickName = edtNickName.text.toString().trim()
                    val shoolName = edtSchoolName.text.toString().trim()
                    val country = binding.txtCountry.text.toString().trim()
                    var base64 = ""
                    if (filePath.isEmpty()) {
                        base64 = ""
                    } else {
                        val bm: Bitmap = BitmapFactory.decodeFile(filePath)
                        val baos = ByteArrayOutputStream()
                        bm.compress(Bitmap.CompressFormat.JPEG, 0, baos)
                        val byteArrayImage: ByteArray = baos.toByteArray()
                        base64 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
                    }

                    Utils.hideKeyBoard(requireActivity())
                    viewModel.callCreateChildApi(
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        Utils.getCustomPref(requireContext(), UserData.ID),
                        childName,
                        selectedYear,
                        schoolId,
                        country,
                        colorName,
                        favBooksIds,
                        preferenceChild!!.generesId,
                        preferenceChild!!.subjectId,
                        preferenceChild!!.charId,
                        currReadingBooks,
                        preferenceChild!!.readBookss,
                        base64,
                        "editProfile",
                        editChildProfileItem!!.id,
                        shoolName,
                        charityName,
                        requireActivity()
                    )
                }
            }
            R.id.edtChildAge -> {
                val datePickerListener =
                    OnDateSetListener { datePicker, year, month, day ->
                        val c = Calendar.getInstance()
                        c[Calendar.YEAR] = year
                        c[Calendar.MONTH] = month
                        c[Calendar.DAY_OF_MONTH] = day
//                        selectedYear = getAge(year, month, day).toString()
                        selectedYear = calculateAge(c.timeInMillis).toString()
                        binding.edtChildAge.setText("$selectedYear Years old")
                    }
                showPopUp(datePickerListener)
            }
            R.id.lnrSchool -> binding.spinnerSchool.performClick()
//            R.id.lnrCharity -> binding.spinnerCharity.performClick()
            R.id.lnrColor -> {
                openOrderDialog()
            }
            R.id.txtBrowzeImage -> checkAndAskCameraPermission()
        }
    }

    private fun openOrderDialog() {
        val dialog = Dialog(requireContext())
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.choose_color_dialog)
        val lnrBlack = dialog.findViewById(R.id.lnrBlack) as LinearLayout
        val lnrWhite = dialog.findViewById(R.id.lnrWhite) as LinearLayout
        val lnrPink = dialog.findViewById(R.id.lnrPink) as LinearLayout
        val lnrBlue = dialog.findViewById(R.id.lnrBlue) as LinearLayout
        val lnrGreen = dialog.findViewById(R.id.lnrGreen) as LinearLayout
        val lnrGray = dialog.findViewById(R.id.lnrGray) as LinearLayout

        lnrBlack.setOnClickListener {
            dialog.dismiss()
            colorName = "Black"
            sendBroadCast("Black")
        }
        lnrWhite.setOnClickListener {
            dialog.dismiss()
            colorName = "White"
            sendBroadCast("White")
        }
        lnrPink.setOnClickListener {
            dialog.dismiss()
            colorName = "Pink"
            sendBroadCast("Pink")
        }
        lnrBlue.setOnClickListener {
            dialog.dismiss()
            colorName = "Blue"
            sendBroadCast("Blue")
        }
        lnrGreen.setOnClickListener {
            dialog.dismiss()
            colorName = "Green"
            sendBroadCast("Green")
        }
        lnrGray.setOnClickListener {
            dialog.dismiss()
            colorName = "Grey"
            sendBroadCast("Grey")
        }
        dialog.show()
    }

    /**
     * Send LocalBroadCast and take the value of action to perform.
     *
     */
    private fun sendBroadCast(color: String) {
        val intent = Intent()
        intent.action = AppConstants.CHANGE_COLOR
        intent.putExtra("color", color)
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
    }

    /**
     * Used for selecting date month,year DatePicker
     */
    private fun showPopUp(datePickerListener: OnDateSetListener) {
        val c = Calendar.getInstance()
        val mYear = c[Calendar.YEAR]
        val mMonth = c[Calendar.MONTH]
        val mDay = c[Calendar.DAY_OF_MONTH]
        val dateDialog =
            DatePickerDialog(
                requireContext(),
                R.style.DatePicker,
                datePickerListener,
                mYear,
                mMonth,
                mDay
            )
        dateDialog.datePicker.maxDate = Date().time
        dateDialog.show()
    }

    /**
     * Interface used to perform  required action.
     */
    interface AddChildFragmentInterface {
        fun openBackFragment()
        fun openReadingFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AddChildFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AddChildFragmentInterface")
        }
    }

    /**
     * Check permissions
     */
    private fun checkAndAskCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), TAKE_PHOTO_REQUEST)
        } else {
            checkAndAskStoragePermission()
        }
    }

    /**
     * Check permissions
     */
    private fun checkAndAskStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                TAKE_STORAGE_REQUEST
            )
        } else {
            openChooser()
        }
    }

    /**
     * Camera request result for permissions
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            TAKE_PHOTO_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkAndAskStoragePermission()
                }
            }
            TAKE_STORAGE_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    openChooser()
                }
            }
        }
    }

    /**
     * open dialog for choose image from gallery and camera
     */
    private fun openChooser() {
        val dialog = Dialog(requireContext())
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_choose_galery)

        val displayRectangle = Rect()
        val window = activity?.window
        window!!.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()

        dialog.window!!.attributes = lp

        val btnCamera = dialog.findViewById<Button>(R.id.btnCamera)
        val btnGallery = dialog.findViewById<Button>(R.id.btnGallery)
        val btnCancel = dialog.findViewById<ImageView>(R.id.btn_CloseDialog)
        dialog.show()
        btnCamera.setOnClickListener {
            takePicture()
            dialog.dismiss()
        }

        btnGallery.setOnClickListener {
            openGallery()
            dialog.dismiss()
        }

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    /**
     * Intent action used for to Capture image from Camera
     */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()
        val uri: Uri = FileProvider.getUriForFile(
            requireContext(), "com.readingmate.android.fileprovider", file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return createTempFile(
            "JPEG_${timeStamp}_", ".jpg", storageDir
        ).apply {
            filePath = absolutePath
        }
    }

    /**
     * Intent action used for to pick image from galley.
     */
    private fun openGallery() {
        val openGalleryIntent = Intent(Intent.ACTION_PICK)
        openGalleryIntent.type = "image/*"
//        openGalleryIntent.type("*/*")
//        openGalleryIntent.type = "file/*"
//        openGalleryIntent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE)
    }

    /**
     * results for choosing  image from Camera and gallery.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val uri = Uri.parse(filePath)
            binding.imgAddChild.setImageURI(uri)
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_GALLERY_CODE) {
            uri = data?.data
            binding.imgAddChild.setImageURI(uri)
//            onSelectFromGalleryResult(data)
            filePath = getRealPathFromURIPath(uri!!, requireActivity())

        } else if (requestCode == 1) {
            if (data != null) {
                val bundle = data.extras
                val bitmap = bundle!!.getParcelable<Bitmap>("data")
                val urii = getImageUri(requireContext(), bitmap!!)
                filePath = getRealPathFromURIPath(urii, requireActivity())
                binding.imgAddChild.setImageBitmap(bitmap)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onSelectFromGalleryResult(data: Intent?) {
        if (data != null) {
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(
                    requireActivity().contentResolver,
                    data.data
                )
                filePath = getStringImage(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun getStringImage(bmp: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

    private fun getImageUri(context: Context, bitmap: Bitmap): Uri {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, byteArrayOutputStream)
        val path =
            MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path)
    }

    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path!!
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            val path = cursor.getString(idx)
            cursor.close()
            path
        }
    }

    private fun setSchoolTypeList() {
        for (i in 0..preferenceChild?.schoolTypeData!!.size - 1) {
            if (from != "dashboard") {
                if (editChildProfileItem!!.schoolTypeName != null) {
                    if (preferenceChild?.schoolTypeData!![i]!!.schoolTypeName == editChildProfileItem!!.schoolTypeName) {
                        binding.spinnerSchool.setSelection(i)
                        break
                    }
                }
            }
        }
    }

    private fun initCharityList() {
        binding.spinnerCharity.onItemSelectedListener = charityListListener
    }

    /**
     * Charity Listener
     */
    private val charityListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            if (from == "dashboard") {
                val charity = charityList[position]
                if (charity.isNotEmpty()) {
                    binding.txtChariry.text = charity
                    charityName = charity
                }
            } else {
                val charity = charityList[position]
                if (charity.isNotEmpty()) {
                    binding.txtChariry.text = charity
                    charityName = charity
                }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun setColorList() {
        for (i in 0..preferenceChild?.colourData!!.size - 1) {
            if (from != "dashboard") {
                if (childDetaildata!!.favColour != null) {
                    if (preferenceChild?.colourData!![i]!!.colourName == childDetaildata!!.favColour) {
                        binding.spinnerColor.setSelection(i)
                        break
                    }
                }
            }
        }
    }

    /**
     * set charity list item to spinner
     */
    private fun setCharityList() {
        for (i in 0..charityList.size - 1) {
            if (charityList[i] == childDetaildata!!.charity) {
                spinnerCharity.setSelection(i)
                break
            }
        }
    }

    private fun getAge(year: Int, month: Int, day: Int): String? {
        //calculating age from dob
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob[year, month] = day
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
        if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
            age--
        }
        return age.toString()
    }

    /**
     * to calculate age
     */
    private fun calculateAge(date: Long): Int {
        val dob = Calendar.getInstance()
        dob.timeInMillis = date
        val today = Calendar.getInstance()
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
        /*if (today[Calendar.DAY_OF_MONTH] < dob[Calendar.DAY_OF_MONTH]) {
            age--
        }*/
        return age
    }

    fun datePicker(context: Context) {
        val calendar = Calendar.getInstance()
        val dateFormatter = SimpleDateFormat("yyyy", Locale.US)
        val datePickerDialog = DatePickerDialog(
            context,
            R.style.DatePicker,
            object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(
                    view: android.widget.DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    val newDate = Calendar.getInstance()
                    newDate.set(year, monthOfYear, dayOfMonth)
                    newDate.set(year, monthOfYear, dayOfMonth)
                    binding.edtChildAge.setText(dateFormatter.format(newDate.time) + " years old")

                }
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    /**
     * BroadcastReceiver for to set specific color
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.CHANGE_COLOR -> {
                    val colorName = intent.extras!!.getString("color")
                    when (colorName) {
                        "Black" -> {
                            txtCircle.setBackgroundResource(R.drawable.red_new_circle)
                        }
                        "White" -> {
                            txtCircle.setBackgroundResource(R.drawable.pink_new_circle)
                        }
                        "Pink" -> {
                            txtCircle.setBackgroundResource(R.drawable.yellow_new_circle)
                        }
                        "Blue" -> {
                            txtCircle.setBackgroundResource(R.drawable.green_new_circle)
                        }
                        "Green" -> {
                            txtCircle.setBackgroundResource(R.drawable.jellyfish_new_circle)
                        }
                        "Grey" -> {
                            txtCircle.setBackgroundResource(R.drawable.blue_new_circle)
                        }
                        "Purple" -> {
                            txtCircle.setBackgroundResource(R.drawable.purple_circle)
                        }
                        else -> {
                            txtCircle.setBackgroundResource(R.drawable.active_readingmate_new_circle)
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }
}
