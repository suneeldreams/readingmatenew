package com.dreams.readingmate.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.dreams.readingmate.R
import com.dreams.readingmate.databinding.ContactUsFragmentBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.util.AlertUtils
import kotlinx.android.synthetic.main.contact_us_fragment.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.spinkit_layout.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ContactUsFragment : Fragment(), KodeinAware, View.OnClickListener {
    override val kodein by kodein()
    private var CountryCodeContainer = "81"
    var valid_email: Boolean = false
    private val viewModel: AuthViewModel by instance<AuthViewModel>()
    private var mListener: ContactUsFragmentInterface? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        return inflater.inflate(R.layout.contact_us_fragment, container, false)
        val binding: ContactUsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.contact_us_fragment, container, false)
        binding.viewmodel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_title.text = resources.getString(R.string.contact_us)
        imgBack.visibility = View.VISIBLE
        imgBack.setOnClickListener(this)
        contactUsBtn.setOnClickListener(this)
        spinKitLayout.setOnClickListener { }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ContactUsFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement ContactUsFragmentInterface")
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.contactUsBtn -> {
                val userName = edtUserName.text.toString().trim()
                val PhoneNumber = PhoneNumber.text.toString().trim()
                val subject = edtEmail.text.toString().trim()
                val edtComment = edtComment.text.toString().trim()
                when {
                    userName.isEmpty() -> {
                        context?.let {
                            AlertUtils.showToast(it, resources.getString(R.string.Please_enter_your_name))
                        }
                        return
                    }
                    PhoneNumber.isEmpty() -> {
                        context?.let {
                            AlertUtils.showToast(it, resources.getString(R.string.Please_enter_contact_number))
                        }
                        return
                    }
                    subject.isEmpty() -> {
                        context?.let {
                            AlertUtils.showToast(it, resources.getString(R.string.Please_enter_your_subject))
                        }
                        return
                    }
                    edtComment.isEmpty() -> {
                        context?.let {
                            AlertUtils.showToast(it, resources.getString(R.string.Please_enter_your_message))
                        }
                        return
                    }
                    else -> {
                        /* viewModel.onContactUSButtonClick(
                                     context as Activity,
                                     Utils.getCustomPref(context!!, UserData.TOKEN),
                                     userName,
                                     PhoneNumber,
                                     subject,
                                     edtComment
                                 )*/
                    }
                }
            }
        }
    }

    interface ContactUsFragmentInterface {
        fun openBackFragment()
    }

//    override fun onStarted() {
//        spinKitLayout.visibility = View.VISIBLE
//    }
//
//    override fun onSuccess(message: String) {
//        spinKitLayout.visibility = View.GONE
//        AlertUtils.showToast(context!!, message)
//    }
//
//    override fun onFailure(message: String) {
//        when {
//            message.contains("401") -> {
//                Utils.handleUnAuthorisedRequest(context!!)
//            }
//            message.contains("409") -> {
//                AlertUtils.showToast(context!!, message)
//            }
//            message.contains("403") -> {
//                AlertUtils.showToast(context!!, message)
//            }
//        }
//        spinKitLayout.visibility = View.GONE
//    }
}
