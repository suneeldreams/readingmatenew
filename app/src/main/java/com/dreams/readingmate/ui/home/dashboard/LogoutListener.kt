package com.dreams.readingmate.ui.home.dashboard


interface LogoutListener {
    fun onStarted()
    fun onSuccess(logout: String)
    fun onFailure(message: String)
}