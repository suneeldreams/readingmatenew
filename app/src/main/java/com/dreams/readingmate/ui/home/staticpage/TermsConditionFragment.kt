package com.dreams.readingmate.ui.home.staticpage

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.dreams.readingmate.R
import com.dreams.readingmate.util.Coroutines
import kotlinx.android.synthetic.main.faq_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class TermsConditionFragment : Fragment(), KodeinAware {

    override val kodein by kodein()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.faq_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun callWebView() = Coroutines.main {
//        Utils.showProgressbar(context!!, context as Activity)
        webview_privacy.webViewClient = WebViewController()
        webview_privacy.settings.javaScriptEnabled = true
        webview_privacy.loadUrl("https://readingmate.co.uk/")
    }

    private inner class WebViewController : WebViewClient() {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.url.toString())
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
//            Utils.hideProgressbar(context!!, context as Activity)
        }
    }

    override fun onResume() {
        super.onResume()
//        Utils.hideProgressbar(context!!, context as Activity)
    }

    override fun onPause() {
        super.onPause()
//        Utils.hideProgressbar(context!!, context as Activity)
    }
}
