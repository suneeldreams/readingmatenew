package com.dreams.readingmate.ui.home.shoping

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.databinding.AddNewLocationBinding
import com.dreams.readingmate.databinding.CheckoutFragmentBinding
import com.dreams.readingmate.databinding.ShopingFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AddNewLocationFragment : BaseFragment<AddNewLocationBinding>(R.layout.add_new_location),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: AddNewLocationFragmentInterface? = null
    private lateinit var binding: AddNewLocationBinding
    private val viewModel: ShopingViewModel by instance<ShopingViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: AddNewLocationBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.add_new_location)
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.btnSaveAddress.setOnClickListener(this)
        observeLiveEvents()
    }
    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.createAddressLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.ADD_LOCATION
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            mListener?.openBackFragment()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * Handle click listener.
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.btnSaveAddress -> {
                val firstName = binding.edtFirstName.text.toString().trim()
                val lastName = binding.edtLastName.text.toString().trim()
                val email = binding.edtEmail.text.toString().trim()
                val mobile = binding.edtMobileNumber.text.toString().trim()
                val address1 = binding.edtLineOfAddress1.text.toString().trim()
                val address2 = binding.edtLineOfAddress2.text.toString().trim()
                val country = binding.edtCountry.text.toString().trim()
                val townCity = binding.edtTownCity.text.toString().trim()
                val pinCode = binding.edtPinCode.text.toString().trim()
                val fullName = "$firstName $lastName"
                val address = "$address1 $address2"
                viewModel.callCreateAddress(
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    requireActivity(),
                    "Mr",
                    fullName,
                    mobile,
                    townCity,
                    email,
                    address,
                    "add4",
                    pinCode,
                    country,
                    "0"
                )
            }
        }
    }


    interface AddNewLocationFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AddNewLocationFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AddNewLocationFragmentInterface")
        }
    }

}