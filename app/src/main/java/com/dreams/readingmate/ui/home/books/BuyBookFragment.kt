package com.dreams.readingmate.ui.home.books

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.CurrentlyReadingAdapter
import com.dreams.readingmate.databinding.BookDetailFragmentBinding
import com.dreams.readingmate.databinding.BooksFragmentBinding
import com.dreams.readingmate.databinding.BuyBookFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.Model
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BuyBookFragment : BaseFragment<BuyBookFragmentBinding>(R.layout.book_detail_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: BuyBookFragmentInterface? = null
    private lateinit var binding: BuyBookFragmentBinding
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    private val viewModel: BooksViewModel by instance<BooksViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: BuyBookFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = "Books"
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgUserProfile.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface BuyBookFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BuyBookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BuyBookFragmentInterface")
        }
    }

    private fun loadList() {
        var comment: Model.Comments = Model.Comments(
            " Hi Shivank", "Hi Buddy", "Shivank Trivedi",
            "https://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg"
        )
        commentList.add(comment)
        commentList.add(comment)
        commentList.add(comment)
        commentList.add(comment)
        commentList.add(comment)
        commentList.add(comment)
    }

}