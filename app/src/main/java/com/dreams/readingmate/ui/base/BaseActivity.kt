package com.dreams.readingmate.ui.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * Base class for all Activities
 * Author: Shivank Trivedi
 * Dated: 20-06-2020
 */

abstract class BaseActivity<VDB : ViewDataBinding> constructor(
        @LayoutRes private val layoutResId: Int) : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // set content view using data-binding
        DataBindingUtil.setContentView<VDB>(this, layoutResId).run {
            // continue initialization
            this.lifecycleOwner = this@BaseActivity
            initComponents(savedInstanceState, this)
        }
    }

    // initialize other components required after onCreate
    abstract fun initComponents(savedInstanceState: Bundle?, binding: VDB)

}