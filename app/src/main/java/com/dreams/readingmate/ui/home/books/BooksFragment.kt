package com.dreams.readingmate.ui.home.books

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.BooksListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.databinding.BooksFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BooksFragment : BaseFragment<BooksFragmentBinding>(R.layout.books_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: BookFragmentInterface? = null
    private lateinit var binding: BooksFragmentBinding
    private var from = ""
    private var bookId = ""
    var prefs: SharedPreferences? = null
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    private val viewModel: BooksViewModel by instance<BooksViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: BooksFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgUserProfile.visibility = View.GONE
        binding.includeToolbar.toolbar_title.text = "Reader List"
        setUpData()
        observeLiveEvents()
    }

    /**
     * get data from bundle and call api to set data when open screen.
     */
    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        val args = arguments
        if (args != null) {
            from = args.getString("from").toString()
            bookId = args.getString("bookId").toString()
        }
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        viewModel.callChildListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            Utils.getCustomPref(requireContext(), UserData.ID)
        )
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.childListLiveData.observe(this, Observer {
            Log.d("childList:-", it.toString())
            binding.recyclerView.adapter = BooksListAdapter(it, mListener, from, this, bookId)
        })
        viewModel.bookFavoriteLiveData.observe(this, Observer {
            callHomeScreen()
        })

        viewModel.bookStatusLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), "This book has been added to your books list.")
            callHomeScreen()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * go to home screen and manage tabs.
     */
    private fun callHomeScreen() {
        prefs!![mUserData.CLICKEDID] = "home"
        Intent(requireContext(), HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(it)
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface BookFragmentInterface {
        fun openBackFragment()
        fun openBuyBookFragment()
        fun openBookDetailFragment(bundle: Bundle)

        fun openAllBookFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BookFragmentInterface")
        }
    }

    fun callFavoriteApi(bookId: String, childId: String) {
        viewModel.addFavoriteBook(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId
        )
    }

    fun callChangeBookStatusApi(bookStatus: String, bookId: String, childId: String) {
        viewModel.bookStatus(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookId, "", bookStatus, childId
        )
    }

}