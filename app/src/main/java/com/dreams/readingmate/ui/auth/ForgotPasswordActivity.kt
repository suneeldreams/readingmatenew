package com.dreams.readingmate.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.databinding.ActivityForgetPasswordBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.util.AlertUtils
import com.jsw.mobility.presentation.navigation.NavigationContract
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ForgotPasswordActivity :
    BaseActivity<ActivityForgetPasswordBinding>(R.layout.activity_forget_password),
    KodeinAware {

    override val kodein by kodein()
    lateinit var binding: ActivityForgetPasswordBinding
    private val viewModel: AuthViewModel by instance<AuthViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ActivityForgetPasswordBinding
    ) {
        this.binding = binding
        binding.viewModel = viewModel

        binding.imgBackPass.setOnClickListener {
            finish()
        }
        binding.btnForgotPassword.setOnClickListener {
            val email = binding.edtEmail.text.toString().trim()
            if (email.isEmpty()) {
                AlertUtils.showToast(
                    this,
                    resources.getString(R.string.Please_enter_your_Email)
                )
            } else {
                viewModel.callForgotPasswordApi(email, this)
            }
        }
    }

}
