package com.dreams.readingmate.ui.home.books

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class BooksViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {
    private val _bookListLiveData = MutableLiveData<BooksDataList>()
    val bookListLiveData: LiveData<BooksDataList>
        get() = _bookListLiveData

    private val _bookDetailLiveData = MutableLiveData<BookDetailsDataItems>()
    val bookDetailLiveData: LiveData<BookDetailsDataItems>
        get() = _bookDetailLiveData

    private val _bookFavoriteLiveData = MutableLiveData<BookDetailsDataItems>()
    val bookFavoriteLiveData: LiveData<BookDetailsDataItems>
        get() = _bookFavoriteLiveData

    private val _removebookFavoriteLiveData = MutableLiveData<BookDetailsDataItems>()
    val removebookFavoriteLiveData: LiveData<BookDetailsDataItems>
        get() = _removebookFavoriteLiveData

    private val _bookReviewLiveData = MutableLiveData<ReviewData>()
    val bookReviewLiveData: LiveData<ReviewData>
        get() = _bookReviewLiveData

    private val _allBookLiveData = MutableLiveData<AllBooksData>()
    val allBookLiveData: LiveData<AllBooksData>
        get() = _allBookLiveData

    private val _childListLiveData = MutableLiveData<List<ChildListItem>>()
    val childListLiveData: LiveData<List<ChildListItem>>
        get() = _childListLiveData

    private val _SeachListLiveData = MutableLiveData<List<FavBookData>>()
    val SeachListLiveData: LiveData<List<FavBookData>>
        get() = _SeachListLiveData

    private val _bookStatusLiveData = MutableLiveData<AddBookShelfData>()
    val bookStatusLiveData: LiveData<AddBookShelfData>
        get() = _bookStatusLiveData

    private val _addCartLiveData = MutableLiveData<AddCartDataItam>()
    val addCartLiveData: LiveData<AddCartDataItam>
        get() = _addCartLiveData

    private val _addBookLiveData = MutableLiveData<AddBookShelfData>()
    val addBookLiveData: LiveData<AddBookShelfData>
        get() = _addBookLiveData

    /*fun callBookListAPI(
        context: Context,
        accessToken: String,
        orderBy: String,
        orderDir: String,
        pageNumber: String,
        pageSize: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.getBookList(accessToken, orderBy, orderDir, pageNumber, pageSize)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _bookListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }*/
    fun callChildListAPI(
        context: Context,
        accessToken: String,
        userId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getChildList(accessToken, userId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _childListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callBookDetailAPI(
        context: Context,
        accessToken: String,
        bookId: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.getBookDetailData(accessToken, bookId, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _bookDetailLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addFavoriteBook(
        context: Context,
        accessToken: String,
        bookId: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.favoriteBook(accessToken, bookId, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    AlertUtils.showToast(context, result.value.message!!)
                    _bookFavoriteLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                    AlertUtils.showToast(context, result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun removeFavoriteBook(
        context: Context,
        accessToken: String,
        bookId: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.removefavoriteBook(accessToken, bookId, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    AlertUtils.showToast(context, result.value.message!!)
                    _removebookFavoriteLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addReview(
        context: Context,
        accessToken: String,
        bookId: String,
        childId: String,
        rating: String,
        review: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.addReview(accessToken, bookId, childId, rating, review)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    AlertUtils.showToast(context, result.value.message!!)
                    _bookReviewLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callAllBooksDataAPI(
        context: Context,
        accessToken: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result =
                    repository.getAllBookData(accessToken, childId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _allBookLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callSearchBookListAPI(
        context: Context,
        accessToken: String,
        search: String,
        pageSize: String,
        cartitems: String,
        filter: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getFavList(accessToken, search, pageSize,cartitems,filter)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _SeachListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun bookStatus(
        mContext: Context,
        accessToken: String,
        bookId: String?,
        pageRead: String?,
        bookStatus: String,
        childId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.bookStatusData(
                    BookStatusDataModel(bookId, pageRead, bookStatus),
                    accessToken,
                    childId
                )
                if (result is ResultState.Success) {
                    _bookStatusLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun addCartData(
        mContext: Context,
        accessToken: String,
        cartData: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addCartData(
                    CartDataModel(cartData), accessToken
                )
                if (result is ResultState.Success) {
                    _addCartLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
//                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }

    fun addBookToBookShelf(
        mContext: Context,
        accessToken: String,
        bookId: String?,
        childId: String?,
        readingStatus: String?
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addBookToBookShelf(
                    accessToken, bookId!!, childId!!, readingStatus
                )
                if (result is ResultState.Success) {
                    _addBookLiveData.postValue(result.value.data)
                    _state.postValue(ProgressState.SUCCESS)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }
}
