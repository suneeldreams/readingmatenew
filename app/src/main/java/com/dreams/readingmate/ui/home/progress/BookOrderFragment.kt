package com.dreams.readingmate.ui.home.progress

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.BookOrderAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.network.responses.RowItems
import com.dreams.readingmate.databinding.BookOrderFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BookOrderFragment : BaseFragment<BookOrderFragmentBinding>(R.layout.book_order_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: BookOrderFragmentInterface? = null
    private lateinit var binding: BookOrderFragmentBinding
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()
    var prefs: SharedPreferences? = null

    override fun initComponents(savedInstanceState: Bundle?, binding: BookOrderFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.recyclerBookShelf.layoutManager = LinearLayoutManager(context)
        callOrderBookApi()
    }

    /**
     * Order book api call.
     */
    private fun callOrderBookApi() {
        viewModel.callOrderBookAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    /**
     * SetUp ToolBar title.
     */
    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.order)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.orderBookListLiveData.observe(this, Observer {
            if (!it.rows.isNullOrEmpty()) {
                binding.recyclerBookShelf.visibility = View.VISIBLE
                binding.txtNoData.visibility = View.GONE
                setBookShelfListData(it.rows)
            } else {
                binding.recyclerBookShelf.visibility = View.GONE
                binding.txtNoData.visibility = View.VISIBLE
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface BookOrderFragmentInterface {
        fun openBackFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openBooksFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BookOrderFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BookOrderFragmentInterface")
        }
    }

    private fun setBookShelfListData(orderBookList: List<RowItems?>?) {
        binding.recyclerBookShelf.adapter =
            BookOrderAdapter(mListener!!, orderBookList)
    }
}