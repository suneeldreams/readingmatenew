package com.dreams.readingmate.ui.auth

import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.CalendarModel
import com.dreams.readingmate.data.network.responses.ChildListItem
import sun.bob.mcalendarview.MCalendarView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Suneel on 30/03/2020.
 */
class ChildCalendar : AppCompatActivity() {
    private var childDetail = ""
    private var childItems1: ChildListItem? = null
    private var childItems: ArrayList<ChildListItem>? = null
    private var chList: ArrayList<CalendarModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calendar_activity)
        setUpData()
    }

    private fun setUpData() {
        val intent = intent
        chList = intent.getParcelableArrayListExtra("hhh")
//        childItems = intent.getStringExtra("calendarData")
        val chList = intent.getStringExtra("listt")
        setCalendar()
    }

    private fun setCalendar() {
        val calendar = Calendar.getInstance().time

        val calendarView =
            findViewById<View>(R.id.childCalendar) as MCalendarView
        calendarView.hasTitle(true)
        val datet = SimpleDateFormat("dd", Locale.US)
        val montht = SimpleDateFormat("MM", Locale.US)
        val yeart = SimpleDateFormat("yyyy", Locale.US)
        val tdate = datet.format(calendar)
        val tmonth = montht.format(calendar)
        val tyear = yeart.format(calendar)
        for (i in 0 until chList!!.size) {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(chList!![i].Date)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            val x = chList!![i].Date
            calendarView.markDate(
                year.toInt(),
                month.toInt(),
                date.toInt()
            )
        }
        /*for (i in childDetail!!.weekData!!.indices) {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childDetail!!.weekData!![i]!!.date!!)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            if (childDetail!!.weekData!![i]!!.isRead!!) {
                if (tdate == date) {
                    calendarView.unMarkDate(
                        year.toInt(),
                        month.toInt(),
                        date.toInt()
                    )
                }
                calendarView.markDate(
                    year.toInt(),
                    month.toInt(),
                    date.toInt()
                )
            } else {
                calendarView.unMarkDate(
                    year.toInt(),
                    month.toInt(),
                    date.toInt()
                )
                if (tdate == date) {
                    calendarView.markDate(
                        DateData(
                            tyear.toInt(),
                            tmonth.toInt(),
                            tdate.toInt()
                        ).setMarkStyle(
                            MarkStyle(
                                MarkStyle.DEFAULT,
                                resources.getColor(R.color.color_active_button)
                            )
                        )
                    )
                }
            }
        }*/
    }
}