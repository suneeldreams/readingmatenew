package com.dreams.readingmate.ui.home.news

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.NewsDetailsFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class NewsDetailsFragment :
    BaseFragment<NewsDetailsFragmentBinding>(R.layout.news_details_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: NewsDetailFragmentInterface? = null
    private lateinit var binding: NewsDetailsFragmentBinding
    private var blogId = ""
    private var sharingURL = ""
    private val viewModel: NewsViewModel by instance<NewsViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: NewsDetailsFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.imgBack.setOnClickListener(this)
        binding.imgBookMark.setOnClickListener(this)
        binding.imgShare.setOnClickListener(this)
        binding.toolbarTitle.text = getString(R.string.news_details)
        setUpData()
        observeLiveEvents()
    }

    /**
     * get data from bundle and call news detail api.
     */
    private fun setUpData() {
        val args = arguments
        if (args != null) {
            blogId = args.getString("blogId").toString()
            callNewsDetailApi()
        }
    }

    /**
     * new detail api
     */
    private fun callNewsDetailApi() {
        viewModel.callNewsDetailListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            blogId
        )
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun callWebView(mainUrl: String) = Coroutines.main {
        binding.webView.webViewClient = WebViewController()
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        binding.webView.loadDataWithBaseURL(
            "",
            mainUrl,
            "text/html",
            "UTF-8",
            null
        )
    }

    /**
     * WebView controller.
     */
    private inner class WebViewController : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
        }
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.newsDetailLiveData.observe(this, Observer {
            binding.txtCategory.text = Utils.getDateForm(it?.createdAt!!) + " . " + it.categoryName
            binding.txtShortDetail.text = it.shortDesc
            sharingURL = it.sharingURL!!
            if (it.isFavorite) {
                binding.imgBookMark.setImageResource(R.drawable.bookmark)
            } else {
                binding.imgBookMark.setImageResource(R.drawable.ic_book_mark)
            }
            it.description?.let { it1 -> callWebView(it1) }
            it.img?.let { it1 ->
                Utils.displayImage(
                    requireContext(),
                    it1, binding.imgNews, R.drawable.ic_place_holder
                )
            }

        })
        viewModel.addRemoveFavLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_NEWS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callNewsDetailApi()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.imgBookMark -> {
                viewModel.addBookMark(
                    requireContext(),
                    Utils.getCustomPref(
                        requireContext(),
                        com.dreams.readingmate.data.db.UserData.TOKEN
                    ),
                    blogId
                )
            }
            R.id.imgShare -> {
                ShareCompat.IntentBuilder.from(requireActivity())
                    .setType("text/plain")
                    .setChooserTitle("Chooser title")
                    .setText(sharingURL)
                    .startChooser();
            }
        }
    }


    interface NewsDetailFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NewsDetailFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement NewsDetailFragmentInterface")
        }
    }

}