package com.dreams.readingmate.ui.home.profile

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.ProfileFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.add_child_fragment.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.shoping_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ProfileFragment : BaseFragment<ProfileFragmentBinding>(R.layout.profile_fragment),
    KodeinAware, NavigationContract, View.OnClickListener {

    override val kodein by kodein()
    private var mListener: ProfileFragmentInterface? = null
    private lateinit var binding: ProfileFragmentBinding
    private var memberShipId = ""
    private val viewModel: ProfileViewModel by instance<ProfileViewModel>()
    override fun initComponents(savedInstanceState: Bundle?, binding: ProfileFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolbar()
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.UPDATE_PROFILE))
        observeNavigationEvents()
        observeLiveEvents()
        setUpData()
        binding.homeRootLayout.setOnClickListener { }
    }

    /**
     * set initial data when screen open.
     */
    private fun setUpData() {
        val image = Utils.getCustomPref(requireContext(), UserData.PROFILE_IMAGE)
        if (image.isNotEmpty()) {
            Utils.displayCircularImage(requireContext(), binding.imgUserProfile, image)
        }
        binding.userName.text = Utils.getCustomPref(requireContext(), UserData.NAME)
        binding.txtCharity.text = Utils.getCustomPref(requireContext(), UserData.CHARITY)
        binding.txtEmail.text = Utils.getCustomPref(requireContext(), UserData.EMAIL)
        binding.txtPhoneNumber.text = Utils.getCustomPref(
            requireContext(),
            UserData.MOBILE
        )
        binding.txtGender.text = Utils.getCustomPref(requireContext(), UserData.GENDER)
        callMembershipApi()
    }

    private fun callMembershipApi() {
        viewModel.callMembershipStatusAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    /**
     * setUp toolBar
     */
    private fun setUpToolbar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.profileToolbar.imgBack.visibility = View.VISIBLE
        binding.profileToolbar.imgBack.setOnClickListener(this)
        binding.btnBuy.setOnClickListener(this)
        binding.profileToolbar.toolbar_title.text = resources.getString(R.string.profile)
    }

    override fun observeLiveEvents() {
        viewModel.membershipStatusLiveData.observe(this, Observer {
            binding.noMemberShip.visibility = View.GONE
            binding.btnBuy.visibility = View.GONE
            memberShipId = it.userMembership!!.id!!
            if (it.userMembership.totalRemainingBooks == 0) {
                binding.txtPlanIntervalCount.text = "0"
            } else {
                binding.txtPlanIntervalCount.text = it.userMembership.totalRemainingBooks.toString()
            }
            binding.membershipName.text = it.planName
            binding.membershipDesc.text = it.planDescription
            binding.membershipPrice.text =
                getString(R.string.currency_pound) + it.planAmount + "/" + it.planInterval
            binding.txtExpiryMembership.text = Utils.getDateForm(it.userMembership.endDate!!)
            if (it.userMembership.status != "Active") {
                binding.txtCancelMemberShip.visibility = View.GONE
            } else {
                binding.txtCancelMemberShip.visibility = View.VISIBLE
            }
        })
        viewModel.cancelSubscriptionLiveData.observe(this, Observer {
            callMembershipApi()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
        viewModel.errorCode.observe(this, Observer {
            if (it == 400) {
                callMembershipApi()
            } else if (it == 404) {
                binding.rltMembershipName.visibility = View.GONE
                binding.membershipPrice.visibility = View.GONE
                binding.rltUpgrade.visibility = View.GONE
                binding.noMemberShip.visibility = View.VISIBLE
                binding.btnBuy.visibility = View.VISIBLE
            }
        })
    }

    override fun observeNavigationEvents() {
        binding.btnUpgrade.setOnClickListener(this)
        binding.txtCancelMemberShip.setOnClickListener(this)
        binding.btnUpdateProfile.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.btnUpdateProfile -> mListener?.openUpdateProfileFragment()
            R.id.btnUpgrade -> mListener?.openSubsciptionFragment()
            R.id.btnBuy -> mListener?.openSubsciptionFragment()
            R.id.txtCancelMemberShip -> {
                viewModel.cancelSubscription(
                    requireContext(),
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    memberShipId
                )
            }
        }
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.UPDATE_PROFILE -> {
                    val image = Utils.getCustomPref(requireContext(), UserData.PROFILE_IMAGE)
                    if (image.isNotEmpty()) {
                        Utils.displayCircularImage(requireContext(), binding.imgUserProfile, image)
                    }
                    binding.userName.text = Utils.getCustomPref(requireContext(), UserData.NAME)
                    binding.txtCharity.text =
                        Utils.getCustomPref(requireContext(), UserData.CHARITY)
                    binding.txtEmail.text = Utils.getCustomPref(requireContext(), UserData.EMAIL)
                    binding.txtPhoneNumber.text = Utils.getCustomPref(
                        requireContext(),
                        UserData.MOBILE
                    )
                    binding.txtGender.text = Utils.getCustomPref(requireContext(), UserData.GENDER)
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }


    interface ProfileFragmentInterface {
        fun openBackFragment()
        fun openUpdateProfileFragment()
        fun openSubsciptionFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProfileFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ProfileFragmentInterface")
        }
    }
}