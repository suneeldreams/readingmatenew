package com.dreams.readingmate.ui.home.profile

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.UpdateProfileFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class UpdateProfileFragment :
    BaseFragment<UpdateProfileFragmentBinding>(R.layout.update_profile_fragment),
    KodeinAware, NavigationContract, View.OnClickListener {
    override val kodein by kodein()
    private val TAKE_PHOTO_REQUEST = 101
    private val TAKE_STORAGE_REQUEST = 102
    private val REQUEST_GALLERY_CODE = 103
    private val REQUEST_IMAGE_CAPTURE = 104
    private var filePath = ""
    var uri: Uri? = null
    private lateinit var genderList: Array<String>
    private lateinit var charityList: Array<String>
    private var mListener: UpdateProfileFragmentInterface? = null
    private lateinit var binding: UpdateProfileFragmentBinding
    private val viewModel: ProfileViewModel by instance<ProfileViewModel>()
    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: UpdateProfileFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolbar()
        setUpData()
        observeNavigationEvents()
        initGenderList()
        initCharityList()
        observeLiveEvents()
    }

    private fun initGenderList() {
        binding.spnrGender.onItemSelectedListener = genderListListener
    }

    private fun initCharityList() {
        binding.spnrCharity.onItemSelectedListener = charityListListener
    }

    /**
     * Listener for select gender.
     */
    private val genderListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val gender = genderList[position]
            if (gender.isNotEmpty()) {
                binding.txtGender.text = gender
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val charityListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val charity = charityList[position]
            if (charity.isNotEmpty()) {
                binding.txtCharity.text = charity
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * set initial data when screen loads.
     */
    private fun setUpData() {
        val image = Utils.getCustomPref(requireContext(), UserData.PROFILE_IMAGE)
        if (image.isNotEmpty()) {
            Utils.displayCircularImage(requireContext(), binding.profileImg, image)
        }
        binding.edtName.setText(Utils.getCustomPref(requireContext(), UserData.NAME))
        binding.edtEmail.setText(Utils.getCustomPref(requireContext(), UserData.EMAIL))
        binding.edtMobile.setText(Utils.getCustomPref(requireContext(), UserData.MOBILE))
        binding.txtGender.text = Utils.getCustomPref(requireContext(), UserData.GENDER)
        genderList = resources.getStringArray(R.array.gender_list)
        charityList = resources.getStringArray(R.array.charity_list)
        binding.spnrGender.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, genderList)
        binding.spnrCharity.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, charityList)
    }

    private fun setUpToolbar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.updateProfileToolbar.imgBack.visibility = View.VISIBLE
        binding.updateProfileToolbar.toolbar_title.text =
            resources.getString(R.string.update_profile)
    }

    /**
     * Click event listener
     */
    override fun observeNavigationEvents() {
        binding.updateProfileToolbar.imgBack.setOnClickListener(this)
//        binding.updateProfileToolbar.setOnClickListener(this)
        binding.iconFrame.setOnClickListener(this)
        binding.btnUpdate.setOnClickListener(this)
        binding.rltGender.setOnClickListener(this)
        binding.rltCharity.setOnClickListener(this)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    override fun observeLiveEvents() {
        viewModel.updateProfileLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), "Profile updated successfully.")
            it.name?.let { it1 -> Utils.saveCustomPref(requireContext(), UserData.NAME, it1) }
            it.email?.let { it1 -> Utils.saveCustomPref(requireContext(), UserData.EMAIL, it1) }
            it.mobile?.let { it1 -> Utils.saveCustomPref(requireContext(), UserData.MOBILE, it1) }
            it.gender?.let { it1 -> Utils.saveCustomPref(requireContext(), UserData.GENDER, it1) }
            it.charity?.let { it1 -> Utils.saveCustomPref(requireContext(), UserData.CHARITY, it1) }
            it.profileImage?.let { it1 ->
                Utils.saveCustomPref(
                    requireContext(),
                    UserData.PROFILE_IMAGE,
                    it1
                )
            }
            val intent = Intent()
            intent.action = AppConstants.UPDATE_PROFILE
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
        })
    }

    /**
     * handle click events.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.icon_frame -> checkAndAskCameraPermission()
            R.id.rltGender -> binding.spnrGender.performClick()
            R.id.rltCharity -> binding.spnrCharity.performClick()
            R.id.btnUpdate -> {
                var base64 = ""
                try {
                    if (filePath == "") {
                        val image = Utils.getCustomPref(requireContext(), UserData.PROFILE_IMAGE)
                        if (image.isNotEmpty()) {
                            val bm: Bitmap = BitmapFactory.decodeFile(image)
                            val baos = ByteArrayOutputStream()
                            bm.compress(Bitmap.CompressFormat.JPEG, 0, baos)
                            val byteArrayImage: ByteArray = baos.toByteArray()
                            base64 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
                        }
                    } else {
                        val bm: Bitmap = BitmapFactory.decodeFile(filePath)
                        val baos = ByteArrayOutputStream()
                        bm.compress(Bitmap.CompressFormat.JPEG, 0, baos)
                        val byteArrayImage: ByteArray = baos.toByteArray()
//                    val base64 = Base64.getEncoder().encodeToString(byteArrayImage)
                        base64 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
//                    image = filePath
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                val name = binding.edtName.text.toString().trim()
                val email = binding.edtEmail.text.toString().trim()
                val number = binding.edtMobile.text.toString().trim()
                val gendar = binding.txtGender.text.toString().trim()
                val charity = binding.txtCharity.text.toString().trim()
                val validEmail = Utils.checkEmailValid(email)
                updateProfileValidation(name, email, number, validEmail, base64, gendar, charity)
            }
        }
    }

    /**
     * Validation for update profile.
     */
    private fun updateProfileValidation(
        name: String,
        email: String,
        number: String,
        validEmail: Boolean,
        image: String,
        gendar: String,
        charity: String
    ) {
        if (name.isEmpty()) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_enter_your_name)
            )
            return
        } else if (email.isEmpty()) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_enter_your_Email)
            )
            return
        } else if (!validEmail) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_enter_valid_Email)
            )
            return
        } /*else if (number.isEmpty()) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_enter_contact_number)
            )
            return
        }*/ else if (gendar.isEmpty()) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_enter_gemder)
            )
            return
        } else if (charity.isEmpty()) {
            AlertUtils.showToast(
                requireContext(),
                resources.getString(R.string.Please_select_charity)
            )
            return
        } else {
            viewModel.callUploadImageApi(
                requireContext(),
                Utils.getCustomPref(requireContext(), UserData.TOKEN),
                name,
                number,
                email,
                gendar,
                charity,
                image
            )
        }

    }

    private fun checkAndAskCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), TAKE_PHOTO_REQUEST)
        } else {
            checkAndAskStoragePermission()
        }
    }

    private fun checkAndAskStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                TAKE_STORAGE_REQUEST
            )
        } else {
            openChooser()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            TAKE_PHOTO_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkAndAskStoragePermission()
                }
            }
            TAKE_STORAGE_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    openChooser()
                }
            }
        }
    }

    private fun openChooser() {
        val dialog = Dialog(requireContext())
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_choose_galery)

        val displayRectangle = Rect()
        val window = activity?.window
        window!!.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()

        dialog.window!!.attributes = lp

        val btnCamera = dialog.findViewById<Button>(R.id.btnCamera)
        val btnGallery = dialog.findViewById<Button>(R.id.btnGallery)
        val btnCancel = dialog.findViewById<ImageView>(R.id.btn_CloseDialog)
        dialog.show()
        btnCamera.setOnClickListener {
            takePicture()
            dialog.dismiss()
        }

        btnGallery.setOnClickListener {
            openGallery()
            dialog.dismiss()
        }

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()
        val uri: Uri = FileProvider.getUriForFile(
            requireContext(), "com.readingmate.android.fileprovider", file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return createTempFile(
            "JPEG_${timeStamp}_", ".jpg", storageDir
        ).apply {
            filePath = absolutePath
        }
    }

    private fun openGallery() {
        val openGalleryIntent = Intent(Intent.ACTION_PICK)
        openGalleryIntent.type = "image/*"
        startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE)
    }

    /**
     * return results after choose and capture image.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val uri = Uri.parse(filePath)
            Utils.displayCircularImage(requireContext(), binding.profileImg, uri)
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_GALLERY_CODE) {
            uri = data?.data
            filePath = getRealPathFromURIPath(uri!!, requireActivity())
            Utils.displayCircularImage(requireContext(), binding.profileImg, uri.toString())

        } else if (requestCode == 1) {
            if (data != null) {
                val bundle = data.extras
                val bitmap = bundle!!.getParcelable<Bitmap>("data")
                val urii = getImageUri(requireContext(), bitmap!!)
                binding.profileImg.setImageURI(urii)
                filePath = getRealPathFromURIPath(urii, requireActivity())
                Utils.displayCircularImage(requireContext(), binding.profileImg, uri.toString())
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getImageUri(context: Context, bitmap: Bitmap): Uri {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, byteArrayOutputStream)
        val path =
            MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path)
    }

    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path!!
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            val path = cursor.getString(idx)
            cursor.close()
            path
        }
    }


    interface UpdateProfileFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is UpdateProfileFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement UpdateProfileFragmentInterface")
        }
    }
}