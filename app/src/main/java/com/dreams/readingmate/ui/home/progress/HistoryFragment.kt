package com.dreams.readingmate.ui.home.progress

import android.accounts.AccountManager
import android.content.ContentResolver
import android.content.Context
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.utils.DateUtils
import com.dreams.readingmate.R
import com.dreams.readingmate.data.network.responses.ChildDetails
import com.dreams.readingmate.data.network.responses.ChildListItem
import com.dreams.readingmate.databinding.HistoryFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.DrawableUtils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import sun.bob.mcalendarview.MarkStyle
import sun.bob.mcalendarview.vo.DateData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class HistoryFragment :
    BaseFragment<HistoryFragmentBinding>(R.layout.history_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: HistoryFragmentInterface? = null
    private lateinit var binding: HistoryFragmentBinding
    private var childItems: ChildListItem? = null
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: HistoryFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        setNewCalendar()
        observeLiveEvents()
    }

    /**
     * setting reading date on calender
     */
    private fun setNewCalendar() {
        val events: ArrayList<EventDay> = ArrayList()
        val calendar = Calendar.getInstance()
        val dateCal = Calendar.getInstance()
        events.add(EventDay(calendar, R.drawable.blue_circle))
        binding.calendarView.setEvents(events)
        for (i in childItems!!.readingHistory!!.indices) {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateF = SimpleDateFormat("dd", Locale.US)
            val monthF = SimpleDateFormat("MM", Locale.US)
            val yearF = SimpleDateFormat("yyyy", Locale.US)

            val newDate = df.parse(childItems?.readingHistory?.get(i)?.createdAt)
            val date = dateF.format(newDate!!)
            val month = monthF.format(newDate)
            val year = yearF.format(newDate)
            dateCal.set(year.toInt(), month.toInt(), date.toInt())
        }
        binding.calendarView.setDate(dateCal)
        binding.calendarView.showCurrentMonthPage()
    }

    private fun setUpData() {
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        val args = arguments
        if (args != null) {
            childItems = args.getParcelable("history")
            binding.txtChildName.text = childItems!!.childName + "'s" + " activity"
        }
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.activity_history)
    }


    private fun observeLiveEvents() {
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface HistoryFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HistoryFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HistoryFragmentInterface")
        }
    }
}