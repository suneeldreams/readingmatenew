package com.dreams.readingmate.ui.home.enumclass

enum class Tabs(val value: String, val id: Int) {
    HOME("Home", 1),
    PROGRESS("Progress", 2),
    BOOKS("Books", 3),
    SHOP("Shop", 4)
}