package com.dreams.readingmate.ui.auth

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.databinding.StripePaymentBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentConfiguration
import com.stripe.android.PaymentIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.Card
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.model.StripeIntent
import com.stripe.android.view.CardInputWidget
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class CheckoutActivity : BaseActivity<StripePaymentBinding>(R.layout.stripe_payment),
    KodeinAware {
    override val kodein by kodein()
    lateinit var binding: StripePaymentBinding
    private val viewModel: AuthViewModel by instance<AuthViewModel>()
    private lateinit var paymentIntentClientSecret: String
    private lateinit var memberShippaymentIntentClientSecret: String
    private lateinit var cvv: String
    private lateinit var expYear: String
    private lateinit var expMonth: String
    private lateinit var cardNo: String
    private lateinit var stripe: Stripe
    private var from = ""
    var prefs: SharedPreferences? = null
    private lateinit var cardPaymentData: Model.cardItem
    private lateinit var memberShipModel: Model.membershipItem
    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: StripePaymentBinding
    ) {
        this.binding = binding
        binding.viewModel = viewModel
        setUpData()
        observeLiveEvents()
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(this)
        val intent = intent
        val isFrom = intent.getStringExtra("from")
        if (isFrom == "membership") {
            memberShipModel = intent.getSerializableExtra("memberShipModel") as Model.membershipItem
            memberShippaymentIntentClientSecret = memberShipModel.clientSecret
            from = memberShipModel.from
            cardNo = memberShipModel.cardNumber
            expMonth = memberShipModel.cardExpMonth
            expYear = memberShipModel.cardExpYear
            cvv = memberShipModel.cvc
        } else {
            cardPaymentData = intent.getSerializableExtra("cardModel") as Model.cardItem
            paymentIntentClientSecret = cardPaymentData.clientSecret
            cardNo = cardPaymentData.cardNo
            expMonth = cardPaymentData.expMonth
            expYear = cardPaymentData.expYear
            cvv = cardPaymentData.cvv
        }

        /*Stripe Test and live key setup here*/
        PaymentConfiguration.init(
            this,
            AppConstants.STRIPE_NEW_TEST_KEY
        )
        startCheckout()
    }

    private fun observeLiveEvents() {
        viewModel.createOrderLiveData.observe(this, Observer {
            openOrderDialog(it.orderNumber)
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(this, it)
            }
        })
    }

    private fun startCheckout() {
        // Hook up the pay button to the card widget and stripe instance
        val payButton: Button = findViewById(R.id.payButton)
        payButton.setOnClickListener {
            if (from == "membership") {
                val testCard =
                    Card.create(cardNo, expMonth.toInt(), expYear.toInt(), cvv)
                val paramsCard =
                    testCard.toPaymentMethodsParams()
                val confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(
                        paramsCard,
                        memberShippaymentIntentClientSecret
                    )
                confirmParams.receiptEmail = Utils.getCustomPref(this, UserData.EMAIL)
                stripe = Stripe(
                    applicationContext,
                    PaymentConfiguration.getInstance(applicationContext).publishableKey
                )
                stripe.confirmPayment(this, confirmParams)
            } else {
                val testCard =
                    Card.create(cardNo, expMonth.toInt(), expYear.toInt(), cvv)
                val paramsCard =
                    testCard.toPaymentMethodsParams()
                val confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(paramsCard, paymentIntentClientSecret)
                confirmParams.receiptEmail = Utils.getCustomPref(this, UserData.EMAIL)

                stripe = Stripe(
                    applicationContext,
                    PaymentConfiguration.getInstance(applicationContext).publishableKey
                )
                stripe.confirmPayment(this, confirmParams)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, object : ApiResultCallback<PaymentIntentResult> {
            override fun onSuccess(result: PaymentIntentResult) {
                val paymentIntent = result.intent
                val status = paymentIntent.status
                if (status == StripeIntent.Status.Succeeded) {
                    if (from == "membership") {
                        checkOutApi(
                            memberShipModel.name,
                            memberShipModel.email,
                            memberShipModel.nameOnCard,
                            memberShipModel.cardNumber,
                            memberShipModel.cvc,
                            memberShipModel.cardExpMonth,
                            memberShipModel.cardExpYear,
                            memberShipModel.postalCode,
                            memberShipModel.subscriptionPlanId,
                            memberShipModel.isUpfront
                        )
                    } else {
                        callOrderApi(
                            cardPaymentData.deliveryCharge,
                            cardPaymentData.grandTotal,
                            cardPaymentData.ititlename,
                            cardPaymentData.iname,
                            cardPaymentData.iaddr1,
                            cardPaymentData.iaddr2,
                            cardPaymentData.iaddr3,
                            cardPaymentData.iaddr4,
                            cardPaymentData.ipcode,
                            cardPaymentData.icountry,
                            cardPaymentData.dtitlename,
                            cardPaymentData.dname,
                            cardPaymentData.daddr1,
                            cardPaymentData.daddr2,
                            cardPaymentData.daddr3,
                            cardPaymentData.daddr4,
                            cardPaymentData.dpcode,
                            cardPaymentData.dcountry,
                            cardPaymentData.trackingsafeplace,
                            cardPaymentData.comm1,
                            cardPaymentData.comm2,
                            cardPaymentData.comm3,
                            cardPaymentData.comm4,
                            cardPaymentData.clientSecret,
                            paymentIntent.id,
                            cardPaymentData.cardData,
                            cardPaymentData.couponCode
                        )
                    }
                } else {
                    AlertUtils.showToast(applicationContext, "Payment failed")
                }
            }

            override fun onError(e: Exception) {
                AlertUtils.showToast(applicationContext, "Payment failed")
            }
        })
    }

    private fun checkOutApi(
        name: String,
        email: String,
        nameOnCard: String,
        cardNumber: String,
        cvv: String,
        cardExpMonth: String,
        cardExpYear: String,
        postalCode: String,
        subscriptionPlanId: String,
        upfront: Boolean
    ) {
        viewModel.createMemberShipPlan(
            this,
            Utils.getCustomPref(this, UserData.TOKEN),
            Utils.getCustomPref(this, UserData.NAME),
            Utils.getCustomPref(this, UserData.EMAIL),
            nameOnCard,
            cardNumber,
            cardExpMonth,
            cardExpYear,
            postalCode,
            subscriptionPlanId,
            cvv,
            upfront
        )
    }

    private fun displayAlert(
        activity: Activity?,
        title: String,
        message: String,
        restartDemo: Boolean = false
    ) {
        if (activity == null) {
            return
        }
        runOnUiThread {
            val builder = AlertDialog.Builder(activity!!)
            builder.setTitle(title)
            builder.setMessage(message)
            if (restartDemo) {
                builder.setPositiveButton("Restart demo") { _, _ ->
                    val cardInputWidget =
                        findViewById<CardInputWidget>(R.id.cardInputWidget)
                    cardInputWidget.clear()
                    startCheckout()
                }
            } else {
                builder.setPositiveButton("Ok", null)
            }
            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun callOrderApi(
        deliveryCharge: String,
        grandTotal: String,
        ititlename: String,
        iname: String,
        iaddr1: String,
        iaddr2: String,
        iaddr3: String,
        iaddr4: String,
        ipcode: String,
        icountry: String,
        dtitlename: String,
        dname: String,
        daddr1: String,
        daddr2: String,
        daddr3: String,
        daddr4: String,
        dpcode: String,
        dcountry: String,
        trackingsafeplace: String,
        comm1: String,
        comm2: String,
        comm3: String,
        comm4: String,
        clientSecretKey: String,
        tokenId: String?,
        cardData: String,
        couponCode: String
    ) {
        viewModel.createOrder(
            this,
            Utils.getCustomPref(this, UserData.TOKEN),
            cardData,
            tokenId!!,
            deliveryCharge,
            grandTotal,
            ititlename,
            iname,
            iaddr1,
            iaddr2,
            iaddr3,
            iaddr4,
            ipcode,
            icountry,
            dtitlename,
            dname,
            daddr1,
            daddr2,
            daddr3,
            daddr4,
            dpcode,
            dcountry,
            trackingsafeplace,
            comm1,
            comm2,
            comm3,
            comm4,
            couponCode
        )
    }

    private fun openOrderDialog(orderNumber: Long?) {
        val dialog = Dialog(this)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.order_confirmation_dialog)
        val btnContinue = dialog.findViewById(R.id.btnContinueShopping) as Button
        val txtGoTOOrder = dialog.findViewById(R.id.txtGoTOOrder) as TextView
        val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
        txtMessage.text =
            resources.getString(R.string.Your_order_successfull)
        /*txtMessage.text =
            resources.getString(R.string.Your_order_confirmation_number_is) + " " + orderNumber + "."*/
        btnContinue.setOnClickListener {
            dialog.dismiss()
            prefs!![mUserData.CLICKEDID] = "home"
            Utils.saveCustomPref(this, UserData.IS_MEMBERSHIP, "0")
            Utils.saveCustomPref(this, UserData.REMAINING_BOOKS, "0")

            startActivity(
                Intent(
                    this,
                    HomeActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
        txtGoTOOrder.setOnClickListener {
            dialog.dismiss()
            prefs!![mUserData.CLICKEDID] = "home"
            Utils.saveCustomPref(this, UserData.IS_MEMBERSHIP, "0")
            Utils.saveCustomPref(this, UserData.REMAINING_BOOKS, "0")
            startActivity(
                Intent(
                    this,
                    HomeActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        }
        dialog.show()
    }
}