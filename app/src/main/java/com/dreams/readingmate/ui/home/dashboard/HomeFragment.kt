package com.dreams.readingmate.ui.home.dashboard

import android.app.Activity
import android.app.Dialog
import android.content.*
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.ChildListAdapter
import com.dreams.readingmate.data.adapter.NextReadBookListAdapter
import com.dreams.readingmate.data.adapter.WeekDateAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.HomeFragmentBinding
import com.dreams.readingmate.ui.auth.LoginActivity
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : BaseFragment<HomeFragmentBinding>(R.layout.home_fragment), NavigationContract,
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var dateList = ArrayList<String>()
    private var dayList = ArrayList<String>()
    private var isFirstTime = true
    private var ratingChildId = ""
    private var changeStatus = ""
    private var ratingbookId = ""
    var prefs: SharedPreferences? = null
    private var mListener: HomeFragmentInterface? = null
    private lateinit var binding: HomeFragmentBinding
    private val viewModel: HomeViewModel by instance<HomeViewModel>()
    private var preferenceChild: PreferenceChildItems? = null

    override fun initComponents(savedInstanceState: Bundle?, binding: HomeFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        callMembershipApi()
        callPreferenceApi()
        setUpData()
        observeLiveEvents()
        observeNavigationEvents()
        getCurrentWeekDate()
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.REFRESH))
    }
/*get current week data*/
    private fun getCurrentWeekDate() {
        val c = GregorianCalendar.getInstance()
        c.add(Calendar.DAY_OF_WEEK, -1)
        c.firstDayOfWeek = Calendar.MONDAY
        c[Calendar.DAY_OF_WEEK] = c.firstDayOfWeek
        c.add(Calendar.DAY_OF_WEEK, -1)
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val startDate: String
        val endDate: String
        startDate = df.format(c.time)
        c.add(Calendar.DAY_OF_MONTH, 6)
        endDate = df.format(c.time)
        val df2: DateFormat = SimpleDateFormat("dd")
        val newDate = df.parse(endDate)
        val currentFormattedDate = df2.format(newDate)
        getAllDates(startDate, endDate, currentFormattedDate)
    }

    private fun getAllDates(
        str_date: String,
        end_date: String,
        currentFormattedDate: String
    ) {
        val dates = ArrayList<Date>()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val startDate = formatter.parse(str_date) as Date
        val endDate = formatter.parse(end_date) as Date
        val interval = 24 * 1000 * 60 * 60.toLong() // 1 hour in millis
        val endTime =
            endDate.time // create your endtime here, possibly using Calendar or Date
        var curTime = startDate.time
        while (curTime <= endTime) {
            dates.add(Date(curTime))
            curTime += interval
        }
        for (i in dates.indices) {
            val ds = formatter.format(dates[i])
            val dt1 = formatter.parse(ds)
            println(" Date is ...$ds")
            val dayf: DateFormat = SimpleDateFormat("EEE")
            val df: DateFormat = SimpleDateFormat("dd")
            dayList.add(dayf.format(dt1))
            dateList.add(df.format(dt1))
        }
        setData(dayList, dateList, currentFormattedDate)
    }

    private fun setData(
        dayList: ArrayList<String>,
        dateList: ArrayList<String>,
        currentDate: String
    ) {
        binding.recyclerDate.adapter =
            WeekDateAdapter(requireContext(), dayList, dateList, currentDate)
    }

    private fun callMembershipApi() {
        viewModel.callMembershipStatusAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    private fun callPreferenceApi() {
        viewModel.callPreferenceChildAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN)
        )
    }

    private fun callChildListApi() {
        viewModel.callChildListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            Utils.getCustomPref(requireContext(), UserData.ID)
        )
    }

    private fun setUpData() {
        binding.recyclerHome.layoutManager = LinearLayoutManager(context)
        binding.recyclerDate.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.toolbar_title.text = getString(R.string.dashboard)
        binding.includeToolbar.imgAdd.visibility = View.VISIBLE
        binding.includeToolbar.imgRecommBook.visibility = View.VISIBLE
//        binding.includeToolbar.imgSetting.visibility = View.VISIBLE
        binding.includeToolbar.imgAdd.setOnClickListener(this)
        binding.includeToolbar.imgRecommBook.setOnClickListener(this)
//        binding.includeToolbar.imgSetting.setOnClickListener(this)
        binding.btnAddChild.setOnClickListener(this)
        prefs = mUserData.customPrefs(requireContext())
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    override fun observeLiveEvents() {
        viewModel.userDetailLiveData.observe(this, Observer {
            preferenceChild = it
            callChildListApi()
        })

        viewModel.childListLiveData.observe(this, Observer {
            if (it.isNotEmpty()) {
                binding.rltPlaceHolder.visibility = View.GONE
                binding.recyclerHome.visibility = View.VISIBLE
                binding.recyclerHome.adapter =
                    ChildListAdapter(
                        it,
                        mListener!!,
                        this,
                        preferenceChild,
                        isFirstTime,
                        changeStatus
                    )

            } else {
                (context as HomeActivity).childEmpty = "0"
                binding.recyclerHome.adapter = null
                binding.recyclerHome.visibility = View.GONE
                binding.rltPlaceHolder.visibility = View.VISIBLE
                val intent = Intent()
                intent.action = AppConstants.DE_ACTIVE_EVENT
                LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            }
        })

        viewModel.removeChildLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), it.message!!)
            callChildListApi()
        })
        viewModel.hardRatingLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), it.message!!)
            openEnjoyDialog(ratingChildId, ratingbookId)
        })

        viewModel.enjoyRatingLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), it.message!!)
            callChangeBookStatusApi("2", ratingbookId, ratingChildId)
            callPreferenceApi()
//            nextBookRead(ratingChildId)
        })
        viewModel.bookStatusLiveData.observe(this, Observer {
            val intent2 = Intent()
            intent2.action = AppConstants.REFRESH
            intent2.action = "status"
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent2)
            nextBookRead(ratingChildId)
        })
        viewModel.updateTickDataLiveData.observe(this, Observer {
            AlertUtils.showToast(
                requireContext(),
                getString(R.string.good_work)
            )
            callChildListApi()
        })
        viewModel.removeTickDataLiveData.observe(this, Observer {
            AlertUtils.showToast(
                requireContext(),
                getString(R.string.reading_removed)
            )
            callChildListApi()
        })
        viewModel.nextBookReadLiveData.observe(this, Observer {
//            openNextBookReadDialog(requireContext(), it)
        })

        viewModel.membershipStatusLiveData.observe(this, Observer {
            Utils.saveCustomPref(requireContext(), UserData.IS_MEMBERSHIP, "1")
            if (it.userMembership!!.totalRemainingBooks != 0) {
                Utils.saveCustomPref(
                    requireContext(),
                    UserData.REMAINING_BOOKS,
                    it.userMembership.totalRemainingBooks.toString()
                )
            } else {
                Utils.saveCustomPref(
                    requireContext(),
                    UserData.REMAINING_BOOKS,
                    "0"
                )
            }
        })
        viewModel.errorCode.observe(this, Observer {
            if (it == 404) {
                Utils.saveCustomPref(requireContext(), UserData.IS_MEMBERSHIP, "0")
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    fun callChangeBookStatusApi(bookStatus: String, bookId: String, childId: String) {
        viewModel.bookStatus(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookId, "", bookStatus, childId
        )
    }

    private fun openNextBookReadDialog(
        context: Context,
        listItems: List<FavBookData>
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.next_read_popup)
        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        val recyclerNextBook = dialog.findViewById(R.id.recyclerNextBook) as RecyclerView
        val txtEmptyBook = dialog.findViewById(R.id.txtEmptyBook) as TextView
        val imgCancel = dialog.findViewById(R.id.imgCancel) as ImageView
        recyclerNextBook.layoutManager = GridLayoutManager(activity, 2)
        if (listItems.isNotEmpty()) {
            recyclerNextBook.visibility = View.VISIBLE
            txtEmptyBook.visibility = View.GONE
            imgCancel.visibility = View.VISIBLE
//            recyclerNextBook.adapter = NextReadBookListAdapter(listItems, mListener, "", dialog)
        } else {
            txtEmptyBook.visibility = View.VISIBLE
            imgCancel.visibility = View.VISIBLE
            recyclerNextBook.visibility = View.INVISIBLE
        }
        imgCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
    }

    /**
     * handle click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgAdd -> {
                val bundle = Bundle()
                bundle.putString("from", "dashboard")
                bundle.putParcelable("child", preferenceChild)
                mListener?.openAddChildFragment(bundle)
            }
            R.id.imgRecommBook -> {
                val bundle = Bundle()
                mListener?.openAllTypeBookFragment(bundle)
            }
            R.id.btnAddChild -> {
                val bundle = Bundle()
                bundle.putString("from", "dashboard")
                bundle.putParcelable("child", preferenceChild)
                mListener?.openAddChildFragment(bundle)
            }
//            R.id.imgSetting -> mListener?.openAccountSettingFragment()
        }
    }


    interface HomeFragmentInterface {
        fun openAddChildFragment(bundle: Bundle)
        fun openAllTypeBookFragment(bundle: Bundle)
        fun openProgressFragment(bundle: Bundle)
        fun openBookShelfFragment(bundle: Bundle)
        fun openAccountSettingFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openNextPopUpFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HomeFragmentInterface")
        }
    }

    /**
     * Logout from app.
     */
    fun logout() {
        context?.let { UserData.clearCustomPref(it) }
        context?.let { UserData.clearGlobalPref(it) }
        context?.startActivity(
            Intent(
                context,
                LoginActivity::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }

    /**
     * api for removing child from child list.
     */
    fun removeChildFromList(id: String?) {
        viewModel.callRemoveChildFromListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN), id
        )
    }

    /**
     * updating tick for week days read.
     */
    fun updateTick(
        childId: String?,
        bookshelfId: String?,
        createdAt: String?,
        isNext: Boolean
    ) {
        isFirstTime = isNext
        viewModel.addWeekTickData(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookshelfId, "", createdAt!!, "", childId!!
        )
    }

    /**
     * BroadcastReceiver for refreshing data
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.REFRESH -> {
                    changeStatus = (context as HomeActivity).refreshBookStatus
                    callPreferenceApi()
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

    /**
     * removing tick api call.
     */
    fun removeTick(historyId: String?, childId: String?) {
        Log.d("removeTick", "removes______")
        viewModel.removeTick(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            historyId, childId!!
        )
    }

    fun nextBookRead(id: String?) {
        val bundle = Bundle()
        bundle.putString("ratingChildId", id)
        mListener?.openNextPopUpFragment(bundle)
        /* viewModel.callNextReadBookAPI(
             requireContext(),
             Utils.getCustomPref(requireContext(), UserData.TOKEN),
             id!!
         )*/
    }

    fun hardBookRating(childId: String?, bookId: String?, hardRating: String?) {
//        nextBookRead("5f99794b2d66c352877ef13f")
        ratingChildId = childId!!
        ratingbookId = bookId!!
        viewModel.hardBookRatingApi(
            context,
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId, hardRating
        )
    }

    fun callEnjoyApi(childId: String?, bookId: String?, rating: String) {
        viewModel.enjoyBookRatingApi(
            context,
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId, rating
        )
    }

    private fun openEnjoyDialog(childId: String?, bookId: String?) {
        val dialog = Dialog(requireContext())
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.enjoy_popup)
        val btnSubmit = dialog.findViewById(R.id.btnSubmit) as Button
        val txtCancel = dialog.findViewById(R.id.txtCancel) as TextView
        val imgEnjoySmyle = dialog.findViewById(R.id.imgEnjoySmyle) as ImageView
        val reviewRatingBar = dialog.findViewById(R.id.reviewRatingBar) as RatingBar
        dialog.show()
//        reviewRatingBar.rating = 3.0F
        imgEnjoySmyle.setImageResource(R.drawable.ic_straite_smily)
        reviewRatingBar.onRatingBarChangeListener = object : RatingBar.OnRatingBarChangeListener {
            override fun onRatingChanged(
                ratingBar: RatingBar,
                rating: Float, fromUser: Boolean
            ) {
//                val ratingValue = ratingBar.rating.toString()
                if (rating.toString() == "1.0" || rating.toString() == "2.0") {
                    imgEnjoySmyle.setImageResource(R.drawable.ic_sad_smily)
                } else if (rating.toString() == "3.0") {
                    imgEnjoySmyle.setImageResource(R.drawable.ic_straite_smily)
                } else if (rating.toString() == "4.0" || rating.toString() == "5.0") {
                    imgEnjoySmyle.setImageResource(R.drawable.ic_happy_smily)
                }
            }
        }
        btnSubmit.setOnClickListener {
            Log.d("Rating", "Rating is-> " + reviewRatingBar.rating.toInt().toString())
            val rating = reviewRatingBar.rating.toInt().toString()
            if (rating != "0") {
                callEnjoyApi(childId, bookId, reviewRatingBar.rating.toInt().toString())
                dialog.dismiss()
            } else {
                AlertUtils.showToast(requireContext(), "Please rate this.")
            }
        }
        txtCancel.setOnClickListener {
            dialog.dismiss()
        }
    }
}