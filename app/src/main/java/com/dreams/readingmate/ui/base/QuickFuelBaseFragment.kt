package com.dreams.readingmate.ui.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 * Base class for all Activities
 * Author: Suneel Maurya
 * Dated: 23-06-2020
 */

abstract class QuickFuelBaseFragment<VDB : ViewDataBinding> constructor(
        @LayoutRes private val layoutResId: Int) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        // set content view using data-binding
        DataBindingUtil.setContentView<VDB>(context as Activity, layoutResId).run {
            // continue initialization
            this.lifecycleOwner = this@QuickFuelBaseFragment
            initComponents(savedInstanceState, this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    // initialize other components required after onCreate
    abstract fun initComponents(savedInstanceState: Bundle?, binding: VDB)

}