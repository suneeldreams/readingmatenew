package com.dreams.readingmate.ui.auth

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.dreams.readingmate.R
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.databinding.ActivitySignupBinding
import com.dreams.readingmate.ui.auth.viewmodel.AuthViewModel
import com.dreams.readingmate.ui.base.BaseActivity
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import com.rilixtech.CountryCodePicker
import kotlinx.android.synthetic.main.activity_signup.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SignUpActivity : BaseActivity<ActivitySignupBinding>(R.layout.activity_signup),
    NavigationContract, KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    lateinit var binding: ActivitySignupBinding
    private val viewModel: AuthViewModel by instance<AuthViewModel>()
    private lateinit var charityList: Array<String>
    var isSelected: Boolean = false
    var prefs: SharedPreferences? = null
    var ccp: CountryCodePicker? = null
    private var CountryCodeContainer = "81"
    var charityName = ""

    override fun initComponents(savedInstanceState: Bundle?, binding: ActivitySignupBinding) {
        this.binding = binding
        binding.viewModel = viewModel
        this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setUpData()
        initCharityList()
        observeLiveEvents()
        observeNavigationEvents()
    }

    private fun initCharityList() {
        binding.spinnerCharity.onItemSelectedListener = charityListListener
    }

    private val charityListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val charity = charityList[position]
            if (charity.isNotEmpty()) {
                binding.edtCharity.setText(charity)
                charityName = charity
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private fun setUpData() {
        prefs = mUserData.customPrefs(this)
//        ccp = findViewById(R.id.countryCode)
        acceptPrivacyPolicy.setOnCheckedChangeListener { _, isChecked ->
            isSelected = isChecked
            if (isChecked) {
                startActivity(Intent(this, TermsConditionActivity::class.java))
            }
        }
        /*CountryCodeContainer = countryCode.defaultCountryCode
        ccp!!.setOnCountryChangeListener { selectedCountry ->
            CountryCodeContainer = selectedCountry.phoneCode
        }*/
        txtSignUpBtn.setTypeface(null, Typeface.BOLD)
        charityList = resources.getStringArray(R.array.charity_list)
        binding.spinnerCharity.adapter =
            ArrayAdapter(this, R.layout.spinner_item_file, charityList)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    override fun observeLiveEvents() {
        viewModel.userSignUpLiveData.observe(this, Observer {
            try {
                it.userInfo!!.name?.let { it1 -> Utils.saveCustomPref(this, UserData.NAME, it1) }
                it.userInfo.id?.let { it1 -> Utils.saveCustomPref(this, UserData.ID, it1) }
                it.userInfo.email?.let { it1 -> Utils.saveCustomPref(this, UserData.EMAIL, it1) }
                it.userInfo.charity?.let { it1 ->
                    Utils.saveCustomPref(
                        this,
                        UserData.CHARITY,
                        it1
                    )
                }
                it.userInfo.city?.let { it1 -> Utils.saveCustomPref(this, UserData.CITY, it1) }
                it.userInfo.gender?.let { it1 -> Utils.saveCustomPref(this, UserData.GENDER, it1) }
                it.userInfo.mobile?.let { it1 -> Utils.saveCustomPref(this, UserData.MOBILE, it1) }

                it.userInfo.image?.let { it1 ->
                    Utils.saveCustomPref(
                        this,
                        UserData.PROFILE_IMAGE,
                        it1
                    )
                }
                prefs!![mUserData.CLICKEDID] = "home"

                Intent(this, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            /*Intent(this, LoginActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }*/
        })
        viewModel.errorMessage.observe(this, Observer {
            this.let { context ->
                AlertUtils.showToast(context, it)
            }
        })
    }

    override fun observeNavigationEvents() {
        super.observeNavigationEvents()
        binding.imgBack.setOnClickListener(this)
        binding.btnSignUp.setOnClickListener(this)
        binding.txtLogin.setOnClickListener(this)
        binding.edtCharity.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.img_Back -> finish()
            R.id.txtLogin -> {
                Intent(this, LoginActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
            R.id.btnSignUp -> {
                val name = binding.edtSignUpName.text.toString().trim()
                val email = binding.edtSignUpEmail.text.toString().trim()
//                val number = binding.edtNumber.text.toString().trim()
                val password = binding.edtSignUpPassword.text.toString().trim()
                val confirmPassword = binding.edtSignUpConfirmPassword.text.toString().trim()
                val validEmail = Utils.checkEmailValid(email)
                signUpValidation(name, email, password, confirmPassword, validEmail, charityName)
            }
            R.id.edtCharity -> binding.spinnerCharity.performClick()
        }
    }

    /**
     * SignUp Validation
     */
    private fun signUpValidation(
        name: String,
        email: String,
        password: String,
        confirmPassword: String,
        validEmail: Boolean,
        charityName: String
    ) {
        if (name.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_name)
            )
            return
        } else if (email.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_Email)
            )
            return
        } else if (!validEmail) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_valid_Email)
            )
            return
        } /*else if (.isEmpty()) {number
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_contact_number)
            )
            return
        } */ else if (password.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_Password)
            )
            return
        } else if (confirmPassword.isEmpty()) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_enter_your_confirmPassword)
            )
            return
        } else if (password != confirmPassword) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.password_not_matched)
            )
            return
        } else if (!isSelected) {
            AlertUtils.showToast(
                this,
                resources.getString(R.string.Please_acept_privacy_policy)
            )
            return
        } else {
            /*Intent(this, LoginActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }*/
            viewModel.callSignUpApi(email, password, name, confirmPassword, this, charityName)
        }
    }
}
