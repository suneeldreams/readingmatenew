package com.dreams.readingmate.ui.auth

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import androidx.legacy.content.WakefulBroadcastReceiver
import androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService
import com.dreams.readingmate.util.AlertUtils


/**
 * Created by Suneel on 30/03/2020.
 */
class AlarmReceiver : WakefulBroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        AlertUtils.showToast(context!!, "Alarm received!")
        var alarmUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        }
        val ringtone = RingtoneManager.getRingtone(context, alarmUri)
        ringtone.play()
        //this will send a notification message

        //this will send a notification message
        val comp = ComponentName(
            context.packageName,
            AlarmService::class.java.name
        )
        startWakefulService(context, intent!!.setComponent(comp))
        resultCode = Activity.RESULT_OK
    }

}