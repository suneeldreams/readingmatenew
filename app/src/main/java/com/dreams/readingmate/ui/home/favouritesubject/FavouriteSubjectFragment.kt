package com.dreams.readingmate.ui.home.favouritesubject

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.FavouriteSubjectAdapter
import com.dreams.readingmate.data.network.responses.FavSubjectData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.ChooseFavoriteSubjectFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.Model
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class FavouriteSubjectFragment :
    BaseFragment<ChooseFavoriteSubjectFragmentBinding>(R.layout.choose_favorite_subject_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var data: List<FavSubjectData?>? = null
    private var favSubjectIdsList = ArrayList<String>()
    private var subjectAdapter: FavouriteSubjectAdapter? = null
    private var preferenceChild: PreferenceChildItems? = null
    private var mListener: FavouriteSubjectFragmentInterface? = null
    private lateinit var binding: ChooseFavoriteSubjectFragmentBinding
    private var commentList: ArrayList<Model.LanguageList> = ArrayList()
    private val viewModel: FavouriteSubjectViewModel by instance<FavouriteSubjectViewModel>()
    var numbers = arrayOf(
        "Airplanes",
        "Art",
        "Bats",
        "Boxing",
        "Cakes",
        "Car",
        "Dogs",
        "Space",
        "Horse",
        "Game",
        "Joker",
        "Sky",
        "Tiger",
        "Zoo"
    )

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: ChooseFavoriteSubjectFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            preferenceChild = args.getParcelable("child")
            this.data = preferenceChild?.favSubjectData
            Log.d("subjectData", preferenceChild?.favSubjectData.toString())
        }
        binding.txtChildName.text = "Hi" + " " + preferenceChild!!.childName + ","
        val numberOfColumns = 2
        binding.recyclerFavouriteSubject.layoutManager = GridLayoutManager(context, numberOfColumns)
        if (preferenceChild!!.from == "dashboard") {
            subjectAdapter =
                FavouriteSubjectAdapter(preferenceChild?.subjectData, mListener!!, this)
        } else {
            subjectAdapter =
                FavouriteSubjectAdapter(preferenceChild?.favSubjectData, mListener!!, this)
        }
        binding.recyclerFavouriteSubject.adapter = subjectAdapter
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener { }
        binding.includeToolbar.mTitle.text = getString(R.string.next)

    }

    private fun observeLiveEvents() {
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                val bundle = Bundle()
                bundle.putParcelable("child", preferenceChild)
                mListener?.openCharacterFragment(bundle)
            }
        }
    }


    interface FavouriteSubjectFragmentInterface {
        fun openBackFragment()
        fun openCharacterFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FavouriteSubjectFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement FavouriteSubjectFragmentInterface")
        }
    }

    fun refreshAdapter(name: String?) {
        /* for (i in data!!.indices) {
 //            data!![i]!!.isSelected = position == i
 //            preferenceChild!!.subjectId = data!![i]!!.id
 //            favSubjectIdsList.add(name!!)
         }*/
        favSubjectIdsList.add(name!!)
        preferenceChild!!.subjectId = favSubjectIdsList
        subjectAdapter!!.notifyDataSetChanged()
    }
}