package com.dreams.readingmate.ui.base


import android.view.View
import androidx.databinding.BindingAdapter

/**
 * Base class for all Activities
 * Author: Shivank Trivedi
 * Dated: 20-06-2020
 */

@BindingAdapter("visible")
fun setVisibility(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}



