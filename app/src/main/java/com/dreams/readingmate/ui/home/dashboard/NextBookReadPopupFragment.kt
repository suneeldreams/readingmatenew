package com.dreams.readingmate.ui.home.dashboard

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.NextReadBookListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.FavBookData
import com.dreams.readingmate.databinding.NextReadPopupFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.Utils
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class NextBookReadPopupFragment :
    BaseFragment<NextReadPopupFragmentBinding>(R.layout.next_read_popup_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: HomeFragmentInterface? = null
    private lateinit var binding: NextReadPopupFragmentBinding
    private val viewModel: HomeViewModel by instance<HomeViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: NextReadPopupFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            val ratingChildId = args.getString("ratingChildId").toString()
            viewModel.callNextReadBookAPI(
                requireContext(),
                Utils.getCustomPref(requireContext(), UserData.TOKEN),
                ratingChildId
            )
        }
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//        binding.includeToolbar.imgBack.visibility = View.VISIBLE
//        binding.includeToolbar.mTitle.visibility = View.VISIBLE
//        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.imgCancel.setOnClickListener(this)
//        binding.includeToolbar.mTitle.setOnClickListener(this)
//        binding.homeRootLayout.setOnClickListener { }
//        binding.includeToolbar.mTitle.text = getString(R.string.next)
        binding.recyclerNextBook.layoutManager = LinearLayoutManager(context)
        binding.recyclerNextBook.layoutManager = GridLayoutManager(activity, 2)
    }

    private fun observeLiveEvents() {
        viewModel.nextBookReadLiveData.observe(this, Observer {
            if (it.isNotEmpty()) {
                binding.recyclerNextBook.visibility = View.VISIBLE
                binding.txtEmptyBook.visibility = View.GONE
                binding.imgCancel.visibility = View.VISIBLE
                val dialog = Dialog(requireContext())
                binding.recyclerNextBook.adapter =
                    NextReadBookListAdapter(it, mListener, "", dialog)
            } else {
                binding.txtEmptyBook.visibility = View.VISIBLE
                binding.imgCancel.visibility = View.VISIBLE
                binding.recyclerNextBook.visibility = View.INVISIBLE
            }
//            openNextBookReadDialog(requireContext(), it)
        })
    }

    private fun openNextBookReadDialog(
        context: Context,
        listItems: List<FavBookData>
    ) {
        val dialog = Dialog(context)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.next_read_popup)
        val displayRectangle = Rect()
        val window = (context as Activity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        window.setGravity(Gravity.CENTER_VERTICAL)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = (displayRectangle.width() * 0.95f).toInt()
        lp.height = (displayRectangle.height() * 0.95f).toInt()
        dialog.window!!.attributes = lp
        val recyclerNextBook = dialog.findViewById(R.id.recyclerNextBook) as RecyclerView
        val txtEmptyBook = dialog.findViewById(R.id.txtEmptyBook) as TextView
        val imgCancel = dialog.findViewById(R.id.imgCancel) as ImageView
        recyclerNextBook.layoutManager = GridLayoutManager(activity, 2)
        if (listItems.isNotEmpty()) {
            recyclerNextBook.visibility = View.VISIBLE
            txtEmptyBook.visibility = View.GONE
            imgCancel.visibility = View.VISIBLE
            recyclerNextBook.adapter = NextReadBookListAdapter(listItems, mListener, "", dialog)
        } else {
            txtEmptyBook.visibility = View.VISIBLE
            imgCancel.visibility = View.VISIBLE
            recyclerNextBook.visibility = View.INVISIBLE
        }
        imgCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.imgCancel -> mListener?.openBackFragment()
        }
    }


    interface HomeFragmentInterface {
        fun openBackFragment()
        fun openBookDetailFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HomeFragmentInterface")
        }
    }
}