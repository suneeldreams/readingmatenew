package com.dreams.readingmate.ui.home.staticpage

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dreams.readingmate.R
import org.kodein.di.android.x.kodein
import org.kodein.di.KodeinAware

class SettingsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.settings_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
