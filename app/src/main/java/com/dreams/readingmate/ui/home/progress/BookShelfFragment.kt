package com.dreams.readingmate.ui.home.progress

import android.content.*
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.BookShelfAdapter
import com.dreams.readingmate.data.adapter.SearchBookShelfAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.db.mUserData
import com.dreams.readingmate.data.db.mUserData.set
import com.dreams.readingmate.data.network.responses.ChildBooksReadData
import com.dreams.readingmate.databinding.BookshelfFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.ui.home.HomeActivity
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BookShelfFragment : BaseFragment<BookshelfFragmentBinding>(R.layout.bookshelf_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: BookShelfFragmentInterface? = null
    private lateinit var binding: BookshelfFragmentBinding
    private var bookShelfList: List<ChildBooksReadData?>? = null
    private var seachBookShelfAdapter: SearchBookShelfAdapter? = null
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()
    private var childId = ""
    private var filter = ""
    private lateinit var searchTypeList: Array<String>
    var prefs: SharedPreferences? = null
    private var createdAt = ""

    override fun initComponents(savedInstanceState: Bundle?, binding: BookshelfFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        initSearchTypeList()
        observeLiveEvents()
        /**
         * search books.
         */
        /*binding.edtSearchBook.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    binding.recyclerBook.visibility = View.VISIBLE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(), "50", "", filter
                    )
                } else {
                    binding.recyclerBook.visibility = View.GONE
                    binding.recyclerBook.adapter = null
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })*/
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.REFRESH_BOOKSHELF_LIST))
    }

    private fun initSearchTypeList() {
        binding.spnrBooksType.onItemSelectedListener = searchTypeListListener
    }

    /**
     * Listener for search type book
     * author and book
     */
    private val searchTypeListListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            val filterType = searchTypeList[position]
            if (filterType.isNotEmpty()) {
                binding.booksType.text = filterType
                if (filterType == "Book") {
                    filter = "book"
                } else {
                    filter = "author"
                }
                Log.d("filterType", filterType)
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    /**
     * setUp initial all data on screen when open.
     */
    private fun setUpData() {
        prefs = mUserData.customPrefs(requireContext())
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.lnrBooksType.setOnClickListener(this)
        binding.rltSearchImage.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener { }
        binding.recyclerBookShelf.layoutManager = LinearLayoutManager(context)
        val args = arguments
        if (args != null) {
            childId = args.getString("childId").toString()
            createdAt = args.getString("createdAt").toString()
            callChildBookShelfListApi()
        }
        searchTypeList = resources.getStringArray(R.array.search_type)
        binding.spnrBooksType.adapter =
            ArrayAdapter(activity as AppCompatActivity, R.layout.spinner_item_file, searchTypeList)
    }

    private fun callChildBookShelfListApi() {
        viewModel.getChildBooksReadList(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN)
            , childId
        )
    }

    /**
     * SetUp ToolBar title.
     */
    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.currently_reading)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.childBookReadListLiveData.observe(this, Observer {
            bookShelfList = it
            if (!it.isNullOrEmpty()) {
                binding.recyclerBookShelf.visibility = View.VISIBLE
                binding.txtNoData.visibility = View.GONE
                setBookShelfListData(bookShelfList)
            } else {
                binding.recyclerBookShelf.visibility = View.GONE
                binding.txtNoData.visibility = View.VISIBLE
            }
        })

        viewModel.removeBookFromShelfLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_CURRENTLY_READING_LIST
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            val intent2 = Intent()
            intent2.action = AppConstants.REFRESH
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent2)
            viewModel.getChildBooksReadList(
                requireContext(),
                Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN)
                , childId
            )
        })

        viewModel.FavBookListLiveData.observe(this, Observer {
            binding.recyclerBook.visibility = View.VISIBLE
            binding.recyclerBook.layoutManager =
                GridLayoutManager(activity, 3)
            if (!it.isNullOrEmpty()) {
                binding.recyclerBook.visibility = View.VISIBLE
                binding.txtNoData.visibility = View.GONE
                seachBookShelfAdapter = SearchBookShelfAdapter(it, mListener!!, this, childId)
                binding.recyclerBook.adapter = seachBookShelfAdapter
            } else {
                binding.recyclerBook.visibility = View.GONE
                binding.txtNoData.visibility = View.VISIBLE
            }
        })

        viewModel.addBookLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_CURRENTLY_READING_LIST
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            val intent2 = Intent()
            intent2.action = AppConstants.REFRESH
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent2)
            callChildBookShelfListApi()
        })

        viewModel.updateWeekDataLiveData.observe(this, Observer {
            callChildBookShelfListApi()
        })

        viewModel.bookFavoriteLiveData.observe(this, Observer {
            val intent = Intent()
            intent.action = AppConstants.REFERESH_BOOKS
            LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
            callChildBookShelfListApi()
        })

        viewModel.bookStatusLiveData.observe(this, Observer {
            AlertUtils.showToast(requireContext(), "This book has been added to your books list.")
            /* val intent = Intent()
             intent.action = AppConstants.REFERESH_CURRENTLY_READING_LIST
             LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
             val intent2 = Intent()
             intent2.action = AppConstants.REFRESH
             LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent2)*/
            prefs!![mUserData.CLICKEDID] = "home"
            startActivity(
                Intent(
                    requireContext(),
                    HomeActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
//            callChildBookShelfListApi()
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.lnrBooksType -> binding.spnrBooksType.performClick()
            R.id.rltSearchImage -> {
                val searchValue = binding.edtSearchBook.text.toString()
                if (searchValue > "1") {
                    binding.recyclerBook.visibility = View.VISIBLE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        searchValue, "50", "", filter
                    )
                } else {
                    binding.recyclerBook.visibility = View.GONE
                    binding.recyclerBook.adapter = null
                }
            }
        }
    }


    interface BookShelfFragmentInterface {
        fun openBackFragment()
        fun openBookDetailFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BookShelfFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BookShelfFragmentInterface")
        }
    }

    fun removeChildFromList(bookId: String?) {
        viewModel.removeBookFromShelf(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            childId,
            bookId!!
        )
    }

    fun AddImageToStringArray(
        bookId: String?,
        pageRead: String?,
        createdAt: String,
        readingStatus: String
    ) {
        binding.recyclerBook.visibility = View.GONE
        binding.edtSearchBook.setText("")
        addPageRead(bookId, pageRead!!, createdAt, readingStatus)
    }

    private fun setBookShelfListData(bookShelfList: List<ChildBooksReadData?>?) {
        binding.recyclerBookShelf.adapter =
            BookShelfAdapter(mListener, bookShelfList, this, createdAt, childId)
    }

    fun addPageRead(
        bookshelfId: String?,
        pageRead: String,
        createdAt: String,
        readingStatus: String
    ) {
        viewModel.addWeekReadData(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookshelfId, pageRead, createdAt, readingStatus, childId
        )
    }

    /**
     * BroadcastReceiver for refreshing data.
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppConstants.REFRESH_BOOKSHELF_LIST -> {
                    callChildBookShelfListApi()
                }
            }
        }
    }

    override fun onDestroy() {
        mListener = null
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

    /**
     * call api for favorites books.
     */
    fun callFavoriteApi(bookId: String, childId: String) {
        viewModel.addFavoriteBook(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            bookId, childId
        )
    }

    /**
     * api for changing book status
     */
    fun callChangeBookStatusApi(bookStatus: String, bookId: String, childId: String) {
        binding.recyclerBook.visibility = View.GONE
        binding.edtSearchBook.setText("")
        viewModel.bookStatus(
            requireContext(),
            Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN),
            bookId, "", bookStatus, childId
        )
    }
}