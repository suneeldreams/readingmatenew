package com.dreams.readingmate.ui.home.subscription

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class SubscriptionViewModel constructor(private val repository: DashboardRepository) :
    BaseViewModel() {
    private val _subscriptionPlanListLiveData = MutableLiveData<List<SubscriptionPlanListData>>()
    val subscriptionPlanListLiveData: LiveData<List<SubscriptionPlanListData>>
        get() = _subscriptionPlanListLiveData

    private val _bundleListLiveData = MutableLiveData<BundleListData>()
    val bundleListLiveData: LiveData<BundleListData>
        get() = _bundleListLiveData

    private val _bundleDetailLiveData = MutableLiveData<BundleDetail>()
    val bundleDetailLiveData: LiveData<BundleDetail>
        get() = _bundleDetailLiveData

    fun callSubscriptionPlanListAPI(
        context: Context,
        accessToken: String,
        planType: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getSubscriptionPlanList(
                    accessToken,planType
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _subscriptionPlanListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callBundleListAPI(
        context: Context,
        accessToken: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getBundleList(
                    accessToken
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _bundleListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callBundleDetailListAPI(
        context: Context,
        accessToken: String,
        slug: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getBundleDetails(accessToken, slug)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _bundleDetailLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }
}
