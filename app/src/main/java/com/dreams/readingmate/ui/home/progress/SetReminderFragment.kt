package com.dreams.readingmate.ui.home.progress

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dreams.readingmate.R
import com.dreams.readingmate.databinding.SetReminderFragmentBinding
import com.dreams.readingmate.ui.auth.AlarmReceiver
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class SetReminderFragment :
    BaseFragment<SetReminderFragmentBinding>(R.layout.set_reminder_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: SetReminderFragmentInterface? = null
    private lateinit var binding: SetReminderFragmentBinding
    val RQS_1 = 1

    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: SetReminderFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()
    }

    /**
     * apply click listener
     */
    private fun setUpData() {
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.txtSetAlarm.setOnClickListener(this)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.reminder)
        binding.includeToolbar.mTitle.text = getString(R.string.save)
    }


    private fun observeLiveEvents() {
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.txtSetAlarm -> {
                binding.txtSetAlarm.text = ""
                openTimePickerDialog(false)
            }
        }
    }

    /**
     * TimePicker dialog to choose time
     */
    private fun openTimePickerDialog(is24r: Boolean) {
        val calendar: Calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
            requireContext(),
            onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE), is24r
        )
        timePickerDialog.setTitle("Set Alarm Time")
        timePickerDialog.show()
    }

    var onTimeSetListener =
        OnTimeSetListener { view, hourOfDay, minute ->
            val calNow = Calendar.getInstance()
            val calSet = calNow.clone() as Calendar
            calSet[Calendar.HOUR_OF_DAY] = hourOfDay
            calSet[Calendar.MINUTE] = minute
            calSet[Calendar.SECOND] = 0
            calSet[Calendar.MILLISECOND] = 0
            if (calSet.compareTo(calNow) <= 0) {
                // Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1)
            }
            setAlarm(calSet)
        }

    /**
     * set alarm
     */
    private fun setAlarm(targetCal: Calendar) {
        val df: DateFormat =
            SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH)
        val result: Date = df.parse("${targetCal.time}".trimIndent())
        val ss: DateFormat =
            SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
        val newDate = ss.format(result)
        binding.txtSetAlarm.text = Utils.getTime(newDate)
        val intent = Intent(requireContext(), AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            requireContext(), RQS_1, intent, 0
        )
        val alarmManager =
            requireActivity().getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!![AlarmManager.RTC_WAKEUP, targetCal.timeInMillis] = pendingIntent
    }


    interface SetReminderFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SetReminderFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement SetReminderFragmentInterface")
        }
    }

}