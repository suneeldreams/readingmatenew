package com.dreams.readingmate.ui.home.news

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.NewsListAdapter
import com.dreams.readingmate.data.adapter.NewsSearchListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.databinding.NewsFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.AppConstants
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class NewsFragment : BaseFragment<NewsFragmentBinding>(R.layout.news_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var mListener: NewsFragmentInterface? = null
    private lateinit var binding: NewsFragmentBinding
    private var isSearch = false
    private val viewModel: NewsViewModel by instance<NewsViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: NewsFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.imgUserProfile.visibility = View.GONE
        binding.includeToolbar.imgAdd.visibility = View.GONE
        binding.includeToolbar.imgSearch.visibility = View.VISIBLE
        binding.includeToolbar.imgSearch.setOnClickListener(this)
        binding.includeToolbar.toolbar_title.text = getString(R.string.news)
        setUpData()
        observeLiveEvents()
        /**
         * TextChangedListener for searching books.
         */
        binding.edtSearchBlog.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()) {
                    isSearch = true
                    binding.rltSearch.visibility = View.VISIBLE
                    binding.rltMain.visibility = View.GONE
                    viewModel.callNewsListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(), "50", "", "", ""
                    )
                } else {
                    binding.recyclerSearch.adapter = null
                    binding.rltSearch.visibility = View.GONE
                    binding.rltMain.visibility = View.VISIBLE
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadCastReceiver, IntentFilter(AppConstants.REFERESH_NEWS))
    }

    private fun setUpData() {
        binding.newsRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerSearch.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        viewModel.callNewsListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            "", "50", "", "", ""
        )
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.newsListLiveData.observe(this, Observer {
            if (isSearch) {
                binding.recyclerSearch.adapter = NewsSearchListAdapter(it.rows, mListener, this)
            } else {
                binding.rltMain.visibility = View.VISIBLE
                binding.newsRecyclerView.adapter = NewsListAdapter(it.rows, mListener, this)
            }
        })
        viewModel.addRemoveFavLiveData.observe(this, Observer {
            viewModel.callNewsListAPI(
                requireContext(),
                Utils.getCustomPref(requireContext(), UserData.TOKEN),
                "", "50", "", "", ""
            )
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    /**
     * handle click events
     */
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.imgSearch -> {
                binding.rltSearch.visibility = View.VISIBLE
                binding.rltMain.visibility = View.GONE
            }
        }
    }


    interface NewsFragmentInterface {
        fun openBackFragment()
        fun openBookDetailFragment(bundle: Bundle)
        fun openNewsDetailFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NewsFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement NewsFragmentInterface")
        }
    }

    /**
     * BroadcastReceiver to refresh the data
     */
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            when (intent?.action) {
                AppConstants.REFERESH_NEWS -> {
                    viewModel.callNewsListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        "", "50", "", "", ""
                    )
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(broadCastReceiver)
    }

    /**
     * api for removing new book mark.
     */
    fun removeBookMark(blogId: String?) {
        viewModel.addBookMark(
            requireContext(),
            Utils.getCustomPref(
                requireContext(),
                com.dreams.readingmate.data.db.UserData.TOKEN
            ),
            blogId
        )
    }

}