package com.dreams.readingmate.ui.home.news

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.model.BookStatusDataModel
import com.dreams.readingmate.data.network.responses.*
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException

class NewsViewModel constructor(private val repository: DashboardRepository) : BaseViewModel() {

    private val _newsListLiveData = MutableLiveData<NewsListData>()
    val newsListLiveData: LiveData<NewsListData>
        get() = _newsListLiveData

    private val _addRemoveFavLiveData = MutableLiveData<AddRemoveFavData>()
    val addRemoveFavLiveData: LiveData<AddRemoveFavData>
        get() = _addRemoveFavLiveData

    private val _newsDetailLiveData = MutableLiveData<NewsDetailsData>()
    val newsDetailLiveData: LiveData<NewsDetailsData>
        get() = _newsDetailLiveData


    fun callNewsListAPI(
        context: Context,
        accessToken: String,
        search: String,
        pageSize: String,
        orderBy: String,
        orderDir: String,
        pageNumber: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getNewsList(
                    accessToken,
                    search,
                    pageSize,
                    orderBy,
                    orderDir,
                    pageNumber
                )
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _newsListLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun callNewsDetailListAPI(
        context: Context,
        accessToken: String,
        blogId: String
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.getNewsDetails(accessToken, blogId)
                if (result is ResultState.Success) {
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                    _newsDetailLiveData.postValue(result.value.data)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    _errorMessage.postValue(result.error.errorMessage)
                }
            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
                AlertUtils.showToast(context, e.message ?: "")
            }
        }
    }

    fun addBookMark(
        mContext: Context,
        accessToken: String,
        bookId: String?
    ) {
        Coroutines.main {
            try {
                _state.postValue(ProgressState.LOADING)
                val result = repository.addBookMark(
                    accessToken,
                    bookId!!
                )
                if (result is ResultState.Success) {
                    _addRemoveFavLiveData.postValue(result.value.data)
                    AlertUtils.showToast(mContext, result.value.message!!)
                    _state.postValue(ProgressState.SUCCESS)
                    println(result.value)
                } else if (result is ResultState.Error) {
                    _state.postValue(ProgressState.ERROR)
                    AlertUtils.showToast(mContext, result.error.errorMessage)
                }

            } catch (e: NoInternetException) {
                _state.postValue(ProgressState.ERROR)
            }
        }
    }
}
