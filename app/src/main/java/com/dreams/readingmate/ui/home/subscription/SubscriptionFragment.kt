package com.dreams.readingmate.ui.home.subscription

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.MemberShipPlanListAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.SubscriptionModel
import com.dreams.readingmate.databinding.SubscriptionFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Utils
import com.jsw.mobility.presentation.navigation.NavigationContract
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class SubscriptionFragment :
    BaseFragment<SubscriptionFragmentBinding>(R.layout.subscription_fragment),
    KodeinAware, NavigationContract, View.OnClickListener {

    override val kodein by kodein()
    private var mListener: SubscriptionFragmentInterface? = null
    private lateinit var binding: SubscriptionFragmentBinding
    private val viewModel: SubscriptionViewModel by instance<SubscriptionViewModel>()
    var arrayList: ArrayList<SubscriptionModel>? = null
    override fun initComponents(savedInstanceState: Bundle?, binding: SubscriptionFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolbar()
        observeLiveEvents()
        binding.homeRootLayout.setOnClickListener { }
        arrayList = ArrayList()
    }

    /**
     * setUp toolBar
     */
    private fun setUpToolbar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.subscriptionToolbar.imgBack.visibility = View.VISIBLE
        binding.subscriptionToolbar.imgBack.setOnClickListener(this)
        binding.txtMonth.setOnClickListener(this)
        binding.txtYear.setOnClickListener(this)
        binding.subscriptionToolbar.toolbar_title.text = resources.getString(R.string.subscription)
        binding.subscriptionPlanRecycler.layoutManager = LinearLayoutManager(context)
        binding.membershipPlanRecyclerView.layoutManager = LinearLayoutManager(context)
        viewModel.callSubscriptionPlanListAPI(
            requireContext(),
            Utils.getCustomPref(requireContext(), UserData.TOKEN),
            "month"
        )
        /* viewModel.callBundleListAPI(
             requireContext(),
             Utils.getCustomPref(requireContext(), UserData.TOKEN)
         )*/
    }

    override fun observeLiveEvents() {
        viewModel.subscriptionPlanListLiveData.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                binding.txtNoActiveMembership.visibility = View.GONE
                binding.membershipPlanRecyclerView.adapter =
                    MemberShipPlanListAdapter(mListener!!, it)
                /*   for (i in it.indices) {
                       arrayList?.add(
                           SubscriptionModel(
                               it[i].planInterval,
                               it[i].planName,
                               it[i].planDescription,
                               it[i].planAmount!!,
                               it[i].id,
                               it[i].upfrontAmount
                           )
                       )
                   }
                   binding.viewpager.adapter =
                       CustomPagerAdapter(requireContext(), arrayList, mListener)
                   binding.viewpager.pageMargin = 20*/
            } else {
                binding.membershipPlanRecyclerView.visibility = View.INVISIBLE
                binding.txtNoActiveMembership.visibility = View.VISIBLE
            }
        })
        /*viewModel.bundleListLiveData.observe(this, Observer {
            if (!it.bundleList.isNullOrEmpty()) {
                binding.bundleName.text = it.bannerBundle!!.bundleName
                binding.bundleDesc.text = it.bannerBundle.bundleSubDescription
                *//*Utils.displayImage(
                    requireContext(),
                    it.bannerBundle.bundleThumbImg!!,
                    binding.bundleImage,
                    R.drawable.ic_place_holder
                )*//*
                binding.subscriptionPlanRecycler.visibility = View.VISIBLE
                binding.subscriptionPlanRecycler.adapter =
                    SubscriptionPlanListAdapter(mListener!!, it.bundleList)
            } else {
                binding.subscriptionPlanRecycler.visibility = View.GONE
            }
        })*/
        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.txtMonth -> {
                binding.txtMonth.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.btn_readingmate_round_bg)
                binding.txtMonth.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                binding.txtYear.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.btn_gray_round_bg)
                binding.txtYear.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.black
                    )
                )
                viewModel.callSubscriptionPlanListAPI(
                    requireContext(),
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    "month"
                )
            }
            R.id.txtYear -> {
                binding.txtMonth.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.btn_gray_round_bg)
                binding.txtMonth.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.black
                    )
                )
                binding.txtYear.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.btn_readingmate_round_bg)
                binding.txtYear.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                viewModel.callSubscriptionPlanListAPI(
                    requireContext(),
                    Utils.getCustomPref(requireContext(), UserData.TOKEN),
                    "year"
                )
            }
        }
    }


    interface SubscriptionFragmentInterface {
        fun openBackFragment()
        fun openBundleDetailFragment(bundle: Bundle)
        fun openCheckoutFragments(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SubscriptionFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement SubscriptionFragmentInterface")
        }
    }
}