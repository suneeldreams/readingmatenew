package com.dreams.readingmate.ui.home.dashboard


interface UpdateDeviceTokenListener {
    fun onStarted()
    fun onSuccess(deviceToken: String, status: Boolean)
    fun onFailure(message: String)
}