package com.dreams.readingmate.ui.auth

interface LogoutListener {
    fun onStarted()
    fun onSuccess(message: String)
    fun onFailure(message: String)
}