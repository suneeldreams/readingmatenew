package com.dreams.readingmate.ui.home.favouritesbooks

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.AllReadyReadFavAdapter
import com.dreams.readingmate.data.adapter.AlreadyReadBooksAdapter
import com.dreams.readingmate.data.db.UserData
import com.dreams.readingmate.data.network.responses.PreferenceChildItems
import com.dreams.readingmate.databinding.AlreadyReadFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AlreadyReadBooksFragment :
    BaseFragment<AlreadyReadFragmentBinding>(R.layout.already_read_fragment),
    KodeinAware,
    View.OnClickListener {

    override val kodein by kodein()
    private var preferenceChild: PreferenceChildItems? = null
    private var favStringList = ArrayList<String>()
    private var allReadyReadBookList = ArrayList<Model.favBookData>()
    private var favStringIdsList = ArrayList<String>()
    var from = ""
    private var mListener: AlreadyReadBookFragmentInterface? = null
    private var allReadyReadAdapter: AllReadyReadFavAdapter? = null
    private var alreadyReadBooksAdapter: AlreadyReadBooksAdapter? = null
    private lateinit var binding: AlreadyReadFragmentBinding
    private val viewModel: FavouriteBooksViewModel by instance<FavouriteBooksViewModel>()

    override fun initComponents(
        savedInstanceState: Bundle?,
        binding: AlreadyReadFragmentBinding
    ) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

        binding.edtAlreadyRead.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 1) {
                    binding.noAlreadyReadBooks.visibility = View.GONE
                    binding.recyclerAllReadyReadBooks.visibility = View.GONE
                    binding.recyclerAlreadyRead.visibility = View.VISIBLE
                    viewModel.callFavoriteBookListAPI(
                        requireContext(),
                        Utils.getCustomPref(requireContext(), UserData.TOKEN),
                        s.toString(),
                        "50",""
                    )
                } else {
//                    binding.noAlreadyReadBooks.visibility = View.VISIBLE
                    binding.recyclerAlreadyRead.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private fun setUpData() {
        val args = arguments
        if (args != null) {
            from = args.getString("from", "")
            preferenceChild = args.getParcelable("child")
            if (preferenceChild!!.from != "dashboard") {
                binding.recyclerAllReadyReadBooks.visibility = View.VISIBLE
            } else {
                binding.recyclerAlreadyRead.visibility = View.GONE
            }
        }
        val numberOfColumns = 3
        binding.recyclerAlreadyRead.layoutManager = GridLayoutManager(context, numberOfColumns)
        binding.recyclerAllReadyReadBooks.layoutManager =
            GridLayoutManager(context, numberOfColumns)
        if (preferenceChild!!.from == "editProfile") {
            if (preferenceChild!!.readBooks!!.isNotEmpty()) {
                binding.noAlreadyReadBooks.visibility = View.GONE
                for (i in 0 until preferenceChild!!.readBooks!!.size) {
                    val id = preferenceChild!!.readBooks!![i]!!.bookId
                    val img = preferenceChild!!.readBooks!![i]!!.img
                    val title = preferenceChild!!.readBooks!![i]!!.title
                    favStringIdsList.add(id!!)
                    val favBook: Model.favBookData = Model.favBookData(id, img!!, title!!)
                    allReadyReadBookList.add(favBook)
                }
            } else {
                binding.noAlreadyReadBooks.visibility = View.VISIBLE
            }
        }
        preferenceChild!!.readBookss = favStringIdsList
        callFavoriteBookAdapter(allReadyReadBookList)
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        (activity as AppCompatActivity).window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.mTitle.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.includeToolbar.mTitle.setOnClickListener(this)
        binding.homeRootLayout.setOnClickListener {}
        binding.includeToolbar.mTitle.text = getString(R.string.next)
    }

    private fun observeLiveEvents() {
        viewModel.FavBookListLiveData.observe(this, Observer {
            Log.d("readingBooks:-", it.toString())
            binding.recyclerAlreadyRead.visibility = View.VISIBLE
            alreadyReadBooksAdapter = AlreadyReadBooksAdapter(it, mListener!!, this)
            binding.recyclerAlreadyRead.adapter = alreadyReadBooksAdapter
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
            R.id.mTitle -> {
                Utils.hideKeyBoard(requireActivity())
                Log.d("ddd", preferenceChild.toString())
                if (favStringIdsList.isNotEmpty()) {
                    val bundle = Bundle()
                    bundle.putParcelable("child", preferenceChild)
                    mListener?.openNewFragmentFragment(bundle)
                } else {
                    AlertUtils.showToast(
                        requireContext(),
                        "Please search books to add as your favourite!"
                    )
                }
            }
        }
    }


    interface AlreadyReadBookFragmentInterface {
        fun openBackFragment()
        fun openNewFragmentFragment(bundle: Bundle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AlreadyReadBookFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement AlreadyReadBookFragmentInterface")
        }
    }

    fun AddImageToStringArray(
        img: String?,
        id: String?,
        title: String?
    ) {
        if (id != null) {
            favStringIdsList.add(id)
        }
        preferenceChild!!.readBookss = favStringIdsList
        Log.d("array", favStringIdsList.toString())
        alreadyReadBooksAdapter!!.notifyDataSetChanged()
        val favBook: Model.favBookData = Model.favBookData(id!!, img!!, title!!)
        allReadyReadBookList.add(favBook)
        if (allReadyReadBookList.isNotEmpty()) {
            binding.recyclerAllReadyReadBooks.visibility = View.VISIBLE
            binding.recyclerAlreadyRead.visibility = View.GONE
            binding.noAlreadyReadBooks.visibility = View.GONE
            callFavoriteBookAdapter(allReadyReadBookList)
        }
        binding.edtAlreadyRead.setText("")
    }

    private fun callFavoriteBookAdapter(allReadyReadBookList: ArrayList<Model.favBookData>) {
        allReadyReadAdapter = AllReadyReadFavAdapter(allReadyReadBookList, mListener!!, this)
        binding.recyclerAllReadyReadBooks.adapter = allReadyReadAdapter
    }

    fun refreshAdapter(position: Int, id: String) {
        if (allReadyReadBookList[position].id == id) {
            allReadyReadBookList.removeAt(position)

        }
        if (favStringIdsList[position] == id) {
            favStringIdsList.removeAt(position)
        }
        preferenceChild!!.readBookss = favStringIdsList
        callFavoriteBookAdapter(allReadyReadBookList)
        allReadyReadAdapter!!.notifyDataSetChanged()
    }
}