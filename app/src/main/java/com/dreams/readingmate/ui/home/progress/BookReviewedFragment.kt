package com.dreams.readingmate.ui.home.progress

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dreams.readingmate.R
import com.dreams.readingmate.data.adapter.BookReviewedAdapter
import com.dreams.readingmate.databinding.BookReviewedFragmentBinding
import com.dreams.readingmate.ui.base.BaseFragment
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Model
import com.dreams.readingmate.util.Utils
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BookReviewedFragment :
    BaseFragment<BookReviewedFragmentBinding>(R.layout.book_reviewed_fragment),
    KodeinAware,
    View.OnClickListener {
    override val kodein by kodein()
    private var mListener: BookReviewedFragmentInterface? = null
    private lateinit var binding: BookReviewedFragmentBinding
    private var commentList: ArrayList<Model.Comments> = ArrayList()
    private val viewModel: ProgressViewModel by instance<ProgressViewModel>()

    override fun initComponents(savedInstanceState: Bundle?, binding: BookReviewedFragmentBinding) {
        this.binding = binding
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setUpToolBar()
        setUpData()
        observeLiveEvents()

    }

    private fun setUpData() {
        binding.includeToolbar.imgBack.visibility = View.VISIBLE
        binding.includeToolbar.imgBack.setOnClickListener(this)
        binding.recyclerBookReviewed.layoutManager = LinearLayoutManager(context)
        val args = arguments
        if (args != null) {
            val childId = args.getString("childId").toString()
            viewModel.getChildBooksReviewedList(
                requireContext(),
                Utils.getCustomPref(requireContext(), com.dreams.readingmate.data.db.UserData.TOKEN)
                , childId
            )
        }
    }

    private fun setUpToolBar() {
        (activity as AppCompatActivity).supportActionBar!!.hide()
        binding.includeToolbar.toolbar_title.text = getString(R.string.book_reviewed)
    }

    /**
     * MVVM ObserverLiveEvents results.
     */
    private fun observeLiveEvents() {
        viewModel.childBookReviewedListLiveData.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                binding.txtNoData.visibility = View.GONE
                binding.recyclerBookReviewed.visibility = View.VISIBLE
                binding.recyclerBookReviewed.adapter = BookReviewedAdapter(mListener, it)
            } else {
                binding.txtNoData.visibility = View.VISIBLE
                binding.recyclerBookReviewed.visibility = View.GONE
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            this.let {
                AlertUtils.showToast(requireContext(), it)
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> mListener?.openBackFragment()
        }
    }


    interface BookReviewedFragmentInterface {
        fun openBackFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BookReviewedFragmentInterface) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement BookReviewedFragmentInterface")
        }
    }

}