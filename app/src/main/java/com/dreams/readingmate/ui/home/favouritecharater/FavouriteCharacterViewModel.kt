package com.dreams.readingmate.ui.home.favouritecharater

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dreams.readingmate.data.network.base.ResultState
import com.dreams.readingmate.data.network.responses.CrateChildModel
import com.dreams.readingmate.data.network.responses.CreateChildData
import com.dreams.readingmate.data.network.responses.UpdateChildModel
import com.dreams.readingmate.data.repositories.DashboardRepository
import com.dreams.readingmate.ui.base.BaseViewModel
import com.dreams.readingmate.ui.base.ProgressState
import com.dreams.readingmate.util.AlertUtils
import com.dreams.readingmate.util.Coroutines
import com.dreams.readingmate.util.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

class FavouriteCharacterViewModel constructor(private val repository: DashboardRepository) :
    BaseViewModel() {
    var fileToUpload: MultipartBody.Part? = null
    var filePath = ""
    private val _createChildLiveData = MutableLiveData<CreateChildData>()
    val createChildLiveData: LiveData<CreateChildData>
        get() = _createChildLiveData

    fun callCreateChildApi(
        accessToken: String,
        userId: String,
        childName: String?,
        nickName: String?,
        age: String?,
        schoolId: String?,
        countryId: String?,
        colorName: String?,
        bookId: ArrayList<String>?,
        generesId: ArrayList<String>?,
        subjectId: ArrayList<String>?,
        charId: ArrayList<String>?,
        filePath: String?,
        from: String?,
        updateChildId: String?,
        mContext: FragmentActivity
    ) {
       /* if (from == "editProfile") {
            Coroutines.main {
                try {
                    _state.postValue(ProgressState.LOADING)
                    val result = repository.updateChild(
                        UpdateChildModel(
                            childName, nickName, age, colorName, schoolId, countryId,
                            bookId, generesId, subjectId, charId, filePath
                        ), accessToken, updateChildId!!
                    )
                    if (result is ResultState.Success) {
                        _createChildLiveData.postValue(result.value.data)
                        _state.postValue(ProgressState.SUCCESS)
                        println(result.value)
                    } else if (result is ResultState.Error) {
                        _state.postValue(ProgressState.ERROR)
                        AlertUtils.showToast(mContext, result.error.errorMessage)
                    }

                } catch (e: NoInternetException) {
                    _state.postValue(ProgressState.ERROR)
                }
            }
        } else {
            Coroutines.main {
                try {
                    _state.postValue(ProgressState.LOADING)
                    val result = repository.createChild(
                        CrateChildModel(
                            childName, nickName, age, colorName, schoolId, countryId, bookId,
                            generesId, subjectId, charId, filePath
                        ), accessToken, userId
                    )
                    if (result is ResultState.Success) {
                        _createChildLiveData.postValue(result.value.data)
                        _state.postValue(ProgressState.SUCCESS)
                        AlertUtils.showToast(mContext, result.value.message!!)
                        println(result.value)
                    } else if (result is ResultState.Error) {
                        _state.postValue(ProgressState.ERROR)
                        AlertUtils.showToast(mContext, result.error.errorMessage)
                    }

                } catch (e: NoInternetException) {
                    _state.postValue(ProgressState.ERROR)
                }
            }.
        }*/

    }

    private fun getRotatedImage(photoPath: String): Bitmap? {
        val ei = ExifInterface(photoPath)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val options: BitmapFactory.Options
        var bitmap: Bitmap? = null
        try {
            bitmap = BitmapFactory.decodeFile(filePath)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
            options = BitmapFactory.Options()
            options.inSampleSize = 2
            bitmap = BitmapFactory.decodeFile(filePath, options)
        }

        var rotatedBitmap: Bitmap? = null
        when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(bitmap!!, 90f)

            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(bitmap!!, 180f)

            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(bitmap!!, 270f)

            ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = bitmap

        }
        return rotatedBitmap
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

}
